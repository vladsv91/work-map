﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using Microsoft.AspNetCore.Http;

namespace Infrastructure
{
    public static class AppCultures
    {
        static AppCultures()
        {
            RuCulture = new CultureData("ru", "ru-Ru");
            EnCulture = new CultureData("en", "en-US");

            CulturesList = new List<CultureData>(2)
            {
                RuCulture,
                EnCulture
            };

            DefaultCulture = EnCulture;
            CulturesDictionary = CulturesList.ToDictionary(_ => _.ShortName);
        }

        public static CultureData DefaultCulture { get; }

        public static CultureData RuCulture { get; }
        public static CultureData EnCulture { get; }
        public static List<CultureData> CulturesList { get; set; }

        public static Dictionary<string, CultureData> CulturesDictionary { get; }


        public static void SetAdminCulture()
        {
            SetCulture("ru");
        }

        public static void SetCulture(string language)
        {
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(language);
        }


        public static CultureData GetCultureDataFromHeaders(HttpRequest request)
        {
            var cultureHeader = request.Headers["Accept-Language"];
            if (cultureHeader.Count > 0)
                return CulturesDictionary.Values.FirstOrDefault(_ => cultureHeader.Any(header => header.StartsWith(_.ShortName)));
            return null;
        }


        public static CultureData GetCultureDataByHost(string host)
        {
            var segments = host.Split('.');
            var last = segments[segments.Length - 1];
            if (last == "com") {
                if (host.Contains("izzyjobs.com")) return RuCulture;
                return EnCulture;
            }
            if (last == "ua") return RuCulture;

            return EnCulture;
        }

        public static CultureData GetCultureDataByCookies(HttpContext httpContext)
        {
            if (httpContext.Request.Cookies.TryGetValue("language", out var languageCookie) &&
                CulturesDictionary.TryGetValue(languageCookie, out var res)) return res;
            return null;
        }


        public static CultureData GetCultureDataByHost(HttpRequest request)
        {
            return GetCultureDataByHost(request.Host.ToString());
        }
    }

    public class CultureData
    {
        public CultureData(string shortName, string fullName)
        {
            ShortName = shortName;
            FullName = fullName;
            CultureInfo = new CultureInfo(fullName);
        }

        public string ShortName { get; set; }
        public string FullName { get; set; }
        public CultureInfo CultureInfo { get; }
    }
}