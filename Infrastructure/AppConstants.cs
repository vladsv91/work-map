﻿namespace Infrastructure
{
    public static class AppConstants
    {
        public static string GoogleApiKey = null;
        public static bool UseNodeSsr = false;

        public static string AppName = null;

        public static class FilesUrls
        {
            public const string FileBaseUrl = "/api/File/Get";

            public const string CompanyMainPhotoUrl = "/api/Companies/GetMainPhoto";
            public const string CompanyPhotoUrl = "/api/Companies/GetPhoto";
            public const string ApplicantPhotoUrl = "/api/Resumes/GetPhoto";
            public const string ResumeFileUrl = "/api/Resumes/GetFile";
            
        }

        public static class Roles
        {
            public const string Company = "Company";
            public const string Applicant = "Applicant";
        }

        public static class PropertiesRules
        {
            public const int ContactPhoneMinLen = 9;
            public const int ContactPhoneMaxLen = 20;

            public const int ContactEmailMinLen = 6;
            public const int ContactEmailMaxLen = 100;

            public const decimal MaxSalaryPerMonth = 99999999999;

            public const int DateMinYears = -120;
            public const int DateMaxYears = -14;

            public const int ShortDescriptionLength = 200;

            public class ContactUs
            {
                public const int FullNameMaxLen = 50;

            }

            public const int MessageMaxLen = 1000;

        }

        public class AuthOptions
        {
            //public static string Issuer = "MyAuthServer"; // издатель токена
            //public static string Audience = "http://localhost:56205/"; // потребитель токена
            //public static string Key = "secretKeyVerySecretKey"; // ключ для шифрации
            //public static double Lifetime = 10;// todo 10 min
            //public static int RefreshTokenLifetimeDays = 30;


            public static string Issuer { get; set; } = null; // издатель токена
            public static string Audience { get; set; } = null; // потребитель токена
            public static string Key { get; set; } = null; // ключ для шифрации
            public static double Lifetime { get; set; } = 0;
            public static double LifetimeInMinutes { get; } = Lifetime;
            public static int RefreshTokenLifetimeDays { get; set; } = 0;

            //return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(KEY));

            public class Claims
            {
                public class Keys
                {
                    public const string UserId = "UserId";
                    //public const string UserType = "UserType";
                }
            }
        }
    }
}