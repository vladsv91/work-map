﻿namespace Infrastructure
{
    public static class Settings
    {
        public static class EmailCredentials
        {
            public static string Email { get; set; }
            public static string Password { get; set; }
            public static string Host { get; set; }
        }

        public static class ContactEmails
        {
            public static string AdminEmail { get; set; }
        }

        public static AppSettings AppSettings { get; set; }
    }

    public class AppSettings
    {
        public int LogsMigrationHours { get; set; } = 2;
    }
}