﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Infrastructure
{
    public static class GeneralExtentions
    {
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        public static bool IsNullOrWhiteSpace(this string str)
        {
            return string.IsNullOrWhiteSpace(str);
        }

        public static bool NotNullAndEmpty(this string str)
        {
            return !string.IsNullOrEmpty(str);
        }

        public static bool NotNullAndWhiteSpace(this string str)
        {
            return !string.IsNullOrWhiteSpace(str);
        }

        public static bool StartsWithSafe(this string str, string search)
        {
            return str?.StartsWith(search) ?? false;
        }

        public static bool ToUpperStartsWithSafe(this string str, string search)
        {
            return str?.ToUpper().StartsWith(search) ?? false;
        }

        public static bool ContainsSafe(this string str, string search)
        {
            return str?.Contains(search) ?? false;
        }

        public static bool ToUpperContainsSafe(this string str, string search)
        {
            return str?.ToUpper().Contains(search) ?? false;
        }

        public static string SubstringSafe(this string str, int length)
        {
            return str == null ? null : (str.Length > length ? str.Substring(0, length) : str);
        }

        public static string SrinkString(this string str, int length)
        {
            return str?.Length > length + 3 ? str.SubstringSafe(length) + "..." : str;
        }

        public static void Forget(this Task task)
        {
            //task.ContinueWith(t => { }, TaskContinuationOptions.OnlyOnFaulted);
        }

        public static string SerializeForJavascript(this object obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.None, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            });
        }

        public static T MapTo<T>(this object obj)
        {
            return Mapper.Instance.Map<T>(obj);
        }
        public static List<T> MapToList<T>(this object obj)
        {
            return Mapper.Instance.Map<List<T>>(obj);
        }
    }
}