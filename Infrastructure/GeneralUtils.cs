﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Infrastructure
{
    public static class GeneralUtils
    {
        public static string CreateUniqId()
        {
            //return Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
            return IdGenerator.GenerateId();
        }

        public static string CreateUniqId(int count)
        {
            //return Guid.NewGuid().ToString().Substring(0, 8).ToUpper();
            return IdGenerator.GenerateId(count);
        }

        public static string JoinUrl(params string[] segments)
        {
            return Path.Combine(segments).Replace('\\', '/');
        }

        public static int GetDifferenceInYears(DateTime startDate, DateTime endDate)
        {
            //Excel documentation says "COMPLETE calendar years in between dates"
            int years = endDate.Year - startDate.Year;

            if (startDate.Month == endDate.Month && // if the start month and the end month are the same
                endDate.Day < startDate.Day // AND the end day is less than the start day
                || endDate.Month < startDate.Month) // OR if the end month is less than the start month
            {
                years--;
            }

            return years;
        }

        // algorithm for circle
        //((lat - f.Address.Latitude) * (lat - f.Address.Latitude) +
        //(lon - f.Address.Longitude) * (lon - f.Address.Longitude) < radiusSqr)
        // algorithm for ellips USE this
        //(((lat - f.Address.Latitude) * (lat - f.Address.Latitude)) / latitudeRadiusSqr +
        //((lon - f.Address.Longitude) * (lon - f.Address.Longitude)) / longitudeRadiusSqr <= 1)));


        public static decimal ConvertKilometersToLatitude(decimal km)
        {
            // https://gis.stackexchange.com/questions/8650/measuring-accuracy-of-latitude-and-longitude
            return km / 111.32m;
        }

        public static void GetLatLngRadiusesSqrts(decimal radiusInKm, out decimal latitudeRadiusSqr, out decimal longitudeRadiusSqr)
        {
            var latitudeRadius = ConvertKilometersToLatitude(radiusInKm);

            // todo, more precier value
            var longitudeRadius = latitudeRadius * 1.563m; // todo, why this value, it is experimental
            latitudeRadiusSqr = latitudeRadius * latitudeRadius;
            longitudeRadiusSqr = longitudeRadius * longitudeRadius;
        }


        

        public static TV TryGetValueOrDefault<T, TV>(this IDictionary<T, TV> dic, T key, TV defValue)
        {
            return dic.TryGetValue(key, out var val) ? val : defValue;
        }

        public static int Years(this DateTime start, DateTime end)
        {
            return end.Year - start.Year - 1 +
                   (end.Month > start.Month ||
                    end.Month == start.Month && (end.Day >= start.Day)
                       ? 1
                       : 0);
        }

        public static int Years(this DateTime start)
        {
            return start.Years(DateTime.UtcNow);
        }


        public static bool IsDebug()
        {
#if DEBUG
            return true;
#else
            return false;
#endif
        }
    }

    public static class IdGenerator
    {
        public const int FriendlyIdLength = 7;
        public const int MiddleIdLength = 10;
        public const int LongIdLength = 14;

        private static readonly char[] Symbols = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
        private static readonly int SymbolsLength;
        private static readonly Random Random = new Random();

        static IdGenerator()
        {
            SymbolsLength = Symbols.Length;
        }

        public static string GenerateId(int length = FriendlyIdLength)
        {
            var chars = new char[length];
            for (var i = 0; i < length; i++) chars[i] = Symbols[Random.Next(0, SymbolsLength)];
            return new string(chars);
        }
    }
}