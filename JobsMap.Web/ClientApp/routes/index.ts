import Router, { Route, RouteRecord } from 'vue-router';
import Vue from 'vue';
import store from 'src/vuex';

//  ----------   for not authorized ---------------------- //
import MainPage from 'src/components/pages/main-page/main-page.vue';
// import NearTheHouse from 'src/components/pages/near-the-house/near-the-house.tsx';

// companies
import CompaniesList from 'src/components/pages/companies/companies-list/companies-list.vue';
import CompanyDetails from 'src/components/pages/companies/company-details/company-details.vue';
import CompanyFilials from 'src/components/pages/companies/company-filials/company-filials.vue';
import CompanyVacancies from 'src/components/pages/companies/company-vacancies/company-vacancies.vue';
import CompanyPhotos from 'src/components/pages/companies/company-photos/company-photos.vue';

// vacancies
import VacanciesList from 'src/components/pages/vacancies/vacancies-list/vacancies-list.vue';
import VacancyDetails from 'src/components/pages/vacancies/vacancy-details/vacancy-details.vue';

// resumes
import ResumesList from 'src/components/pages/resumes/resumes-list/resumes-list.vue';
import ResumesDetails from 'src/components/pages/resumes/resume-details/resume-details.vue';
//  ----------   for not authorized ---------------------- //


// account but not authorized  -------------
import SignUp from 'src/components/pages/sign-up/sign-up.vue';
import AsCompany from 'src/components/pages/sign-up/as-company/as-company.vue';
import AsApplicant from 'src/components/pages/sign-up/as-applicant/as-applicant.vue';

import ConfirmEmail from 'src/components/pages/confirm-email/confirm-email.vue';
import ResetPassword from 'src/components/pages/reset-password/reset-password.vue';
// -------------- account but not authorized


//  --------- Authorized, todo, check is auth and Role in hooks  ----------------- //
import MyProfile from 'src/components/pages/my-profile/my-profile.vue';

import AccountSettings from 'src/components/pages/my-profile/my-account-settings/my-account-settings.vue';
import ChangingEmail from 'src/components/pages/my-profile/my-account-settings/changing-email/changing-email.vue';

// my-company
import EditMyCompanyProfile from 'src/components/pages/my-profile/my-company-profile/edit-company-profile/edit-company-profile.vue';
import MyCompanyPhotos from 'src/components/pages/my-profile/my-company-profile/my-company-photos/my-company-photos.vue';
import MyCompanyFilials from 'src/components/pages/my-profile/my-company-profile/my-company-filials/my-company-filials.vue';
import EditCompanyFilial from 'src/components/pages/my-profile/my-company-profile/my-company-filials/edit-company-filial/edit-company-filial.vue';
import EditVacancy from 'src/components/pages/my-profile/my-company-profile/my-company-vacancies/edit-vacancy/edit-vacancy.vue';
import MyCompanyVacancies from 'src/components/pages/my-profile/my-company-profile/my-company-vacancies/my-company-vacancies.vue';
import MyReceivedResumes from 'src/components/pages/my-profile/my-company-profile/my-received-resumes/my-received-resumes.vue';

// my-applicant
import EditApplicantProfile from 'src/components/pages/my-profile/my-applicant-profile/edit-applicant-profile/edit-applicant-profile.vue';
import MyApplicantResumes from 'src/components/pages/my-profile/my-applicant-profile/my-applicant-resumes/my-applicant-resumes.vue';
import EditResume from 'src/components/pages/my-profile/my-applicant-profile/my-applicant-resumes/edit-resume/edit-resume.vue';
import MyPreferredAddresses from 'src/components/pages/my-profile/my-applicant-profile/my-preferred-addresses/my-preferred-addresses.vue';
import MyApplicantSentResumes from 'src/components/pages/my-profile/my-applicant-profile/my-sent-resumes/my-sent-resumes.vue';
import EditPreferredAddress from 'src/components/pages/my-profile/my-applicant-profile/my-preferred-addresses/edit-preferred-address/edit-preferred-address.vue';


import { getAllMatchedRoutes, isAllowedAnonymousForMatchedRoutes } from 'src/utils/utils';
import components, { getListAndDetailsComponent } from 'src/components/general/components.tsx';
import { Constants } from 'src/utils/constants.ts';


const { userRoles } = Constants;

Vue.use(Router);

const myProfileRoutes = {
  path: '/my',
  component: MyProfile,
  name: 'MyProfile',
  children: [
    {
      path: 'settings',
      component: AccountSettings,
      name: 'AccountSettings',
      children: [
        { path: 'changing-email', component: ChangingEmail, name: 'ChangingEmail' },
      ],
    },

    {
      path: 'company',
      meta: { role: userRoles.company },
      component: components.RouterViewWrap,
      children: [
        { path: 'info', component: EditMyCompanyProfile, name: 'MyCompanyInfo' },
        { path: 'filials', component: MyCompanyFilials, name: 'MyCompanyFilials' },
        { path: 'add-filial', component: EditCompanyFilial, name: 'AddFilial' },
        { path: 'edit-filial/:filialId', component: EditCompanyFilial, name: 'EditFilial' },
        { path: 'vacancies', component: MyCompanyVacancies, name: 'MyCompanyVacancies' },
        { path: 'add-vacancy', component: EditVacancy, name: 'AddVacancy' },
        { path: 'edit-vacancy/:vacancyId', component: EditVacancy, name: 'EditVacancy' },
        { path: 'photos', component: MyCompanyPhotos, name: 'MyCompanyPhotos' },
        { path: 'received-resumes', component: MyReceivedResumes, name: 'MyReceivedResumes' },
      ],
    },
    {
      path: 'applicant',
      meta: { role: userRoles.applicant },
      component: components.RouterViewWrap,
      children: [
        { path: 'info', component: EditApplicantProfile, name: 'MyApplicantInfo' },
        { path: 'resumes', component: MyApplicantResumes, name: 'MyResumes' },
        { path: 'add-resume', component: EditResume, name: 'AddResume' },
        { path: 'edit-resume/:resumeId', component: EditResume, name: 'EditResume' },
        { path: 'preferred-addresses', component: MyPreferredAddresses, name: 'MyPreferredAddresses' },
        { path: 'sent-resumes', component: MyApplicantSentResumes, name: 'MyApplicantSentResumes' },
        { path: 'add-preferred-address', component: EditPreferredAddress, name: 'AddPreferredAddress' },
        {
          path: 'edit-preferred-address/:preferredAddressId',
          component: EditPreferredAddress, name: 'EditPreferredAddress',
        },
      ],
    },
  ],
};

const notForSsrRoutes = [
  { path: '/confirm-email', component: ConfirmEmail, name: 'ConfirmEmail', meta: { allowAnonymous: true } },
  { path: '/reset-password', component: ResetPassword, name: 'ResetPassword', meta: { allowAnonymous: true } },
];

// NOTE: these routes should not call, due to disabling ssr in server for these
if (window.isServer) {
  const rec = (route) => {
    route.component = components.DummyForRoute;
    if (route.children) {
      route.children.forEach(ch => {
        rec(ch);
      });
    }
  };
  rec(myProfileRoutes);
  notForSsrRoutes.forEach(rec);
}

const router = new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
    { path: '', component: MainPage, name: 'Main', meta: { allowAnonymous: true, noTitleSuffix: true } },
    // { path: '/near-the-house', component: NearTheHouse, name: 'NearTheHouse', meta: { allowAnonymous: true } },
    {
      // not change urls, because used in mvc controllers
      path: '/companies',
      component: getListAndDetailsComponent(), props: { component: CompaniesList, routeName: 'CompaniesList' },
      name: 'CompaniesList', meta: { allowAnonymous: true },
      children: [
        {
          path: ':companyId', component: CompanyDetails, name: 'CompanyDetails',
          children: [
            { path: 'filials', component: CompanyFilials, name: 'CompanyDetailsFilials' },
            { path: 'vacancies', component: CompanyVacancies, name: 'CompanyDetailsVacancies' },
            { path: 'photos', component: CompanyPhotos, name: 'CompanyDetailsPhotos' },
          ],
        },
      ],
    },

    {
      // not change urls, because used in mvc controllers
      path: '/vacancies',
      component: getListAndDetailsComponent(), props: { component: VacanciesList, routeName: 'VacanciesList' },
      name: 'VacanciesList', meta: { allowAnonymous: true },
      children: [{ path: ':vacancyId', component: VacancyDetails, name: 'VacancyDetails' }],
    },

    {
      // not change urls, because used in mvc controllers
      path: '/resumes',
      component: getListAndDetailsComponent(), props: { component: ResumesList, routeName: 'ResumesList' },
      name: 'ResumesList', meta: { allowAnonymous: true },
      children: [{ path: ':resumeId', component: ResumesDetails, name: 'ResumeDetails' }],
    },


    // authorized
    myProfileRoutes,

    // account, but not authorized
    {
      path: '/sign-up',
      redirect: { name: 'SignUpAsCompany' },
      component: SignUp,
      name: 'SignUp',
      meta: { allowAnonymous: true },
      children: [
        { path: 'company', component: AsCompany, name: 'SignUpAsCompany' },
        { path: 'applicant', component: AsApplicant, name: 'SignUpAsApplicant' },
      ],
    },

    ...notForSsrRoutes,

    { path: '*', redirect: { name: 'Main' } },
  ],
});

if (!window.isServer) {
  router.beforeResolve(checkAllUserPermissions);
}
function checkAllUserPermissions(to, from, next) {
  if (store.state.isOpenedModal) {
    next(false);
    return;
  }
  const allMatchedRoutes = getAllMatchedRoutes(to);

  if (isAllowedAnonymousForMatchedRoutes(allMatchedRoutes)) {
    next();
  } else if (!store.getters.isAuthenticated) {
    next({ name: 'Main' });
  } else {
    store.dispatch('RetrieveUserInfoOrFromCache').then((userInfo) => {
      const { type } = userInfo;
      if (allMatchedRoutes.findIndex(_ => _.meta.role != null && _.meta.role !== type) >= 0) {
        next({ name: 'Main' });
      }
      next();
    }, () => { next({ name: 'Main' }); });
  }
}


router.beforeEach((to, from, next) => {
  const nrLoc = (store.state as any).localization.localizedTemplates.titles.namedRoutes;

  let nearestTitle;
  const nearestRouteWithTitle = to.matched.slice().reverse().find(r => {
    nearestTitle = nrLoc[r.name];
    return nearestTitle ? r as any : null;
  });

  if (nearestRouteWithTitle) {
    if (!nearestRouteWithTitle.meta || !nearestRouteWithTitle.meta.noTitleSuffix) {
      nearestTitle += ` - ${Constants.appName}`;
    }
  }

  // if (process.env.NODE_ENV === 'production') {
  document.title = nearestTitle || Constants.appName;
  // }
  next();
});

export default router;
