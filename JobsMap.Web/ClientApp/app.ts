﻿import Vue from 'vue';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import Vuelidate from 'vuelidate';

import App from 'src/components/App/App.vue';
import 'src/api/interceptors';
import 'src/utils/google-maps-api-callback';

import VueMaterial from 'vue-material';
import { UiSlider } from 'keen-ui';

import store from './vuex';
import router from './routes';


import GlobalMixin from './mixins/global-mixin';

import AddressFields from './components/inputs/address-fields/address-fields.vue';
import { PaginationWrap } from 'src/components/pagination/pagination.ts';
import Pagination from './components/pagination/pagination.vue';
import MForm from './components/inputs/m-form/m-form.vue';
import NumberInput from './components/inputs/number-input/number-input.vue';
import MButton from './components/inputs/m-button/m-button.vue';
import MInput from './components/inputs/m-input/m-input.vue';
import MDatepicker from './components/inputs/m-datepicker/m-datepicker.vue';
import ValidationWrapper from './components/inputs/validation-wrapper/validation-wrapper.vue';
import Field from './components/inputs/field/field.vue';
import Tabs from './components/tabs/tabs.vue';
import { Multiselect } from './components/inputs/multiselect';


import './filters';
import './services';
import './components/inputs/multiselects';
import './components/general';
import './mixins/vue-prototype';

if (process.env.NODE_ENV === 'production') {
  const { config } = Vue;
  config.productionTip = false;
  config.performance = false;
  config.devtools = false;
  // TODO: fix
  (config as any).debug = false;
  config.silent = true;
}


Vue.mixin(GlobalMixin);
Vue.use(Vuelidate);
Vue.use(VueMaterial);
Vue.use(VueAwesomeSwiper);

Vue.component('number-input', NumberInput);
Vue.component('validation-wrapper', ValidationWrapper);
Vue.component('field', Field);
Vue.component('Multiselect', Multiselect);
Vue.component('m-button', MButton);
Vue.component('m-input', MInput);
Vue.component('m-datepicker', MDatepicker);
Vue.component('AddressFields', AddressFields);
Vue.component('Pagination', Pagination);
Vue.component('PaginationWrap', PaginationWrap);
Vue.component('MForm', MForm);
Vue.component('Tabs', Tabs);
Vue.component('UiSlider', UiSlider);


const app = new Vue({
  router,
  store,
  render: (h) => h(App),
});


export { app, store, router };
