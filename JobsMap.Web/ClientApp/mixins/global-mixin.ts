export default {
  // fix for tsx
  beforeCreate() {
    const originalCreateElement = this.$createElement;
    this.$createElement = (tag, data, ...children) => originalCreateElement(tag, data, children);
  },

  created() {
    let createdFront;
    if (!window.isServer && (createdFront = this.$options.createdFront)) {
      createdFront.call(this);
    }
  },
};
