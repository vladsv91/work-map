import Vue from 'vue';
import Modal from 'src/components/modal/modal.vue';


export default Vue.extend({
  data() {
    return {
      loadingPromise: null as Promise<any>,
    };
  },
  methods: {
    close() {
      this.$emit('close');
    },
    apply() {
      this.$emit('apply');
    },
    validateAndApply() {
      if (this.isInvalid) {
        return;
      }
      this.loadingPromise = this.apply() as any;
    },
  },
  components: {
    Modal,
  },
});

