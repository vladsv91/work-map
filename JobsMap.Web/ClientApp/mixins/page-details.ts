import { Constants } from 'src/utils/constants.ts';

const pageDetailsMixin = {
  data() {
    return {
      itemDetails: {},
    };
  },
  methods: {
    // abstract
    getItemData() { },

    retrieveItemDetails() {
      this.getItemData(this.itemId).then(this.onRetrievedData, this.onRetrievingError);
    },

    onRetrievedData(response) {
      this.itemDetails = response.data;
    },

    onRetrievingError(response) {
      this.showServiceError(response);
    },

    onCreated() {
      this.tryGetDataFromWindowAndRetrieve();
    },

    tryGetDataFromWindowAndRetrieve() {
      // get data for SEO
      const { entityDataFromServer } = window;
      if (entityDataFromServer &&
        entityDataFromServer.entityDataType === Constants.entityDataTypes.itemDetails &&
        entityDataFromServer.data &&
        entityDataFromServer.data.id === this.itemId) {
        delete window.entityDataFromServer;
        this.onRetrievedData({ data: entityDataFromServer.data });
      }
      // Anyway retrieve data by ajax
      this.retrieveItemDetails();
    },

    onChangedItemId(itemId) {
      if (this._inactive) return;
      this.retrieveDataOnChangingId(itemId);
    },

    retrieveDataOnChangingId() {
      this.retrieveItemDetails();
    },

  },
  computed: {
    itemId() {
      // todo, hack
      const { params } = this.$route;
      return params[Object.keys(params)[0]];
    },
  },
  watch: {
    itemId(itemId) {
      this.onChangedItemId(itemId);
    },
  },
  components: {},
  created() {
    this.onCreated();
  },
};

const CompanyDetailsEntityMixin = {
  mixins: [pageDetailsMixin],
  methods: {
    tryGetDataFromWindowAndRetrieve() {
      // get data for SEO
      const { companyEntityDataFromServer } = window;
      if (companyEntityDataFromServer &&
        companyEntityDataFromServer.entityDataType === Constants.entityDataTypes.companyEntityList && // maybe delete
        companyEntityDataFromServer.entityName === this.entityName &&
        companyEntityDataFromServer.data) {
        delete window.companyEntityDataFromServer;
        this.onRetrievedData({ data: companyEntityDataFromServer.data });
      }
      // Anyway retrieve data by ajax
      this.retrieveItemDetails();
    },
  },
};

const EditItemMixin = {
  mixins: [pageDetailsMixin],
  data() {
    return {
      formModel: this.getInitialFormModel(),
    };
  },
  methods: {
    // abstract
    getInitialFormModel() { },
    saveAddingItem() { },
    saveUpdatingItem() { },
    //

    onRetrievedData(response) {
      this.formModel = Object.assign({}, this.formModel, response.data);
    },


    apply() {
      let promise;
      if (this.isAddMode) {
        promise = this.saveAddingItem(this.formModel).then(this.onSaveAddingItemSuccess, this.onSaveError);
      } else {
        promise = this.saveUpdatingItem(this.formModel).catch(this.onSaveError);
      }
      this.setPromiseToSubmit(promise);
    },

    onSaveAddingItemSuccess(response) {
      this.onSaveSuccess(response);
    },

    onSaveSuccess() { },
    onSaveError(response) {
      this.showServiceError(response);
    },

    setPromiseToSubmit(promise) {
      this.$refs.submitBtn.setPromise(promise);
    },

    onCreated() {
      if (this.isEditMode) {
        this.retrieveItemDetails();
      }
    },

    retrieveDataOnChangingId(itemId) {
      if (itemId) {
        this.retrieveItemDetails();
      } else {
        this.formModel = this.getInitialFormModel();
      }
    },
  },

  computed: {
    header() { return this.isAddMode ? this.t.addHeader : this.t.editHeader; },
    isAddMode() {
      return !this.itemId;
    },
    isEditMode() {
      return !this.isAddMode;
    },
    // absttract
    t() { },
  },
};


export { EditItemMixin, CompanyDetailsEntityMixin };


export default pageDetailsMixin;
