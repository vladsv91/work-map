import { deepClone, deepEqual } from 'src/utils/deep-utils.ts';
import Utils from 'src/utils/utils';
import { Constants } from '../utils/constants';

const defaultItemsPerPage = 10;

export default {
  data() {
    return {
      items: [],
      totalItemsCount: null,
      submitBtnPromise: null,
      formModel: {
        searchString: null,
      },
      paginationOptions: {
        page: 1,
        itemsPerPage: defaultItemsPerPage,
        recalculateTotalCount: true,
      },
    };
  },
  methods: {
    // abstract, returns promise
    getItemsByFilter() { },
    // abstract
    getFormModelParams() {
      return this.$getServiceParams(this.formModel);
      // todo, parse formModel
      // const res = Object.assign({}, this.formModel);
      // console.warn('Must override method \'getFormModelParams\'');
      // return res;
    },


    apply() {
      this.retrieveItemsByFilter();
    },
    retrieveItemsByFilter() {
      const { paginationOptions } = this;
      if (!deepEqual(this.formModel, this.formModelCloned)) {
        if (this.formModelCloned) {
          paginationOptions.recalculateTotalCount = true;
          paginationOptions.page = 1;
        }
        this.formModelCloned = deepClone(this.formModel);
      } else {
        paginationOptions.recalculateTotalCount = false;
      }

      // todo, save filters in url
      // this.$router.replace({
      //   query: encodeUriParams({ f: this.formModel, p: paginationOptions }),
      //   append: true,
      // });
      this.submitBtnPromise = this.getItemsByFilter(this.getServiceParams())
        .then(this.onRetrievedData, this.showServiceError);
    },

    onRetrievedData(response) {
      const { data } = response;
      const { totalItemsCount } = data;

      this.items = data.items;
      if (totalItemsCount != null) {
        this.totalItemsCount = totalItemsCount;
      }
    },

    onChangedPagination(paginationOptions) {
      Object.assign(this.paginationOptions, paginationOptions);
      this.retrieveItemsByFilter();
    },

    getServiceParams() {
      const fmp = this.getFormModelParams();
      this.setPaginationParams(fmp);
      return fmp;
    },


    setPaginationParams(fmp) {
      return fmp.pagination = this.paginationOptions;
    },

    setFormModelFromQueryParams() {
      if (this._isInactive || this._inactive) { return; }
      const { query } = this.$route;
      this.formModel.searchString = query.s;
      let page = +query.page || 1;
      if (page < 1) { page = 1; }
      this.paginationOptions.page = page;

      let size = +query.size || defaultItemsPerPage;
      if (size < 1) { size = 1; } else {
        if (size > Constants.validations.maxItemsPerPage) { size = Constants.validations.maxItemsPerPage; }
      }
      this.paginationOptions.itemsPerPage = size;
    },

    setQueryByFormModel() {
      const newQueryObj = this.getAppendToRouteQuery({
        s: this.formModel.searchString,
        page: this.paginationOptions.page,
        size: this.paginationOptions.itemsPerPage,
      });
      if (!newQueryObj.s) {
        delete newQueryObj.s;
      }
      if (newQueryObj.page === 1) {
        delete newQueryObj.page;
      }
      if (newQueryObj.size === defaultItemsPerPage) {
        delete newQueryObj.size;
      }
      this.$router.replace({ query: newQueryObj });
    },

    // todo, query
    getFmMapToQuery() {
      // abstract;
    },

    onSearchByAddressesInput() {
      this.debounceRetrieveDataFromMap();
    },
  },
  watch: {
    '$route.query': {
      handler() { this.setFormModelFromQueryParams(); },
      deep: true,
    },
    formModel: {
      handler() { this.setQueryByFormModel(); },
      deep: true,
    },
    paginationOptions: {
      // 31. List or details query is inactive
      handler() { this.setQueryByFormModel(); },
      deep: true,
    },
  },
  components: {},
  computed: {
    t() {
      return this.$t.searchList;
    },
    collapsedAddresses: {
      get() {
        return this.$store.state.layout.collapsedAddresses;
      },
      set(value) {
        this.$store.commit('SetLayoutValue', ['collapsedAddresses', value]);
      },
    },
  },
  created() {
    this.setFormModelFromQueryParams();
    this.formModelCloned = null;
    this.retrieveItemsByFilter();
  },

  // CRUTCH: small huck
  activated() {
    // this.setFormModelFromQueryParams();
    this._isInactive = false;
    this.setQueryByFormModel();
  },
  deactivated() {
    this._isInactive = true;
  },
  mounted() {
    this.debounceRetrieveDataFromMap = Utils.debounce(() => {
      this.retrieveItemsByFilter();
    }, 1000);
  },
};
