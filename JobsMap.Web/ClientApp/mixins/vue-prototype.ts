import Vue from 'vue';
import { setIdProperty, getServiceParams } from 'src/utils/utils';
import EventHub from 'src/utils/EventHub.ts';
import { Constants } from 'src/utils/constants.ts';
import './snackbar';

const vuePrototype = Vue.prototype;

vuePrototype.$bus = EventHub;
vuePrototype.$consts = Constants;

// temporary

if (process.env.NODE_ENV === 'development') {
  // tslint:disable-next-line: no-console
  vuePrototype.clg = (...args: any[]) => console.log(...args);
}


Object.defineProperty(vuePrototype, 'userInfo', {
  get() { return this.$g.userInfo; },
});

Object.defineProperty(vuePrototype, 'isAuthenticated', {
  get() { return this.$g.isAuthenticated; },
});


Object.defineProperty(vuePrototype, 'isValid', {
  get() { return !this.isInvalid; },
});

Object.defineProperty(vuePrototype, 'isInvalid', {
  get() {
    const { $v } = this;
    if (!$v || !$v.$touch) {
      return false;
    }

    $v.$touch();
    // if (process.env.NODE_ENV === 'development') {
    //   return false;
    // }
    return $v.$invalid;
  },
});


vuePrototype.setIdProperty = setIdProperty;
vuePrototype.$getServiceParams = getServiceParams;


// add $bus event hadler per component instance
vuePrototype.$busOn = function (eventName, handler) {
  const bus = this.$bus;
  bus.$on(eventName, handler);
  let events = this.busEventHandlers;
  if (!events) {
    this._hasHookEvent = true;
    this.$on('hook:destroyed', () => {
      for (let i = 0; i < events.length; i++) {
        const event = events[i];
        bus.$off(event.eventName, event.handler);
      }
      delete this.busEventHandlers;
    });
    events = this.busEventHandlers = [];
  }
  events.push({ eventName, handler });
};


vuePrototype.validateAndSave = function () {
  if (this.isInvalid) { return; }
  return this.save();
};

vuePrototype.validateAndApply = function () {
  if (this.isInvalid) { return; }
  return this.apply();
};


vuePrototype.getAppendToRouteQuery = function (obj) {
  const newQueryObj = Object.assign({}, this.$route.query, obj);
  return newQueryObj;
};

vuePrototype.addRouteQuery = function (obj) {
  const newQueryObj = this.getAppendToRouteQuery(obj);
  this.$router.replace({ query: newQueryObj });
};

vuePrototype.addDisposableEventListener = function (obj, eventName, handler, bubble = true) {
  obj.addEventListener(eventName, handler, bubble);
  this._hasHookEvent = true;
  this.$on('hook:destroyed', () => {
    obj.removeEventListener(eventName, handler);
  });
};


// vue router && vuex fix
Object.defineProperty(vuePrototype, '_route', {
  set(value) { this.__route = value; }, // todo: maybe redundand setter
  get() { return this.__route || window.vm.$route; },
});

Object.defineProperty(vuePrototype, '_router', {
  set(value) { this.__router = value; }, // todo: maybe redundand setter
  get() { return this.__router || window.vm.$router; },
});


Object.defineProperty(vuePrototype, '$store', {
  set(value) { this._store = value; }, // todo: maybe redundand setter
  get() { return this._store || window.vm.$store; },
});

// Object.defineProperty(vuePrototype, '_routerRoot', {
//   set(value) { this.__routerRoot = value; },
//   get() { return this.__routerRoot; },
// });


// Object.defineProperty(vuePrototype, '$store', {
//   set(value) { this._$store = value; },
//   get() { return this._$store; },
// });
