import Vue from 'vue';



export default {
  data() {
    return {
      // beacouse not reactive must be
      // map: null,
      // mapMarkers: [],
    };
  },
  methods: {
    getAddressesList() {
      // abstract
    },

    onLoadedMapsApi() {
      if (this.map) { return; }
      this.initMap();
      this.setMarkers();
    },

    getMapElement() {
      return this.$children.find(_ => (_.$options.name || _.$options._componentTag) === 'GoogleMapWrap').map;
      // return this.$refs.mapComponent.map;
    },

    initMap() {
      this.map = new google.maps.Map(this.getMapElement(), this.mapOptions);
      this.$emit('on-inithialized-map');
    },

    setMarkers() {
      // console.info('Render map markers!!!!!!!!!!!!!!!!!!!');
      const { mapMarkers, map } = this;
      if (!map) { return; }
      this.removeAllMarkers();

      const addressesList = this.getAddressesList();
      if (!addressesList || !addressesList.length) { return; }

      for (let i = 0; i < addressesList.length; i++) {
        const addressObj = addressesList[i];
        this.setMarkerOnMap(addressObj.address, addressObj, map, mapMarkers);
      }
      this.setMapCenter();
      if (this.enabledClusteringMarkers) {
        this.clusterMarkers();
      }
      // this.$emit('setted-markers');
    },

    getMarkersForClustering() {
      return this.mapMarkers.map(_ => _.marker);
    },

    clusterMarkers() {
      const { MarkerClusterer } = window;
      if (!MarkerClusterer) { return; }
      this.markerClusterer = new MarkerClusterer(this.map, this.getMarkersForClustering(), {
        imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m',
        maxZoom: 13,
      });
    },

    clearClusteredMarkers() {
      const { markerClusterer } = this;
      if (markerClusterer) {
        markerClusterer.clearMarkers();
      }
    },

    setMarkerOnMap(address, addressObj, map, mapMarkers) {
      const marker = this.createMarkerData(address, addressObj, map, mapMarkers);
      mapMarkers.push(marker);
    },

    createMarkerData(address, addressObj, map) {
      const marker = new google.maps.Marker({
        position: { lat: address.latitude, lng: address.longitude },
        map: map,
        draggable: this.isDraggableMarkers,
      });
      const { name } = addressObj;
      if (name) {
        marker.setTitle(name);
      }
      this.markerWindow(addressObj, marker);
      return {
        marker,
      };
    },

    markerWindow(addressObj, marker, openImmediately) {
      if (addressObj.contentComponent) {
        let infoWindow;
        let mountedComponent;

        const createInfoWindow = () => {
          if (!infoWindow) {
            const InfoWindowComponent = Vue.extend(addressObj.contentComponent);
            const componentIns = new InfoWindowComponent({ propsData: addressObj.contentProps });
            mountedComponent = componentIns.$mount();
            infoWindow = new google.maps.InfoWindow({
              content: mountedComponent.$el,
            });
            infoWindow.open(this.map, marker);
            infoWindow.addListener('closeclick', () => {
              mountedComponent.$destroy();
              infoWindow = null;
              mountedComponent = null;
            });
          } else {
            infoWindow.close();
            mountedComponent.$destroy();
            infoWindow = null;
            mountedComponent = null;
          }
        };

        marker.addListener('click', createInfoWindow);
        if (openImmediately) { createInfoWindow(); }
      }
    },

    setMapCenter() {
      const { mapMarkers, map } = this;
      if (!map) { return; }

      const mapBounds = map.getBounds();

      if (!mapBounds) {
        const mapMarkersLength = mapMarkers.length;
        if (mapMarkersLength > 0) {
          if (mapMarkersLength === 1) {
            map.setCenter(mapMarkers[0].marker.position);
          } else {
            // fit at first time
            const bounds = new google.maps.LatLngBounds();
            for (let i = 0; i < mapMarkers.length; i++) {
              bounds.extend(mapMarkers[i].marker.getPosition());
            }
            map.fitBounds(bounds);
          }
        }
        return;
      }

      for (let i = 0; i < mapMarkers.length; i++) {
        const { position } = mapMarkers[i].marker;
        if (mapBounds.contains(position)) {
          return;
        }
      }
      if (mapMarkers.length > 0) {
        map.setCenter(mapMarkers[0].marker.position);
      }
    },
    removeAllMarkers() {
      if (!this.map) { return; }
      const { mapMarkers } = this;
      for (let i = 0; i < mapMarkers.length; i++) {
        const marker = mapMarkers[i];
        marker.marker.setMap(null);
        if (marker.circle) {
          marker.circle.setMap(null);
        }
      }
      mapMarkers.length = 0;
      this.clearClusteredMarkers();
    },
    setCallBackOnMounted() {
      this.$onLoadMapsApi(this.onLoadedMapsApi);
    },

    // getDefaultMapOptions() {
    //   return { zoom: 3, center: { lat: 30, lng: 30 }, gestureHandling: 'greedy' };
    // },
  },

  // todo
  // updated() {
  //   return this.$refs.map;
  // },

  created() {
    // not reactive
    this.isDraggableMarkers = true;
    this.enabledClusteringMarkers = false;
    // this.mapComponent = null;
    this.map = null;
    // Object.defineProperty(this, 'map', {
    //   get() {
    //     return this.$refs.mapComponent ? this.$refs.mapComponent.map : null;
    //   },
    // });
    // this.map = null;
    this.mapMarkers = []; // {marker: {}, circle: ?}
    this.mapOptions = { zoom: 3, center: { lat: 30, lng: 30 } };
    // gestureHandling: 'greedy'
  },
  mounted() {
    this.setCallBackOnMounted();
    this.$busOn('ToggleMapFullScreen', (isFullScreen) => {
      if (this.map) {
        this.map.setOptions({ gestureHandling: isFullScreen ? 'greedy' : 'cooperative' });
      } else {
        this.$on('on-inithialized-map', () => {
          this.map.setOptions({ gestureHandling: isFullScreen ? 'greedy' : 'cooperative' });
        });
      }
    });
  },
};
