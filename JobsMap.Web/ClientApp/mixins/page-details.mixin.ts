import { Constants } from 'src/utils/constants.ts';
import Vue from 'vue';
import { Watch, Component } from 'vue-property-decorator';
import { AxiosResponse, AxiosError } from 'axios';


@Component
export class PageDetailsMixin<T> extends Vue {
  public itemDetails: T = {} as T;

  // abstract
  // public abstract getItemData(itemId: string): Promise<AxiosResponse<T>>;
  public getItemData(itemId: string): Promise<AxiosResponse<T>> { return null; }

  public created() {
    this.onCreated();
  }

  public retrieveItemDetails() {
    this.getItemData(this.itemId).then(this.onRetrievedData.bind(this), this.onRetrievingError.bind(this));
  }

  public onRetrievedData(response: AxiosResponse<T>) {
    this.itemDetails = response.data;
  }

  public onRetrievingError(response: AxiosError) {
    this.showServiceError(response);
  }

  public onCreated() {
    this.tryGetDataFromWindowAndRetrieve();
  }

  public tryGetDataFromWindowAndRetrieve() {
    // get data for SEO
    const { entityDataFromServer } = window;
    if (entityDataFromServer &&
      entityDataFromServer.entityDataType === Constants.entityDataTypes.itemDetails &&
      entityDataFromServer.data &&
      entityDataFromServer.data.id === this.itemId) {
      delete window.entityDataFromServer;
      this.onRetrievedData({ data: entityDataFromServer.data } as AxiosResponse);
    }
    // Anyway retrieve data by ajax
    this.retrieveItemDetails();
  }

  public onChangedItemId(itemId: string) {
    if ((this as any)._inactive) {
      return;
    }
    this.retrieveDataOnChangingId(itemId);
  }

  public retrieveDataOnChangingId(itemId: string) {
    this.retrieveItemDetails();
  }

  get itemId() {
    // todo, hack
    const { params } = this.$route;
    return params[Object.keys(params)[0]];
  }

  @Watch('itemId')
  public itemIdChanged(itemId: string) {
    this.onChangedItemId(itemId);
  }

}
