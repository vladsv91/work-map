import safe from 'src/filters/safe-property-filter';
import Vue from 'vue';

const vuePrototype = Vue.prototype;

vuePrototype.getResponseErrorMessage = function (error) {
  const typeofError = typeof error;
  if (typeofError === 'string') { return error; }
  if (typeofError === 'object' && error) {
    let response;
    if (response = error.response) {
      const statusCode = response.status;
      if (statusCode === 401) {
        return this.$t.errorMessages.loggedout;
      } else if (statusCode === 403) {
        return this.$t.errorMessages.forbidden;
      }
    }
  }

  // Fluent validation error
  const r = safe(error, 'response.data');
  if (!r) { return ''; }
  if (r.errorMessage) { return r.errorMessage; }
  return { message: r }; // error object
};

vuePrototype.getResponseSuccessMessage = function (response) {
  if (typeof response === 'string') { return response; }
  response = safe(response, 'data.message');
  return response;
};

vuePrototype.getResponseErrorMessageOrDefault = function (request) {
  return this.getResponseErrorMessage(request) || this.$tg.genericErrorMessage;
};
vuePrototype.getResponseSuccessMessageOrDefault = function (response) {
  return this.getResponseSuccessMessage(response) || this.$tg.genericSuccessMessage;
};


const showServiceSuccess = function (response) {
  return this.$showSuccess(this.getResponseSuccessMessageOrDefault(response));
};
const showServiceError = function (request) {
  return this.$showError(this.getResponseErrorMessageOrDefault(request));
};

Object.defineProperty(vuePrototype, 'showServiceSuccess', {
  get() { return showServiceSuccess.bind(this); },
});

Object.defineProperty(vuePrototype, 'showServiceError', {
  get() { return showServiceError.bind(this); },
});

vuePrototype.$showSuccessSnackbar = showServiceSuccess;
vuePrototype.$showErrorSnackbar = showServiceError;
