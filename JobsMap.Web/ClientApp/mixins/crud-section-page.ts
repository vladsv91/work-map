export default {
  data() {
    return {
      items: [],

      deactivateBtnRefName: 'deactivateItemBtn',
      deleteBtnRefName: 'deleteItemBtn',
      activateBtnRefName: 'activateItemBtn',
    };
  },
  methods: {
    // abstract
    deleteItemService() { },
    deactivateItemService() { },
    activateItemService() { },
    getItemsService() { },


    showDeleteItemConfirmation(item, index) {
      return this.$showConfirmationDialog().then(() => {
        return this.deleteItem(item, index);
      });
    },

    // todo, maybe apply arguments
    showDeactivateItemConfirmation(item, index) {
      return this.$showConfirmationDialog().then(() => {
        return this.deactivateItem(item, index);
      });
    },
    deleteItem(item, index) {
      const itemId = item.id;
      const promise = this.deleteItemService(itemId).then(() => {
        this.onDeletedItem(item, itemId);
      }, this.showServiceError);
      this.setPromiseToBtn(this.deleteBtnRefName, index, promise);
    },
    deactivateItem(item, index) {
      const itemId = item.id;
      const promise = this.deactivateItemService(itemId).then(() => {
        this.onDeactivatedItem(item);
      }, this.showServiceError);
      this.setPromiseToBtn(this.deactivateBtnRefName, index, promise);
    },
    setPromiseToBtn(name, index, promise) {
      let btnRef = this.$refs[name];
      // eslint-disable-next-line
      if (btnRef && (btnRef = btnRef[index])) { btnRef.setPromise(promise); }
    },
    onDeletedItem(item) {
      const itemId = item.id;
      const index = this.items.findIndex(_ => _.id === itemId);
      if (index >= 0) {
        this.items.splice(index, 1);
      } else {
        console.error('not fount item by id after deletion');
      }
    },
    onDeactivatedItem(item) {
      item.isDisabled = true;
    },

    showActivateItemConfirmation(item, index) {
      return this.$showConfirmationDialog().then(() => {
        return this.activateItem(item, index);
      });
    },
    activateItem(item, index) {
      const itemId = item.id;
      const promise = this.activateItemService(itemId).then(() => {
        this.onActivatedItem(item, itemId);
      }, this.showServiceError);
      this.setPromiseToBtn(this.activateBtnRefName, index, promise);
    },
    onActivatedItem(item) {
      item.isDisabled = false;
    },

    retrieveItems() {
      this.getItemsService().then(this.onRetrievedItems, this.showServiceError);
    },

    onRetrievedItems(response) {
      this.items = response.data;
    },
    retrieveItemsOnCreated() {
      this.retrieveItems();
    },
  },
  created() {
    this.retrieveItemsOnCreated();
    // this.onCreated();
  },
  computed: {},
  components: {},
};
