﻿import 'src/utils/polyfills';
import 'src/utils/clientStart';
import { app } from './app';

window.vm = app;
// store.replaceState(window.__INITIAL_STATE__);

app.$mount('#app');
