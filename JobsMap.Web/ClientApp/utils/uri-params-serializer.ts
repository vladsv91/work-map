export default function encodeUriParams(params) {
  const result = {};

  function f1(propName, obj) {
    const hop = Object.prototype.hasOwnProperty;
    for (const prop in obj) {
      if (hop.call(obj, prop)) {
        const val = obj[prop];
        const newProp = propName + (propName ? '.' : '') + prop;
        if (val && typeof val === 'object' && !Array.isArray(val)) {
          f1(newProp, val);
        } else {
          result[newProp] = val;
        }
      }
    }
  }

  f1('', params);

  return result;
}
