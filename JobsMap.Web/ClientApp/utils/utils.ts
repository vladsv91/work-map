import { debounce } from './debounce';
import { KeyValue, Func2 } from 'src/shared/general-types';
import { Route, RouteRecord } from 'vue-router';
import { AddressPositionModel } from 'src/shared/address.model';

function setIdProperty(obj: KeyValue, propName: string) {
  const val = obj[propName];
  if (val) {
    obj[`${propName}Id`] = val.id;
    delete obj[propName];
  }
}

// todo, create recurcive
function getServiceParams(formModel: KeyValue) {
  const res = Object.assign({}, formModel);

  const hop = Object.prototype.hasOwnProperty;
  for (const key in res) {
    if (hop.call(res, key)) {
      const value = res[key];
      if (typeof value === 'object' && value) {
        const valueObjId = value.id;
        if (valueObjId) {
          res[`${key}Id`] = valueObjId;
          delete res[key];
        }
      }
    }
  }

  return res;
}

function getAllMatchedRoutes(route: Route) {
  const allMatchedRoutes = route.matched;
  return (allMatchedRoutes && allMatchedRoutes.length) ? allMatchedRoutes : [route];
}


function isAllowedAnonymousForMatchedRoutes(allMatchedRoutes: Route[] | RouteRecord[]) {
  return allMatchedRoutes.findIndex(_ => _.meta.allowAnonymous) >= 0;
}

export { setIdProperty, getServiceParams, getAllMatchedRoutes, isAllowedAnonymousForMatchedRoutes };

export const Utils = {
  isMobile(): boolean {
    return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
  },
  distanceInKilometers(addr1: AddressPositionModel, addr2: AddressPositionModel): number {
    // addr1.latitude, addr1.longitude, addr2.latitude, addr2.longitude
    const radlat1 = Math.PI * addr1.latitude / 180;
    const radlat2 = Math.PI * addr2.latitude / 180;
    // const radlon1 = Math.PI * lng1 / 180;
    // const radlon2 = Math.PI * lng2 / 180;
    const theta = addr1.longitude - addr2.longitude;
    const radtheta = Math.PI * theta / 180;
    let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;

    // Get in in kilometers
    dist *= 1.609344;

    return dist;
  },

  debounce,

  capitalize(str?: string) {
    if (!str) {
      return '';
    }
    return `${str[0].toUpperCase()}${str.substr(1)}`;
  },

  objectForEach(obj: KeyValue, func: Func2<string, any>): void {
    const hop = Object.prototype.hasOwnProperty;
    for (const key in obj) {
      if (hop.call(obj, key)) {
        func(key, obj[key]);
      }
    }
  },

};

export default Utils;
