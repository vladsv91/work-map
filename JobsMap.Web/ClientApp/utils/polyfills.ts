import 'es5-shim/es5-shim';
import 'es5-shim/es5-sham';
import 'es6-shim/es6-shim';
import 'es6-shim/es6-sham';
import PromisePolifill from 'promise-polyfill';

findIndexPolyfill();
findPolyfill();
promisePolyfill();
objectValues();
arrayIncludes();

Object.assign

function objectValues() {
  if (!window.Object.values) {
    window.Object.values = function (object) {
      return window.Object.keys(object).map(key => object[key]);
    };
  }
}

function arrayIncludes() {
  if (!window.Array.prototype.includes) {
    Object.defineProperty(Array.prototype, 'includes', {
      value: function (searchElement, fromIndex) {
        if (this == null) {
          throw new TypeError('"this" is null or not defined');
        }

        // 1. Let O be ? ToObject(this value).
        const o = Object(this);

        // 2. Let len be ? ToLength(? Get(O, "length")).
        const len = o.length >>> 0;

        // 3. If len is 0, return false.
        if (len === 0) {
          return false;
        }

        // 4. Let n be ? ToInteger(fromIndex).
        //    (If fromIndex is undefined, this step produces the value 0.)
        const n = fromIndex | 0;

        // 5. If n ≥ 0, then
        //  a. Let k be n.
        // 6. Else n < 0,
        //  a. Let k be len + n.
        //  b. If k < 0, let k be 0.
        let k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

        function sameValueZero(x, y) {
          return x === y || (typeof x === 'number' && typeof y === 'number' && Number.isNaN(x) && Number.isNaN(y));
        }

        // 7. Repeat, while k < len
        while (k < len) {
          // a. Let elementK be the result of ? Get(O, ! ToString(k)).
          // b. If SameValueZero(searchElement, elementK) is true, return true.
          if (sameValueZero(o[k], searchElement)) {
            return true;
          }
          // c. Increase k by 1.
          k++;
        }

        // 8. Return false
        return false;
      },
    });
  }
}

function findIndexPolyfill() {
  Array.prototype.findIndex = Array.prototype.findIndex || function (callback, args) {
    if (this === null) {
      throw new TypeError('Array.prototype.findIndex called on null or undefined');
    } else if (typeof callback !== 'function') {
      throw new TypeError('callback must be a function');
    }
    const list = Object(this);
    const length = list.length >= 0 ? list.length : 0;
    const thisArg = args;
    for (let i = 0; i < length; i++) {
      if (callback.call(thisArg, list[i], i, list)) {
        return i;
      }
    }
    return -1;
  };
}

function findPolyfill() {
  Array.prototype.find = Array.prototype.find || function (callback, args) {
    if (this === null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    } else if (typeof callback !== 'function') {
      throw new TypeError('callback must be a function');
    }
    const list = Object(this);
    const length = list.length >= 0 ? list.length : 0;
    const thisArg = args;
    for (let i = 0; i < length; i++) {
      const element = list[i];
      if (callback.call(thisArg, element, i, list)) {
        return element;
      }
    }
  };
}


function promisePolyfill() {
  if (!window.Promise) {
    window.Promise = PromisePolifill;
  }
}
