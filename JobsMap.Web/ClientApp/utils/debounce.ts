export function debounce(func: (...a: any[]) => void, waitInMs: number, immediate?: boolean) {
  let timeout: number;
  return function (...args: any[]) {
    const context = this;
    const later = () => {
      timeout = null;
      if (!immediate) {
        func.call(context, ...args);
      }
    };
    const callNow = immediate && !timeout;
    if (timeout) {
      clearTimeout(timeout);
    }
    timeout = setTimeout(later, waitInMs);
    if (callNow) {
      func.call(context, ...args);
    }
  };
}

export function searchDebounce(func: (...a: any[]) => void, wait: number, getValue?: () => string) {
  let lastValue = '';
  return debounce(function (...args) {
    const newValue = (typeof getValue === 'function' ? getValue.call(this) : args[0] || '');
    if (newValue.trim() !== lastValue && (newValue === '' || /\S/.test(newValue))) {
      lastValue = newValue;
      func.call(this, ...args);
    }
  }, wait);
}
