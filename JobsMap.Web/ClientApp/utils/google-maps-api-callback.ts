﻿import Vue from 'vue';

let hasLoaded = false;
const loadMapsApiCallbacks = [];

// google load maps cb
window.initMap = () => {
  hasLoaded = true;
  loadMapsApiCallbacks.forEach(cb => {
    try {
      cb();
    } catch (e) {
      console.error(e);
    }
  });
};

Vue.prototype.$onLoadMapsApi = (cb) => {
  if (hasLoaded) {
    cb();
  } else {
    loadMapsApiCallbacks.push(cb);
  }
};

Object.defineProperty(Vue.prototype, '$hasLoadedMapsApi', {
  get() { return hasLoaded; },
});
