/* eslint-disable */

// Join string, replace " -," to ""

for (var i = 0; i < s.length; i++) {
    s[i] = s[i].replace(/[,-/ ]([\S])/gi, function (f, ref) { return ref.toUpperCase() })
        .replace(/[\(\)]/gi, '')
}

// replace ids to names from HH
for (var i = 0; i < s.length; i++) {
    si = s[i];
    t = t.replace(`"${si.id}",`, `"${si.name}", // ${si.id}`)
}

// for resources localization
(function f() {
    const languages = [{ id: 'ru' }, { id: 'en' }];

    window.toOneLocalizedObject = function (params) {
        const result = {};
        function f1(propName, obj) {
            for (const prop in obj) {
                const val = obj[prop];
                // const newProp = propName + (propName ? '' : '') + prop;
                const newProp = capitalize(propName) + capitalize(prop);
                if (val && typeof val === 'object' && !isLanguagesObject(val)) {
                    f1(newProp, val);
                } else {
                    result[newProp] = val;
                }
            }
        }
        f1('', params);


        // Object.key(b).join('\n')
        // Object.values(b).map(_=>_.en || '-').join('\n')
        // Object.values(b).map(_=>_.ru || '-').join('\n')

        return result;
    }

    function capitalize(str = '') {
        return str.substr(0, 1).toLocaleUpperCase() + str.substr(1);
    }

    function isLanguagesObject(obj) {
        const keys = Object.keys(obj);
        // guck typing, if have some keys as languages ids
        return keys.length > 0 && keys.indexOf(languages[0].id) >= 0 && keys.indexOf(languages[1].id) >= 0;
    }

})();


// from vue template to Razor
s.replace(/{{ /gi, '@')
    .replace(/ }}/gi, '')
    .replace(/\$tg(\S+)/gi, function (a, b) {
        return 'JsLocalization.General' + b.replace(/\.(\S)/gi, function (a2, b2) { return b2.toUpperCase() })
    }).replace(/\$t(\S+)/gi, function (a, b) {
        return 'JsLocalization.' + b.replace(/\.(\S)/gi, function (a2, b2) { return b2.toUpperCase() })
    })
    .replace(/(itemDetails)(\S+)/gi, function (whole, a, b) {
        return a + b.replace(/\.(\S)/gi, function (a2, b2) { return '.' + b2.toUpperCase() })
    }).replace(/ \| Safe\(\'name\'\)/gi, '?.Name');
// | ToLocalTime | FormatDateTime maybe


// replace 
for (var i = 0; i < a.length; i++) { t = t.replace('"' + a[i].key + '"', function () { return `//${a[i].en}, ${a[i].ru}\n` + '"' + a[i].key + '"' }) }
a = [];
for (var i = 0; i < a1.length; i++) { a.push({ key: a1[i], en: a2[i], ru: a3[i] }) }

// Search doubles
s = t.split('\n')
a = {}
s.forEach(_ => _ in a ? a[_]++ : a[_] = 1)
b = {}
for (var key in a) if (a[key] > 1) b[key] = a[key];
