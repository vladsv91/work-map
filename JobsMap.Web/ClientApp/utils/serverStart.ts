import { createWindow } from 'domino';


const window = createWindow('<div></div>');

global.window = window;
global.window.isServer = true;
global.navigator = window.navigator;
global.document = window.document;
global.Element = (window as any).Element;
