export default {
  max(lessThanCount, defMax) {
    if (lessThanCount) {
      lessThanCount -= 2;
      return lessThanCount > 0 ? lessThanCount : 1;
    }
    return defMax;
  },
  min(moreThanCount, maxCount) {
    if (moreThanCount) {
      moreThanCount += 2;
      return moreThanCount <= maxCount ? moreThanCount : maxCount;
    }
    return 2;
  },
};
