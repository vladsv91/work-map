import * as customRules from 'src/utils/custom-validation-rules.ts';
import { FullAddressModel } from 'src/shared/address.model';

const addressValidationRules = {
  formattedAddress: { required: customRules.requiredWS },
  country: { required: customRules.requiredWS },
  city: { required: customRules.requiredWS },
  street: { required: customRules.requiredWS },
  streetNumber: { required: customRules.requiredWS },
};

const lightAddressValidationRules = {
  formattedAddress: {
    required: customRules.requiredWS,
  },
};

function getInitialAddress(): FullAddressModel {
  return {
    formattedAddress: '',
    description: '',
    country: '',
    city: '',
    street: '',
    streetNumber: '',
    latitude: null,
    longitude: null,
  };
}

export default addressValidationRules;

export {
  getInitialAddress,
  lightAddressValidationRules,
};
