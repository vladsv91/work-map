const cookies = {
  setCookie(name: string, value: string, days?: number) {
    let expires = '';
    if (days) {
      const date = new Date();
      date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
      expires = `; expires=${date.toUTCString()}`;
    }
    document.cookie = `${name}=${value || ''}${expires}; path=/`;
  },

  // getCookie(name) {
  //   const matches = document.cookie.match(new RegExp(
  //     `(?:^|; )${name.replace(/([.$?*|{}()[]\\\/\+^])/g, '\\$1')}=([^;]*)`,
  //   ));
  //   return matches ? decodeURIComponent(matches[1]) : undefined;
  // },

  // deleteCookie(name) {
  //   cookies.setCookie(name, '', {
  //     expires: -1,
  //   });
  // },

  deleteAll() {
    document.cookie.split(';').forEach((c) => {
      document.cookie = c.replace(/^ +/, '').replace(/=.*/, `=;expires=${new Date().toUTCString()};path=/`);
    });
  },
};


export default cookies;
