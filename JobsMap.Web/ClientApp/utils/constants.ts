const Constants = {
  validations: {
    maxSalary: 99999999999,
    maxEmployeeCount: 999999,
    maxRadiusInKm: 999,
    maxExperienceYears: 100,
    emailMaxLen: 100,
    phoneMaxLen: 16,

    maxItemsPerPage: 50,
  },
  appName: envVars.appName,
  userRoles: {
    company: 1,
    applicant: 2,
  },
  entityDataTypes: {
    itemDetails: 0,
    list: 1,
    companyEntityList: 2,
  },
  nearAddressInKm: 0.05, // 50 meters
};

export { Constants };

