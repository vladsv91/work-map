import deepEqual from 'deep-equal';
import deepClone from 'src/components/inputs/multiselect/utils';


export {
  deepEqual,
  deepClone,
};

export default deepClone;
