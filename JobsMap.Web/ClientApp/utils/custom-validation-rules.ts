import { req, withParams, ref } from 'vuelidate/lib/validators/common';
import * as defaultRules from 'vuelidate/lib/validators';


const passwordLengthRule = ((minLen, maxLen) =>
  withParams({ type: 'passwordLengthRule', minLen, maxLen },
    (value) => {
      if (!value) {
        return true;
      }
      if (value.length < minLen || value.length > maxLen) {
        return false;
      }
      return true;
    }))(6, 20);

// todo, check
const passwordRegexRule = (value) => {
  if (!value) {
    return true;
  }
  return /^[A-Za-z0-9!@#$()\-_+=]+$/.test(value);
};

const passwordRules = {
  passwordLengthRule,
  passwordRegexRule,
};

// todo, rid of this
const greaterDateThan = (dateFrom) =>
  withParams({ type: 'greaterDateThan', dateFrom },
    (value) => {
      if (!value || !dateFrom) {
        return true;
      }
      if (!value.getDate) {
        value = new Date(value);
      }

      if (typeof dateFrom === 'string') {
        dateFrom = new Date(dateFrom);
      }

      return dateFrom < value;
    });

// todo, rid of this
const greaterDateOrEqualsTo = (dateFrom: Date) =>
  withParams({ type: 'passwordLengthRule', dateFrom },
    (value: Date) => {
      if (!value || !dateFrom) {
        return true;
      }
      if (!value.getDate) {
        value = new Date(value);
      }

      if (typeof dateFrom === 'string') {
        dateFrom = new Date(dateFrom);
      }

      return dateFrom <= value;
    });


const rules = defaultRules;

// WS - white space
const requiredWS = (value) => {
  if (typeof value === 'string') {
    return /\S/.test(value);
  }
  return rules.required(value);
};


// WS - white space
const requiredIfWS = (prop) => {
  return withParams({ type: 'requiredIfWS', prop }, function (value, parentVm) {
    return ref(prop, this, parentVm) ? requiredWS(value) : true;
  });
};

const regex = (pattern) =>
  withParams({ type: 'regex', pattern },
    (value: string) => {
      if (!value) { return true; }
      return pattern.test(value);
    });

const minValueEx = (min) => withParams({ type: 'minValueEx', min }, value =>
  !req(value) || ((!/\s/.test(value) || value instanceof Date) && +value > +min),
);

const maxValueEx = (max) => withParams({ type: 'maxValueEx', max }, value =>
  !req(value) || ((!/\s/.test(value) || value instanceof Date) && +value < +max),
);


export {
  passwordLengthRule,
  passwordRegexRule,
  passwordRules,
  greaterDateThan,
  greaterDateOrEqualsTo,
  requiredWS,
  requiredIfWS,
  regex,
  minValueEx,
  maxValueEx,
  rules,
};

export default defaultRules;
