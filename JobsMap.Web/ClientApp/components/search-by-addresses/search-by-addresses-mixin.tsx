import HasMapMixin from 'src/mixins/has-map-mixin';
import SearchByAddresses from 'src/components/search-by-addresses/search-by-addresses.vue';
import Utils from 'src/utils/utils';
import { MarkerData } from 'src/shared/address.model';

export default {
  mixins: [HasMapMixin],
  data() {
    return {
      selectedPlace: null,
      radiusInKilometers: 1.5,
      mainText: this.$t.searchList.clickOnMapTip,
    };
  },
  methods: {

    getAddressesList() {
      const { selectedPlace } = this;
      // selected place is first
      return selectedPlace ? [selectedPlace].concat(this.restAddresses()) : this.restAddresses();
      // return selectedPlace ? [selectedPlace] : null;
    },


    onInithializedMap() {
      this.map.addListener('click', this.addOrUpdateSelectedPlace);
    },

    getMapElement() {
      return this.$refs.sba.$refs.mapComponent.map;
    },

    onRadiusInput(value) {
      if (value !== undefined) {
        this.radiusInKilometers = value;
      }
      this.updateSelectedPlaceRadius();
      this.emitInput();
    },

    addOrUpdateSelectedPlace(gMapsEvent) {
      const { latLng } = gMapsEvent;
      if (this.selectedPlace) {
        this.updateSelectedPlaceLocation(gMapsEvent);
      } else {
        const self = this;

        this.selectedPlace = {
          address: {
            latitude: latLng.lat(), longitude: latLng.lng(),
          },
          name: 'My location',
          isSelectedPlace: true,
          contentComponent: {
            render(h) {

              const onInputCb = (event) => self.onRadiusInput(+event.target.value);

              return (<div style='min-width: 200px'>
                <div>{self.$tg.radiusInKilometers}: {self.radiusInKilometers}{self.$tg.km}</div>
                <input
                  attrs={{
                    type: 'range',
                    step: '0.1',
                    min: '0.1',
                    max: '50',
                    value: self.radiusInKilometers,
                  }}
                  class='selected-place-range'
                  on={{ input: onInputCb }} />
              </div>);
            },
          },
        };
        this.onChangedModel();
      }
    },

    updateSelectedPlaceLocation(gMapsEvent) {
      const { latLng } = gMapsEvent;
      this.selectedPlace.address = { latitude: latLng.lat(), longitude: latLng.lng() };
      const markerData = this.mapMarkers.find(_ => _.circle);
      if (markerData) {
        markerData.circle.setCenter(latLng);
        markerData.marker.setPosition(latLng);
      }
      this.emitInput();
    },

    updateSelectedPlaceRadius() {
      const markerData = this.mapMarkers.find(_ => _.circle);
      if (markerData) {
        markerData.circle.setRadius(this.radiusInKilometers * 1000);
      }
    },

    removeLocation() {
      this.selectedPlace = null;
      this.onChangedModel();
    },

    createMarkerData(address, addressObj, map): MarkerData {
      const mapkerPosition = { lat: address.latitude, lng: address.longitude };

      const result: MarkerData = {} as MarkerData;

      if (!addressObj.isSelectedPlace) {
        const marker = new google.maps.Marker({
          position: mapkerPosition,
          map,
          title: addressObj.name,
          draggable: true,
        });
        this.markerWindow(addressObj, marker);

        result.marker = marker;
      } else {
        const { existingSelectedPlace } = this;

        if (existingSelectedPlace) {
          result.marker = existingSelectedPlace.marker;
          result.circle = existingSelectedPlace.circle;
          existingSelectedPlace.marker.setMap(this.map);
          existingSelectedPlace.circle.setMap(this.map);
        } else {
          const marker = new google.maps.Marker({
            position: mapkerPosition,
            map,
            title: addressObj.name,
            icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
            draggable: true,
          });

          const circle = new google.maps.Circle({
            center: mapkerPosition,
            map,
            radius: this.radiusInKilometers * 1000,
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
          });

          marker.addListener('drag', (gMapsEvent) => {
            if (this.selectedPlace) {
              this.updateSelectedPlaceLocation(gMapsEvent);
            }
          });

          this.markerWindow(addressObj, marker, Utils.isMobile());

          this.existingSelectedPlace = {
            marker,
            circle,
          };

          result.marker = marker;
          result.circle = circle;
        }
        result.isSelectedPlace = true;
      }

      return result;
    },


    getMarkersForClustering() {
      return this.mapMarkers.filter(_ => !_.isSelectedPlace).map(_ => _.marker);
    },

    onChangedModel() {
      this.emitInput();
      this.setMarkers();
    },

    emitInput() {
      let address = null;
      const { selectedPlace = null, radiusInKilometers } = this;
      if (selectedPlace) {
        address = {
          latitude: selectedPlace.address.latitude,
          longitude: selectedPlace.address.longitude,
          radiusInKilometers,
        };
        // todo, in validation
        if (!radiusInKilometers) {
          address.radiusInKilometers = this.radiusInKilometers = 1;
        }
      }
      this.$emit('input', address);
    },

    // abstract
    restAddresses() { },
  },
  // watch: {
  //   props: {
  //     deep: true,
  //     handler() {
  //       this.setMarkers();
  //     },
  //   },
  // },
  computes: {

  },
  components: {
    SearchByAddresses,
  },
  created() {
    // this.mapOptions = this.getDefaultMapOptions();
    this.enabledClusteringMarkers = true;
    this.$on('on-inithialized-map', this.onInithializedMap);
    // this.debounceEmitChange =
  },
  props: {
    companies: {
      type: Array,
    },
  },
};
