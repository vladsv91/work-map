export default {
  name: 'ListAndDetails',
  render(h) {
    return (<div><keep-alive>
      {(this.$route.name === this.routeName) ? h(this.component) : ''}</keep-alive>
      <router-view></router-view>
    </div>);
  },
  props: {
    routeName: {},
    component: {},
  },
};
