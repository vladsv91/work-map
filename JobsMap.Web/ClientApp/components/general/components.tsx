
import MSlider from './MSlider';

import GoogleMapWrap from 'src/components/general/GoogleMapWrap/GoogleMapWrap.vue';

import { ImageWrap } from 'src/components/general/ImageWrap/ImageWrap.tsx';
import ListAndDetails from './ListAndDetails';

const components = {
  BackLink: {
    functional: true,
    render(h, context) {
      return h('router-link', {
        staticClass: 'md-button card-header__back md-icon-button',
        attrs: { to: { name: context.props.to } },
      }, [h('md-icon', 'chevron_left')]);
    },
    props: ['to'],
  },

  IconLink: {
    functional: true,
    render(h, context) {
      return h('router-link', {
        staticClass: 'md-button md-theme-default md-fab',
        attrs: { to: { name: context.props.to } },
      }, [h('md-icon', context.slots().default)]);
    },
    props: ['to'],
  },

  RouterViewWrap: {
    // functional: true -> error in routes
    render(h) { return h('router-view'); },
  },

  GoogleMapWrap: (GoogleMapWrap as any).options,
  ImageWrap: (ImageWrap as any).options,

  DummyForRoute: {
    render(h) {
      return <div></div>;
    }
  },

  MSlider,
};


export default components;

// function because new instance because keep-alive
function getListAndDetailsComponent() {
  return ListAndDetails;
}

export { getListAndDetailsComponent };
