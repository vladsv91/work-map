import Vue, { VNode } from 'vue';
import './ImageWrap.scss';

export const ImageWrap = Vue.extend({
  render(h): VNode {
    return (<div>
      <div>
        <img attrs={{ src: this.src, alt: this.alt }} class={this.imgClass} />
      </div>
    </div>);
  },
  data() {
    return {
    };
  },
  methods: {

  },
  props: {
    src: {},
    alt: {},
    imgClass: {}
  },
});


