export default {
  render(h) {
    return <ui-slider attrs={{ step: 0.1, value: this.internalKeenValue }} on={{ input: this.onInput }}></ui-slider>;
  },
  data() {
    return {
      internalKeenValue: this.normalizeForKeenSlider(this.value),
    };
  },
  methods: {
    onInput(v) {
      this.internalKeenValue = v;
      this.$emit('input', this.normalizeForCustomSlider(v));
    },
    normalizeForKeenSlider(value) {
      const { min, max } = this;
      return +(100 / (max - min) * (value - max) + 100).toFixed(2);
    },
    normalizeForCustomSlider(value) {
      const { min, max } = this;
      return +((max - min) / 100 * (value - 100) + max).toFixed(2);
    },
  },
  watch: {
    value(v) {
      this.internalKeenValue = this.normalizeForKeenSlider(v);
    },
  },
  props: {
    value: {},
    min: {
      type: Number, default: 0,
    },
    max: { type: Number, default: 100 },
  },
};
