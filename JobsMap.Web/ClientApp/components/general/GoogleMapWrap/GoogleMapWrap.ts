// use: <GoogleMap ref="mapComponent"/>
import Vue from 'vue';

export interface MapData {
  isFullScreen: boolean;
  timerId: number | null;
}

export default Vue.extend({
  data(): MapData {
    return {
      isFullScreen: false,
      timerId: null,
    };
  },
  methods: {
    toggleFullScreen() {
      this.$bus.$emit('ToggleMapFullScreen', this.isFullScreen = !this.isFullScreen);
    },
  },
  computed: {
    containerStyle(): { [key: string]: any } {
      const mapHeight = this.mapHeight;
      return { height: mapHeight ? `${this.mapHeight}px` : null };
    },
    mapHeight(): any {
      return this.$store.state.layout.mapHeight;
    }
  },
  created() {
    Object.defineProperty(this, 'map', {
      get() { return this.$refs.map; },
    });
  },
  mounted() {
    this.timerId = setInterval(() => {
      const newHeight = parseInt((this.$refs.mapContainer as HTMLElement).style.height as string, 10);
      if (newHeight !== this.mapHeight && newHeight) {
        this.$store.commit('SetLayoutValue', ['mapHeight', newHeight]);
      }
      // 
    }, 2000);
  },
  destroyed() {
    if (this.timerId) {
      clearInterval(this.timerId);
      this.timerId = null;
    }
  },
});
