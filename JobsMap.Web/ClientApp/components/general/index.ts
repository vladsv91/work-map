import Vue from 'vue';

import Components from './components';


forIn(Components);

function forIn(items) {
  const hop = Object.prototype.hasOwnProperty;
  for (const key in items) {
    if (hop.call(items, key)) {
      const item = items[key];
      if (!item.name) { item.name = key; }
      Vue.component(key, item);
    }
  }
}
