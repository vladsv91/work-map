import HasMapMixin from 'src/mixins/has-map-mixin';
import {
  infoWindowComponent as searchVacanciesByAddressesInfoWindowComponent,
} from 'src/components/pages/vacancies/vacancies-list/search-vacancy-by-addresses/search-vacancy-by-addresses.tsx';
import Utils from 'src/utils/utils';
import { Constants } from 'src/utils/constants';

const infoWindowComponent = {
  mixins: [searchVacanciesByAddressesInfoWindowComponent],
  methods: {
    getVacancyInfoHtml(h, vacancy, index) {
      return (<div class="m-b-10">
        <div>
          {index ? <span>{index}.</span> : null}
          <span class="grey-color">{this.$tg.filial}: </span>
          {vacancy.companyFilial.name}
        </div>
        <div>
          <span class="grey-color">{this.$tg.address}: </span>
          {vacancy.companyFilial.address.formattedAddress}
        </div>

        <router-link attrs={{ to: { name: 'VacancyDetails', params: { vacancyId: vacancy.id } } }}>
          <span class="grey-color">{this.$tg.vacancy}: </span>
          {vacancy.name}
        </router-link>

      </div>);
    },

    getNearestVacancies() {
      const currentFilial = this.vacancy.companyFilial;
      const similarAddreses = [];
      this.allVacancies.forEach(vacancy => {
        const filial = vacancy.companyFilial;
        if (filial === currentFilial) { return; } // compare referenses
        if (Utils.distanceInKilometers(filial.address, currentFilial.address) <
          Constants.nearAddressInKm) { // 50 meters
          similarAddreses.push({ vacancy, filial });
        }
      });
      return similarAddreses;
    },
  },
};
export default {
  mixins: [HasMapMixin],
  methods: {
    getAddressesList() {
      const companyVacancies = this.companyVacancies;
      return this.companyVacancies.map(vacancy => ({
        ...vacancy, address: vacancy.companyFilial.address,
        contentComponent: infoWindowComponent,
        contentProps: { vacancy, allVacancies: companyVacancies },
      }));
    },
  },
  watch: {
    companyVacancies() {
      this.setMarkers();
    },
  },
  created() {
    this.enabledClusteringMarkers = true;
  },
  props: { companyVacancies: {} },
};
