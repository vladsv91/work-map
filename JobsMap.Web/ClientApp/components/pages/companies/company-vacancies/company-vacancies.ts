import services from 'src/services';
import { CompanyDetailsEntityMixin } from 'src/mixins/page-details';
import VacanciesOnMap from './vacancies-on-map/vacancies-on-map.vue';

export default {
  mixins: [CompanyDetailsEntityMixin],
  data() {
    return {
      entityName: 'CompanyVacancies',
      companyVacancies: [],
    };
  },
  methods: {
    getItemData: services.companiesService.getVacancies,

    onRetrievedData(response) { this.companyVacancies = response.data; },
  },
  computed: {
    t() { return this.$t.companies.vacancies; },
  },
  components: { VacanciesOnMap },
};
