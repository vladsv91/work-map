import services from 'src/services';
import PageDetailsMixin from 'src/mixins/page-details';

export default {
  mixins: [PageDetailsMixin],
  data() {
    return {};
  },
  methods: {
    getItemData: services.companiesService.getDetails,
  },
  computed: {
    t() {
      return this.$t.companies.details;
    },
    tabs() {
      const { $tg, itemDetails } = this;
      return [
        { caption: $tg.mainInfo, to: { name: 'CompanyDetails' } },
        { caption: `${$tg.filials} (${(itemDetails.filialsCount || 0)})`, to: { name: 'CompanyDetailsFilials' } },
        { caption: `${$tg.vacancies} (${(itemDetails.vacanciesCount || 0)})`, to: { name: 'CompanyDetailsVacancies' } },
        { caption: `${$tg.photos} (${(itemDetails.photosCount || 0)})`, to: { name: 'CompanyDetailsPhotos' } },
      ];
    },
  },
};
