import SearchByAddressesMixin from 'src/components/search-by-addresses/search-by-addresses-mixin.tsx';
import Utils from 'src/utils/utils';
import { Constants } from 'src/utils/constants';

const infoWindowComponent = {
  render(h) {
    const nearest = this.nearestCompanies;
    return (<div>
      {(nearest.length ? <div class="com-info-on-map-label">{this.t.currentMarkerCompany}:</div> : null)}
      {this.getCompanyInfoHtml(h, this.company, this.currentFilial, null)}
      {this.getNearestCompaniesHtml(h)}
    </div>);
  },
  methods: {
    getNearestCompaniesHtml(h) {
      const nearest = this.nearestCompanies;
      if (nearest.length === 0) { return; }
      return (<div class="m-t-10">
        <div class="com-info-on-map-label">{this.t.nearestCompaniesAndFilials}:</div>
        {nearest.map((_, index) => this.getCompanyInfoHtml(h, _.company, _.filial, index + 1))}
      </div>);
    },

    getCompanyInfoHtml(h, company, filial, index) {
      return <div class="m-b-10">
        {index ? <span>{index}.</span> : null}
        <router-link attrs={{ to: { name: 'CompanyDetails', params: { companyId: company.id } } }}>
          <span class="grey-color">{this.$tg.company}: </span>
          {company.name}
          <div>
            <span class="grey-color">{this.$tg.filial}: </span>
            {filial.name}
          </div>
        </router-link>
        <div>
          <span class="grey-color">{this.$tg.address}: </span>
          {filial.address.formattedAddress}
        </div>
      </div>;
    },

    getNearestCompanies() {
      const companies = this.allCompanies;
      const { currentFilial } = this;
      const currFilId = currentFilial.id;
      const similarAddreses = [];
      companies.forEach(company => {
        company.filials.forEach(filial => {
          if (filial.id === currFilId) { return; }
          if (Utils.distanceInKilometers(filial.address, currentFilial.address) < Constants.nearAddressInKm) { // 50 meters
            similarAddreses.push({ company, filial });
          }
        });
      });
      return similarAddreses;
    },
  },
  computed: {
    nearestCompanies() {
      return this.getNearestCompanies();
    },
    t() { return this.$t.searchCompaniesByAddresses; },
  },
  props: {
    company: { type: Object },
    allCompanies: { type: Array },
    currentFilial: { type: Object },
  },
};

export default {
  mixins: [SearchByAddressesMixin],
  data() {
    return {
      // mainText: 'Click on map to pick the companies in that region.',
    };
  },
  methods: {
    restAddresses() {
      const { companies } = this;
      if (!companies || !companies.length) { return []; }

      if (!companies[0].filials) { return []; }
      const addressesObjs = [];

      companies.forEach(company => {
        company.filials.forEach(filial => {
          addressesObjs.push({
            name: `${company.name}\nfilial: ${filial.name}`,
            address: filial.address,
            contentComponent: infoWindowComponent,
            contentProps: { company, allCompanies: companies, currentFilial: filial },
          });
        });
      });
      return addressesObjs;
    },
  },
  watch: {
    companies() {
      this.setMarkers();
    },
  },
  props: {
    companies: {
      type: Array,
    },
  },
};
