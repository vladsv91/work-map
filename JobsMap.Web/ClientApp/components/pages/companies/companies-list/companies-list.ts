import services from 'src/services';
import ImageFallback from 'components/image-fallback/image-fallback.vue';
// import * as customRules from 'src/utils/custom-validation-rules.ts';
import Collapsible from 'components/collapsible/collapsible.vue';
import FilterablePageableMixin from 'src/mixins/filterable-pageable.ts';
// import minMax from 'src/utils/min-max-depending.js';
import SearchCompaniesByAddresses from './search-companies-by-addresses/search-companies-by-addresses.vue';


export default {
  mixins: [FilterablePageableMixin],
  data() {
    return {
      entityName: 'Company',
      formModel: {
        industry: null,
        moreThanEmployeeCount: null,
        lessThanEmployeeCount: null,
        address: null,

        moreThanFilialsCount: null,
        lessThanFilialsCount: null,
        moreThanVacanciesCount: null,
        lessThanVacanciesCount: null,
      },
      // collapsedAddresses: true,
    };
  },
  methods: {
    getItemsByFilter: services.companiesService.getByFilter,


    getFormModelParams() {
      const res = Object.assign({}, this.formModel);
      this.setIdProperty(res, 'industry');
      res.includeAddresses = !this.collapsedAddresses;
      if (!res.includeAddresses) {
        res.address = null;
      }
      return res;
    },
  },
  computed: {
    // t() {
    //   return this.$t.companies;
    // },
    mtecMax() {
      let { lessThanEmployeeCount } = this.formModel;
      if (lessThanEmployeeCount) {
        lessThanEmployeeCount -= 2;
        return lessThanEmployeeCount > 0 ? lessThanEmployeeCount : 1;
      }
      return this.$consts.validations.maxEmployeeCount;
    },
    ltecMin() {
      let { moreThanEmployeeCount } = this.formModel;
      if (moreThanEmployeeCount) {
        moreThanEmployeeCount += 2;
        const maxEmpCount = this.$consts.validations.maxEmployeeCount;
        return moreThanEmployeeCount <= maxEmpCount ? moreThanEmployeeCount : maxEmpCount;
      }
      return 2;
    },
  },
  components: {
    ImageFallback,
    Collapsible,
    SearchCompaniesByAddresses,
  },
  created() {},
};
