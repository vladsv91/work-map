import services from 'src/services';
import { CompanyDetailsEntityMixin } from 'src/mixins/page-details';
// it is bad to use details mixin in this page

export default {
  mixins: [CompanyDetailsEntityMixin],
  data() {
    return {
      entityName: 'CompanyPhotos',
      companyPhotos: [],
      swiperOption: {
        autoHeight: true,
        spaceBetween: 20,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      },
    };
  },
  methods: {
    onLoadedImage() {
      const { swiper } = this.$refs;
      if (swiper) {
        swiper.$forceUpdate();
      }
    },

    getItemData: services.companiesService.getPhotos,

    onRetrievedData(response) {
      this.companyPhotos = response.data;
    },

  },
  computed: {
    t() {
      return this.$t.companies.photos;
    },
  },
  components: {},
};
