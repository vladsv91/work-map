import services from 'src/services';
import HasMapMixin from 'src/mixins/has-map-mixin';
import { CompanyDetailsEntityMixin } from 'src/mixins/page-details';

export default {
  mixins: [HasMapMixin, CompanyDetailsEntityMixin],
  data() {
    return {
      entityName: 'CompanyFilials',
      companyFilials: [],
    };
  },
  methods: {
    getItemData: services.companiesService.getFilials,

    onRetrievedData(response) {
      this.companyFilials = response.data;
    },

    getAddressesList() {
      return this.companyFilials;
    },
  },
  computed: {
    t() {
      return this.$t.companies.filials;
    },
    // items: {
    //   get() { return this.companyFilials; },
    //   set(value) { return this.companyFilials = value; },
    // },
  },
  watch: {
    companyFilials() {
      this.setMarkers();
    },
  },
  components: {},
  created() {
    this.enabledClusteringMarkers = true;
  },
};
