export default {
  data() {
    return {};
  },
  methods: {},
  computed: {
    tabs() {
      return [
        //
        {
          caption: this.t.asCompanyMenu,
          to: { name: 'SignUpAsCompany' },

        },
        {
          caption: this.t.asApplicantMenu,
          to: { name: 'SignUpAsApplicant' },
        },
      ];
    },
    t() { return this.$t.signUp; },
  },
};
