import rules, * as customRules from 'src/utils/custom-validation-rules.ts';
import RegisterMixin from '../sign-up-mixin';

export default {
  mixins: [RegisterMixin],
  data() {
    return {
      formModel: {
        fullName: '',
      },
    };
  },
  methods: {
    apply() {
      const promise = this.$store.dispatch('RegisterAsApplicant', this.$getServiceParams(this.formModel)).then(this.onRegistered, this.onError);
      this.$refs.submitBtn.setPromise(promise);
    },
  },
  computed: {
    t() {
      return this.$t.signUp.asApplicant;
    },
  },
  validations: {
    formModel: {
      fullName: {
        required: customRules.requiredWS,
        minLength: rules.minLength(3),
        maxLength: rules.maxLength(100),
      },

      email: {
        required: customRules.requiredWS,
        email: rules.email,
        minLength: rules.minLength(6),
        maxLength: rules.maxLength(50),
      },

      password: {
        required: customRules.requiredWS,
        minLength: rules.minLength(6),
        maxLength: rules.maxLength(30),
      },

      confirmPassword: {
        required: customRules.requiredWS,
        sameAs: rules.sameAs('password'),
      },
    },
  },
};
