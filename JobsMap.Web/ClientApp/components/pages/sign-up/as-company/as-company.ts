import rules, * as customRules from 'src/utils/custom-validation-rules.ts';
// import addressValidationRules from 'src/utils/address-utils.ts';
import { lightAddressValidationRules } from 'src/utils/address-utils.ts';
import RegisterMixin from '../sign-up-mixin';

export default {
  mixins: [RegisterMixin],
  data() {
    return {
      formModel: {
        companyName: null,
        companyId: null,
        mainOfficeAddress: null,
        industry: null,
        employeeCount: null,

      },
    };
  },
  methods: {
    apply() {
      const promise = this.$store.dispatch('RegisterAsCompany', this.$getServiceParams(this.formModel)).then(this.onRegistered, this.onError);
      this.$refs.submitBtn.setPromise(promise);
    },
  },
  computed: {
    t() {
      return this.$t.signUp.asCompany;
    },
  },
  components: {},
  validations() {
    return {
      formModel: {
        companyName: {
          required: customRules.requiredWS,
          minLength: rules.minLength(2),
          maxLength: rules.maxLength(100),
          regex: customRules.regex(/^[a-zA-Z0-9а-ёА-ЯА-Яа-яёЁЇїІіЄєҐґ!. -]+$/),
        },

        companyId: {
          minLength: rules.minLength(2),
          maxLength: rules.maxLength(30),
          regex: customRules.regex(/^[a-zA-Z0-9][a-zA-Z0-9.-]*[a-zA-Z0-9]$/),
        },

        employeeCount: {
          required: customRules.requiredWS,
          minValue: rules.minValue(1),
          maxValue: rules.maxValue(this.$consts.validations.maxEmployeeCount),
        },
        industry: {
          required: customRules.requiredWS,
        },

        mainOfficeAddress: lightAddressValidationRules,

        email: {
          required: customRules.requiredWS,
          email: rules.email,
          minLength: rules.minLength(6),
          maxLength: rules.maxLength(50),
        },

        password: {
          required: customRules.requiredWS,
          minLength: rules.minLength(6),
          maxLength: rules.maxLength(30),
        },

        confirmPassword: {
          required: customRules.requiredWS,
          sameAs: rules.sameAs('password'),
        },
      },
    };
  },
};
