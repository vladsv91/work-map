// import rules, * as customRules from 'src/utils/custom-validation-rules.ts';

export default {
  data() {
    return {
      formModel: {

        email: null,
        password: null,
        confirmPassword: null,
      },
      isRegistered: false,
    };
  },
  methods: {
    onRegistered() {
      this.isRegistered = true;
      // todo, show success and email confirmation
    },
    onError(response) {
      this.showServiceError(response);
    },
  },
  created() {},
  computed: {
    successMessage() {
      return this.$t.signUp.signUpSuccessMessage;
    },
  },
  components: {},
  // validations: {
  //   formModel: {
  //     email: {
  //       required: customRules.requiredWS,
  //       email: rules.email,
  //       minLength: rules.minLength(6),
  //       maxLength: rules.maxLength(50),
  //     },

  //     password: {
  //       required: customRules.requiredWS,
  //       minLength: rules.minLength(6),
  //       maxLength: rules.maxLength(30),
  //     },

  //     confirmPassword: {
  //       required: customRules.requiredWS,
  //       sameAs: rules.sameAs('password'),
  //     },
  //   },
  // },
};
