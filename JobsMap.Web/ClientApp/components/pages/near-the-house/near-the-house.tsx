import Vue, { VNode } from 'vue';
import './near-the-house.scss';

export default Vue.extend({
  render(h): VNode {
    return <div class="near-the-house"></div>;
  },
  mounted() {
    this.$commit('headerClass', 'main-page-header');
  },
  beforeDestroy() {
    this.$commit('headerClass', '');
  },
  computed: {
    t() {
    },
  },
  components: {
  },
});
