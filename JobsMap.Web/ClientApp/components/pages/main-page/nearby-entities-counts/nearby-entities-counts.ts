export default {
  data() {
    return {
      nearbyEntitiesCounts: null,
      // nearbyEntitiesCounts: { companiesCount: 4, vacanciesCount: 3, resumesCount: 0, radiusInKm: 5.0, hasData: true },
    };
  },
  methods: {
    retrieveNearbyEntitiesCounts(latitude, longitude) {
      this.services.locationService.getNearbyEntitiesCounts({ latitude, longitude }).then((response) => {
        this.nearbyEntitiesCounts = response.data;
      });
    },
  },
  createdFront() {
    navigator.geolocation.getCurrentPosition((data) => {
      this.retrieveNearbyEntitiesCounts(data.coords.latitude, data.coords.longitude);
    }, () => {
      const ipAddress = window.serverData.ipAddress;
      this.services.ipService.getInfoByIp(ipAddress).then((response) => {
        const data = response.data;
        this.retrieveNearbyEntitiesCounts(data.lat, data.lon);
      });
    });
  },

  computed: {
    t() { return this.$t.nearbyEntitiesCounts; },
  },
};
