import NearbyEntitiesCounts from './nearby-entities-counts/nearby-entities-counts.vue';

export default {
  mounted() {
    this.$commit('headerClass', 'main-page-header');
  },
  beforeDestroy() {
    this.$commit('headerClass', '');
  },
  computed: {
    t() {
      return this.$t.mainPage;
    },
  },
  components: {
    NearbyEntitiesCounts,
  },
};
