import services from 'src/services';
import * as customRules from 'src/utils/custom-validation-rules.ts';
import LoginModal from 'components/layouts/header/login-modal/login-modal.vue';


const confirmationStatuses = {
  loading: 0,
  success: 1,
  error: 2,
  openForResending: 3,
};

const resendConfirmationStatuses = {
  none: 0,
  loading: 1,
  success: 2,
  error: 3,
};

export default {
  data() {
    return {
      confirmationStatuses,
      resendConfirmationStatuses,
      confirmationStatus: confirmationStatuses.loading,
      resendConfirmationStatus: resendConfirmationStatuses.none,
      resendEmailModel: {
        email: '',
        password: '',
      },
    };
  },
  methods: {
    openLoginModal() {
      this.$mountModal(LoginModal);
    },
    resendEmail() {
      if (this.isInvalid) return;
      return services.accountService.resendConfirmationEmailByName(this.resendEmailModel).then(() => {
        this.resendConfirmationStatus = resendConfirmationStatuses.success;
      }, error => {
        this.resendConfirmationStatus = resendConfirmationStatuses.error;
        this.showServiceError(error);
      });
    },
  },
  computed: {
    t() {
      return this.$t.confirmEmail;
    },
  },
  components: {},
  created() {
    const { userId, token, step } = this.$route.query;
    if (!userId || !token) {
      if (step !== 'resending') {
        this.$router.replace({ name: 'Main' });
      } else {
        this.confirmationStatus = confirmationStatuses.openForResending;
      }
      return;
    }
    this.$store.dispatch('ConfirmEmail', { userId, token: token.replace(/ /ig, '+') }).then(() => {
      this.confirmationStatus = confirmationStatuses.success;
    }, error => {
      this.confirmationStatus = confirmationStatuses.error;
      this.showServiceError(error);
    });
  },
  validations: {
    resendEmailModel: {
      email: {
        required: customRules.requiredWS,
        email: customRules.rules.email,
      },
      password: {
        required: customRules.requiredWS,
      },
    },
  },
};
