import SearchByAddressesMixin from 'src/components/search-by-addresses/search-by-addresses-mixin.tsx';

const infoWindowComponent = {
  render(h) {
    const { resume } = this;
    const resumeName = resume.name;
    const { fullName } = resume.applicant;
    return h('div',
      [
        h('router-link', {
          class: 'd-ib',
          attrs: { to: { name: 'ResumeDetails', params: { resumeId: resume.id } } },
        }, [`${this.$tg.resume}: ${resumeName}`, h('div', `${this.$tg.fullNameLabel}: ${fullName}`)]),
        h('div', [h('span', [`${this.$tg.experienceYears}: ${resume.experienceYears}`])]),
      ]);
  },
  props: { resume: { type: Object } },
};

export default {
  mixins: [SearchByAddressesMixin],
  data() {
    return {
      // mainText: 'Click on map to pick the resumes in that region.',
    };
  },
  watch: {
    resumes() {
      this.setMarkers();
    },
  },
  methods: {
    restAddresses() {
      const { resumes } = this;
      if (!resumes || !resumes.length) return [];

      if (!resumes[0].applicant.preferredAddresses) return [];
      const addressesObjs = [];

      resumes.forEach(resume => {
        const resumeName = resume.name;
        const { fullName } = resume.applicant;
        resume.applicant.preferredAddresses.forEach(preferredAddress => {
          addressesObjs.push({
            name: `Name: ${fullName}\nResume: ${resumeName}`,
            address: preferredAddress.address,
            contentComponent: infoWindowComponent,
            contentProps: { resume },
          });
        });
      });
      return addressesObjs;
    },
  },
  components: {},
  created() { },
  props: {
    resumes: {
      type: Array,
    },
  },
};
