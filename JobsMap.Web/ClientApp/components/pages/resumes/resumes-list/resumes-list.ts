import services from 'src/services';
// import * as customRules from 'src/utils/custom-validation-rules.ts';

import Collapsible from 'components/collapsible/collapsible.vue';
import FilterablePageableMixin from 'src/mixins/filterable-pageable.ts';
import SearchResumesByAddresses from './search-resumes-by-addresses/search-resumes-by-addresses.vue';


export default {
  mixins: [FilterablePageableMixin],
  data() {
    return {
      entityName: 'Resume',
      formModel: {
        profession: null,
        employmentType: null,
        currency: null,
        lessThanSalaryPerMonth: null,
        moreThanSalaryPerMonth: null,

        lessThanExperienceYears: null,
        moreThanExperienceYears: null,

        address: null,
      },
      // collapsedAddresses: true,
    };
  },
  methods: {
    getItemsByFilter: services.resumesService.getByFilter,

    getFormModelParams() {
      const res = Object.assign({}, this.formModel);
      this.setIdProperty(res, 'profession');
      this.setIdProperty(res, 'currency');
      this.setIdProperty(res, 'employmentType');
      res.includeAddresses = !this.collapsedAddresses;
      if (!res.includeAddresses) {
        res.address = null;
      }
      return res;
    },

  },
  computed: {
    // t() {
    //   return this.$t.resumes;
    // },
    mtsMax() {
      const { lessThanSalaryPerMonth } = this.formModel;
      if (lessThanSalaryPerMonth) {
        return lessThanSalaryPerMonth > 0 ? lessThanSalaryPerMonth : 1;
      }
      return this.$consts.validations.maxSalary;
    },
    ltsMin() {
      const { moreThanSalaryPerMonth } = this.formModel;
      if (moreThanSalaryPerMonth) {
        const { maxSalary } = this.$consts.validations;
        return moreThanSalaryPerMonth <= maxSalary ? moreThanSalaryPerMonth : maxSalary;
      }
      return 0;
    },

    mtexMax() {
      const { lessThanExperienceYears } = this.formModel;
      if (lessThanExperienceYears) {
        return lessThanExperienceYears;
      }
      return this.$consts.validations.maxExperienceYears;
    },
    ltexMin() {
      const { moreThanExperienceYears } = this.formModel;
      if (moreThanExperienceYears) {
        const { maxExperienceYears } = this.$consts.validations;
        return moreThanExperienceYears <= maxExperienceYears ? moreThanExperienceYears : maxExperienceYears;
      }
      return 0;
    },

  },
  components: {
    Collapsible,
    SearchResumesByAddresses,
  },
};
