import services from 'src/services';
import PageDetailsMixin from 'src/mixins/page-details';


export default {
  mixins: [PageDetailsMixin],
  methods: { getItemData: services.resumesService.getDetails },
  computed: {
    t() { return this.$t.resumes.details; },
    hasPhoto() {
      const { applicant } = this.itemDetails;
      return applicant && applicant.photoUri;
    },
  },
};
