import services from 'src/services';
import { lightAddressValidationRules } from 'src/utils/address-utils.ts';
import rules, * as customRules from 'src/utils/custom-validation-rules.ts';
import { EditItemMixin } from 'src/mixins/page-details';

export default {
  mixins: [EditItemMixin],
  methods: {
    getItemData: services.myCompanyService.getFilialDetails,
    saveAddingItem(formModel) {
      return this.$store.dispatch('AddCompanyFilial', formModel);
    },
    saveUpdatingItem(formModel) {
      return this.$store.dispatch('UpdateCompanyFilial', formModel);
    },
    onSaveAddingItemSuccess() {
      this.$router.push({ name: 'MyCompanyFilials' });
    },

    getInitialFormModel() {
      return {
        name: null,
        id: null,
        address: null,
        employeeCount: null,
        isDisabled: false,
      };
    },
  },
  computed: {
    t() {
      return this.$t.company.filials.edit;
    },
  },
  components: {},
  validations() {
    return {
      formModel: {
        name: {
          required: customRules.requiredWS,
          minLength: rules.minLength(2),
          maxLength: rules.maxLength(50),
        },

        employeeCount: {
          required: customRules.requiredWS,
          minValue: rules.minValue(1),
          maxValue: rules.maxValue(this.$consts.validations.maxEmployeeCount),
        },
        address: lightAddressValidationRules,
      },
    };
  },
};
