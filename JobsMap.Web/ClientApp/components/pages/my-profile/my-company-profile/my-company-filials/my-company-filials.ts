import services from 'src/services';
import CrudSectionPage from 'src/mixins/crud-section-page.ts';

export default {
  mixins: [CrudSectionPage],
  data() {
    return {
      items: this.$g.myCompanyFilials || [],
    };
  },
  methods: {
    deactivateItemService: services.myCompanyService.deactivateFilial,
    activateItemService: services.myCompanyService.activateFilial,
    getItemsService() { return this.$store.dispatch('RetrieveCompanyFilials'); },
  },
  created() {},
  computed: {},
  components: {},
};
