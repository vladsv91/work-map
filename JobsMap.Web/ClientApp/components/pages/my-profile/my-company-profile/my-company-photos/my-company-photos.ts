import CrudSectionPage from 'src/mixins/crud-section-page.ts';
import services from 'src/services';


export default {
  mixins: [CrudSectionPage],
  data() {
    return {
      swiperOption: {
        autoHeight: true,
        spaceBetween: 20,
        pagination: {
          el: '.swiper-pagination',
          clickable: true,
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      },
      uploadImageFile: null,
      items: this.$g.myCompanyPhotos || [],
    };
  },
  methods: {
    deleteItemService: services.myCompanyService.deletePhoto,
    getItemsService() { return this.$store.dispatch('RetrieveCompanyPhotos'); },

    onLoadedImage() {
      const { swiper } = this.$refs;
      if (swiper) {
        swiper.$forceUpdate();
      }
    },
    deleteItem(item, index) {
      const itemId = item.id;
      if (itemId) {
        const promise = this.deleteItemService(itemId).then(() => {
          this.onDeletedItem(item);
        }, this.showServiceError);
        this.$refs[`deletePhotoBtn${index}`][0].setPromise(promise);
      } else {
        this.items.splice(index, 1);
      }
    },
    savePhoto(photoItem, index) {
      const promise = services.myCompanyService.addPhoto(this.uploadImageFile, photoItem.companyFilial ? photoItem.companyFilial.id : null)
        .then((response) => {
          photoItem.id = response.data.id;
          photoItem.photoUri = response.data.url;
        }, this.showServiceError);
      this.$refs[`savePhotoBtn${index}`][0].setPromise(promise);
    },
    onFileChanged(file) {
      if (!file) return;
      this.uploadImageFile = file;
      const reader = new FileReader();
      reader.onloadend = () => {
        this.items.push({
          id: null,
          photoUri: reader.result,
          companyFilial: null,
        });
      };
      reader.readAsDataURL(file);
    },
  },
  created() {

  },
  computed: {},
  components: {},
};
