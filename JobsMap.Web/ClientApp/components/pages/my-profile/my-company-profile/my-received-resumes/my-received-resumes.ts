import CrudSectionPage from 'src/mixins/crud-section-page.ts';

export default {
  mixins: [CrudSectionPage],
  data() {
    return {
      items: this.$g.myCompanyReceivedResumes || [],
    };
  },
  methods: {
    getItemsService() { return this.$store.dispatch('RetrieveCompanyReceivedResumes'); },
  },
  created() {},
  computed: {

  },
  components: {},
};
