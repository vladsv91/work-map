import rules, * as customRules from 'src/utils/custom-validation-rules.ts';

export default {
  data() {
    return {
      hasMainPhotoUriChanged: false,
      formModel: {
        name: null,
        mainPhotoUri: null,
        contactEmail: null,
        contactPhone: null,
        websiteUrl: null,
        description: null,
        employeeCount: null,
        industry: null,
      },
      companyMainPhotoFile: null,
    };
  },
  methods: {
    save() {
      this.$refs.submitBtn.setPromise(this.$store.dispatch('UpdateCompanyMainInfo', this.formModel).catch(this.showServiceError));
    },

    onFileChanged(file) {
      this.companyMainPhotoFile = file;
      if (!file) return;

      this.hasMainPhotoUriChanged = true;
      const reader = new FileReader();
      reader.onloadend = () => this.formModel.mainPhotoUri = reader.result;
      reader.readAsDataURL(file);
    },

    saveCompanyMainPhoto() {
      this.$refs.saveCompanyPhotoBtn.setPromise(this.$store.dispatch('UploadCompanyMainPhoto', this.companyMainPhotoFile).then((response) => {
        this.formModel.mainPhotoUri = response.data;
        this.hasMainPhotoUriChanged = false;
      }, this.showServiceError));
    },
  },
  created() {
    const { myCompanyMainInfo } = this.$g;
    if (myCompanyMainInfo) {
      this.formModel = Object.assign({}, this.formModel, myCompanyMainInfo);
    } else {
      this.$store.dispatch('RetrieveCompanyMainInfo').then(data => {
        this.formModel = Object.assign({}, this.formModel, data);
      }, this.showServiceError);
    }
  },
  computed: {
    t() {
      return this.$t.editCompanyProfile;
    },
  },
  validations: {
    formModel: {
      name: {
        required: customRules.requiredWS,
      },
      websiteUrl: {
        url: customRules.rules.url,
      },
      contactPhone: {
        required: customRules.requiredWS,
        minLength: rules.minLength(5),
        maxLength: rules.maxLength(20),
      },
      contactEmail: {
        required: customRules.requiredWS,
        email: rules.email,
        minLength: rules.minLength(6),
        maxLength: rules.maxLength(100),
      },
      description: {
        required: customRules.requiredWS,
      },
      employeeCount: {
        required: customRules.requiredWS,
      },
      industry: {
        required: customRules.requiredWS,
      },
    },
  },
  components: {},
};
