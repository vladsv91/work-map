import rules, * as customRules from 'src/utils/custom-validation-rules.ts';
import services from 'src/services';
import { EditItemMixin } from 'src/mixins/page-details';

export default {
  mixins: [EditItemMixin],
  methods: {
    getItemData: services.myCompanyService.getVacancyDetails,
    saveAddingItem(formModel) {
      return this.$store.dispatch('AddCompanyVacancy', formModel);
    },
    saveUpdatingItem(formModel) {
      return this.$store.dispatch('UpdateCompanyVacancy', formModel);
    },
    onSaveAddingItemSuccess() {
      this.$router.push({ name: 'MyCompanyVacancies' });
    },

    getInitialFormModel() {
      return {
        id: null,
        name: null,
        salaryPerMonth: null,
        currency: null,
        companyFilial: null,
        profession: null,
        employmentType: null,
        // contactPhone: null,
        // contactEmail: null,
        isDisabled: false,

        description: null,
        requirements: null,
        duties: null,
        workingConditions: null,
      };
    },
  },
  computed: {
    t() {
      return this.$t.vacancies.edit;
    },
  },
  components: {},
  props: {},
  validations() {
    return {
      formModel: {
        name: {
          required: customRules.requiredWS,
          minLength: rules.minLength(5),
          maxLength: rules.maxLength(100),
        },
        salaryPerMonth: {
          minValue: rules.minValue(0),
          maxValue: rules.maxValue(this.$consts.validations.maxSalary),
        },
        currency: {
          required: customRules.requiredWS,
        },
        companyFilial: {
          required: customRules.requiredWS,
        },
        profession: {
          required: customRules.requiredWS,
        },
        // contactPhone: {
        //   required: customRules.requiredWS,
        //   minLength: rules.minLength(5),
        //   maxLength: rules.maxLength(20),
        // },
        // contactEmail: {
        //   required: customRules.requiredWS,
        //   email: rules.email,
        //   minLength: rules.minLength(6),
        //   maxLength: rules.maxLength(100),
        // },
        description: {
          required: customRules.requiredWS,
          minLength: rules.minLength(50),
          maxLength: rules.maxLength(2000),
        },
        requirements: {
          required: customRules.requiredWS,
          minLength: rules.minLength(20),
          maxLength: rules.maxLength(1000),
        },
        duties: {
          required: customRules.requiredWS,
          minLength: rules.minLength(20),
          maxLength: rules.maxLength(1000),
        },
        workingConditions: {
          required: customRules.requiredWS,
          minLength: rules.minLength(20),
          maxLength: rules.maxLength(1000),
        },
        employmentType: {
          required: customRules.requiredWS,
        },
      },
    };
  },
};
