import services from 'src/services';
import CrudSectionPage from 'src/mixins/crud-section-page.ts';

export default {
  mixins: [CrudSectionPage],
  data() {
    return {
      items: this.$g.myCompanyVacancies || [],
    };
  },
  methods: {
    deactivateItemService: services.myCompanyService.deactivateVacancy,
    activateItemService: services.myCompanyService.activateVacancy,
    getItemsService() { return this.$store.dispatch('RetrieveCompanyVacancies'); },
  },
  created() {},
  computed: {

  },
  components: {},
};
