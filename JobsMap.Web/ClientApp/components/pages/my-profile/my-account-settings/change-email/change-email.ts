import services from 'src/services';
import * as customRules from 'src/utils/custom-validation-rules.ts';


export default {
  data() {
    return {
      formModel: {
        newEmail: null,
      },
    };
  },
  methods: {
    apply() {
      // todo, backend check currentEMail
      return services.accountService.sendEmailChangingEmail(this.formModel).then(() =>
        this.showServiceSuccess(this.t.successMessage), this.showServiceError);
    },
  },
  created() {},
  computed: {
    t() {
      return this.$t.accountSettings.changeEmail;
    },
    newEmailValidationMessages() {
      return { notCurrent: this.t.notCurrentValidationMessage };
    },
  },
  validations() {
    return {
      formModel: {
        newEmail: {
          required: customRules.requiredWS,
          email: customRules.rules.email,
          notCurrent: (value) => {
            if (!value) return true;
            return value.toUpperCase() !== this.$g.userInfo.email.toUpperCase();
          },
        },
      },
    };
  },
  components: {},
};
