import services from 'src/services';
import * as customRules from 'src/utils/custom-validation-rules.ts';


export default {
  data() {
    return {
      formModel: {
        currentPassword: null,
        newPassword: null,
        confirmPassword: null,
      },
    };
  },
  methods: {
    apply() {
      return services.accountService.changePassword(this.formModel).then(() => {
        this.showServiceSuccess(this.t.successChangedPassword);
      }, this.showServiceError);
    },
  },
  computed: {
    t() {
      return this.$t.accountSettings.changePassword;
    },
  },
  validations: {
    formModel: {
      currentPassword: {
        required: customRules.requiredWS,
      },
      newPassword: {
        required: customRules.requiredWS,
      },
      confirmPassword: {
        required: customRules.requiredWS,
        sameAs: customRules.rules.sameAs('newPassword'),
      },
    },
  },
  components: {},
};
