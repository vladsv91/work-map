import ChangeEmail from './change-email/change-email.vue';
import ChangePassword from './change-password/change-password.vue';


export default {
  data() {
    return {};
  },
  methods: {},
  created() {},
  computed: {
    t() {
      return this.$t.accountSettings;
    },
  },
  validations: {},
  components: {
    ChangeEmail,
    ChangePassword,
  },
};
