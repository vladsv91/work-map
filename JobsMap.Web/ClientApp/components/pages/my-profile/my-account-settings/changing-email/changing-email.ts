import services from 'src/services';

const confirmationStatuses = {
  loading: 0,
  success: 1,
  error: 2,
};

export default {
  data() {
    return {
      confirmationStatuses,
      confirmationStatus: confirmationStatuses.loading,
    };
  },
  methods: {},
  computed: {
    t() {
      return this.$t.accountSettings.changingEmail;
    },
  },
  components: {},
  created() {
    const { newEmail, token } = this.$route.query;
    if (!newEmail || !token) {
      this.$router.replace({ name: 'Main' });
      return;
    }
    services.accountService.changeEmail({ newEmail, token: token.replace(/ /ig, '+') }).then(() => {
      this.confirmationStatus = confirmationStatuses.success;
    }, error => {
      this.confirmationStatus = confirmationStatuses.error;
      this.showServiceError(error);
    });
  },
};
