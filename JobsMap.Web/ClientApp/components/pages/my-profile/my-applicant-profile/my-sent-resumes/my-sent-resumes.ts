import CrudSectionPage from 'src/mixins/crud-section-page.ts';

export default {
  mixins: [CrudSectionPage],
  data() { return { items: this.$g.myApplicantSentResumes || [] }; },
  methods: {
    getItemsService() { return this.$store.dispatch('RetrieveApplicantSentResumes'); },
  },
};
