import services from 'src/services';
import { lightAddressValidationRules } from 'src/utils/address-utils.ts';
import * as customRules from 'src/utils/custom-validation-rules.ts';
import { EditItemMixin } from 'src/mixins/page-details';

export default {
  mixins: [EditItemMixin],
  methods: {
    getItemData: services.myApplicantService.getPreferredAddressDetails,
    saveAddingItem(formModel) {
      return this.$store.dispatch('AddApplicantPreferredAddress', formModel);
    },
    saveUpdatingItem(formModel) {
      return this.$store.dispatch('UpdateApplicantPreferredAddress', formModel);
    },
    onSaveAddingItemSuccess() {
      this.$router.push({ name: 'MyPreferredAddresses' });
    },
    getInitialFormModel() {
      return {
        id: null,
        address: null,
        radiusInKilometers: 1.5,
      };
    },
  },
  computed: {
    t() {
      return this.$t.applicant.preferredAddresses.edit;
    },
  },
  components: {},
  validations() {
    return {
      formModel: {
        radiusInKilometers: {
          required: customRules.requiredWS,
          minValueEx: customRules.minValueEx(0),
          maxValue: customRules.rules.maxValue(this.$consts.validations.maxRadiusInKm),
        },

        address: lightAddressValidationRules,
      },
    };
  },
};
