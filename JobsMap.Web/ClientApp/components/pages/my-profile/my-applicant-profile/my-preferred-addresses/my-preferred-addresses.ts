import services from 'src/services';
import { getInitialAddress } from 'src/utils/address-utils.ts';
import CrudSectionPage from 'src/mixins/crud-section-page.ts';
import AddressesMap from './addresses-map/addresses-map.vue';
// import { deepClone } from './addresses-map/addresses-map.vue';

export default {
  mixins: [CrudSectionPage],
  data() {
    return {
      items: this.$g.myApplicantPreferredAddresses || [],
    };
  },
  methods: {
    deleteItemService: services.myApplicantService.deletePreferredAddress,
    getItemsService() { return this.$store.dispatch('RetrieveApplicantPreferredAddresses'); },
    onAddMarker(ltLg) {
      const data = {
        id: null,
        radiusInKilometers: 1,
        address: Object.assign(getInitialAddress(), {
          latitude: ltLg.lat(),
          longitude: ltLg.lng(),
        }),
      };
      this.$store.dispatch('AddApplicantPreferredAddress', data).then(() => {

      }, this.showServiceError);
    },
  },
  created() {},
  computed: {},
  components: {
    AddressesMap,
  },
};
