import HasMapMixin from 'src/mixins/has-map-mixin';

export default {
  mixins: [HasMapMixin],
  data() {
    return {};
  },
  methods: {
    getAddressesList() {
      return this.preferredAddresses;
    },

    onMapClick(event) {
      this.addMarker(event.latLng);
    },

    addMarker(latLng) {
      this.$emit('add-marker', latLng);
    },

    createMarkerData(address, addressObj, map, mapMarkers) {
      const mapkerPosition = { lat: address.latitude, lng: address.longitude };
      const marker = new google.maps.Marker({
        position: mapkerPosition,
        map: map,
        label: (mapMarkers.length + 1).toString(),
      });
      const circle = new google.maps.Circle({
        center: mapkerPosition,
        map: map,
        radius: addressObj.radiusInKilometers * 1000,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35,
      });

      return {
        marker,
        circle,
      };
    },

  },
  computed: {
    t() {
      return this.$t.applicant.preferredAddresses.map;
    },
  },
  watch: {
    preferredAddresses() {
      this.setMarkers();
    },
  },
  created() {
    // this.mapOptions = this.getDefaultMapOptions();
    this.$on('on-inithialized-map', () => {
      this.map.addListener('click', this.onMapClick);
    });
  },
  props: {
    preferredAddresses: {},
  },
};
