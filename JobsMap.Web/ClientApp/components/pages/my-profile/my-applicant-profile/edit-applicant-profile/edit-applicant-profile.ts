import rules, * as customRules from 'src/utils/custom-validation-rules.ts';

export default {
  data() {
    return {
      hasPhotoUriChanged: false,
      formModel: {
        fullName: null,
        photoUri: null,
        dateOfBirth: null,
        contactEmail: null,
        contactPhone: null,
        websiteUrl: null,
        description: null,
      },
      applicantPhotoFile: null,
    };
  },
  methods: {
    save() {
      this.$refs.submitBtn.setPromise(this.$store.dispatch('UpdateApplicantMainInfo', this.formModel).catch(this.showServiceError));
    },

    onFileChanged(file) {
      this.applicantPhotoFile = file;
      if (!file) return;

      this.hasPhotoUriChanged = true;
      const reader = new FileReader();
      reader.onloadend = () => this.formModel.photoUri = reader.result;
      reader.readAsDataURL(file);
    },

    saveApplicantPhoto() {
      this.$refs.saveApplicantPhotoBtn.setPromise(this.$store.dispatch('UploadApplicantPhoto', this.applicantPhotoFile).then((response) => {
        this.formModel.photoUri = response.data;
        this.hasPhotoUriChanged = false;
      }, this.showServiceError));
    },
  },
  created() {
    const { myApplicantMainInfo } = this.$g;
    if (myApplicantMainInfo) {
      this.formModel = Object.assign({}, this.formModel, myApplicantMainInfo);
    } else {
      this.$store.dispatch('RetrieveApplicantMainInfo').then(data => {
        this.formModel = Object.assign({}, this.formModel, data);
      }, this.showServiceError);
    }
  },
  computed: {
    t() {
      return this.$t.editApplicantProfile;
    },
  },
  validations() {
    const maxYears = new Date();
    maxYears.setHours(0, 0, 0, 0);
    maxYears.setFullYear(new Date().getFullYear() - 120);

    const minYears = new Date();
    minYears.setHours(0, 0, 0, 0);
    minYears.setFullYear(new Date().getFullYear() - 14);

    return {

      formModel: {
        fullName: {
          required: customRules.requiredWS,
          minLength: rules.minLength(3),
          maxLength: rules.maxLength(100),
        },
        dateOfBirth: {
          minValue: rules.minValue(maxYears), // 120 years old
          maxValue: rules.maxValue(minYears), // 14 years old
        },
        websiteUrl: {
          url: rules.url,
          minLength: rules.minLength(10),
          maxLength: rules.maxLength(200),
        },
        contactPhone: {
          minLength: rules.minLength(5),
          maxLength: rules.maxLength(20),
        },
        contactEmail: {
          email: rules.email,
          minLength: rules.minLength(6),
          maxLength: rules.maxLength(100),
        },
        description: {
          maxLength: rules.maxLength(1000),
        },
      },
    };
  },
  components: {},
};
