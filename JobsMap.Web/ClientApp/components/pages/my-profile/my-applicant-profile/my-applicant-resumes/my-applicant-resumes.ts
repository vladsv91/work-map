import services from 'src/services';
import CrudSectionPage from 'src/mixins/crud-section-page.ts';

export default {
  mixins: [CrudSectionPage],
  data() {
    return {
      items: this.$g.myApplicantResumes || [],
    };
  },
  methods: {
    deactivateItemService: services.myApplicantService.deactivateResume,
    deleteItemService: services.myApplicantService.deleteResume,
    activateItemService: services.myApplicantService.activateResume,
    getItemsService() { return this.$store.dispatch('RetrieveApplicantResumes'); },

    openUploadFileForResume(resume, index) {
      this.uploadFileForResume = { resume, index };
      const { uploadResumeFileInput } = this.$refs;
      uploadResumeFileInput.value = null;
      uploadResumeFileInput.click();
    },

    onFileChanged(file) {
      if (!file) return;
      const promise = services.myApplicantService.uploadResumeFile(file, this.uploadFileForResume.resume.id).then((response) => {
        this.showServiceSuccess(response);
        this.$set(this.uploadFileForResume.resume, 'resumeFileUri', response.data);
      }, this.showServiceError);
      this.$refs[`uploadResumeFile${this.uploadFileForResume.index}`][0].setPromise(promise);
    },

    deleteResumeFile(resume, index) {
      const promise = services.myApplicantService.deleteResumeFile(resume.id).then(() => {
        resume.resumeFileUri = null;
      }, this.showServiceError);
      this.$refs[`deleteResumeFile${index}`][0].setPromise(promise);
    },
  },
  created() {},
  computed: {},
  components: {},
};
