import services from 'src/services';
import rules, * as customRules from 'src/utils/custom-validation-rules.ts';
import { EditItemMixin } from 'src/mixins/page-details';

export default {
  mixins: [EditItemMixin],
  methods: {
    getItemData: services.myApplicantService.getResumeDetails,
    saveAddingItem(formModel) {
      return this.$store.dispatch('AddApplicantResume', formModel);
    },
    saveUpdatingItem(formModel) {
      return this.$store.dispatch('UpdateApplicantResume', formModel);
    },
    onSaveAddingItemSuccess() {
      this.$router.push({ name: 'MyResumes' });
    },

    getInitialFormModel() {
      return {
        name: null,
        id: null,
        isDisabled: false,

        profession: null,
        experienceYears: null,
        skills: null,
        description: null,
        salaryPerMonth: null,
        currency: null,
        // resumeFileUri: null,
        workExperiences: [],
      };
    },

  },
  computed: {
    t() {
      return this.$t.resumes.edit;
    },
  },
  components: {},
  validations() {
    return {
      formModel: {
        name: {
          required: customRules.requiredWS,
          minLength: rules.minLength(2),
          maxLength: rules.maxLength(50),
        },

        experienceYears: {
          required: customRules.requiredWS,
          minValue: rules.minValue(0),
          maxValue: rules.maxValue(100),
        },
        skills: {
          required: customRules.requiredWS,
          maxLength: rules.maxLength(2000),
        },
        description: {
          required: customRules.requiredWS,
          maxLength: rules.maxLength(2000),
        },

        salaryPerMonth: {
          minValue: rules.minValue(0),
          maxValue: rules.maxValue(this.$consts.validations.maxSalary),
        },
        currency: {
          required: customRules.requiredIfWS((model) => model.salaryPerMonth != null),
        },
        profession: { required: customRules.requiredWS },
      },
    };
  },
};
