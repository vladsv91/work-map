// import store from 'src/vuex';
import MyProfilePage from 'components/pages/my-profile/my-profile-page/my-profile-page.vue';


export default {
  data() {
    return {};
  },
  methods: {

  },
  computed: {
    tabs() {
      return this.$g.isCompany ? [
        //
        {
          caption: this.$t.myProfile.menuItems.myCompanyInfo,
          to: { name: 'MyCompanyInfo' },

        },
        {
          caption: this.$t.myProfile.menuItems.myCompanyPhotos,
          to: { name: 'MyCompanyPhotos' },
        },
        {
          caption: this.$t.myProfile.menuItems.myCompanyFilials,
          to: { name: 'MyCompanyFilials' },
        },
        {
          caption: this.$t.myProfile.menuItems.myCompanyVacancies,
          to: { name: 'MyCompanyVacancies' },
        },
        {
          caption: this.$t.myProfile.menuItems.myCompanyReceivedResumes,
          to: { name: 'MyReceivedResumes' },
        },
        {
          caption: this.$t.myProfile.menuItems.accountSettings,
          to: { name: 'AccountSettings' },
        },
      ] : this.$g.isApplicant ? [
        //
        {
          caption: this.$t.myProfile.menuItems.myApplicant.info,
          to: { name: 'MyApplicantInfo' },
        },
        {
          caption: this.$t.myProfile.menuItems.myApplicant.myResumes,
          to: { name: 'MyResumes' },
        },
        {
          caption: this.$t.myProfile.menuItems.myApplicant.myPreferredAddresses,
          to: { name: 'MyPreferredAddresses' },
        },
        {
          caption: this.$t.myProfile.menuItems.myApplicant.mySentResumes,
          to: { name: 'MyApplicantSentResumes' },
        },
        {
          caption: this.$t.myProfile.menuItems.accountSettings,
          to: { name: 'AccountSettings' },
        },
      ] : [];
    },
    t() {
      return this.$t.myProfile;
    },
  },
  created() {},
  components: {
    MyProfilePage,
  },
};
