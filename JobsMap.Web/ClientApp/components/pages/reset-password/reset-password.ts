import services from 'src/services';
import rules, * as customRules from 'src/utils/custom-validation-rules.ts';


// todo, localize and design

export default {
  data() {
    return {
      formModel: {
        newPassword: '',
        confirmPassword: '',
      },
      step: 0,
    };
  },
  methods: {
    apply() {
      const promise = services.accountService.resetPassword(this.getServiceParams()).then(() => {
        this.step = 1;
      }, this.showServiceError);
      this.$refs.submitBtn.setPromise(promise);
    },
    getServiceParams() {
      const { userId, token } = this.$route.query;
      return {
        newPassword: this.formModel.newPassword,
        userId,
        token: token.replace(/ /ig, '+'),
      };
    },
    openLoginModal() {
      this.$bus.$emit('open-login-modal');
    },
  },
  computed: {
    t() {
      return this.$t.resetPassword;
    },
    t2() {
      return this.$t.accountSettings.changePassword;
    },
  },
  components: {},
  created() {
    const { userId, token } = this.$route.query;
    if (!userId || !token) {
      this.$router.replace({ name: 'Main' });
    }
  },
  validations: {
    formModel: {
      newPassword: {
        required: customRules.requiredWS,
        minLength: rules.minLength(6),
        maxLength: rules.maxLength(30),
      },

      confirmPassword: {
        required: customRules.requiredWS,
        sameAs: rules.sameAs('newPassword'),
      },
    },
  },
};
