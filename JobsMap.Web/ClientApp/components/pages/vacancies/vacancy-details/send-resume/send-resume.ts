import services from 'src/services';
import * as customRules from 'src/utils/custom-validation-rules.ts';


export default {
  data() {
    return {
      formModel: {
        resume: null,
        message: null,
      },
    };
  },
  methods: {
    apply() {
      const serviceParams = this.$getServiceParams(this.formModel);
      serviceParams.vacancyId = this.vacancyId;
      const promise = services.myApplicantService.sendResume(serviceParams).then((response) => {
        this.showServiceSuccess(response);
        this.$emit('sent');
      }, this.showServiceError);
      this.$refs.submitBtn.setPromise(promise);
    },
  },
  computed: {
    t() {
      return this.$t.vacancies.details;
    },
  },
  props: {
    vacancyId: {
      type: String,
      required: true,
    },
  },
  validations: {
    formModel: {
      resume: {
        required: customRules.requiredWS,
      },
    },
  },
};
