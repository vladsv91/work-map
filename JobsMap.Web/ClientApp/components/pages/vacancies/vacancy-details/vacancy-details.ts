import { Component, Mixins } from 'vue-property-decorator';

import services from 'src/services';
import { PageDetailsMixin } from 'src/mixins/page-details.mixin';
import SendResume from './send-resume/send-resume.vue';
import { VacancyDetailsModel } from 'src/shared/vacancy.model';


export class VacancyDetailsMixin extends PageDetailsMixin<VacancyDetailsModel> { }

@Component({
  components: { SendResume },
})
export default class VacancyDetailsComponent extends Mixins(VacancyDetailsMixin) {
  public isOpenedSendingResume = false;
  public hasSentResume = false;

  public getItemData(itemId: string) {
    return services.vacanciesService.getDetails(itemId);
  }

  public onClickSendResume() {
    if (!this.$g.isAuthenticated || !this.$g.isApplicant) {
      this.showServiceError(this.$tg.sendResumeErrorAuth as any);
      return;
    }
    this.isOpenedSendingResume = true;
  }
  public onSentResume() {
    this.hasSentResume = true;
    this.isOpenedSendingResume = false;
  }

  // get t() {
  //   return this.$t.vacancies.details;
  // }
}
