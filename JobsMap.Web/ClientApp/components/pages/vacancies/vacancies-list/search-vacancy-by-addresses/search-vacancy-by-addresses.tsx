import SearchByAddressesMixin from 'src/components/search-by-addresses/search-by-addresses-mixin.tsx';
import Utils from 'src/utils/utils';
import { Constants } from 'src/utils/constants';

export const infoWindowComponent = {
  render(h) {
    const nearest = this.nearestVacancies;
    return (<div>
      {(nearest.length ? <div class="com-info-on-map-label">{this.t.currentMarkerVacacny}:</div> : null)}
      {this.getVacancyInfoHtml(h, this.vacancy, null)}
      {this.getNearestVacanciesHtml(h)}
    </div>);
  },
  methods: {
    getNearestVacanciesHtml(h) {
      const nearest = this.nearestVacancies;
      if (nearest.length === 0) { return; }
      return <div class="m-t-10">
        <div class="com-info-on-map-label">{this.t.nearestVacanciesAndFilials}:</div>
        {nearest.map((_, index) => this.getVacancyInfoHtml(h, _.vacancy, index + 1))}
      </div>;
    },

    getVacancyInfoHtml(h, vacancy, index) {
      return <div class="m-b-10">
        {index ? <span>{index}.</span> : null}
        <router-link attrs={{ to: { name: 'CompanyDetails', params: { companyId: vacancy.company.id } } }}>
          <span class="grey-color">{this.$tg.company}: </span>
          {vacancy.company.name}
          <div>
            <span class="grey-color">{this.$tg.filial}: </span>
            {vacancy.companyFilial.name}
          </div>
        </router-link>
        <div>
          <span class="grey-color">{this.$tg.address}: </span>
          {vacancy.companyFilial.address.formattedAddress}
        </div>

        <router-link attrs={{ to: { name: 'VacancyDetails', params: { vacancyId: vacancy.id } } }}>
          <span class="grey-color">{this.$tg.vacancy}: </span>
          {vacancy.name}
        </router-link>

      </div >;
    },

    getNearestVacancies() {
      const currentFilial = this.vacancy.companyFilial;
      const currFilId = currentFilial.id;
      const similarAddreses = [];
      this.allVacancies.forEach(vacancy => {
        const filial = vacancy.companyFilial;
        if (filial.id === currFilId) { return; }
        if (Utils.distanceInKilometers(filial.address, currentFilial.address) < Constants.nearAddressInKm) { // 50 meters
          similarAddreses.push({ vacancy, filial });
        }
      });
      return similarAddreses;
    },
  },
  computed: {
    nearestVacancies() { return this.getNearestVacancies(); },
    t() { return this.$t.searchVacanciesByAddresses; },
  },
  props: {
    vacancy: { type: Object },
    allVacancies: { type: Array },
  },
};

export default {
  mixins: [SearchByAddressesMixin],
  data() {
    return {
      // mainText: 'Click on map to pick the vacancies in that region.',
    };
  },
  watch: {
    vacancies() {
      this.setMarkers();
    },
  },
  methods: {
    restAddresses() {
      const { vacancies } = this;
      if (!vacancies || !vacancies.length) { return []; }

      return vacancies.map(vacancy => ({
        ...vacancy,
        address: vacancy.companyFilial.address,
        contentComponent: infoWindowComponent,
        contentProps: { vacancy, allVacancies: vacancies },
      }));
    },
  },
  components: {},
  created() { },
  props: {
    vacancies: {
      type: Array,
    },
  },
};
