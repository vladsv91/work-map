import services from 'src/services';
// import * as customRules from 'src/utils/custom-validation-rules.ts';

import Collapsible from 'components/collapsible/collapsible.vue';
import FilterablePageableMixin from 'src/mixins/filterable-pageable.ts';
import SearchByAddresses from './search-vacancy-by-addresses/search-vacancy-by-addresses.vue';


export default {
  mixins: [FilterablePageableMixin],
  data() {
    return {
      entityName: 'Vacancy',
      formModel: {
        profession: null,
        employmentType: null,
        currency: null,
        lessThanSalaryPerMonth: null,
        moreThanSalaryPerMonth: null,

        address: null,
      },
      // collapsedAddresses: true,
    };
  },
  methods: {
    getItemsByFilter: services.vacanciesService.getByFilter,

    getFormModelParams() {
      const res = Object.assign({}, this.formModel);
      this.setIdProperty(res, 'profession');
      this.setIdProperty(res, 'currency');
      this.setIdProperty(res, 'employmentType');
      return res;
    },
  },
  computed: {
    // t() {
    //   return this.$t.vacancies;
    // },
    mtsMax() {
      const { lessThanSalaryPerMonth } = this.formModel;
      if (lessThanSalaryPerMonth) {
        return lessThanSalaryPerMonth > 0 ? lessThanSalaryPerMonth : 1;
      }
      return this.$consts.validations.maxSalary;
    },
    ltsMin() {
      const { moreThanSalaryPerMonth } = this.formModel;
      if (moreThanSalaryPerMonth) {
        const { maxSalary } = this.$consts.validations;
        return moreThanSalaryPerMonth <= maxSalary ? moreThanSalaryPerMonth : maxSalary;
      }
      return 0;
    },
  },
  components: {
    Collapsible,
    SearchByAddresses,
  },
};
