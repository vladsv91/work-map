export default {
  data() {
    return {};
  },
  methods: {

  },
  created() {},

  computed: {
    content() {
      const values = this.name.split(' ');
      if (values.length > 1) {
        return values[0][0] + values[1][0];
      }
      return values[0].substr(0, 2);
    },
    imageFallbackStyles() {
      return {
        width: `${this.width}px`,
        height: `${this.height}px`,
      };
    },
  },
  props: {
    imgSrc: {},
    name: {},
    width: {
      default: 100,
    },
    height: {
      default: 100,
    },
  },
};
