import Modal from 'src/components/modal/modal.vue';
import './confirmation-dialog.scss';
import { Component, Prop, Vue } from 'vue-property-decorator';
import { VNode, CreateElement } from 'vue';


@Component({
  components: {
    Modal,
  }
})
export default class ConfirmationDialogComponent extends Vue implements ConfirmationDialogModel {
  message = '';
  yes: (() => void) | null = null;
  no: (() => void) | null = null;
  yesButtonCaption = '';
  caption = '';


  get yesButtonCaptionComputed(): any {
    return this.yesButtonCaption || this.$tg.yes;
  }
  get captionComputed(): any {
    return this.caption || this.$tg.confirmation;
  }
  get messageComputed(): any {
    return this.message || this.$tg.areYouSure;
  }

  resetData(): void {
    Object.assign(this.$data, getData());
  }
  show(data: ConfirmationDialogModel): void {
    Object.assign(this.$data, getData(), data);
  }
  yesClick(): void {
    if (typeof this.yes === 'function') {
      this.yes();
    }
  }
  noClick(): void {
    if (typeof this.no === 'function') {
      this.no();
    }
  }

  created(): void {
    Object.assign(this, getData());
  }

  render(h: CreateElement): VNode {
    return <modal on={{ close: this.noClick, ok: this.yesClick }}
      attrs={{
        okCaption: this.yesButtonCaptionComputed,
        cancelCaption: this.$t.general.cancel,
      }}
      class="confirmation-dialog"
    >
      <h3 slot="header">
        {this.captionComputed}
      </h3>
      <div slot="body">
        {this.messageComputed}
      </div>
    </modal>;
  }
};

export interface ConfirmationDialogModel {
  message?: string;
  yes: (() => void) | null | undefined;
  no: (() => void) | null | undefined;
  yesButtonCaption?: string;
  caption?: string;
}

function getData(): ConfirmationDialogModel {
  return {
    message: '',
    yes: null,
    no: null,
    yesButtonCaption: '',
    caption: '',
  };
}
