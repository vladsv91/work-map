import * as customRules from 'src/utils/custom-validation-rules.ts';


export default {
  data() {
    return {
      formModel: {
        email: null,
        password: null,
      },
    };
  },
  methods: {
    validateAndApply() {
      if (this.isInvalid) return;
      return this.$store.dispatch('Login', this.formModel).then(() => {
        this.$emit('close');
      }, (error) => {
        this.showServiceError(error);
        // const emailNeed = safe(error, 'response.data.NeedEmailConfirmation');
        // if (emailNeed) {
        //   this.$emit('need-email-confirmation');
        // }
      });
    },
  },
  created() {},
  computed: {},
  components: {},
  validations: {
    formModel: {
      email: {
        required: customRules.requiredWS,
        email: customRules.rules.email,
      },
      password: {
        required: customRules.requiredWS,
      },
    },
  },
};
