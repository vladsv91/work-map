import * as customRules from 'src/utils/custom-validation-rules.ts';
import { Constants } from 'src/utils/constants.ts';
import services from 'src/services';

export default {
  data() {
    return {
      formModel: {
        email: null,
      },
      step: 0,
    };
  },
  methods: {
    validateAndApply() {
      if (this.step === 1) {
        this.$emit('close');
        return;
      }
      if (this.isInvalid) { return; }
      return services.accountService.sendPasswordResettingEmail(this.formModel).then(() => {
        this.step = 1;
      }, this.showServiceError);
    },
  },
  created() { },
  computed: {
    t() {
      // todo, localize
      return this.$t.signIn.forgotPassword;
    },
  },
  components: {},
  validations: {
    formModel: {
      email: {
        required: customRules.requiredWS,
        email: customRules.rules.email,
        maxLength: customRules.rules.maxLength(Constants.validations.emailMaxLen),
      },
    },
  },
};
