import ModalMixin from 'src/mixins/modal-mixin';


import ForgotPassword from 'src/components/layouts/header/login-modal/forgot-password/forgot-password.vue';
import Login from 'src/components/layouts/header/login-modal/login/login.vue';


import mixins from 'vue-typed-mixins';
import Vue from 'vue';

export default mixins(ModalMixin).extend({
  data() {
    return {
      isOpenForgotPasssword: false,
    };
  },
  methods: {
    apply() {
      const promise = (this.$refs.submitForm as Vue).validateAndApply();
      if (promise) {
        this.loadingPromise = promise;
      }
    },
    forgotPasssword() {
      this.isOpenForgotPasssword = true;
    },
    onResendClick() {
      this.$store.commit('isOpenedModal', false);
      this.close();
      this.$router.push({ name: 'ConfirmEmail', query: { step: 'resending' } });
    },
  },
  created() { },
  computed: {
    t(): any {
      return this.$t.signIn;
    },
    header(): string {
      return this.isOpenForgotPasssword ? this.t.forgotPasswordHeader : this.t.loginHeader;
    },
    okCaption(): string {
      return this.isOpenForgotPasssword ? this.$tg.ok : this.t.loginHeader;
    },
  },
  components: {
    ForgotPassword,
    Login,
  },
});
