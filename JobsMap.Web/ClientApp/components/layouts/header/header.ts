// import Vue from 'vue';
import services from 'src/services';
import ContactUsModal from './contact-us-modal/contact-us-modal.vue';
import LoginModal from './login-modal/login-modal.vue';


// Vue.component('ContactUsModal', ContactUsModal);


export default {
  data() {
    return {
      isDrawerDisplay: this.isShowingDrawer,
    };
  },
  methods: {
    onSelectLanguage(lang) {
      if (!lang) return;
      this.$store.commit('SetLanguage', lang);
      if (this.isAuthenticated) {
        services.accountService.setLanguage(lang.id);
        const { userInfo } = this;
        if (userInfo) {
          userInfo.language = lang.id;
        }
      }
      this.$showSuccessSnackbar(this.$t.header.changedLanguageMessage);
    },
    toggleDrawerVisibility() {
      this.isShowingDrawer = !this.isShowingDrawer;
    },
    openModalForLogin() {
      this.$mountModal((LoginModal as any).options);
    },
    openModalForContactUs() {
      this.$mountModal((ContactUsModal as any).options);
    },
    logout() {
      this.$store.dispatch('Logout');
    },
    setDrawerVisibility(value) {
      this.$store.commit('SetDrawerVisibility', value);
    },

    closeDrawer() {
      this.isShowingDrawer = false;
    },
  },
  computed: {
    t() {
      return this.$t.header;
    },
    isShowingDrawer: {
      get() {
        return this.$store.state.layout.isShowingDrawer;
      },
      set(value) {
        setTimeout(() => this.isDrawerDisplay = value, 200);
        this.setDrawerVisibility(value);
      },
    },
  },
};
