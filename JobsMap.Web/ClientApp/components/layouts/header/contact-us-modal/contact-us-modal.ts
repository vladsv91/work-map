import services from '../../../../services';
import rules, * as customRules from '../../../../utils/custom-validation-rules.ts';
import ModalMixin from '../../../../mixins/modal-mixin';
import mixins from 'vue-typed-mixins'


const ContactUsModal = mixins(ModalMixin).extend({
  data() {
    return {
      formModel: {
        email: undefined,
        fullName: undefined,
        phoneNumber: undefined,
        message: undefined,
      },
    };
  },
  methods: {
    apply() {
      return services.contactUsService.create(this.formModel).then(() => {
        this.showServiceSuccess(this.t.successMessage);
        this.close();
      }, this.showServiceError);
    },
  },
  computed: {
    t(): any {
      return this.$t.contactUs;
    },
  },
});

(ContactUsModal as any).options.validations = function () {
  return {
    formModel: {
      email: {
        required: customRules.requiredWS,
        email: rules.email,
        maxLength: customRules.rules.maxLength(this.$consts.validations.emailMaxLen),
      },
      fullName: {
        maxLength: customRules.rules.maxLength(50),
      },
      phoneNumber: {
        // todo, validation phoe number
        maxLength: customRules.rules.maxLength(this.$consts.validations.phoneMaxLen),
      },
      message: {
        maxLength: customRules.rules.maxLength(1000),
      },
    },
  };
}

export default ContactUsModal;
