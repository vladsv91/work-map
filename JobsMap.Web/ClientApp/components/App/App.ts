import Vue from 'vue';
import AppHeader from 'components/layouts/header/header.vue';
import SnackbarManager from './snackbar-manager/snackbar-manager.vue';
import ConfirmationDialogManager from './confirmation-dialog-manager/confirmation-dialog-manager.vue';

export default {
  name: 'App',
  data() {
    return {
      requestsCount: 0,
      mountedComponent: getInitialMountedComponentData(),
    };
  },
  methods: {
    onBeginApiRequest() {
      this.requestsCount++;
    },
    onEndApiRequest() {
      const a = --this.requestsCount;
      if (a < 0) {
        this.requestsCount = 0;
      }
    },
  },
  created() {
    const VuePrototype = Vue.prototype;
    VuePrototype.$showSuccess = (...args) => { this.$refs.snackbarManager.showSuccessMessage(...args); };
    VuePrototype.$showError = (...args) => { this.$refs.snackbarManager.showErrorMessage(...args); };
    VuePrototype.$showConfirmationDialog = (...args) => {
      return this.$refs.confirmationDialogManager.showConfirmationDialog(...args);
    };

    const unmountComponent = () => {
      this.mountedComponent = getInitialMountedComponentData();
    };

    VuePrototype.$mountComponent = (options) => {
      if (!options.component) {
        options = { component: options };
      }
      this.mountedComponent = Object.assign(getInitialMountedComponentData(), options);
      return unmountComponent;
    };
    VuePrototype.$unmountComponent = unmountComponent;


    VuePrototype.$mountModal = (options) => {
      if (!options.component) { options = { component: options }; }
      if (!options.on) { options.on = {}; }
      if (options.on.close) {
        const originalClose = options.on.close;
        options.on.close = (...args: any[]) => {
          originalClose.apply(this, ...args);
          unmountComponent();
        };
      } else {
        options.on.close = unmountComponent;
      }
      return this.$mountComponent(options);
    };

    this.$busOn('begin-api-request', this.onBeginApiRequest);
    this.$busOn('end-api-request', this.onEndApiRequest);

    this.$busOn('cleared-tokens', () => {
      this.$commit('Logout');
    });

    this.$busOn('created-tokens', () => {
      this.$commit('Login');
    });


    if (this.isAuthenticated) {
      this.$store.dispatch('RetrieveUserInfoOrFromCache');
    }
  },
  components: {
    SnackbarManager,
    ConfirmationDialogManager,
    AppHeader,
  },
};


function getInitialMountedComponentData() {
  return {
    component: null,
    props: null,
    on: null,
  };
}
