export default {
  data() {
    return {
      snackbars: [],
    };
  },
  methods: {
    showErrorMessage(options) {
      this.showMessage(options, 2);
    },
    showSuccessMessage(options) {
      this.showMessage(options, 1);
    },
    showMessage(options, type) {
      if (typeof options === 'string') {
        options = {
          message: options,
          // message: Math.random().toString(),
          // temp
          // duration: null,
        };
      }
      if ('duration' in options && !options.duration) {
        options.duration = Infinity;
      }

      const snackbarProps = Object.assign(this.getInitialSnackbarProps(), options);
      snackbarProps.type = type;

      this.snackbars.push(snackbarProps);
      if (snackbarProps.duration !== Infinity) {
        setTimeout(() => {
          const index = this.snackbars.indexOf(snackbarProps); // ref search
          if (index !== -1)
            this.closeSnackbar(this.snackbars[index], index);
        }, snackbarProps.duration);
      }
    },

    closeSnackbar(snackbarProps, index) {
      this.snackbars.splice(index, 1);
    },
    getSnackbarStyle(index) {
      if (index === 0) return null;
      const prevSnackbar = this.$refs.snackbars[index - 1].$el;

      const rect = prevSnackbar.getBoundingClientRect();

      const padd = 7;

      let bottom = window.innerHeight - rect.top + padd;

      // animation maybe not end
      const minBottom = index * (48 + padd) + 24;
      if (bottom < minBottom) bottom = minBottom;

      return { bottom: `${bottom}px` };
    },
    getInitialSnackbarProps() {
      return {
        position: 'left',
        duration: 5000,
        message: '',
        type: 1,
      };
    },
  },
  created() { },
  mounted() { },
  destroyed() { },
  components: {},
};
