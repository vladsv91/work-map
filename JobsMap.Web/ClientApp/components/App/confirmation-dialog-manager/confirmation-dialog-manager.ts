import { Component, Vue } from 'vue-property-decorator';
import ConfirmationDialogComponent, { ConfirmationDialogModel } from 'src/components/confirmation-dialog/confirmation-dialog.tsx';

@Component({
  components: {
    ConfirmationDialog: ConfirmationDialogComponent,
  }
})
export default class ConfirmationDialogManagerComponent extends Vue {
  $refs!: {
    confirmationDialog: ConfirmationDialogComponent,
  };

  isShowingConfirmationDialog = false;

  // ---- public --- //
  public showConfirmationDialog(options: ConfirmationDialogModel | string): Promise<void> {
    this.isShowingConfirmationDialog = true;

    return new Promise((resolve, reject) => {
      this.$nextTick(() => {
        const confirmationDialog = this.$refs.confirmationDialog;
        if (!options) {
          options = {} as ConfirmationDialogModel;
        }

        if (typeof options === 'string') {
          options = { message: options } as ConfirmationDialogModel;
        }
        confirmationDialog.show(options);


        confirmationDialog.yes = () => {
          this.isShowingConfirmationDialog = false;
          resolve();
        };
        confirmationDialog.no = () => {
          this.isShowingConfirmationDialog = false;
          reject();
        };
      });
    });
  }
  // ---- public --- //

}
