export default {
  methods: {
    close() {
      this.$emit('close');
    },
    ok() {
      this.$emit('ok');
    },
    onModalMaskClick(event) {
      if (event.target === this.$refs.modalWrapper) {
        this.close();
      }
    },
    onKeyUp(event) {
      if (event.keyCode === 27) {
        this.close();
      }
    },
    onModalBlur() {
      // check IE, and polyfil
      // if (event.relatedTarget) { // || document.activeElement){
      //   if (!event.relatedTarget.closest('.modal-container')) {
      //     this.$refs.modalContainer.focus();
      //     event.preventDefault();
      //     event.stopImmediatePropagation();
      //   }
      // } else {
      //   event.preventDefault();
      //   event.stopImmediatePropagation();
      //   event.stopPropagation();
      //   this.$refs.modalContainer.focus();
      //   setTimeout(() => {
      //     this.$refs.modalContainer.focus();
      //   }, 1);
      // }
    },
  },
  mounted() {
    this.$refs.modalContainer.focus();
    // todo: not woks
    this.addDisposableEventListener(document, 'backbutton', this.close, false);
  },
  created() {
    this.$store.commit('isOpenedModal', true);
    window.addEventListener('keyup', this.onKeyUp);
  },
  destroyed() {
    this.$store.commit('isOpenedModal', false);
    window.removeEventListener('keyup', this.onKeyUp);
  },
  computed: {
    getOkCaption() {
      return this.okCaption || this.$tg.apply;
    },
    getCancelCaption() {
      return this.cancelCaption || this.$tg.cancel;
    },
  },
  props: {
    okCaption: {
      type: String,
    },
    cancelCaption: {
      type: String,
    },
    okButtonClass: {
      type: String,
    },
    header: {
      type: String,
    },
    loadingPromise: {},
  },
};
