const pageSizesOptions = [5, 10, 25, 50];


export default {
  data() {
    return {
      pageSizesOptions,
      internalItemsPerPage: this.itemsPerPage || pageSizesOptions[1],
      internalPageNumber: this.pageNumber || 1,
    };
  },
  methods: {
    onSelectPageSizes(newItemsPerPage) {
      this.internalItemsPerPage = newItemsPerPage;
      const { totalPagesCount } = this;
      if (totalPagesCount < this.internalPageNumber) {
        this.internalPageNumber = totalPagesCount;
      } else if (this.internalPageNumber <= 0) {
        this.internalPageNumber = 0;
      }

      this.emitChanging();
    },
    selectFistPage() {
      this.selectPageNumber(1);
    },
    selectLastPage() {
      this.selectPageNumber(this.totalPagesCount);
    },
    selectPrevPage() {
      this.selectPageNumber(this.internalPageNumber - 1);
    },
    selectNextPage() {
      this.selectPageNumber(this.internalPageNumber + 1);
    },
    selectPageNumber(pageNumber) {
      if (this.internalPageNumber === pageNumber || pageNumber < 1 || pageNumber > this.totalPagesCount) { return; }
      this.internalPageNumber = pageNumber;
      this.emitChanging();
    },
    emitChanging() {
      this.$emit('change-pagination', { page: this.internalPageNumber, itemsPerPage: this.internalItemsPerPage });
    },

    getQuery(page) {
      const newQueryObj = this.getAppendToRouteQuery({
        page: page < 1 ? 1 : (page > this.totalPagesCount ? this.totalPagesCount : page),
      });
      return { query: newQueryObj };
    },
  },
  computed: {
    visiblePageNumbers() {
      const maxBtns = 4;

      const { internalPageNumber, totalPagesCount } = this;
      let startPage = internalPageNumber - (maxBtns / 2);
      if (startPage < 1) {
        startPage = 1;
      }
      let endPage = startPage + maxBtns;
      if (endPage > totalPagesCount) {
        endPage = totalPagesCount;
        if (startPage > 1) {
          startPage = endPage - maxBtns;
          if (startPage < 1) {
            startPage = 1;
          }
        }
      }

      // eslint-disable-next-line prefer-spread
      return Array.apply(null, { length: (endPage - startPage + 1) || 1 } as any).map((a, i) => i + startPage);
    },
    totalPagesCount() {
      return (((this.totalItemsCount - 1) / this.internalItemsPerPage) + 1) ^ 0;
    },
  },
  components: {},
  watch: {
    pageNumber(val) {
      this.internalPageNumber = val;
    },
  },
  props: {
    itemsPerPage: { type: Number },
    totalItemsCount: { type: Number },
    pageNumber: { type: Number, default: 1 },
  },
};


const PaginationWrap = {
  functional: true,
  render(h, vm) {
    return h('Pagination', {
      attrs: {
        totalItemsCount: vm.parent.totalItemsCount,
        pageNumber: vm.parent.paginationOptions.page,
        itemsPerPage: vm.parent.paginationOptions.itemsPerPage,
      },
      on: { 'change-pagination': vm.parent.onChangedPagination },
    });
  },
};

export {
  PaginationWrap,
};
