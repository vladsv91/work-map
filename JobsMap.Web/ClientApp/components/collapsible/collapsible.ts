export default {
  data() {
    return {
      internalCollapsed: this.collapsed,
    };
  },
  methods: {
    onHeaderClick() {
      const isCol = this.internalCollapsed = !this.internalCollapsed;
      this.$emit('update:collapsed', isCol);
    },
  },
  created() {},
  computed: {

  },
  components: {
    // for keep alive
    ContentSlot: {
      render(h) {
        return h('div', {
          staticClass: 'collapsible-content',
        }, this.$slots.default);
      },
    },
  },
  watch: {
    collapsed(val) {
      this.internalCollapsed = val;
    },
  },
  props: {
    collapsed: {
      type: Boolean,
      default: true,
    },
    header: {
      type: String,
    },
  },
};
