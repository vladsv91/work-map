import dateformat from 'dateformat';

export default {
  data() {
    return {
      internalValue: this.value,
    };
  },
  methods: {
    clearValue() {
      this.onDatepickerInput(null);
      this.$refs.mdDatepicker.modelDate = '';
    },
    onDatepickerInput(val: string) {
      this.internalValue = val;
      this.$emit('input', val ? new Date(val) : null);
    },
  },
  watch: {
    value(value) {
      this.internalValue = value ? dateformat(value, 'yyyy-mm-dd') : null;
    },
  },
  props: {
    value: {},
  },
};
