import { getInitialAddress } from 'src/utils/address-utils.ts';


export default {
  data() {
    return {
      address: getInitialAddress(),
    };
  },
  methods: {
    onChangedInputProp(newValue) {
      // if (!newValue || !newValue.formattedAddress) {
      //   this.address = getInitialAddress();
      // } else {
      //   Object.assign(this.address, newValue);
      // }
      this.address = newValue;
    },
    onChangedAddress(address) {
      this.$emit('input', address);
    },
  },
  created() {
    this.onChangedInputProp(this.value);
  },
  computed: {
    formattedAddress() {
      return this.address ? this.address.formattedAddress : '';
    },
    addressCountry() {
      return this.address ? this.address.country : '';
    },
    addressCity() {
      return this.address ? this.address.city : '';
    },
    addressStreet() {
      return this.address ? this.address.street : '';
    },
    addressStreetNumber() {
      return this.address ? this.address.streetNumber : '';
    },
    vsC() {
      return this.vs || {};
    },
    t() {
      return this.$t.addressFields;
    },
  },
  components: {},
  watch: {
    value(newValue) {
      this.onChangedInputProp(newValue);
    },
  },
  props: {
    value: {
      type: Object,
    },
    vs: {
      type: Object,
    },
    disabled: {
      type: Boolean,
      default: false,
    },
  },
};

// function getInitialAddress() {
//   return {
//     formattedAddress: '',
//     description: '',
//     country: '',
//     city: '',
//     street: '',
//     streetNumber: '',
//     latitude: '',
//     longitude: '',
//   };
// }
