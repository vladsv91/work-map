export default {
  data() {
    return {
      pointer: 0,
      visibleElements: this.maxHeight / this.optionHeight,
    };
  },
  props: {
    showPointer: {
      type: Boolean,
      default: true,
    },
    optionHeight: {
      type: Number,
      default: 40,
    },
  },
  computed: {
    pointerPosition() {
      return this.pointer * this.optionHeight;
    },
  },
  watch: {
    filteredOptions() {
      this.pointerAdjust();
    },
  },
  methods: {
    optionHighlight(index, option) {
      const { canShowCheckbox } = this;
      return {
        'multiselect__option--highlight': index === this.pointer && this.showPointer,
        'multiselect__option--selected': !this.autocomplete && this.isSelected(option),
        'multiselect__option--with-checkbox': canShowCheckbox,
        'multiselect__option--without-checkbox': !canShowCheckbox,
        'multiselect__option--cannot-deselect': !this.canDeselect,
      };
    },
    addPointerElement({ key } = 'Enter') {
      /* istanbul ignore else */
      if (this.filteredOptions.length > 0) {
        this.select(this.filteredOptions[this.pointer], key);
      }
      this.pointerReset();
    },
    pointerForward() {
      if (this.pointer < this.filteredOptions.length - 1) {
        this.pointer++;
        if (this.$refs.list.scrollTop <= this.pointerPosition - this.visibleElements * this.optionHeight) {
          this.$refs.list.scrollTop = this.pointerPosition - (this.visibleElements - 1) * this.optionHeight;
        }
        if (this.filteredOptions[this.pointer].$isLabel) {
          this.pointerForward();
        } else {
          this.emitHighlightPointerEvent();
        }
      }
    },
    emitHighlightPointerEvent() {
      this.$emit('highlight-option', this.filteredOptions[this.pointer]);
    },
    pointerBackward() {
      if (this.pointer > 0) {
        this.pointer--;
        if (this.$refs.list.scrollTop >= this.pointerPosition) {
          this.$refs.list.scrollTop = this.pointerPosition;
        }
        if (this.filteredOptions[this.pointer].$isLabel) {
          this.pointerBackward();
        } else {
          this.emitHighlightPointerEvent();
        }
      } else if (this.filteredOptions[0].$isLabel) this.pointerForward();
    },
    pointerReset() {
      if (!this.closeOnSelect) return;
      this.pointer = 0;
      if (this.$refs.list) {
        this.$refs.list.scrollTop = 0;
      }
    },
    pointerAdjust() {
      if (this.pointer >= this.filteredOptions.length - 1) {
        this.pointer = this.filteredOptions.length ?
          this.filteredOptions.length - 1 :
          0;
      }
    },
    pointerSet(index) {
      this.pointer = index;
    },
  },
};
