import deepClone from './utils';
import utils from '../../../utils/utils';

function isEmpty(opt) {
  if (opt === 0) return false;
  if (Array.isArray(opt) && opt.length === 0) return true;
  return !opt;
}

function includes(str, query) {
  if (str === undefined) str = 'undefined';
  if (str === null) str = 'null';
  if (str === false) str = 'false';
  const text = str.toString().toLowerCase();
  return text.indexOf(query.trim()) !== -1;
}

function filterOptions(options, search, label, customLabel) {
  return options.filter(option => includes(customLabel(option, label), search));
}

function stripGroups(options) {
  return options.filter(option => !option.$isLabel);
}

function flattenOptions(values, label) {
  return options =>
    options.reduce((prev, curr) => {
      if (curr[values] && curr[values].length) {
        prev.push({
          $groupLabel: curr[label],
          $isLabel: true,
        });
        return prev.concat(curr[values]);
      }
      return prev;
    }, []);
}

function filterGroups(search, label, values, groupLabel, customLabel) {
  return groups =>
    groups.map((group) => {
      if (!group[values]) {
        console.warn('Options passed to vue-multiselect do not contain groups, despite the config.');
        return [];
      }
      const groupOptions = filterOptions(group[values], search, label, customLabel);

      return groupOptions.length ? {
        [groupLabel]: group[groupLabel],
        [values]: groupOptions,
      } : [];
    });
}

const flow = (...fns) => x => fns.reduce((v, f) => f(v), x);

export default {
  data() {
    return {
      search: '',
      isOpen: false,
      prefferedOpenDirection: 'below',
      optimizedHeight: this.maxHeight,
      internalValue: this.value || this.value === 0 ?
        deepClone(Array.isArray(this.value) ? this.value : [this.value]) : [],
    };
  },
  props: {
    /**
     * Decide whether to filter the results based on search query.
     * Useful for async filtering, where we search through more complex data.
     * @type {Boolean}
     */
    internalSearch: {
      type: Boolean,
      default: true,
    },
    /**
     * Array of available options: Objects, Strings or Integers.
     * If array of objects, visible label will default to option.label.
     * If `labal` prop is passed, label will equal option['label']
     * @type {Array}
     */
    options: {
      type: Array,
      default() {
        return [];
      },
    },
    /**
     * Equivalent to the `multiple` attribute on a `<select>` input.
     * @default false
     * @type {Boolean}
     */
    multiple: {
      type: Boolean,
      default: false,
    },
    /**
     * Presets the selected options value.
     * @type {Object||Array||String||Integer}
     */
    value: {
      type: null,
      default() {
        return [];
      },
    },
    /**
     * Key to compare objects
     * @default 'id'
     * @type {String}
     */
    trackBy: {
      type: String,
    },
    /**
     * Label to look for in option Object
     * @default 'label'
     * @type {String}
     */
    label: {
      type: String,
    },
    /**
     * Enable/disable search in options
     * @default true
     * @type {Boolean}
     */
    searchable: {
      type: Boolean,
      default: true,
    },
    /**
     * Clear the search input after select()
     * @default true
     * @type {Boolean}
     */
    clearOnSelect: {
      type: Boolean,
      default: true,
    },
    /**
     * Hide already selected options
     * @default false
     * @type {Boolean}
     */
    hideSelected: {
      type: Boolean,
      default: false,
    },
    /**
     * Equivalent to the `placeholder` attribute on a `<select>` input.
     * @default 'Select option'
     * @type {String}
     */
    placeholder: {
      type: String,
      default: '',
    },
    /**
     * Allow to remove all selected values
     * @default true
     * @type {Boolean}
     */
    allowEmpty: {
      type: Boolean,
      default: true,
    },
    /**
     * Reset this.internalValue, this.search after this.internalValue changes.
     * Useful if want to create a stateless dropdown.
     * @default false
     * @type {Boolean}
     */
    resetAfter: {
      type: Boolean,
      default: false,
    },
    /**
     * Enable/disable closing after selecting an option
     * @default true
     * @type {Boolean}
     */
    closeOnSelect: {
      type: Boolean,
      default: true,
    },
    /**
     * Function to interpolate the custom label
     * @default false
     * @type {Function}
     */
    customLabel: {
      type: Function,
      default(option, label) {
        if (isEmpty(option)) return '';
        return label ? option[label] : option;
      },
    },
    /**
     * Disable / Enable tagging
     * @default false
     * @type {Boolean}
     */
    taggable: {
      type: Boolean,
      default: false,
    },
    /**
     * String to show when highlighting a potential tag
     * @default 'Press enter to create a tag'
     * @type {String}
     */
    tagPlaceholder: {
      type: String,
      default: 'Press enter to create a tag',
    },
    /**
     * Number of allowed selected options. No limit if false.
     * @default False
     * @type {Number}
     */
    max: {
      type: Number,
    },
    /**
     * Will be passed with all events as second param.
     * Useful for identifying events origin.
     * @default null
     * @type {String|Integer}
     */
    id: {
      default: null,
    },
    /**
     * Limits the options displayed in the dropdown
     * to the first X options.
     * @default 1000
     * @type {Integer}
     */
    optionsLimit: {
      type: Number,
      default: 1000,
    },
    /**
     * Name of the property containing
     * the group values
     * @default 1000
     * @type {String}
     */
    groupValues: {
      type: String,
    },
    /**
     * Name of the property containing
     * the group label
     * @default 1000
     * @type {String}
     */
    groupLabel: {
      type: String,
    },
    /**
     * Array of keyboard keys to block
     * when selecting
     * @default 1000
     * @type {String}
     */
    blockKeys: {
      type: Array,
      default() {
        return [];
      },
    },
    preserveSearch: {
      type: Boolean,
      default: false,
    },
    selectProps: {
      type: String,
    },
    enabledSelectPicker: {
      type: Boolean,
      default: true,
    },
    hideTags: {
      type: Boolean,
      default: false,
    },
    canDeselect: {
      type: Boolean,
      default: true,
    },
    inputLabel: {
      type: String,
    },
    autocomplete: {
      type: Boolean,
    },
  },
  computed: {
    searchEl() {
      return this.$refs.search;
    },
    canShowCheckbox() {
      return this.multiple && !this.hideSelected;
    },
    getPlaceholder() {
      return this.placeholder || this.$t.multiselects.multiselect.placeholder;
    },
    filteredOptions() {
      const search = this.search || '';
      const normalizedSearch = search.toLowerCase();

      let options = this.options.concat();

      options = this.hideSelected ?
        options.filter(this.isNotSelected) :
        options;

      if (this.internalSearch) {
        options = this.groupValues ?
          this.filterAndFlat(options, normalizedSearch, this.label) :
          filterOptions(options, normalizedSearch, this.label, this.customLabel);
      } else {
        options = this.groupValues ? flattenOptions(this.groupValues, this.groupLabel)(options) : options;
      }

      if (this.taggable && normalizedSearch.length && !this.isExistingOption(normalizedSearch)) {
        options.unshift({
          isTag: true,
          label: search,
        });
      }

      return options.slice(0, this.optionsLimit);
    },
    valueKeys() {
      if (this.trackBy) {
        return this.internalValue.map(element => element[this.trackBy]);
      }
      return this.internalValue;
    },
    optionKeys() {
      const options = this.groupValues ? this.flatAndStrip(this.options) : this.options;
      return options.map(element => this.customLabel(element, this.label).toString().toLowerCase());
    },
    currentOptionLabel() {
      if (this.multiple) {
        return this.searchable ? '' : this.getPlaceholder;
      }
      if (this.internalValue[0]) {
        return this.getOptionLabel(this.internalValue[0]);
      }
      return this.searchable ? '' : this.getPlaceholder;

      // return this.multiple ?
      //   this.searchable ? '' : this.getPlaceholder :
      //   this.internalValue[0] ?
      //   this.getOptionLabel(this.internalValue[0]) :
      //   this.searchable ? '' : this.getPlaceholder;
    },
  },
  watch: {
    internalValue() {
      if (this.resetAfter && this.internalValue.length) {
        this.search = '';
        this.internalValue = [];
      }
    },
    search() {
      const s = this.search;
      if (this.autocomplete) {
        this.internalValue = [s];
        this.fireInput(s);
      }
      this.$emit('search-change', s, this.id);
    },
    value(value) {
      this.internalValue = this.getInternalValue(value);
    },
  },
  destroyed() {
    document.removeEventListener('click', this.onDocumentClick);
  },
  methods: {
    /**
     * Converts the internal value to the external value
     * @returns {Object||Array||String||Integer} returns the external value
     */
    getValue() {
      if (this.multiple) {
        return deepClone(this.internalValue);
      }
      return this.internalValue.length === 0 ? null :
        deepClone(this.internalValue[0]);
    },
    /**
     * Converts the external value to the internal value
     * @returns {Array} returns the internal value
     */
    getInternalValue(value) {
      return value === null || value === undefined ? [] : this.multiple ?
        deepClone(value) :
        deepClone([value]);
    },
    /**
     * Filters and then flattens the options list
     * @param  {Array}
     * @returns {Array} returns a filtered and flat options list
     */
    filterAndFlat(options, search, label) {
      return flow(
        filterGroups(search, label, this.groupValues, this.groupLabel, this.customLabel),
        flattenOptions(this.groupValues, this.groupLabel),
      )(options);
    },
    /**
     * Flattens and then strips the group labels from the options list
     * @param  {Array}
     * @returns {Array} returns a flat options list without group labels
     */
    flatAndStrip(options) {
      return flow(
        flattenOptions(this.groupValues, this.groupLabel),
        stripGroups,
      )(options);
    },
    /**
     * Updates the search value
     * @param  {String}
     */
    updateSearch(query) {
      this.search = query;
    },
    /**
     * Finds out if the given query is already present
     * in the available options
     * @param  {String}
     * @returns {Boolean} returns true if element is available
     */
    isExistingOption(query) {
      return !this.options ?
        false :
        this.optionKeys.indexOf(query) > -1;
    },
    /**
     * Finds out if the given element is already present
     * in the result value
     * @param  {Object||String||Integer} option passed element to check
     * @returns {Boolean} returns true if element is selected
     */
    isSelected(option) {
      const opt = this.trackBy ?
        option[this.trackBy] :
        option;
      return this.valueKeys.indexOf(opt) > -1;
    },
    /**
     * Finds out if the given element is NOT already present
     * in the result value. Negated isSelected method.
     * @param  {Object||String||Integer} option passed element to check
     * @returns {Boolean} returns true if element is not selected
     */
    isNotSelected(option) {
      return !this.isSelected(option);
    },
    /**
     * Returns empty string when options is null/undefined
     * Returns tag query if option is tag.
     * Returns the customLabel() results and casts it to string.
     *
     * @param  {Object||String||Integer} Passed option
     * @returns {Object||String}
     */
    getOptionLabel(option) {
      if (isEmpty(option)) return '';
      if (option.isTag) return option.label;
      if (option.$isLabel) return option.$groupLabel;

      return this.customLabel(option, this.label) || '';
    },
    /**
     * Add the given option to the list of selected options
     * or sets the option as the selected option.
     * If option is already selected -> remove it from the results.
     *
     * @param  {Object||String||Integer} option to select/deselect
     * @param  {Boolean} block removing
     */
    select(option, key) {
      if (this.blockKeys.indexOf(key) !== -1 || this.disabled || option.$isLabel || option.$isDisabled) return;

      if (this.max && this.multiple && this.internalValue.length === this.max) return;
      if (option.isTag) {
        this.$emit('tag', option.label, this.id);
        this.search = '';
        if (this.closeOnSelect && !this.multiple) this.deactivate();
      } else {
        if (this.autocomplete) {
          this.search = option;
        } else {
          const isSelected = this.isSelected(option);
          if (isSelected && this.canDeselect && !this.autocomplete) {
            if (key !== 'Tab') this.removeElement(option);
            return;
          } if (this.multiple) {
            this.internalValue.push(option);
          } else {
            this.internalValue = [option];
          }
          this.$emit('select', deepClone(option), this.id);

          this.fireInput();
        }

        if (this.clearOnSelect && !this.autocomplete) this.search = '';
      }

      if (this.closeOnSelect) this.deactivate();
    },
    /**
     * Removes the given option from the selected options.
     * Additionally checks this.allowEmpty prop if option can be removed when
     * it is the last selected option.
     *
     * @param  {type} option description
     * @returns {type}        description
     */
    removeElement(option, shouldClose = true) {
      if (this.disabled) return;

      if (!this.allowEmpty && this.internalValue.length <= 1) {
        this.deactivate();
        return;
      }

      const index = typeof option === 'object' ?
        this.valueKeys.indexOf(option[this.trackBy]) :
        this.valueKeys.indexOf(option);

      this.internalValue.splice(index, 1);
      this.$emit('remove', deepClone(option), this.id);
      this.fireInput();


      if (this.closeOnSelect && shouldClose) this.deactivate();
    },

    fireInput() {
      const val = this.getValue();
      this.$emit('input', val, this.id);

      // specify selected property
      // if (this.selectProps) {
      //   if (this.multiple) {
      //     val = val.map(item => item[this.selectProps]);
      //   } else {
      //     if(val)
      //       val = val[this.selectProps];
      //   }
      //   this.$emit('change-model', val);
      // }
    },
    /**
     * Calls this.removeElement() with the last element
     * from this.internalValue (selected element Array)
     *
     * @fires this#removeElement
     */
    removeLastElement() {
      if (this.blockKeys.indexOf('Delete') !== -1) return;

      if (this.search.length === 0 && this.multiple && Array.isArray(this.internalValue)) {
        this.removeElement(this.internalValue[this.internalValue.length - 1], false);
      }
    },
    /**
     * Opens the multiselect’s dropdown.
     * Sets this.isOpen to TRUE
     */
    activate() {
      if (this.isOpen || this.disabled) return;

      this.adjustPosition();

      if (this.groupValues && this.pointer === 0 && this.filteredOptions.length) {
        this.pointer = 1;
      }

      this.isOpen = true;

      if (this.searchable) {
        if (!this.preserveSearch && !this.autocomplete) this.search = '';

        this.$nextTick(() => {
          if (utils.isMobile()) {
            setTimeout(() => {
              document.addEventListener('click', this.onDocumentClick);
            }, 10);
          } else {
            this.searchEl.focus();
          }
        });
      } else {
        this.$el.focus();
      }
      this.$emit('open', this.id);
    },
    /**
     * Closes the multiselect’s dropdown.
     * Sets this.isOpen to FALSE
     */
    deactivate() {
      // temp
      // return;

      if (!this.isOpen) return;

      this.isOpen = false;

      if (this.searchable) {
        this.searchEl.blur();
      } else {
        this.$el.blur();
      }
      if (!this.preserveSearch && !this.autocomplete) this.search = '';
      this.$emit('close', this.getValue(), this.id);
    },

    onDocumentClick(event) {
      if (!event.target.closest('.multiselect')) {
        this.deactivate();
      }
    },

    /**
     * Call this.activate() or this.deactivate()
     * depending on this.isOpen value.
     *
     * @fires this#activate || this#deactivate
     * @property {Boolean} isOpen indicates if dropdown is open
     */
    toggle() {
      if (this.isOpen) {
        this.deactivate();
      } else {
        this.activate();
      }
    },
    /**
     * Updates the hasEnoughSpace variable used for
     * detecting where to expand the dropdown
     */
    adjustPosition() {
      if (typeof window === 'undefined') return;

      const spaceAbove = this.$el.getBoundingClientRect().top;
      const spaceBelow = window.innerHeight - this.$el.getBoundingClientRect().bottom;
      const hasEnoughSpaceBelow = spaceBelow > this.maxHeight;

      if (hasEnoughSpaceBelow || spaceBelow > spaceAbove || this.openDirection === 'below' || this.openDirection === 'bottom') {
        this.prefferedOpenDirection = 'below';
        this.optimizedHeight = Math.min(spaceBelow, this.maxHeight) - 40;
      } else {
        this.prefferedOpenDirection = 'above';
        this.optimizedHeight = Math.min(spaceAbove, this.maxHeight) - 40;
      }
    },
  },
};
