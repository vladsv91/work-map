export default {
  data() {
    return {
      timerId: null,
    };
  },
  props: {
    scope: {},
    value: {},
    validationMessages: {
      type: Object,
    },
  },
  methods: {
    clearValidationTimeout() {
      if (this.timerId != null) {
        clearTimeout(this.timerId);
        this.timerId = null;
      }
    },
  },
  watch: {
    value(value, oldValue) {
      // for handling ie and edge bug (or feature)
      if (!this.hasChangedFirstTime) {
        this.hasChangedFirstTime = true;
        if (value === '' && oldValue == null) {
          return;
        }
      }

      const { scope } = this;
      if (!scope) return;
      scope.$reset();
      this.clearValidationTimeout();
      this.timerId = setTimeout(() => {
        scope.$touch();
        this.timerId = null;
      }, 1000);
    },
  },
  mounted() {},
  computed: {
    hasError() {
      const { scope } = this;
      if (!scope) return false;
      return scope.$error && scope.$dirty;
    },
    firstError() {
      const { scope } = this;
      if (!scope) return;
      for (const key in scope.$params) {
        if (!scope[key]) {
          let { validationMessages } = this;
          if (validationMessages && (validationMessages = validationMessages[key])) {
            return validationMessages;
          }
          // return 'Error';
          const message = this.validationTemplates[key];
          if (message)
            return typeof message === 'function' ? message(scope.$params[key]) : message;
          return key;
        }
      }
      return '';
    },
    validationTemplates() {
      return this.$t.validationMessages;
    },
  },
};
