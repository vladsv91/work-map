import MultiselectMixin from 'src/components/inputs/multiselects/item-multiselect-mixin';
import services from 'src/services';

const multiselects = {
  IndustryMultiselect: {
    enabledCachePerType: true,
    methods: { getData: services.lookupService.getAllIndustries },
    computed: {
      getLocalizableTemplate() { return this.$tm.industry; },
      groupValuesComputed() { return 'values'; },
      groupLabelComputed() { return 'typeName'; },
      multiselectOptions() { return (this.multiselectProps.options || []).map(item => { item.typeName = item.type.name; return item; }); },
    },
  },

  MyResumeMultiselect: {
    clearDataOnLogout: true,
    methods: {
      getData: services.myApplicantService.getAllResumes,
      getChildren(h) {
        return [(<div slot="noItems"><router-link attrs={{ to: { name: 'AddResume' } }}>
          {this.getLocalizableTemplate.noItems}</router-link></div>)];
      },
    },
    computed: {
      getLocalizableTemplate() { return this.$tm.myResume; },
      multiselectOptions() { return (this.multiselectProps.options || []).filter(_ => !_.isDisabled); },
    },
  },


  ProfessionMultiselect: {
    enabledCachePerType: true,
    methods: { getData: services.lookupService.getAllProfessions },
    computed: {
      getLocalizableTemplate() { return this.$tm.profession; },
      groupValuesComputed() { return 'values'; },
      groupLabelComputed() { return 'typeName'; },
      multiselectOptions() { return (this.multiselectProps.options || []).map(item => { item.typeName = item.type.name; return item; }); },
    },
  },

  EmploymentTypeMultiselect: {
    enabledCachePerType: true,
    methods: { getData: services.lookupService.getAllEmploymentTypes },
    computed: { getLocalizableTemplate() { return this.$tm.employmentType; } },
  },
  CurrencyMultiselect: {
    enabledCachePerType: true,
    methods: { getData: services.lookupService.getAllCurrencies },
    computed: { getLocalizableTemplate() { return this.$tm.currency; } },
  },
  FilialMultiselect: {
    clearDataOnLogout: true,
    methods: { getData: services.myCompanyService.getAllCompanyFilials },
    computed: { getLocalizableTemplate() { return this.$tm.myCompanyFilial; } },
  },
};


const hop = Object.prototype.hasOwnProperty;
for (const key in multiselects) {
  if (hop.call(multiselects, key)) {
    const mult = multiselects[key];
    mult.mixins = [MultiselectMixin];
    mult.name = key;
  }
}

export default multiselects;
