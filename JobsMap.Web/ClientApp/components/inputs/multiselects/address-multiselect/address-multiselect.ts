import MultiselectMixin from 'src/components/inputs/multiselects/item-multiselect-mixin';
import { searchDebounce } from 'src/utils/debounce';
import { FullAddressModel } from 'src/shared/address.model';

export default {
  data() {
    return {
      lastSavedSearch: '',
      selectedAddress: null,
    };
  },
  methods: {
    onHighlightOption(option) {
      this.setSearchValue(option.description);
    },
    setSearchValue(search) {
      this.mult.search = search;
      this.lastSavedSearch = search;
    },
    onSelect(option) {
      this.selectedAddress = option;
      this.onHighlightOption(option);

      this.getPlaceService().getDetails({
        placeId: option.place_id,
      }, this.getPlaceDetailsCallback);
    },

    getPlaceDetailsCallback(place, status) {
      let addressComponents: google.maps.GeocoderAddressComponent[];

      if (status !== google.maps.places.PlacesServiceStatus.OK || !place.geometry ||
        !(addressComponents = place.address_components)) {
        this.multiselectProps.options = [];
        this.selectedAddress = null;
        this.$emit('input', null);
        return;
      }

      const addressComponentsMap = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name',
      };

      const returnData = {} as any;

      for (let i = 0; i < addressComponents.length; i++) {
        const addressComponent = addressComponents[i];
        const addressType = addressComponent.types[0];
        if (addressComponentsMap[addressType]) {
          const val = addressComponent[addressComponentsMap[addressType]];
          returnData[addressType] = val;
        }
      }

      returnData.latitude = place.geometry.location.lat();
      returnData.longitude = place.geometry.location.lng();

      const addressData = {
        formattedAddress: place.formatted_address,
        description: this.selectedAddress.description,
        country: returnData.country,
        city: returnData.locality,
        street: returnData.route,
        streetNumber: returnData.street_number,
        latitude: returnData.latitude,
        longitude: returnData.longitude,
      };

      // this.setSearchValue(place.formatted_address);
      this.$emit('input', addressData);
    },

    findAddresses: searchDebounce(function (searchText) {
      if (!searchText || searchText === this.lastSavedSearch) {
        return;
      }

      this.multiselectProps.isLoading = true;

      const service = this.getAutocompleteService();
      service.getPlacePredictions({
        input: searchText,
        options: {
          types: ['address'],
        },
      }, this.onRetrievedPlacePredictions);
    }, 300),

    onRetrievedPlacePredictions(predictions, status) {
      this.multiselectProps.isLoading = false;

      if (status !== google.maps.places.PlacesServiceStatus.OK) {
        this.multiselectProps.options = [];
        return;
      }
      this.multiselectProps.options = predictions;
    },

    emitInput(value) {
      this.selectedAddress = value;
      if (!value)
        this.$emit('input', value);
    },

    optionLabel(option) {
      return option ? option.description || option.formattedAddress : '';
    },

    getPlaceService() {
      const { placeService } = this;
      if (placeService) return placeService;
      const map = new google.maps.Map(document.createElement('a'));
      return (this.placeService = new google.maps.places.PlacesService(map));
    },

    getAutocompleteService() {
      return this.autocompleteService ?
        this.autocompleteService :
        (this.autocompleteService = new google.maps.places.AutocompleteService());
    },
    onChangedValueProp(value) {
      // if (value && value.formattedAddress) {
      //   // todo, hack
      //   this.mult.$refs.search.value = value.formattedAddress;
      // }

      // todo, refucktoring, and norm display search
      if (value) {
        if (this.selectedAddress) {
          const placeId = this.selectedAddress.place_id;
          const { description } = this.selectedAddress;
          this.selectedAddress = value;
          if (description === this.selectedAddress.description) {
            this.$set(this.selectedAddress, 'place_id', placeId);
          }
        } else {
          this.selectedAddress = value;
        }
        const descr = value.description || value.formattedAddress;
        this.mult.$refs.search.value = this.mult.search = descr;
      } else {
        this.selectedAddress = null;
        this.mult.$refs.search.value = '';
      }
    },
  },
  mixins: [MultiselectMixin],
  computed: {
    enabledSelectPicker() {
      return this.multiselectProps.options && this.multiselectProps.options.length > 0;
    },
    getLocalizableTemplate() {
      return this.$tm.address;
    },
    getPlaceholder() {
      const t = this.getLocalizableTemplate;
      return this.placeholder || t.placeholder;
      // return this.placeholder || 'Select';
    },
  },
  mounted() {
    this.onChangedValueProp(this.value);
  },
  watch: {
    value(newValue) {
      this.onChangedValueProp(newValue);
    },
  },
  props: {
    hideSelected: {
      type: Boolean,
      default: false,
    },
    canDeselect: {
      type: Boolean,
      default: true,
    },
  },
};
