const cachedOptions = {};

export default {
  name: 'custom-vue-multiselect',
  render(h) {
    const vm = this;
    return h('multiselect', {
      attrs: {
        canDeselect: vm.canDeselect,
        closeOnSelect: vm.closeOnSelectComputed,
        multiple: vm.multiple,
        hideTags: vm.hideTags,
        placeholder: vm.getPlaceholder,
        inputLabel: vm.getPlaceholder,
        disabled: vm.disabled,
        label: 'name',
        trackBy: 'id',
        groupValues: vm.groupValuesComputed,
        groupLabel: vm.groupLabelComputed,
        hideSelected: vm.hideSelectedComputed,
        loading: vm.multiselectProps.isLoading,
        options: vm.multiselectOptions,
      },
      on: { open: vm.onOpenMultiselect },
      model: {
        value: vm.selectedOptions,
        callback: function ($$v) {
          vm.selectedOptions = $$v;
        },
        expression: 'selectedOptions',
      },
    }, this.getChildren(h));
  },
  enabledCachePerType: false,
  clearDataOnLogout: false,
  data() {
    return {
      selectedOptions: this.value || [],
      cache: true,
      hasRequestedForLoadingData: false,
      hasLoadedDataFromCache: false,
      multiselectProps: {
        isLoading: false,
        options: this.options || [],
      },
    };
  },
  methods: {
    getData() { return Promise.resolve([]); }, // abstract

    getChildren() { },

    onOpenMultiselect() {
      this.loadDataOrFromCache();
    },

    loadDataOrFromCache() {
      if ((this.cache && this.hasRequestedForLoadingData) || this.hasLoadedDataFromCache) {
        return;
      }
      if (this.$options.enabledCachePerType) {
        const data = cachedOptions[this.$options.name];
        if (data) {
          this.multiselectProps.options = data;
          this.hasLoadedDataFromCache = true;
          return;
        }
      }

      this.loadData();
    },

    loadData() {
      this.hasRequestedForLoadingData = true;

      this.multiselectProps.isLoading = true;

      // interceptorsConfig.disableLoaderOnce();
      this.getData().then((response) => {
        this.onReceiveData(response);
        this.onResponseBase();
      }).catch((error) => {
        this.onResponseBase();
        this.hasRequestedForLoadingData = false;
        this.showServiceError(error);
      });
    },

    onReceiveData(response) {
      const options = this.multiselectProps.options = this.getDataFromResponse(response);
      if (this.$options.enabledCachePerType) {
        cachedOptions[this.$options.name] = options;
        this.hasLoadedDataFromCache = true;
      }
      this.$emit('receive-data');
    },
    getDataFromResponse(response) {
      return response.data;
    },
    onResponseBase() {
      this.multiselectProps.isLoading = false;
    },

    emitInput(value) {
      this.selectedOptions = value;
      this.$emit('input', value);
    },
  },
  watch: {
    value(newValue) {
      this.selectedOptions = newValue;
    },
  },
  created() {
    if (this.tryAutocomplete) {
      this.$on('receive-data', () => {
        const opts = this.multiselectOptions;
        if (opts && opts.length === 1) {
          this.emitInput(opts[0]);
        }
      });
      this.loadDataOrFromCache();
    }
    // } else {
    // const vs = this.$parent.scope; // try get va;idation scope from validation wrapper
    // if (vs && 'required' in vs) {
    //   this.loadDataOrFromCache();
    // }
    // }
  },
  mounted() {
    const { mult } = this;
    mult.$on('select', (...params) => this.$emit('select', ...params));
    mult.$on('input', this.emitInput);

    if (this.$options.clearDataOnLogout) {
      this.$watch('isAuthenticated', (value) => {
        if (!value) {
          this.multiselectProps.options = [];
          this.hasLoadedDataFromCache = false;
          this.hasRequestedForLoadingData = false;
          this.emitInput(null);
        }
      });
    }
  },
  computed: {
    closeOnSelectComputed() {
      const { closeOnSelect } = this;
      return closeOnSelect == null ? !this.multiple : closeOnSelect;
    },
    mult() {
      return this.$children[0];
    },
    getPlaceholder() {
      const t = this.getLocalizableTemplate;
      return this.placeholder || this.multiple ? t.multiple : t.single;
      // return this.placeholder || 'Select';
    },
    groupValuesComputed() { return undefined; },
    groupLabelComputed() { return undefined; },

    multiselectOptions() { return this.multiselectProps.options; },

    // abstract
    getLocalizableTemplate() {
      // return this.$tm.myTemplate;
    },
    $tm() {
      return this.$t.multiselects;
    },
    hideSelectedComputed() {
      const { hideSelected } = this;
      if (hideSelected == null) {
        return !this.multiple && this.canDeselect === false;
      }
      return hideSelected;
    },
  },
  props: {
    multiple: {
      type: Boolean,
      default: false,
    },
    value: {
      default() {
        return [];
      },
    },
    hideSelected: {
      type: Boolean,
      default: null,
    },
    options: {
      type: Array,
    },
    hideTags: {
      type: Boolean,
      default: false,
    },
    placeholder: {
      type: String,
    },
    closeOnSelect: {
      type: Boolean,
      default: null,
    },
    canDeselect: {
      type: Boolean,
      default: true,
    },
    disabled: { type: Boolean },
    tryAutocomplete: { type: Boolean },
  },
};
