import Vue from 'vue';

import AddressMultiselect from 'components/inputs/multiselects/address-multiselect/address-multiselect.vue';
import Multiselects from 'components/inputs/multiselects/multiselects';

const multiselects = {
  AddressMultiselect,
};

forIn(multiselects);
forIn(Multiselects);

function forIn(items) {
  const hop = Object.prototype.hasOwnProperty;
  for (const key in items) {
    if (hop.call(items, key)) {
      Vue.component(key, items[key]);
    }
  }
}
