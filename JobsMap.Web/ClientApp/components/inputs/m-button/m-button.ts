export default {
  name: 'm-button',
  data() {
    return {
      isLoadingInternal: this.isLoading,
    };
  },
  methods: {
    // for parent calls
    setPromise(promise) {
      if (promise) {
        this.isLoadingInternal = true;
        promise.then(() => {
          this.isLoadingInternal = false;
        }, () => {
          this.isLoadingInternal = false;
        });
      } else {
        this.isLoadingInternal = this.isLoading;
      }
    },
  },
  watch: {
    loadingPromise(promise) {
      this.setPromise(promise);
    },
    isLoading(value) {
      this.isLoadingInternal = value;
    },
  },
  computed: {
    disabledComputed() {
      return this.isLoadingInternal || this.disabled;
    },
  },
  created() {
    this.setPromise(this.loadingPromise);
  },
  props: {
    disabled: {
      type: Boolean,
    },
    loadingPromise: {},
    isLoading: { type: Boolean, default: false },
  },
};
