export default {
  // TODO: almost in all places of code. Create as functional
  methods: {
    onFormSubmit(event) {
      const promise = this.$listeners.submit(event) || event.promise; // workaround, instead this.$emit('submit', event);
      if (promise && promise instanceof Promise) {
        const defS = this.$slots.default;
        if (!defS) return;
        defS.forEach(slot => {
          if (slot && (slot = slot.componentInstance) && slot.setPromise) { // m-button
            slot.setPromise(promise);
          }
        });
      }
    },
  },
};
