export default {
  data() {
    return {
      inputValue: '',
    };
  },
  methods: {
    checkForLastSymbol() {
      const { inputValue } = this;


      if (inputValue && inputValue.length) {
        if (inputValue[0] === '.') {
          if (inputValue.length === 1) {
            this.inputValue = '0';
            this.$emit('input', 0);
          } else {
            this.inputValue = `0${inputValue}`;
          }
        } else if (inputValue[inputValue.length - 1] === '.') {
          this.inputValue += '0';
        }
      }

      const defVal = this.defaultValue;
      if (!inputValue && defVal != null) {
        this.inputValue = defVal;
        this.$emit('input', this.defaultValue);
      }
    },
    onBlur($event) {
      this.checkForLastSymbol();
      if (this.lazyValidate) {
        this.validateAndSetValue();
      }
      this.$emit('blur', $event);
    },

    onEnter() {
      if (this.lazyValidate) {
        this.validateAndSetValue();
      }
    },

    validateAndSetValue() {
      let { inputValue } = this;
      if (inputValue == null || inputValue === '') return;
      inputValue = +inputValue;
      const newVal = this.validateValue();
      if (inputValue !== newVal) {
        this.inputValue = newVal;
        this.$refs.input.value = newVal;
        this.$emit('input', newVal);
      }
    },

    validateValue() {
      let { inputValue } = this;
      if (inputValue == null || inputValue === '') return;
      inputValue = +inputValue;
      const { min, max } = this;
      if (inputValue !== null && isDef(min) && inputValue < min) {
        inputValue = this.min;
        this.inputValue = inputValue;
      }
      if (inputValue !== null && isDef(max) && inputValue > max) {
        inputValue = this.max;
        this.inputValue = inputValue;
      }
      return inputValue;
    },

    onInput(/* target */) {
      const { isInt } = this;
      let inputValue = this.inputValue = (this.inputValue || '').replace(' ', '').replace(',', '.');
      if (isInt) {
        this.inputValue = inputValue = inputValue.replace('.', '');
      }

      if (inputValue && inputValue.length > this.maxLen) {
        this.inputValue = inputValue = inputValue.slice(0, this.maxLen);
      }

      let isOnlyMinus = false;
      if (!isNumeric(inputValue, isInt)) {
        const array = [];
        let settedDot = isInt;
        for (let i = 0; i < inputValue.length; i++) {
          const char = inputValue[i];
          if (char >= '0' && char <= '9') {
            array.push(char);
          } else if (char === '-' && i === 0) {
            array.push('-');
            isOnlyMinus = true;
          } else if (!settedDot && i > 0) {
            settedDot = true;
            array.push('.');
          }
        }
        inputValue = array.join('');
        if (inputValue === '.')
          inputValue = '';
        if (isOnlyMinus)
          isOnlyMinus = inputValue.length === 1;
        if (inputValue === '-.')
          inputValue = '-0.';
        this.inputValue = inputValue;
      }

      inputValue = (isOnlyMinus || !inputValue) ? null : +inputValue;

      if (!this.lazyValidate) {
        inputValue = this.validateValue();
      }

      // target.value = this.inputValue || '';
      this.$emit('input', inputValue);
    },
    setInputValue(newValue) {
      if (this.inputValue === '-0.' && newValue === 0)
        return;
      this.inputValue = (newValue != null && newValue.toString && newValue.toString()) || '';
    },
  },
  watch: {
    value(newValue) {
      this.setInputValue(newValue);
    },
  },
  created() {
    this.setInputValue(this.value);
  },
  props: {
    value: {
      type: Number,
      default: null,
    },
    maxLen: {
      type: Number,
      default: 20,
    },
    isInt: {
      type: Boolean,
      default: false,
    },
    min: {
      type: Number,
      default: null,
    },
    max: {
      type: Number,
      default: null,
    },
    defaultValue: {
      type: Number,
      default: null,
    },
    lazyValidate: {
      type: Boolean,
      default: true,
    },
  },
};

function isDef(n) {
  return n !== null && n !== undefined;
}


function isNumeric(n, isInt) {
  return !Number.isNaN(isInt ? parseInt(n, 10) : parseFloat(n)) && Number.isFinite(n);
}
