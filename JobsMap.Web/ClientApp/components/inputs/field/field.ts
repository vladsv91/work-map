export default {
  data() {
    return {
      id: Math.random().toString().substr(4),
      inputValue: null,
    };
  },
  methods: {
    onSlotInputValueChanged(value) {
      this.inputValue = value;
      // console.log(value);
    },
  },
  created() {
    const defSlot = this.$slots.default[0];

    let pr;
    if (defSlot.componentOptions && defSlot.componentOptions.propsData) {
      pr = defSlot.componentOptions.propsData;
    } else if (defSlot.data && defSlot.data.domProps) {
      pr = defSlot.data.domProps;
    }
    if (pr && 'value' in pr) {
      this.inputValue = pr.value;
    }
  },
  mounted() {
    const defSlot = this.$slots.default[0];
    defSlot.elm.id = this.id;


    // hack, hack, hack
    if (defSlot.componentInstance) {
      defSlot.componentInstance.$on('input', this.onSlotInputValueChanged);
    } else if (defSlot.tag === 'input' || defSlot.tag === 'textarea') {
      defSlot.elm.addEventListener('input', (e) => {
        this.onSlotInputValueChanged(e.target.value);
      });
    }
    this.inputValue = '';
  },
  computed: {
    isRequired() {
      const vs = this.vsComputed;
      if (!vs) return false;

      const pms = vs.$params;
      if (!('required' in pms)) {
        return false;
      }

      const { required } = pms;
      return !required || required.type === 'requiredWS' || required.type === 'required';
    },
    vsComputed() {
      const { vs } = this;
      if (vs) return vs;
      const defSlot = this.$slots.default[0];
      if (!defSlot) return;
      const defSlotModel = defSlot.data.model;
      if (!defSlotModel) return;

      const { expression } = defSlotModel;
      let slotContextV = defSlot.context.$v;
      if (!slotContextV) return;

      const splitedExpression = expression.split('.');
      for (let i = 0; i < splitedExpression.length; i++) {
        const objectKey = splitedExpression[i];
        slotContextV = slotContextV[objectKey];
        if (!slotContextV) return;
      }
      return slotContextV;
    },
  },
  props: {
    label: {},
    // not required, because vsComputed
    vs: {}, // validation scope
    vms: {}, // validation messages
    // value: {},
  },
};
