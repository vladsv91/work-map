/* eslint-disable no-underscore-dangle */
/* eslint-disable indent */

export default {
  functional: true,
  methods: {},
  props: {
    // maxLength: { type: Number }, attribute exists
  },
  render(h, vm) {
    const { data } = vm;
    data.on = {
      input: e => {
        // const str = e.target.value;
        // maxLength
        return vm.listeners.input ? vm.listeners.input(e.target.value, e) : null;
      },
    };
    data.staticClass = data.staticClass ? `fc ${data.staticClass}` : 'fc';

    const { props } = data;
    if (props && props.value) {
      let { attrs } = data;
      if (!attrs)
        attrs = data.attrs = {};
      // if(!data.attrs.value && props.value)
      attrs.value = props.value;
    }
    return h('input', data);
    // const c = vm._c;
    // const { input } = vm.listeners;
    // if (input) {
    //   vm.listeners.input = function ($event) {
    //     input($event.target.value, $event);
    //   };
    // }

    // const { staticClass } = vm.data;

    // return c(
    //   'input',
    //   vm._g(vm._b({
    //         staticClass: staticClass ? `fc ${staticClass}` : 'fc',
    //       },
    //       'input',
    //       Object.assign({}, vm.data.attrs, vm.data.props),
    //       false,
    //     ),
    //     vm.listeners,
    //   ),
    // );
  },
};
