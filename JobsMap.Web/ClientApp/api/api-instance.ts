import axios, { AxiosInstance } from 'axios';

// tslint:disable-next-line: no-empty-interface
export interface ApiInstance extends AxiosInstance {

}


const api = axios.create({
  baseURL: '/api/',
}) as ApiInstance;


export default api;
export { api };
