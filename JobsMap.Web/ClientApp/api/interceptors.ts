import { AxiosRequestConfig, AxiosError } from 'axios';

import encodeUriParams from 'src/utils/uri-params-serializer';
import api from 'src/api/api-instance';
import tokens from 'src/api/tokens';
import store from 'src/vuex';
import EventHub from 'src/utils/EventHub';

import services from 'src/services';
import { KeyValue } from 'src/shared/general-types';


const getDefaultHeaders = (disabledAuthHeaders: boolean) => {
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Accept-Language': store.getters.language.header,
  };
  if (!disabledAuthHeaders) {
    setAuthToken(headers);
  }
  return headers;
};

function setAuthToken(headers: KeyValue) {
  headers.Authorization = `Bearer ${tokens.getAccessToken()}`;
}

const adjustConfigParameters = (config) => {
  if (config.defaultHeaders !== false) {
    const defHeaders = getDefaultHeaders(config.disabledAuthHeaders);

    const configHeaders = config.headers;
    const proto = Object.prototype;
    for (const key in defHeaders) {
      if (proto.hasOwnProperty.call(defHeaders, key) && !(key in configHeaders)) {
        configHeaders[key] = defHeaders[key];
      }
    }
  }

  if (config.params) {
    config.params = encodeUriParams(config.params);
  }

  return config;
};


function setupRequestInterceptor() {
  api.interceptors.request.use((config: AxiosRequestConfig) => new Promise<AxiosRequestConfig>((resolve, reject) => {
    config = adjustConfigParameters(config);
    EventHub.$emit('begin-api-request', config);

    if (!config.dontUseExpirationHandler) {
      handleAccessTokenExpiration(resolve, reject, config);
      return;
    }
    return resolve(config);
  }));

  api.interceptors.response.use((response) => {
    EventHub.$emit('end-api-request', response);
    return response;
  }, (error: AxiosError) => {
    EventHub.$emit('end-api-request', error);
    if (error && error.response && error.response.status === 401) {
      store.commit('Logout');
    }
    return Promise.reject(error);
  });
}

let resolvingPromise = null;

function handleAccessTokenExpiration(resolve, reject, config) {
  // todo, handle allowAnonymous in config, that don't request a lot, or handle after, 401
  if (resolvingPromise) {
    resolvingPromise.then(() => {
      resolve(config);
    }, (error) => {
      reject(error);
    });
    return;
  }
  const now = new Date();
  // 20 seccond before expiration
  // const utcNow = toUtc(now);
  const expiresOnStr = tokens.getTokenExpiration();

  if (!expiresOnStr) {
    resolve(config);
    return;
  }
  // expiresOn = toUtc(new Date(expiresOn));
  const expiresOn = new Date(expiresOnStr);
  expiresOn.setSeconds(expiresOn.getSeconds() - 20); // 20 secconds before expiration
  if (now > expiresOn) {
    resolvingPromise = services.accountService.refreshToken(tokens.getRefreshToken()).then((response) => {
      resolvingPromise = null;
      tokens.setTokensData(response.data);
      setAuthToken(config.headers);
      resolve(config);
    }, (error) => {
      // todo, check
      resolvingPromise = null;
      store.commit('Logout');
      reject(error);
    });
  } else {
    resolve(config);
  }
}


setupRequestInterceptor();
