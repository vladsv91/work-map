import EventHub from 'src/utils/EventHub';
import { KeyValue } from 'src/shared/general-types';
import Utils from 'src/utils/utils';

const keys = {
  accessToken: 'accessToken',
  refreshToken: 'refreshToken',
  expiresOn: 'expiresOn',
};

if (!window.isServer) {
  window.addEventListener('storage', (event: StorageEvent) => {
    if (event.key === keys.refreshToken) {
      if (event.newValue) {
        EventHub.$emit('created-tokens');
      } else {
        EventHub.$emit('cleared-tokens');
      }
    }
  });
}

const tokens = {
  getAccessToken() {
    return window.isServer ? '' : localStorage.getItem(keys.accessToken);
  },
  getTokenExpiration() {
    return localStorage.getItem(keys.expiresOn);
  },
  getRefreshToken() {
    return localStorage.getItem(keys.refreshToken);
  },
  setTokensData(data: KeyValue) {
    Utils.objectForEach(data, (key, val) => setValue(key, val));
  },
  clearTokensData() {
    Utils.objectForEach(keys, (key) => removeValue(key));
  },
};

function setValue(key, value) {
  localStorage.setItem(key, value);
}

function removeValue(key) {
  localStorage.removeItem(key);
}

export default tokens;
