import Vue from 'vue';
import safePropertyFilter from './safe-property-filter';
import Utils from 'src/utils/utils';


const filters = {
  ShortenStr(str: string, length = 120) {
    if (!str) { return ''; }
    return str.length > length ? `${str.substr(0, length - 3)}...` : str;
  },
  Safe: safePropertyFilter,

  AgeByBirthDate(birthday: Date) {
    if (!birthday) { return null; }
    const ageDifMs = Date.now() - (new Date(birthday)).getTime();
    const ageDate = new Date(ageDifMs);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
  },

  ToLocalTime(dateParam: Date) {
    if (!dateParam) { return dateParam; }

    const date = new Date(dateParam);
    const minutesOffset = date.getTimezoneOffset();
    const ticks = date.getTime();
    const localedDate = new Date(ticks - (minutesOffset * 60000));
    return typeof dateParam === 'string' ? localedDate.toISOString() : localedDate;
  },

  FormatDateTime(date: Date) {
    if (!date) { return date; }
    date = new Date(date);
    return `${date.toLocaleString('ru', { day: '2-digit', month: '2-digit', year: 'numeric' })} ${
      date.toLocaleString('ru', { hour: '2-digit', minute: '2-digit', second: '2-digit' })}`;
  },

};


Utils.objectForEach(filters, (key, value) => Vue.filter(key, value));
