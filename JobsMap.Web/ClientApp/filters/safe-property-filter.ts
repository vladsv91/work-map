import { KeyValue } from 'src/shared/general-types';

export default function (obj: KeyValue, propPath: string) {
  if (!propPath || typeof propPath.split !== 'function' || !obj) {
    return '';
  }
  const props = propPath.split('.');
  for (let i = 0; i < props.length; i += 1) {
    obj = obj[props[i]];
    if (!obj) {
      return undefined;
    }
  }
  return obj;
}
