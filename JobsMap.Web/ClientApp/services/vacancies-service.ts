import api from 'src/api/api-instance';
import { VacancyDetailsModel } from 'src/shared/vacancy.model';
import { AxiosPromise } from 'axios';

const controller = 'Vacancies';

export default {
  getByFilter(params) {
    return api.get(`${controller}/GetByFilter`, { params });
  },
  getDetails(vacancyId: string): AxiosPromise<VacancyDetailsModel> {
    return api.get(`${controller}/GetDetails`, { params: { vacancyId } });
  },

};
