import api from 'src/api/api-instance';


const controller = 'Account';
export default {

  registerAsCompany(data) {
    return api({
      url: `${controller}/RegisterAsCompany`,
      method: 'POST',
      data,
    });
  },

  registerAsApplicant(data) {
    return api({
      url: `${controller}/RegisterAsApplicant`,
      method: 'POST',
      data,
    });
  },

  // login(data) {
  //   return api({
  //     url: `${controller}/Login`,
  //     method: 'POST',
  //     data,
  //   });
  // },

  login(data) {
    return api({
      url: `${controller}/Token`,
      method: 'POST',
      data,
      disabledAuthHeaders: true,
      dontUseExpirationHandler: true,
    });
  },

  setLanguage(language) {
    return api({
      url: `${controller}/SetLanguage`,
      method: 'POST',
      params: { language },
    });
  },

  refreshToken(refreshToken) {
    return api({
      url: `${controller}/RefreshToken`,
      method: 'POST',
      params: { refreshToken },
      disabledAuthHeaders: true,
      dontUseExpirationHandler: true,
    });
  },

  logout(refreshToken) {
    return api({
      url: `${controller}/Logout`,
      method: 'POST',
      params: { refreshToken },
    });
  },

  resendConfirmationEmailByName(data) {
    return api({
      url: `${controller}/ResendConfirmationEmailByName`,
      method: 'POST',
      data,
    });
  },

  confirmEmail(data) {
    return api({
      url: `${controller}/ConfirmEmail`,
      method: 'POST',
      data,
    });
  },

  changePassword(data) {
    return api({
      url: `${controller}/ChangePassword`,
      method: 'POST',
      data,
    });
  },

  sendPasswordResettingEmail(data) {
    return api({
      url: `${controller}/SendPasswordResettingEmail`,
      method: 'POST',
      data,
    });
  },

  sendEmailChangingEmail(data) {
    return api({
      url: `${controller}/SendEmailChangingEmail`,
      method: 'POST',
      data,
    });
  },

  changeEmail(data) {
    return api({
      url: `${controller}/ChangeEmail`,
      method: 'POST',
      data,
    });
  },

  resetPassword(data) {
    return api({
      url: `${controller}/ResetPassword`,
      method: 'POST',
      data,
    });
  },

  getMyInfo() {
    return api({
      url: `${controller}/GetMyInfo`,
      method: 'GET',
    });
  },
};
