import api from 'src/api/api-instance';

const controller = 'ContactUs';
export default {
  create(data) { return api.post(`${controller}/Create`, data); },
};
