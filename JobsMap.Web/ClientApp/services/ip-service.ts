import axios from 'axios';


export default {
  getInfoByIp(ip) {
    return axios.get(`http://ip-api.com/json/${ip}`);
  },
};
