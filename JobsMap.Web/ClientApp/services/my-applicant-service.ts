import api from 'src/api/api-instance';

const controller = 'MyApplicant';

export default {
  update(data) {
    return api.post(`${controller}/Update`, data);
  },

  getMainInfo() {
    return api.get(`${controller}/GetMainInfo`);
  },

  uploadPhoto(imageFile) {
    const formData = new FormData();
    formData.append('image', imageFile, imageFile.name);
    return api.post(`${controller}/UploadPhoto`, formData);
  },

  uploadResumeFile(file, resumeId) {
    const formData = new FormData();
    formData.append('file', file, file.name);
    formData.append('resumeId', resumeId);
    return api.post(`${controller}/UploadResumeFile`, formData);
  },

  deleteResumeFile(resumeId) {
    return api({ url: `${controller}/DeleteResumeFile`, method: 'POST', params: { resumeId } });
  },

  sendResume(data) {
    return api({ url: `${controller}/SendResume`, method: 'POST', data });
  },

  getAllResumes() {
    return api.get(`${controller}/GetAllResumes`);
  },
  deactivateResume(resumeId) {
    return api({ url: `${controller}/DeactivateResume`, method: 'POST', params: { resumeId } });
  },
  activateResume(resumeId) {
    return api({ url: `${controller}/ActivateResume`, method: 'POST', params: { resumeId } });
  },
  deleteResume(resumeId) {
    return api({ url: `${controller}/DeleteResume`, method: 'POST', params: { resumeId } });
  },
  getResumeDetails(resumeId) {
    return api.get(`${controller}/GetResumeDetails`, { params: { resumeId } });
  },
  addResume(data) {
    return api({ url: `${controller}/AddResume`, method: 'POST', data });
  },

  updateResume(data) {
    return api({ url: `${controller}/UpdateResume`, method: 'POST', data });
  },

  getAllPreferredAddresses() {
    return api.get(`${controller}/GetAllPreferredAddresses`);
  },
  getPreferredAddressDetails(preferredAddressId) {
    return api.get(`${controller}/GetPreferredAddressDetails`, { params: { preferredAddressId } });
  },
  deletePreferredAddress(preferredAddressId) {
    return api({ url: `${controller}/DeletePreferredAddress`, method: 'POST', params: { preferredAddressId } });
  },
  addPreferredAddress(data) {
    return api({ url: `${controller}/AddPreferredAddress`, method: 'POST', data });
  },
  updatePreferredAddress(data) {
    return api({ url: `${controller}/UpdatePreferredAddress`, method: 'POST', data });
  },

  getAllSentResumes() {
    return api.get(`${controller}/GetAllSentResumes`);
  },
};
