import api from 'src/api/api-instance';

const controller = 'Lookup';

export default {

  getAllCurrencies() {
    return api.get(`${controller}/GetAllCurrencies`);
  },
  getAllIndustries() {
    return api.get(`${controller}/GetAllIndustries`);
  },
  getAllEmploymentTypes() {
    return api.get(`${controller}/GetAllEmploymentTypes`);
  },
  getAllProfessions() {
    return api.get(`${controller}/GetAllProfessions`);
  },
};
