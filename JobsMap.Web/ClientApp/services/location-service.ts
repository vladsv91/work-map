import api from 'src/api/api-instance';

const controller = 'Location';
export default {
  getNearbyEntitiesCounts(params) { return api.get(`${controller}/GetNearbyEntitiesCounts`, { params }); },
};
