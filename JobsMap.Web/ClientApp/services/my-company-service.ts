import api from 'src/api/api-instance';

const controller = 'MyCompany';

export default {
  update(data) {
    return api({
      url: `${controller}/Update`,
      method: 'POST',
      data,
    });
  },

  getMainInfo() {
    return api({
      url: `${controller}/GetMainInfo`,
      method: 'GET',
    });
  },

  uploadMainPhoto(imageFile) {
    const formData = new FormData();
    formData.append('image', imageFile, imageFile.name);
    return api({
      url: `${controller}/UploadMainPhoto`,
      method: 'POST',
      data: formData,
    });
  },

  getAllCompanyPhotos() {
    return api.get(`${controller}/GetAllCompanyPhotos`);
  },

  deletePhoto(companyPhotoId) {
    return api({
      url: `${controller}/deletePhoto`,
      method: 'POST',
      params: { companyPhotoId },
    });
  },

  addPhoto(imageFile, filialId) {
    const formData = new FormData();
    formData.append('image', imageFile, imageFile.name);
    formData.append('filialId', filialId || '');
    return api({
      url: `${controller}/AddPhoto`,
      method: 'POST',
      data: formData,
    });
  },

  getAllCompanyFilials() {
    return api.get(`${controller}/GetAllCompanyFilials`);
  },
  deactivateFilial(filialId) {
    return api({ url: `${controller}/DeactivateFilial`, method: 'POST', params: { filialId } });
  },
  activateFilial(filialId) {
    return api({ url: `${controller}/ActivateFilial`, method: 'POST', params: { filialId } });
  },
  getFilialDetails(filialId) {
    return api.get(`${controller}/GetFilialDetails`, {
      params: { filialId },
    });
  },
  addFilial(data) {
    return api({ url: `${controller}/AddFilial`, method: 'POST', data });
  },

  updateFilial(data) {
    return api({ url: `${controller}/UpdateFilial`, method: 'POST', data });
  },

  getAllCompanyVacancies() {
    return api.get(`${controller}/GetAllCompanyVacancies`);
  },
  getVacancyDetails(vacancyId) {
    return api.get(`${controller}/GetVacancyDetails`, {
      params: { vacancyId },
    });
  },
  deactivateVacancy(vacancyId) {
    return api({ url: `${controller}/DeactivateVacancy`, method: 'POST', params: { vacancyId } });
  },
  activateVacancy(vacancyId) {
    return api({ url: `${controller}/ActivateVacancy`, method: 'POST', params: { vacancyId } });
  },
  addVacancy(data) {
    return api({ url: `${controller}/AddVacancy`, method: 'POST', data });
  },
  updateVacancy(data) {
    return api({ url: `${controller}/UpdateVacancy`, method: 'POST', data });
  },

  getAllReceivedResumes() {
    return api.get(`${controller}/GetAllReceivedResumes`);
  },

};
