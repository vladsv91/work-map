import Vue from 'vue';

import ipService from './ip-service';
import accountService from './account-service';
import companiesService from './companies-service';
import contactUsService from './contact-us-service';
import lookupService from './lookup-service';
import myApplicantService from './my-applicant-service';
import myCompanyService from './my-company-service';
import resumesService from './resumes-service';
import vacanciesService from './vacancies-service';
import locationService from './location-service';

export default Vue.prototype.services = {
  ipService,
  accountService,
  companiesService,
  contactUsService,
  lookupService,
  myApplicantService,
  myCompanyService,
  resumesService,
  vacanciesService,
  locationService,
};
