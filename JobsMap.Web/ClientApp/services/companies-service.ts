import api from 'src/api/api-instance';

const controller = 'Companies';

export default {
  getByFilter(params) {
    return api.get(`${controller}/GetByFilter`, { params });
  },
  getDetails(companyId) {
    return api.get(`${controller}/GetDetails`, { params: { companyId } });
  },

  getFilials(companyId) {
    return api.get(`${controller}/GetFilials`, { params: { companyId } });
  },

  getVacancies(companyId) {
    return api.get(`${controller}/GetVacancies`, { params: { companyId } });
  },

  getPhotos(companyId) {
    return api.get(`${controller}/GetPhotos`, { params: { companyId } });
  },

};
