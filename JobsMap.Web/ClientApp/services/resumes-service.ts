import api from 'src/api/api-instance';

const controller = 'Resumes';

export default {
  getByFilter(params) {
    return api.get(`${controller}/GetByFilter`, { params });
  },
  getDetails(resumeId) {
    return api.get(`${controller}/GetDetails`, { params: { resumeId } });
  },

};
