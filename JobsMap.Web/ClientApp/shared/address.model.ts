export interface AddressPositionModel {
    latitude: number;
    longitude: number;
}


export interface FullAddressModel extends AddressPositionModel {
    formattedAddress: string;
    description: string;
    country: string;
    city: string;
    street: string;
    streetNumber: string;
}


export interface MarkerData {
    marker: google.maps.Marker;
    circle: google.maps.Circle;
    isSelectedPlace?: boolean;
}
