export type Func = () => void;

export type Func1<T> = (t: T) => void;

export type Func2<T, T1> = (t: T, t1: T1) => void;

export type Action<T = any> = () => T;

export interface KeyValue {
    [key: string]: any;
}
