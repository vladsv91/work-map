import { FullAddressModel } from './address.model';

export interface LookupModel {
    id: string;
    name: string;
}


export interface CompanyShortInfoModel {
    websiteUrl: string;
    contactPhone: string;
    contactEmail: string;
    id: string;
    name: string;
    employeeCount: number;
    description: string;
    industry: LookupModel;
    mainPhotoUri: string;
}


export interface CompanyFilialShortModel {
    id: string;
    name: string;
    address: FullAddressModel;
}

export interface VacancyDetailsModel {
    company: CompanyShortInfoModel;
    requirements: string;
    duties: string;
    workingConditions: string;
    id: string;
    name: string;
    createdOnUtc: Date;
    updatedOnUtc: Date;
    salaryPerMonth: number;
    description: string;
    profession: LookupModel;
    currency: LookupModel;
    employmentType: LookupModel;
    companyFilial: CompanyFilialShortModel;
}
