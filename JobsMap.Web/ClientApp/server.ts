﻿
import 'src/utils/serverStart';
import { Constants } from 'src/utils/constants';
import { app, router, store } from './app';


export default function (context) {
  return new Promise((resolve) => {
    Object.assign(global.window, context.window);

    // fixing for ssr
    store.commit('SetLanguage', { id: context.window.langId });
    Constants.appName = context.window.appName;

    // throw new Error(JSON.stringify(context));
    router.push(context.url);
    router.onReady(() => {
      resolve(app);
    }, () => {
      resolve(app);
    });
  });
}
