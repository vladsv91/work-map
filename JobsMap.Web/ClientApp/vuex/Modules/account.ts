import services from 'src/services/index.ts';
import tokens from 'src/api/tokens.ts';
import { getAllMatchedRoutes, isAllowedAnonymousForMatchedRoutes } from '../../utils/utils';

export default {
  state: {
    isAuthenticated: !!tokens.getAccessToken(),
    userInfo: null,
  },

  mutations: {
    Logout(state) {
      state.isAuthenticated = false;
      state.userInfo = null;
      // todo, refactor
      clear(this.state.myApplicant);
      clear(this.state.myCompany);

      function clear(st) {
        const hop = Object.prototype.hasOwnProperty;
        for (const key in st) {
          if (hop.call(st, key)) {
            st[key] = null;
          }
        }
      }

      tokens.clearTokensData();
      const { $router } = window.vm;
      const { currentRoute } = $router;
      const allMatchedRoutes = getAllMatchedRoutes(currentRoute);
      if (!isAllowedAnonymousForMatchedRoutes(allMatchedRoutes)) {
        $router.replace({ name: 'Main' });
      }
    },
    Login(state) {
      state.isAuthenticated = true;
    },
    SetUserInfo(state, userInfo) {
      state.userInfo = userInfo;
    },
    ConfirmEmail(state) {
      // todo, rid of this method
      if (state.isAuthenticated && state.userInfo) {
        state.userInfo.emailConfirmed = true;
      }
    },
  },

  actions: {
    RetrieveUserInfoOrFromCache({ commit, state }, noCache) {
      if (!noCache && state.userInfo) {
        return Promise.resolve(state.userInfo);
      }
      return services.accountService.getMyInfo().then(response => {
        commit('SetUserInfo', response.data);
        commit('SetLanguage', { id: response.data.language });
        return response.data;
      });
    },
    Logout({ commit }) {
      return services.accountService.logout(tokens.getRefreshToken()).then(response => {
        commit('Logout');
        return response;
      }, (response) => {
        commit('Logout');
        throw response;
      });
    },
    Login({ commit, dispatch }, data) {
      return services.accountService.login(data).then(response => {
        tokens.setTokensData(response.data);
        commit('Login');
        dispatch('RetrieveUserInfoOrFromCache');
        return response;
      });
    },
    ConfirmEmail({ commit }, data) {
      return services.accountService.confirmEmail(data).then(response => {
        commit('ConfirmEmail');
        return response;
      });
    },
    RegisterAsCompany(store, data) {
      return services.accountService.registerAsCompany(data);
    },
    RegisterAsApplicant(store, data) {
      return services.accountService.registerAsApplicant(data);
    },
  },

  getters: {
    isAuthenticated: (state) => state.isAuthenticated,
    userInfo: (state) => state.userInfo,
    isCompany: (state) => {
      const u = state.userInfo;
      return u ? u.type === 1 : false;
    },
    isApplicant: (state) => {
      const u = state.userInfo;
      return u ? u.type === 2 : false;
    },
  },
};
