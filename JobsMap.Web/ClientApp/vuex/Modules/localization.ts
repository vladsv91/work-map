import Vue from 'vue';
import localizationTemplates from './localization-templates';
import cookies from '../../utils/cookies';

Object.defineProperty(Vue.prototype, '$t', {
  get() { return this.$store.state.localization.localizedTemplates; },
});


Object.defineProperty(Vue.prototype, '$tg', {
  get() { return this.$store.state.localization.localizedTemplates.general; },
});


const languages = getLanguages();
const language = getSelectedLanguage();
const localizedTemplates = localizeTemplates(language.id);

export default {

  state: {
    localizedTemplates: localizedTemplates,
    allTemplates: localizationTemplates,
    language: language,
    languages: languages,
  },

  mutations: {
    SetLanguage(state, lang) {
      if (!lang) { return; }
      const langId = lang.id;
      state.language = state.languages.find(_ => _.id === langId) || state.languages[0];
      state.localizedTemplates = localizeTemplates(langId);

      if (!window.isServer) {
        localStorage.setItem('language', langId);
        cookies.setCookie('language', langId);
      }
    },
  },


  getters: {
    languageId: (state) => state.language.id,
    language: (state) => state.language,
  },
};

function localizeTemplates(langId, localizationTemplatesObject = localizationTemplates, localizedObject = {}) {
  for (const template in localizationTemplatesObject) {
    const obj = localizationTemplatesObject[template];
    if (isLanguagesObject(obj)) {
      localizedObject[template] = obj[langId];
    } else if (obj && typeof obj === 'object') {
      localizedObject[template] = Object.assign({}, obj);
      localizeTemplates(langId, obj, localizedObject[template]);
    }
  }
  return localizedObject;
}

function isLanguagesObject(obj) {
  const keys = Object.keys(obj);
  // guck typing, if have some keys as languages ids
  return keys.length > 0 && keys.indexOf(languages[0].id) >= 0 && keys.indexOf(languages[1].id) >= 0;
}

function getSelectedLanguage() {
  // temporary
  if (window.isServer) {
    return languages[0];
  }

  let languageId;
  const lsLangId = languageId = localStorage.getItem('language');
  if (!lsLangId) {
    languageId = (window.langId || navigator.language || '').substring(0, 2);
  }
  let selectedLanguage;
  if (languageId) {
    selectedLanguage = languages.find(_ => _.id === languageId);
  }
  if (!selectedLanguage) { selectedLanguage = languages[0]; }

  if (!lsLangId) {
    localStorage.setItem('language', selectedLanguage.id);
  }
  cookies.setCookie('language', selectedLanguage.id);

  return selectedLanguage;
}

function getLanguages() {
  return [
    {
      id: 'ru',
      name: 'Рус',
      header: 'ru-RU',
    },
    {
      id: 'en',
      name: 'Eng',
      header: 'en-US',
    },
  ];
}
