import services from 'src/services';
import { deepClone } from 'src/utils/deep-utils.ts';
import { getServiceParams } from 'src/utils/utils';

// for company

export default {
  state: {
    myCompanyMainInfo: null,
    companyPhotos: null,
    companyFilials: null,
    companyVacancies: null,
    companyReceivedResumes: null,
  },

  mutations: {
    SetCompanyMainInfo(state, myCompanyMainInfo) {
      state.myCompanyMainInfo = myCompanyMainInfo;
    },
    SetCompanyPhotos(state, data) {
      state.companyPhotos = data;
    },
    SetCompanyFilials(state, data) {
      state.companyFilials = data;
    },
    UpdateCompanyFilial(state, data) {
      updateItem(state, data, state.companyFilials);
    },
    AddCompanyFilial(state, data) {
      addItem(state, data, state.companyFilials);
    },
    UpdateCompanyVacancy(state, data) {
      updateItem(state, data, state.companyVacancies);
    },
    AddCompanyVacancy(state, data) {
      addItem(state, data, state.companyVacancies);
    },
    SetCompanyVacancies(state, data) {
      state.companyVacancies = data;
    },
    UpdateCompanyMainPhotoUri(state, url) {
      state.myCompanyMainInfo.mainPhotoUri = url;
    },
    SetCompanyReceivedResumes(state, data) {
      state.companyReceivedResumes = data;
    },
  },

  actions: {
    RetrieveCompanyMainInfo({ commit }) {
      return services.myCompanyService.getMainInfo().then(response => {
        const { data } = response;
        commit('SetCompanyMainInfo', data);
        return data;
      });
    },
    RetrieveCompanyPhotos({ commit }) {
      return services.myCompanyService.getAllCompanyPhotos().then(response => {
        commit('SetCompanyPhotos', response.data);
        return response;
      });
    },
    RetrieveCompanyFilials({ commit }) {
      return services.myCompanyService.getAllCompanyFilials().then(response => {
        commit('SetCompanyFilials', response.data);
        return response;
      });
    },
    UpdateCompanyFilial({ commit }, model) {
      model = deepClone(model);
      return services.myCompanyService.updateFilial(model).then(response => {
        commit('UpdateCompanyFilial', model);
        return response;
      });
    },
    AddCompanyFilial({ commit }, model) {
      model = deepClone(model);
      return services.myCompanyService.addFilial(model).then(response => {
        model.id = response.data;
        commit('AddCompanyFilial', model);
        return response;
      });
    },
    RetrieveCompanyVacancies({ commit }) {
      return services.myCompanyService.getAllCompanyVacancies().then(response => {
        commit('SetCompanyVacancies', response.data);
        return response;
      });
    },
    UpdateCompanyVacancy({ commit }, model) {
      model = deepClone(model);
      return services.myCompanyService.updateVacancy(getServiceParams(model)).then(response => {
        commit('UpdateCompanyVacancy', model);
        return response;
      });
    },
    AddCompanyVacancy({ commit }, model) {
      model = deepClone(model);
      return services.myCompanyService.addVacancy(getServiceParams(model)).then(response => {
        model.id = response.data;
        commit('AddCompanyVacancy', model);
        return response;
      });
    },
    UpdateCompanyMainInfo({ commit }, data) {
      return services.myCompanyService.update(getServiceParams(data)).then(response => {
        commit('SetCompanyMainInfo', data);
        return response;
      });
    },
    UploadCompanyMainPhoto({ commit }, imageFile) {
      return services.myCompanyService.uploadMainPhoto(imageFile).then(response => {
        commit('UpdateCompanyMainPhotoUri', response.data);
        return response;
      });
    },
    RetrieveCompanyReceivedResumes({ commit }) {
      return services.myCompanyService.getAllReceivedResumes().then(response => {
        commit('SetCompanyReceivedResumes', response.data);
        return response;
      });
    },
  },

  getters: {
    myCompanyMainInfo: (state) => state.myCompanyMainInfo,
    myCompanyPhotos: (state) => state.companyPhotos,
    myCompanyFilials: (state) => state.companyFilials,
    myCompanyVacancies: (state) => state.companyVacancies,
    myCompanyReceivedResumes: (state) => state.companyReceivedResumes,
  },
};


// function getServiceParamsForUpdatingMainInfo(formModel) {
//   const res = Object.assign({}, formModel);
//   res.industryId = res.industry ? res.industry.id : null;
//   delete res.industry;
//   return res;
// }

// function getServiceParamsForVacancy(formModel) {
//   const res = Object.assign({}, formModel);

//   setIdProperty(res, 'profession');
//   setIdProperty(res, 'employmentType');
//   setIdProperty(res, 'currency');
//   setIdProperty(res, 'companyFilial');

//   return res;
// }

function updateItem(state, item, items) {
  if (!items) return;
  const { id } = item;
  const index = items.findIndex(_ => _.id === id);
  if (index >= 0) {
    items[index] = Object.assign({}, items[index], item);
  } else {
    console.warn('No found item for updating.');
  }
}

function addItem(state, item, items) {
  if (!items) return;
  items.push(item);
}
