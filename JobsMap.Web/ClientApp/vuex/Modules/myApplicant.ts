import services from 'src/services';
import { deepClone } from 'src/utils/deep-utils';
import dateformat from 'dateformat';
import { getServiceParams } from 'src/utils/utils';

// for applicant
// todo, change methods
export default {
  state: {
    myApplicantMainInfo: null,
    applicantResumes: null,
    applicantPreferredAddresses: null,
    applicantSentResumes: null,
  },

  mutations: {
    SetApplicantMainInfo(state, myApplicantMainInfo) {
      state.myApplicantMainInfo = myApplicantMainInfo;
    },
    SetApplicantResumes(state, data) {
      state.applicantResumes = data;
    },
    UpdateApplicantResume(state, data) {
      updateItem(state, data, state.applicantResumes);
    },
    AddApplicantResume(state, data) {
      addItem(state, data, state.applicantResumes);
    },
    UpdateApplicantPreferredAddress(state, data) {
      updateItem(state, data, state.applicantPreferredAddresses);
    },
    AddApplicantPreferredAddress(state, data) {
      addItem(state, data, state.applicantPreferredAddresses);
    },
    SetApplicantPreferredAddresses(state, data) {
      state.applicantPreferredAddresses = data;
    },
    UpdateApplicantPhotoUri(state, url) {
      state.myApplicantMainInfo.photoUri = url;
    },
    SetApplicantSentResumes(state, data) {
      state.applicantSentResumes = data;
    },
  },

  actions: {
    RetrieveApplicantMainInfo({ commit }) {
      return services.myApplicantService.getMainInfo().then(response => {
        const { data } = response;
        commit('SetApplicantMainInfo', data);
        return data;
      });
    },
    RetrieveApplicantResumes({ commit }) {
      return services.myApplicantService.getAllResumes().then(response => {
        commit('SetApplicantResumes', response.data);
        return response;
      });
    },
    UpdateApplicantResume({ commit }, model) {
      model = deepClone(model);
      return services.myApplicantService.updateResume(getServiceParams(model)).then(response => {
        commit('UpdateApplicantResume', model);
        return response;
      });
    },
    AddApplicantResume({ commit }, model) {
      model = deepClone(model);
      return services.myApplicantService.addResume(getServiceParams(model)).then(response => {
        model.id = response.data;
        commit('AddApplicantResume', model);
        return response;
      });
    },
    RetrieveApplicantPreferredAddresses({ commit }) {
      return services.myApplicantService.getAllPreferredAddresses().then(response => {
        commit('SetApplicantPreferredAddresses', response.data);
        return response;
      });
    },
    UpdateApplicantPreferredAddress({ commit }, model) {
      model = deepClone(model);
      return services.myApplicantService.updatePreferredAddress(getServiceParamsForPreferredAddress(model)).then(response => {
        commit('UpdateApplicantPreferredAddress', model);
        return response;
      });
    },
    AddApplicantPreferredAddress({ commit }, model) {
      model = deepClone(model);
      return services.myApplicantService.addPreferredAddress(getServiceParamsForPreferredAddress(model)).then(response => {
        const { data } = response;
        model.id = data.id;
        model.address.formattedAddress = data.formattedAddress;
        commit('AddApplicantPreferredAddress', model);
        return response;
      });
    },
    UpdateApplicantMainInfo({ commit }, data) {
      return services.myApplicantService.update(getServiceParamsForUpdatingMainInfo(data)).then(response => {
        commit('SetApplicantMainInfo', data);
        return response;
      });
    },
    UploadApplicantPhoto({ commit }, imageFile) {
      return services.myApplicantService.uploadPhoto(imageFile).then(response => {
        commit('UpdateApplicantPhotoUri', response.data);
        return response;
      });
    },
    RetrieveApplicantSentResumes({ commit }) {
      return services.myApplicantService.getAllSentResumes().then(response => {
        commit('SetApplicantSentResumes', response.data);
        return response;
      });
    },
  },

  getters: {
    myApplicantMainInfo: (state) => state.myApplicantMainInfo,
    myApplicantResumes: (state) => state.applicantResumes,
    myApplicantPreferredAddresses: (state) => state.applicantPreferredAddresses,
    myApplicantSentResumes: (state) => state.applicantSentResumes,
  },
};


function getServiceParamsForUpdatingMainInfo(formModel) {
  const res = Object.assign({}, formModel);
  const { dateOfBirth } = res;
  if (dateOfBirth) {
    res.dateOfBirth = dateformat(dateOfBirth, 'yyyy-mm-dd');
  }
  return res;
}

function getServiceParamsForPreferredAddress(formModel) {
  const res = Object.assign({}, formModel);
  res.address = {
    longitude: res.address.longitude,
    latitude: res.address.latitude,
  };
  return res;
}

// function getServiceParamsForResume(formModel) {
//   const res = Object.assign({}, formModel);
//   setIdProperty(res, 'currency');
//   setIdProperty(res, 'profession');
//   return res;
// }

function updateItem(state, item, items) {
  if (!items) return;
  const { id } = item;
  const index = items.findIndex(_ => _.id === id);
  if (index >= 0) {
    items[index] = Object.assign({}, items[index], item);
  } else {
    console.warn('No found item for updating.');
  }
}

function addItem(state, item, items) {
  if (items) items.push(item);
}
