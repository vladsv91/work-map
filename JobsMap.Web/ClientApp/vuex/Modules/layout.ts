import { KeyValue } from 'src/shared/general-types';

const baseState = {
  isShowingDrawer: false,
  collapsedAddresses: false,
  mapHeight: 400,
};

if (window.isServer) {
  baseState.collapsedAddresses = true;
} else {
  let layoutData: string | KeyValue = localStorage.getItem('layout');
  if (layoutData) {
    try {
      layoutData = JSON.parse(layoutData);
    } catch (e) {
      layoutData = {};
    }
    const hop = Object.prototype.hasOwnProperty;
    for (const key in layoutData as KeyValue) {
      if (hop.call(layoutData, key)) {
        baseState[key] = layoutData[key];
      }
    }
  }
}

baseState.isShowingDrawer = false;

export default {
  state: baseState,

  mutations: {
    SetDrawerVisibility(state, value) {
      state.isShowingDrawer = value;
    },
    ToggleDrawerVisibility(state) {
      state.isShowingDrawer = !state.isShowingDrawer;
    },
    SetLayoutValue(state, [key, value]) {
      state[key] = value;
      const ld = Object.assign({}, state);
      delete ld.isShowingDrawer;
      localStorage.setItem('layout', JSON.stringify(ld));
    },
  },

  actions: {
    // RetrieveUserInfoOrFromCache()
  },

  getters: {},
};
