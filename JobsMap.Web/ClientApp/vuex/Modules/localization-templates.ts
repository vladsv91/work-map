import { Constants } from 'src/utils/constants.ts';

function pluralize(number: number, one: string, two: string, five?: string) {
  let n = Math.abs(number);

  five = five || two;

  n %= 100;
  if (n >= 5 && n <= 20) {
    return five;
  }
  n %= 10;
  if (n === 1) {
    return one;
  }
  if (n >= 2 && n <= 4) {
    return two;
  }
  return five;
}


export const LocatizationTemplates = {
  general: {
    genericErrorMessage: { ru: 'Произошла ошибка.', en: 'An error occurred.' },
    genericSuccessMessage: { ru: 'Операция завершена успешно.', en: 'Operation completed successfully.' },


    address: { ru: 'Адрес', en: 'Address' },
    name: { ru: 'Название', en: 'Name' },
    nameLabel: { ru: 'Название', en: 'Name' },
    namePh: { ru: 'Введите название', en: 'Enter name' },

    fullNameLabel: { ru: 'ФИО', en: 'Full name' },
    fullNamePh: { ru: 'Введите ФИО', en: 'Enter full name' },

    active: { ru: 'Активный', en: 'Active' },
    inactive: { ru: 'Неактивный', en: 'Inactive' },

    activate: { ru: 'Активировать', en: 'Activate' },
    deactivate: { ru: 'Деактивировать', en: 'Deactivate' },
    delete: { ru: 'Удалить', en: 'Delete' },

    // dateFromPh: { ru: 'Дата с', en: 'Date from' },
    // dateToPh: { ru: 'Дата по', en: 'Date to' },
    // dateFrom: { ru: 'от', en: 'from' },
    // dateTo: { ru: 'до', en: 'to' },
    // date: { ru: 'Дата', en: 'Date' },
    loginVerb: { ru: 'Войти', en: 'Login' },


    confirmation: { ru: 'Подтверждение', en: 'Confirmation' },

    apply: { ru: 'Применить', en: 'Apply' },
    send: { ru: 'Отправить', en: 'Send' },

    save: { ru: 'Сохранить', en: 'Save' },
    add: { ru: 'Добавить', en: 'Add' },
    next: { ru: 'Далее', en: 'Next' },

    edit: { ru: 'Редактировать', en: 'Edit' },
    more: { ru: 'еще', en: 'more' },

    cancel: { ru: 'Отмена', en: 'Cancel' },
    back: { ru: 'Назад', en: 'Back' },
    close: { ru: 'Закрыть', en: 'Close' },


    yes: { ru: 'Да', en: 'Yes' },
    no: { ru: 'Нет', en: 'No' },
    ok: { ru: 'Ок', en: 'Ok' },

    areYouSure: { ru: 'Вы уверены?', en: 'Are you sure?' },

    menu: { ru: 'Меню', en: 'Menu' },
    myProfileMenu: { ru: 'Меню моего профиля', en: 'My profile menu' },

    description: { ru: 'Описание', en: 'Description' },


    // auth
    emailLabel: { ru: 'E-mail', en: 'E-mail' },
    emailPh: { ru: 'Введите e-mail', en: 'Enter e-mail' },
    passwordLabel: { ru: 'Пароль', en: 'Password' },
    passwordPh: { ru: 'Введите пароль', en: 'Enter password' },

    professionLabel: { ru: 'Профессия', en: 'Profession' },

    vacancy: { ru: 'Вакансия', en: 'Vacancy' },
    company: { ru: 'Компания', en: 'Company' },
    resume: { ru: 'Резюме', en: 'Resume' },

    vacancies: { ru: 'Вакансии', en: 'Vacancies' },
    companies: { ru: 'Компании', en: 'Companies' },
    resumes: { ru: 'Резюме', en: 'Resumes' },
    filials: { ru: 'Филиалы', en: 'Filials' },
    photos: { ru: 'Фотографии', en: 'Photos' },
    filial: { ru: 'Филиал', en: 'Filial' },

    vacanciesCount: { ru: 'Количество вакансий', en: 'Vacancies count' },

    mainInfo: { ru: 'Основная информация', en: 'Main info' },


    noCompanies: { ru: 'Нету компаний', en: 'No companies' },
    noResumes: { ru: 'Нету резюме', en: 'No resumes' },
    noVacancies: { ru: 'Нету вакансий', en: 'No vacancies' },
    noPhotos: { ru: 'Нету фотографий', en: 'No photos' },


    skills: { ru: 'Навыки', en: 'Skills' },
    age: { ru: 'Возраст', en: 'Age' },
    radiusInKilometers: { ru: 'Радиус в км', en: 'Radius in kilometers' },
    enterNumber: { ru: 'Введите число', en: 'Enter number' },

    uploadFile: { ru: 'Загрузить файл', en: 'Upload file' },
    resumeFile: { ru: 'Файл резюме', en: 'Resume file' },

    downloadResumeFile: { ru: 'Скачать файл резюме', en: 'Download resume file' },
    viewResumeFile: { ru: 'Посмотреть файл резюме', en: 'View resume file' },

    messageLabel: { ru: 'Сообщение', en: 'Message' },

    sendResume: { ru: 'Отправить резюме', en: 'Send resume' },
    sendResumeErrorAuth: { ru: 'Войдите в систему как соискатель.', en: 'Please login as applicant.' },

    sent: { ru: 'Отправлено', en: 'Sent' },

    createdOnSite: { ru: 'Создано на сайте', en: 'Created on the site' },
    createdOn: { ru: 'Создано', en: 'Created on' },
    updatedOn: { ru: 'Обновлено', en: 'Updated on' },

    actions: { ru: 'Действия', en: 'Actions' },

    noData: { ru: 'Нет данных', en: 'No data' },
    main: { ru: 'Главный', en: 'Main' },

    experienceYears: { ru: 'Лет опыта', en: 'Experience years' },

    km: { ru: 'км', en: 'km' },
  },

  validationMessages: {
    error: { ru: 'Ошибка', en: 'Error' },
    required: { ru: 'Обязательное поле', en: 'Required field' },
    float: { ru: 'Должно быть числом', en: 'Must be number' },
    integer: { ru: 'Должно быть целым числом', en: 'Must be integer' },
    number: { ru: 'Должно быть числом', en: 'Must be number' },
    numeric: { ru: 'Должно быть числом', en: 'Must be number' },

    // lessThan: {
    //   ru(value) { return `Должно быть меньше чем ${value}`; },
    //   en(value) { return `Must less than ${value}`; },
    // },
    // lessThanOrEqualTo: {
    //   ru(value) { return `Должно быть меньше или равно ${value}`; },
    //   en(value) { return `Must less than or equal to ${value}`; },
    // },
    // greaterThan: {
    //   ru(value) { return `Должно быть больше чем ${value}`; },
    //   en(value) { return `Must greater than ${value}`; },
    // },
    greaterThanOrEqualTo: {
      ru({ value }) { return `Должно быть больше или равно ${value}`; },
      en({ value }) { return `Must greater than or equal to ${value}`; },
    },
    between: {
      ru({ min, max }) { return `Должно быть между ${min} и ${max}`; },
      en({ min, max }) { return `Must between ${min} and ${max}`; },
    },
    // size: {
    //   ru(value) { return `Размер должен быть ${value}`; },
    //   en(value) { return `Size must be ${value}`; },
    // },
    // length: {
    //   ru(value) { return `Длина должна быть ${value}`; },
    //   en(value) { return `Length must be ${value}`; },
    // },
    minValue: {
      ru({ min }) { return `Должно быть больше или равно ${min}`; },
      en({ min }) { return `Must greater than or equals to ${min}`; },
    },
    maxValue: {
      ru({ max }) { return `Должно быть меньше или равно ${max}`; },
      en({ max }) { return `Must less than or equals to ${max}`; },
    },
    minValueEx: {
      ru({ min }) { return `Должно быть больше ${min}`; },
      en({ min }) { return `Must greater than ${min}`; },
    },
    maxValueEx: {
      ru({ max }) { return `Должно быть меньше ${max}`; },
      en({ max }) { return `Must less than ${max}`; },
    },
    minLength: {
      ru({ min }) { return `Должно иметь как минимум ${min} символов`; },
      en({ min }) { return `Must have ${min} characters at least`; },
    },
    maxLength: {
      ru({ max }) { return `Должно иметь максимум ${max} символов`; },
      en({ max }) { return `Must have ${max} characters at most`; },
    },
    // todo, params right names
    lengthBetween: {
      ru({ min, max }) { return `Должно быть между ${min} и ${max}`; },
      en({ min, max }) { return `Length must between ${min} and ${max}`; },
    },
    // todo, params right names
    greaterDateThan: {
      ru({ dateFrom }) { return `Дата до должна быть больше чем ${dateFrom}`; },
      en({ dateFrom }) { return `Date to must greater than ${dateFrom}`; },
    },
    sameAs: { ru: 'Не соответствует', en: 'Not matched' },
    match: { ru: 'Не соответствует', en: 'Not matched' },
    regex: { ru: 'Неверный формат', en: 'Invalid format' },
    digit: { ru: 'Должно быть цифрой', en: 'Must be digit' },
    email: { ru: 'Неверный e-mail', en: 'Invalid email' },
    url: { ru: 'Неверная ссылка', en: 'Invalid url' },
  },

  titles: {
    namedRoutes: {

      // todo, titles
      Main: { ru: `${Constants.appName} - Сайт для поиска работы рядом с домом`, en: `${Constants.appName} - Site for job search near the house` },
      NearTheHouse: { ru: `работы рядом с домом`, en: `near the house` },
      CompaniesList: { ru: 'Компании', en: 'Companies' },
      CompanyDetails: { ru: 'Информация о компании', en: 'Company info' },
      CompanyDetailsFilials: { ru: 'Филиалы компании', en: 'Company filials' },
      CompanyDetailsVacancies: { ru: 'Вакансии компании', en: 'Company vacancies' },
      CompanyDetailsPhotos: { ru: 'Фотографии компании', en: 'Company photos' },
      VacanciesList: { ru: 'Вакансии', en: 'Vacancies' },
      VacancyDetails: { ru: 'Информация о вакансии', en: 'Vacancy info' },
      ResumesList: { ru: 'Резюме', en: 'Resumes' },
      ResumeDetails: { ru: 'Информация о резюме', en: 'Resume info' },
      MyProfile: { ru: 'Мой профиль', en: 'My profile' },
      AccountSettings: { ru: 'Настройки аккаунта', en: 'Account settings' },
      ChangingEmail: { ru: 'Смена email', en: 'Changing email' },
      MyCompanyInfo: { ru: 'Информация о моей компании', en: 'My company information' },
      MyCompanyFilials: { ru: 'Филиалы моей компании', en: 'My company filials' },
      AddFilial: { ru: 'Добавить филиал', en: 'Add filial' },
      EditFilial: { ru: 'Редактировать филиал', en: 'Edit filial' },
      MyCompanyVacancies: { ru: 'Вакансии моей компании', en: 'My company vacancies' },
      AddVacancy: { ru: 'Добавить вакансию', en: 'Add vacancy' },
      EditVacancy: { ru: 'Редактировать вакансию', en: 'Edit vacancy' },
      MyCompanyPhotos: { ru: 'Фотографии моей компании', en: 'My company photos' },
      MyReceivedResumes: { ru: 'Полученные резюме', en: 'Received resumes' },
      MyApplicantInfo: { ru: 'Информация обо мне', en: 'My info' },
      MyResumes: { ru: 'Мои резюме', en: 'My resumes' },
      AddResume: { ru: 'Добавить резюме', en: 'Add resume' },
      EditResume: { ru: 'Редактировать резюме', en: 'Edit resume' },
      MyPreferredAddresses: { ru: 'Мои предпочтительные адреса', en: 'My preferred addresses' },
      MyApplicantSentResumes: { ru: 'Отправленные резюме', en: 'Sent resumes' },
      AddPreferredAddress: { ru: 'Добавить предпочтительный адрес', en: 'Add preferred address' },
      EditPreferredAddress: { ru: 'Редактировать предпочтительный адрес', en: 'Edit preferred address' },
      SignUp: { ru: 'Зарегистрироваться', en: 'Sign up' },
      SignUpAsCompany: { ru: 'Зарегистрироваться как компания', en: 'Register as company' },
      SignUpAsApplicant: { ru: 'Зарегистрироваться как соискатель', en: 'Register as applicant' },
      ConfirmEmail: { ru: 'Подтверждение электонной почты', en: 'Email confirmation' },
      ResetPassword: { ru: 'Сброс пароля', en: 'Reset password' },
    },
  },

  errorMessages: {
    loggedout: { ru: 'Вы вышли из системы, пожалуйста войдите в систему.', en: 'You are logged out, please log in.' },
    forbidden: {
      ru: 'Запрещено! Пожалуйста, войдите в систему под другим пользователем.',
      en: 'Forbidden! Please sign in under a different user.',
    },
  },

  nearbyEntitiesCounts: {
    findJobNearTheHouse: { ru: 'Найти работу рядом с домом.', en: 'Find a Job near the house.' },

    header: {
      ru(radius) { return `В радиусе ${radius} километров есть:`; },
      en(radius) { return `Within a radius of ${radius} kilometers there are:`; },
    },
    companies: {
      ru(val) { return pluralize(val, 'компания', 'компании', 'компаний'); },
      en(val) { return pluralize(val, 'company', 'companies'); },
    },
    vacancies: {
      ru(val) { return pluralize(val, 'вакансия', 'вакансии', 'вакансий'); },
      en(val) { return pluralize(val, 'vacancy', 'vacancies'); },
    },
    applicants: {
      ru(val) { return pluralize(val, 'соискатель', 'соискателя', 'соискателей'); },
      en(val) { return pluralize(val, 'applicant', 'applicants'); },
    },
  },

  mainPage: {
    findJob: { ru: 'Найти работу?', en: 'Find Job?' },
    mainDescription: { ru: 'Сайт для поиска работы рядом с домом.', en: 'Site for job search near the house.' },
    features: {
      companiesSearch: {
        ru: 'Поиск компаний на карте с указанным радиусом.',
        en: 'Search for companies on the map with specified radius.',
      },
      vacanciesSearch: {
        ru: 'Поиск вакансий на карте с указанным радиусом.',
        en: 'Search for vacancies on the map with specified radius.',
      },
      resumesSearch: {
        ru: 'Поиск резюме на карте с указанным радиусом.',
        en: 'Search for resumes on the map with specified radius.',
      },

      companiesSearchHeader: {
        ru: 'Поиск компаний',
        en: 'Companies search',
      },
      vacanciesSearchHeader: {
        ru: 'Поиск вакансий',
        en: 'Vacancies search',
      },
      resumesSearchHeader: {
        ru: 'Поиск резюме',
        en: 'Resumes search',
      },

    },
  },

  searchCompaniesByAddresses: {
    currentMarkerCompany: { ru: 'Компания текущего маркера', en: 'Current marker company' },
    nearestCompaniesAndFilials: { ru: 'Ближайшие компании и филиалы', en: 'Nearest companies and filials' },
  },

  searchVacanciesByAddresses: {
    currentMarkerVacacny: { ru: 'Вакансия текущего маркера', en: 'Current marker vacancy' },
    nearestVacanciesAndFilials: { ru: 'Ближайшие вакансии и филиалы', en: 'Nearest vacancies and filials' },
  },

  searchList: {
    additionalFiltersHeader: { ru: 'Дополнительные фильтры', en: 'Additional filters' },
    filters: { ru: 'Фильтры', en: 'Filters' },
    searchVerb: { ru: 'Искать', en: 'Search' },
    searchDotted: { ru: 'Поиск...', en: 'Search...' },

    showOnMapOrFindByAddress: { ru: 'Показать на карте или найти по адресу', en: 'Show on map or find by address' },
    messageAboutDrag: {
      ru: 'Один красный маркер может заслонять другой макер, поэтому вы можете его перетащить в другое место.',
      en: 'One red marker can obscure another token, so you can drag it to another location.',
    },
    messageAboutMoreInfo: {
      ru: 'Кликните на маркер, чтобы узнать подробней информацию',
      en: 'Click on the marker to find out more information',
    },


    removeLocation: { ru: 'Удалить местоположение', en: 'Remove location' },

    moreThanEmployeeCountLabel: { ru: 'Больше, чем количество сотрудников', en: 'More than employee count' },
    lessThanEmployeeCountLabel: { ru: 'Меньше, чем количество сотрудников', en: 'Less than employee count' },

    moreThanExperienceYearsLabel: { ru: 'Больше, чем лет опыта', en: 'More than experience years' },
    lessThanExperienceYearsLabel: { ru: 'Менее, чем лет опыта', en: 'Less than experience years' },

    moreThanSalaryPerMonthLabel: { ru: 'Больше, чем заработная плата в месяц', en: 'More than salary per month' },
    lessThanSalaryPerMonthLabel: { ru: 'Меньше, чем заработная плата в месяц', en: 'Less than salary per month' },

    moreThanFilialsCountLabel: { ru: 'Больше, чем филиалов', en: 'More than filials' },
    lessThanFilialsCountLabel: { ru: 'Менее, чем филиалов', en: 'Less than filials' },

    moreThanVacanciesCountLabel: { ru: 'Больше, чем вакансий', en: 'More than vacancies count' },
    lessThanVacanciesCountLabel: { ru: 'Менее, чем вакансий', en: 'Less than vacancies count' },


    clickOnMapTip: { ru: 'Нажмите на карту, чтобы выбрать элементы в этом регионе.', en: 'Click on map to pick the items in that region.' },
  },

  contactUs: {
    header: { ru: 'Связаться с нами', en: 'Contact us' },

    fullNameLabel: { ru: 'Полное имя', en: 'Full name' },
    fullNamePh: { ru: 'Введите полное имя', en: 'Enter full name' },
    phoneNumberLabel: { ru: 'Номер телефона', en: 'Phone number' },
    phoneNumberPh: { ru: 'Введите номер телефона', en: 'Enter phone number' },
    messageLabel: { ru: 'Сообщение', en: 'Message' },

    successMessage: { ru: 'Отлично! Мы свяжемся с вами.', en: 'Excellent! We will contact you.' },
  },

  multiselects: {
    multiselect: {
      notFound: {
        ru: 'Не найдено элементов. Измените поисковый запрос.',
        en: 'No items found. Change the search query.',
      },
      noItems: {
        ru: 'Нету элементов.',
        en: 'There are no elements.',
      },
      noOtherItems: {
        ru: 'Нету других элементов.',
        en: 'There are no other elements.',
      },
      placeholder: { ru: 'Выберите вариант', en: 'Select option' },
    },
    address: {
      placeholder: { ru: 'Начните вводить адрес', en: 'Start typing the address' },
    },
    currency: {
      single: { ru: 'Выберите валюту', en: 'Select currency' },
      multiple: { ru: 'Выберите валюты', en: 'Select currencies' },
    },
    employmentType: {
      single: { ru: 'Выберите вид занятости', en: 'Select employment type' },
      multiple: { ru: 'Выберите виды занятости', en: 'Select employment types' },
    },
    myCompanyFilial: {
      single: { ru: 'Выберите филиал', en: 'Select filial' },
      multiple: { ru: 'Выберите филиалы', en: 'Select filials' },
    },
    industry: {
      single: { ru: 'Выберите индустрию', en: 'Select industry' },
      multiple: { ru: 'Выберите индустрии', en: 'Select industries' },
    },

    profession: {
      single: { ru: 'Выберите профессию', en: 'Select profession' },
      multiple: { ru: 'Выберите профессии', en: 'Select professions' },
    },

    myResume: {
      single: { ru: 'Выберите резюме', en: 'Select resume' },
      multiple: { ru: 'Выберите резюме', en: 'Select resumes' },

      noItems: {
        ru: 'Нету элементов. Создать',
        en: 'There are no elements. Create',
      },
    },
  },

  addressFields: {
    fullAddressLabel: { ru: 'Полный адресс', en: 'Full address' },
    fullAddressTip: {
      ru: 'Начните вводить полный адрес и другие поля будут заполнены автоматически.',
      en: 'Start typing full address and other fields will be filled automatically.',
    },

    countryLabel: { ru: 'Страна', en: 'Country' },
    cityLabel: { ru: 'Город', en: 'City' },
    streetLabel: { ru: 'Улица', en: 'Street' },
    streetNumberLabel: { ru: 'Номер улицы', en: 'Street number' },
  },

  header: {
    languagePh: { ru: 'язык', en: 'lang' },
    myProfile: { ru: 'Мой профиль', en: 'My profile' },
    logout: { ru: 'Выйти', en: 'Logout' },
    signUp: { ru: 'Зарегистрироваться', en: 'Sign up' },
    signIn: { ru: 'Войти', en: 'Sing in' },

    changedLanguageMessage: { ru: 'Для полного перевода сайта перезагрузите страницу.', en: 'For full site translation, reload the page.' },
  },

  signIn: {
    loginHeader: { ru: 'Войти', en: 'Login' },
    forgotPasswordHeader: { ru: 'Забыли пароль', en: 'Forgot password' },
    forgotPassword: {
      successMessage: { ru: 'Успех! Проверьте ваш e-mail.', en: 'Success! Check your e-mail.' },
    },

    resendEmail: { ru: 'Отправить повторно e-mail', en: 'Resend email' },
  },

  resetPassword: {
    header: { ru: 'Сброс пароля', en: 'Reset password' },

    successMessage1: { ru: 'Успех! Вы можете ', en: 'Success! You can ' },
    successMessage2: { ru: 'войти в систему.', en: 'login.' },
  },
  signUp: {
    header: { ru: 'Зарегистрироваться', en: 'Sign up' },
    asCompanyMenu: { ru: 'Как компания', en: 'As company' },
    asApplicantMenu: { ru: 'Как соискатель', en: 'As applicant' },

    confirmPasswordLabel: { ru: 'Подтвердите пароль', en: 'Confirm password' },

    signUpSuccessMessage: { ru: 'Успех, проверьте электронную почту.', en: 'Success, check your e-mail.' },

    asCompany: {
      header: { ru: 'Зарегистрироваться как компания', en: 'Register as company' },
      companyInfo: { ru: 'Информация о компании', en: 'Company info' },
      accountInfo: { ru: 'Информация об аккаунте', en: 'Account Info' },

      companyIdLabel: { ru: 'Id компании для URL', en: 'Company Id for URL' },
      companyIdPh: { ru: 'Введите Id компании', en: 'Enter company Id' },
      companyIdTip: {
        ru: 'Введите Id для URL, Если вы не введете, то значение подставится автоматически. Пример: my-company',
        en: 'Enter Id for the URL, If you do not enter, the value will be substituted automatically. Example: my-company',
      },

    },
    asApplicant: {
      header: { ru: 'Зарегистрироваться как соискатель', en: 'Register as applicant' },

      applicantInfo: { ru: 'Информация о соискателе', en: 'Applicant info' },
      accountInfo: { ru: 'Информация об аккаунте', en: 'Account Info' },

    },
  },

  pagination: {
    itemsPerPage: { ru: 'Значений на странице', en: 'Items per page' },
  },

  accountSettings: {

    changeEmail: {
      header: { ru: 'Сменить e-mail', en: 'Change e-mail' },

      newEmailLabel: { ru: 'Новый e-mail', en: 'New e-mail' },
      newEmailPh: { ru: 'Введите новый e-mail', en: 'Enter new e-mail' },

      successMessage: { ru: 'Успех, проверьте электронную почту.', en: 'Success, check your email.' },

      notCurrentValidationMessage: {
        ru: 'Новый email не должен быть таким как текущий email.',
        en: 'The new email does not have to be the same as the current email.',
      },
    },

    changingEmail: {
      header: { ru: 'Изменение электронной почты', en: 'Changing email' },

      loadingMessage: { ru: 'Подождите пока меняется е-mail', en: 'Please wait while email is changing.' },
      successMessage: { ru: 'Успех, электронная почта изменилась.', en: 'Success, email has changed.' },
      errorMessage: { ru: 'Ошибка смены email.', en: 'Error changing email.' },
    },

    changePassword: {
      header: { ru: 'Изменить пароль', en: 'Change password' },

      oldPasswordLabel: { ru: 'Старый пароль', en: 'Old password' },
      oldPasswordPh: { ru: 'Введите старый пароль', en: 'Enter old password' },

      newPasswordLabel: { ru: 'Новый пароль', en: 'New password' },
      newPasswordPh: { ru: 'Введите новый пароль', en: 'Enter new password' },

      confirmNewPasswordLabel: { ru: 'Подтвердите пароль', en: 'Confirm password' },
      confirmNewPasswordPh: { ru: 'Подтвердите новый пароль', en: 'Confirm new password' },

      successChangedPassword: { ru: 'Пароль успешно изменен.', en: 'Password changed successfully.' },
    },
  },

  // generic
  company: {
    employeeCountLabel: { ru: 'Количество сотрудников', en: 'Employee count' },
    employeeCountPh: { ru: 'Введите количество сотрудников', en: 'Enter employee count' },

    industryLabel: { ru: 'Индустрия', en: 'Industry' },
    filialLabel: { ru: 'Филиал', en: 'Filial' },

    contactPhoneLabel: { ru: 'Контактный телефон', en: 'Contact phone' },
    contactPhonePh: { ru: 'Введите контактный телефон', en: 'Enter contact phone' },

    contactEmailLabel: { ru: 'Контактный e-mail', en: 'Contact email' },
    contactEmailPh: { ru: 'Введите контактный e-mail', en: 'Enter contact email' },

    websiteLabel: { ru: 'Веб-сайт', en: 'Website' },
    employmentTypeLabel: { ru: 'Вид занятости', en: 'Employment type' },

    filials: {
      edit: {
        editHeader: { ru: 'Редактировать филиал', en: 'Edit filial' },
        addHeader: { ru: 'Добавить филиал', en: 'Add filial' },

        cannotSaveTip: { ru: 'Нельзя обновить неактивный филиал', en: 'Can not update an inactive filial' },
      },
    },
  },
  companies: {
    // todo, localize
    details: {},
  },
  applicant: {
    // todo, rid of copy/paste of code
    contactPhoneLabel: { ru: 'Контактный телефон', en: 'Contact phone' },
    contactPhonePh: { ru: 'Введите контактный телефон', en: 'Enter contact phone' },

    contactEmailLabel: { ru: 'Контактный e-mail', en: 'Contact email' },
    contactEmailPh: { ru: 'Введите контактный e-mail', en: 'Enter contact email' },

    birthDateLabel: { ru: 'Дата рождения', en: 'Birth date' },
    birthDatePh: { ru: 'Выберите дату рождения', en: 'Select birth date' },

    preferredAddresses: {
      edit: {
        editHeader: { ru: 'Редактировать предпочтительный адрес', en: 'Edit preferred address' },
        addHeader: { ru: 'Добавить предпочтительный адрес', en: 'Add preferred address' },

        radiusInKilometersLabel: { ru: 'Радиус в километрах', en: 'Radius in kilometers' },
        radiusInKilometersPh: { ru: 'Введите радиус в километрах', en: 'Enter radius in kilometers' },
      },
      map: {
        header: { ru: 'Адреса на карте', en: 'Addresses on the map' },
        tipAboutAbilityToAdding: {
          ru: 'Вы можете добавить предпочтительный адрес расположения работы кликнув по карте.',
          en: 'You can add the preferred work location address by clicking on the map.',
        },
      },
    },
  },
  resumes: {
    edit: {
      editHeader: { ru: 'Редактировать резюме', en: 'Edit resume' },
      addHeader: { ru: 'Добавить резюме', en: 'Add resume' },
      cannotSaveTip: { ru: 'Нельзя обновить неактивное резюме', en: 'Can not update an inactive resume' },

      experienceYearsLabel: { ru: 'Количество лет опыта в данной сфере', en: 'Number of years of your experience in this sphere' },
      experienceYearsPh: { ru: 'Введите количество лет опыта', en: 'Enter the number of years of your experience' },

      salaryPerMonthLabel: { ru: 'Зарплата в месяц', en: 'Salary per month' },
      salaryPerMonthPh: { ru: 'Введите зарплату', en: 'Enter salary' },

      currencyLabel: { ru: 'Валюта', en: 'Currency' },

      descriptionLabel: { ru: 'Описание', en: 'Description' },
      skillsLabel: { ru: 'Ваши навыки', en: 'Your skills' },
    },
    details: {
      aboutApplicant: { ru: 'О соискателе', en: 'About applicant' },
    },
  },
  vacancies: {
    edit: {
      editHeader: { ru: 'Редактировать вакансию', en: 'Edit vacancy' },
      addHeader: { ru: 'Добавить вакансию', en: 'Add vacancy' },

      employmentTypeLabel: { ru: 'Вид занятости', en: 'Employment type' },

      salaryPerMonthLabel: { ru: 'Зарплата в месяц', en: 'Salary per month' },
      salaryPerMonthPh: { ru: 'Введите зарплату', en: 'Enter salary' },

      currencyLabel: { ru: 'Валюта', en: 'Currency' },

      descriptionLabel: { ru: 'Описание', en: 'Description' },
      requirementsLabel: { ru: 'Требования', en: 'Requirements' },
      dutiesLabel: { ru: 'Обязанности', en: 'Duties' },
      workingConditionsLabel: { ru: 'Рабочие условия', en: 'Working conditions' },

      cannotSaveTip: { ru: 'Нельзя обновить неактивную вакансию', en: 'Can not update an inactive vacancy' },
    },
  },
  myProfile: {
    header: { ru: 'Мой профиль', en: 'My profile' },
    menuItems: {
      myCompanyInfo: { ru: 'Информация о компании', en: 'Company info' },
      myCompanyPhotos: { ru: 'Фотографии компании', en: 'Company photos' },
      myCompanyFilials: { ru: 'Филиалы', en: 'Filials' },
      myCompanyVacancies: { ru: 'Вакансии', en: 'Vacancies' },
      myCompanyReceivedResumes: { ru: 'Отклики на вакансии', en: 'Responses to vacancies' },
      accountSettings: { ru: 'Настройки аккаунта', en: 'Account settings' },

      myApplicant: {
        info: { ru: 'Моя информация', en: 'My info' },
        myResumes: { ru: 'Мои резюме', en: 'My resumes' },
        myPreferredAddresses: { ru: 'Мои предпочтительные адреса', en: 'My preferred addresses' },
        mySentResumes: { ru: 'Отправленные резюме', en: 'Sent resumes' },
      },
    },
    myProfilePage: {
      header: { ru: 'Мой профиль', en: 'My profile' },
    },
  },
  mySentResumes: {
    unviewedByCompany: { ru: 'не просмотрено компанией', en: 'unviewed by company' },
    viewedByCompany: { ru: 'просмотрено компанией', en: 'viewed by company' },
    rejectedByCompany: { ru: 'отклонено компанией', en: 'rejected by company' },
  },
  editCompanyProfile: {
    noPhoto: { ru: 'Нет фото', en: 'No photo' },
    websiteLabel: { ru: 'Веб-сайт', en: 'Website' },
    websitePh: { ru: 'Введите url веб-сайта', en: 'Enter website url' },
    descriptionLabel: { ru: 'Описание', en: 'Description' },
  },
  editApplicantProfile: {
    noPhoto: { ru: 'Нет фото', en: 'No photo' },
    websiteLabel: { ru: 'Веб-сайт', en: 'Website' },
    websitePh: { ru: 'Введите url веб-сайта', en: 'Enter website url' },
    descriptionLabel: { ru: 'О себе', en: 'About yourself' },
  },
  confirmEmail: {
    header: { ru: 'Подтверждение электонной почты', en: 'Email confirmation' },
    loadingMessage: { ru: 'Подождите, пока письмо будет подтверждено.', en: 'Please wait, while email is confirming.' },

    errorMessage1: { ru: 'Ошибка подтверждения электронной почты!', en: 'Error confirmation email!' },
    errorMessage2: { ru: 'Вы можете повторно отправить письмо на электронную почту.', en: 'You can resend email.' },

    successResendingMessage: { ru: 'Успех, проверьте свой адрес электронной почты.', en: 'Success, check your email.' },

    resendEmail: { ru: 'Отправить повторно', en: 'Resend Email' },

    confirmationSuccessMessage: { ru: 'Успешное подтверждения e-mail.', en: 'Successful email confirmation.' },
  },
};


export default LocatizationTemplates;
