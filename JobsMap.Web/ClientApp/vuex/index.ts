import Vue from 'vue';
import Vuex from 'vuex';

import localization from './Modules/localization';
import account from './Modules/account';
import layout from './Modules/layout';
import myCompany from './Modules/myCompany';
import myProfile from './Modules/myProfile';
import myApplicant from './Modules/myApplicant';
import Utils from 'src/utils/utils';
import { KeyValue } from 'src/shared/general-types';
import { RootState } from './store.models';

Vue.use(Vuex);

Object.defineProperty(Vue.prototype, '$g', {
  get() { return this.$store.getters; },
});

Vue.prototype.$commit = function (...args: any[]) {
  return this.$store.commit(...args);
};

const initialState: RootState = {
  isOpenedModal: false,
  headerClass: '',
};

const mutations = {
  // Set${stateKey};
};


Utils.objectForEach(initialState, (key) => {
  // const mutationMethodName = `Set${Utils.capitalize(key)}`;
  const mutationMethodName = key;
  mutations[mutationMethodName] = (state: KeyValue, val: any) => {
    state[key] = val;
  };
});


export default new Vuex.Store<RootState>({
  state: initialState,
  mutations,
  modules: {
    localization,
    account,
    layout,
    myCompany,
    myProfile,
    myApplicant,
  },
});
