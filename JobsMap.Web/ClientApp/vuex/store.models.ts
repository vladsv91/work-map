export interface RootState {
  isOpenedModal: boolean;
  headerClass: string;
}