import Vue, { VNode } from 'vue';
import { Commit, Store } from 'vuex';
import LocatizationTemplates from 'src/vuex/Modules/localization-templates';
import { Constants } from 'src/utils/constants';
import { AxiosError } from 'axios';


declare module 'vue/types/vue' {
  interface Vue {
    $bus: Vue;
    showServiceError: (response: string | { data: object } | AxiosError) => void;
    showServiceSuccess: (response: string | { data: object } | AxiosError) => void;
    isInvalid: boolean;
    isValid: boolean;
    validateAndApply: () => any;
    $commit: Commit;

    $t: typeof LocatizationTemplates;
    $tg: typeof LocatizationTemplates.general;
    $consts: typeof Constants;

    // TODO: type set
    $g: any;

  }
}



declare module 'vue/types/options' {
  interface ComponentOptions<V extends Vue> {
    validations?: () => {},
  }
}

