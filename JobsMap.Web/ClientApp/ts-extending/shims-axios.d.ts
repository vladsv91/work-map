import * as s from 'axios';

declare module 'axios' {
  interface AxiosRequestConfig {
    dontUseExpirationHandler?: boolean;
    disabledAuthHeaders?: boolean;
  }
}
