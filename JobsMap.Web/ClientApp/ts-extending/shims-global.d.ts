import Vue, { VNode } from 'vue';
import { KeyValue, Func } from 'src/shared/general-types';
import { Constants } from 'src/utils/constants';
import { EnvVars } from 'src/shared/envVars.model';
import { ServerDataModel } from 'src/shared/server-data.model';



declare global {
  namespace JSX {
    interface Element extends VNode { }
    interface ElementClass extends Vue { }
    interface IntrinsicElements {
      [elem: string]: any
    }
  }


  // global env variables
  const envVars: EnvVars;

  interface Window {

    // TODO: move to vuex
    entityDataFromServer: KeyValue;
    companyEntityDataFromServer: KeyValue;

    serverData: ServerDataModel;

    // TODO: desirable remove these
    vm: Vue;
    isServer: boolean;
    langId: string;



    // for google
    initMap: Func;
    MarkerClusterer: typeof MarkerClusterer;

    // for ssr
    __INITIAL_STATE__: KeyValue;



    // FOR poliffyls
    Object: ObjectConstructor;
    Array: ArrayConstructor;
    Promise: PromiseConstructor;
  }

  namespace NodeJS {

    interface Global {
      window: Window;

      navigator: Navigator;
      document: Document;
      Element: Element;

    }
  }
}
