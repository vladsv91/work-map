// http://eslint.org/docs/user-guide/configuring
const isProd = process.env.NODE_ENV === 'production';
module.exports = {
  root: true,
  // parser: 'babel-eslint',
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module',
    ecmaVersion: 6,
    ecmaFeatures: {
      jsx: true
    }
  },
  env: {
    browser: true,
  },
  // extends: 'airbnb-base',

  extends: [
    'plugin:vue/base',
    // 'plugin:vue/recommended'
  ],


  parser: 'vue-eslint-parser',
  // required to lint *.vue files
  plugins: [
    'vue',
    // 'html'
  ],
  // check if imports actually resolve
  'settings': {
    'import/resolver': {
      'webpack': {
        'config': 'webpack.config.js'
      }
    }
  },
  // add your custom rules here
  'rules': {
    "prefer-destructuring": ["error", {
      "array": false,
      "object": true
    }],

    'linebreak-style': 0,
    'newline-per-chained-call': 0,
    'space-before-function-paren': 0,
    'object-shorthand': 0,
    'consistent-return': 0,
    'no-cond-assign': 0,
    'curly': 0,
    'no-param-reassign': 0,
    'no-use-before-define': 0,
    'no-plusplus': 0,
    'no-multi-assign': 0,
    'func-names': 0,
    'no-confusing-arrow': 0,
    'arrow-body-style': 0,
    'no-return-assign': 0,
    'arrow-parens': 0,
    'no-nested-ternary': 0,
    'max-len': ['error', 150],
    'no-restricted-syntax': ["error", "WithStatement"],
    'no-mixed-operators': 0,
    'function-paren-newline': 0,
    'object-curly-newline': 0,
    'import/no-named-as-default': 0,
    'object-curly-spacing': 0,
    'prefer-rest-params': 0,
    'no-bitwise': 0,
    'no-underscore-dangle': 0,
    'prefer-spread': 0,
    'object-property-newline': 0,
    'no-unused-vars': ["error", { "argsIgnorePattern": "^h$" }],
    'prefer-destructuring': 0,
    'guard-for-in': 0,

    // 'no-debugger': isProd ? 2 : 0,
    // 'no-console': isProd ? 1 : 0,
    'no-debugger': 0,
    'no-console': 0,
    'import/prefer-default-export': 0,
    'implicit-arrow-linebreak': 0,
    'nonblock-statement-body-position': 0,
    'operator-linebreak': 0,
    'no-else-return': 0,
    'import/no-cycle': 0,
  }
}
