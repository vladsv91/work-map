using System;
using System.Linq;
using System.Text;
using AutoMapper;
using BusinessLogic.Models.MyAccount;
using BusinessLogic.Services;
using BusinessLogic.Services.ForAuthorized;
using BusinessLogic.Services.ForAuthorized.ForApplicantRole;
using BusinessLogic.Services.ForAuthorized.ForCompanyRole;
using BusinessLogic.Utils;
using Data;
using Data.Entities.Account;
using Data.Entities.Common;
using FluentValidation.AspNetCore;
using Google.Maps;
using Infrastructure;
using JobsMap.Web.Custom;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;


namespace JobsMap.Web
{
    public class RedirectToNonWwwRule : IRule
    {
       public virtual void ApplyRule(RewriteContext context)
       {
           var req = context.HttpContext.Request;
           if (req.Host.Value.StartsWith("www.", StringComparison.OrdinalIgnoreCase))
           {
               var host = new HostString(req.Host.Value.Substring(4));
               var newUrl = UriHelper.BuildAbsolute(req.Scheme, host, req.PathBase, req.Path, req.QueryString);
               var response = context.HttpContext.Response;
               response.StatusCode = StatusCodes.Status301MovedPermanently;
               response.Headers[HeaderNames.Location] = newUrl;
               context.Result = RuleResult.EndResponse;
               return;
           }

           context.Result = RuleResult.ContinueRules;
       }
    }


    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", true, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true, true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();

            var emailCredentials = Configuration.GetSection("EmailCredentials");
            var contactEmails = Configuration.GetSection("ContactEmails");
            Settings.EmailCredentials.Email = emailCredentials["Email"];
            Settings.EmailCredentials.Password = emailCredentials["Password"];
            Settings.EmailCredentials.Host = emailCredentials["Host"];

            Settings.ContactEmails.AdminEmail = contactEmails["AdminEmail"];




            var authOptions = Configuration.GetSection("AuthOptions");
            AppConstants.AuthOptions.Issuer = authOptions["Issuer"];
            AppConstants.AuthOptions.Audience = authOptions["Audience"];
            AppConstants.AuthOptions.Key = authOptions["Key"];
            AppConstants.AuthOptions.Lifetime = authOptions.GetValue<double>("Lifetime");
            AppConstants.AuthOptions.RefreshTokenLifetimeDays = authOptions.GetValue<int>("RefreshTokenLifetimeDays");


            AppConstants.AppName = Configuration["AppName"];
            AppConstants.GoogleApiKey = Configuration["GoogleApiKey"];
            AppConstants.UseNodeSsr = Configuration.GetValue<bool>(nameof(AppConstants.UseNodeSsr));

            Settings.AppSettings = Configuration.GetSection("AppSettings").Get<AppSettings>();
        }

        public IConfigurationRoot Configuration { get; }

        public static IdentityBuilder AddIdentity<TUser, TRole>(IServiceCollection services)
            where TUser : class where TRole : class
        {
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddScoped<IUserValidator<TUser>, UserValidator<TUser>>();
            services.TryAddScoped<IPasswordValidator<TUser>, PasswordValidator<TUser>>();
            services.TryAddScoped<IPasswordHasher<TUser>, PasswordHasher<TUser>>();
            services.TryAddScoped<ILookupNormalizer, UpperInvariantLookupNormalizer>();
            services.TryAddScoped<IRoleValidator<TRole>, RoleValidator<TRole>>();
            services.TryAddScoped<IdentityErrorDescriber>();
            services.TryAddScoped<ISecurityStampValidator, SecurityStampValidator<TUser>>();
            services.TryAddScoped<IUserClaimsPrincipalFactory<TUser>, UserClaimsPrincipalFactory<TUser, TRole>>();
            services.TryAddScoped<UserManager<TUser>, AspNetUserManager<TUser>>();
            services.TryAddScoped<SignInManager<TUser>, SignInManager<TUser>>();
            services.TryAddScoped<RoleManager<TRole>, AspNetRoleManager<TRole>>();

            return new IdentityBuilder(typeof(TUser), typeof(TRole), services);
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connection = Configuration.GetConnectionString("DefaultConnection");
            var logsDbConnection = Configuration.GetConnectionString("LogsDbConnection");

            services.AddDbContext<JobsMapContext>(options => options.UseSqlServer(connection));
            services.AddDbContext<JobsMapLogsContext>(options => options.UseSqlServer(logsDbConnection));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddFluentValidation(fvc =>
            {
                fvc.RegisterValidatorsFromAssemblyContaining<RegisterVM>();
                //fvc.ImplicitlyValidateChildProperties = true; todo, mb
            });
            services.AddAutoMapper();
            Mapper.Initialize(cfg =>
                cfg.AddProfiles("BusinessLogic")
            );

            services.AddMemoryCache();


            services.AddScoped<IMyApplicantService, MyApplicantService>();
            services.AddScoped<IMyCompanyService, MyCompanyService>();
            services.AddScoped<IAccountService, AccountService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            //services.AddScoped<IUnitOfWork>(a =>
            //{
            //    var db  = a.GetService<JobsMapContext>();
            //    return new UnitOfWork(db);
            //});
            services.AddSingleton<ICurrentUser, CurrentUser>();
            services.AddSingleton<IMailService, MailService>();
            services.AddScoped<UserManager<ApplicationUser>, UserManager<ApplicationUser>>();
            services.AddScoped<DbContext, JobsMapContext>();
            //services.AddScoped(typeof(IRepository<>), typeof(RepositoryGeneric<,>));
            //services.AddScoped(typeof(IRepositoryGuid<>), typeof(RepositoryGeneric<,>));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IRepositoryGuid<>), typeof(RepositoryGuid<>));


            services.AddScoped(typeof(IRepositoryGuid<>), typeof(RepositoryGuid<>));

            services.AddScoped<ICompaniesService, CompaniesService>();
            services.AddScoped<ILocationsServiceService, LocationsServiceService>();
            services.AddScoped<IVacanciesService, VacanciesService>();
            services.AddScoped<IResumesService, ResumesService>();
            services.AddScoped<IContactUsService, ContactUsService>();
            services.AddScoped<IApplicantSendingResumeService, ApplicantSendingResumeService>();
            services.AddScoped<IMyCompanySendingResumeService, MyCompanySendingResumeService>();
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IMyCompanyFilialsService, MyCompanyFilialsService>();
            services.AddScoped<IMyCompanyVacanciesService, MyCompanyVacanciesService>();
            services.AddScoped<IMyCompanyPhotosService, MyCompanyPhotosService>();

            services.AddScoped<ICustomLogger>(a =>
            {
                var db = a.GetService<JobsMapLogsContext>();

                return new DbLogger(
                    a.GetService<ICurrentUser>(),
                    new RepositoryGuid<Log>(db),
                    new UnitOfWork(db));
            });

            // services.Configure<IISOptions>(options =>
            // {
            //     options.ForwardClientCertificate = false;
            // });

            services.AddEntityFrameworkNpgsql().AddDbContext<JobsMapPostgresContext>();




            AddIdentity<ApplicationUser, IdentityRole<string>>(services).AddEntityFrameworkStores<JobsMapContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireLowercase = false;
                options.Password.RequiredUniqueChars = 1;
            });


            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        // укзывает, будет ли валидироваться издатель при валидации токена
                        ValidateIssuer = true,
                        // строка, представляющая издателя
                        ValidIssuer = AppConstants.AuthOptions.Issuer,

                        // будет ли валидироваться потребитель токена
                        ValidateAudience = true,
                        // установка потребителя токена
                        ValidAudience = AppConstants.AuthOptions.Audience,
                        // будет ли валидироваться время существования
                        ValidateLifetime = true,

                        // установка ключа безопасности
                        IssuerSigningKey =
                            new SymmetricSecurityKey(Encoding.ASCII.GetBytes(AppConstants.AuthOptions.Key)),
                        // валидация ключа безопасности
                        ValidateIssuerSigningKey = true
                    };
                });


            //services.AddLocalization(options => options.ResourcesPath = "Resources");


        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true,
                    EnvParam = new
                    {
                        useNodeSsr = AppConstants.UseNodeSsr,
                        appName = AppConstants.AppName,
                    },
                });
            }
            else {
                app.UseExceptionHandler("/");
            }


            app.Use(async (context, next) =>
            {
                try
                {
                    await next();
                }
                catch (Exception e)
                {
                    await context.GetLogger().LogAsync(LogType.Error, ApiExceptionHandlerAttribute.GetExceptionFullMessage(e), context);
                    throw;
                }
            });

            app.UseRewriter(new RewriteOptions
            {
               Rules =
               {
                   new RedirectToNonWwwRule()
               }
            });

            var cultList = AppCultures.CulturesList.Select(_ => _.CultureInfo).ToArray();
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(AppCultures.DefaultCulture.CultureInfo),
                SupportedCultures = cultList,
                SupportedUICultures = cultList,
                RequestCultureProviders = AppCulturesWeb.RequestCultureProviders.OfType<IRequestCultureProvider>().ToArray(),
            });


            app.UseStaticFiles();




            app.UseAuthentication();

            app.Use(async (context, next) =>
            {
                await context.GetLogger().LogAsync(LogType.Info, "All Requests", context);
                await next();
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Home}/{action=Index}/{id?}");

                app.MapWhen(x => !x.Request.Path.Value.StartsWith("/api/"), builder =>
                {
                    builder.UseMvc(route =>
                    {
                        route.MapSpaFallbackRoute(
                            "spa-fallback",
                            new {controller = "Home", action = "Index"});
                    });
                });
            });

            GoogleSigned.AssignAllServices(new GoogleSigned(AppConstants.GoogleApiKey));

            Schedules.RegisterJobs(app.ApplicationServices);


#if DEBUG
            //DouStartActionsTemporary.AddCompanies();
            //DouStartActionsTemporary.ChangeAddresses().Wait();
            //DouStartActionsTemporary.SetCompaniesContacts();

            //HhStartActionsTemporary.AddCompaniesWithVacancies();
            //HhStartActionsTemporary.SetVacanciesProfessions();
#endif
        }
    }
}
