using Data.Entities.Common;
using JobsMap.Web.Custom;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;

namespace JobsMap.Web.Controllers
{
    //[Culture]
    public class BaseController : Controller
    {
        [NonAction]
        protected IActionResult ImageResult(FileData fileData)
        {
            return FileResultBase(fileData, "image/generic", "Image");
        }

        [NonAction]
        protected IActionResult DocumentResult(FileData fileData)
        {
            return FileResultBase(fileData, "application/pdf", "Document");
        }

        [NonAction]
        private IActionResult FileResultBase(FileData fileData, string contentType, string fileName)
        {
            return File(fileData.Data, contentType, fileData.UpdatedOnUtc, EntityTagHeaderValue.Any);
            //return File(fileData.Data, contentType, $"{fileName}{fileData.FileExtension}", fileData.UpdatedOnUtc, EntityTagHeaderValue.Any);
        }
    }
}
