using System.Threading.Tasks;
using BusinessLogic.Models.Common;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;

namespace JobsMap.Web.Controllers.api
{
    public class LocationController : BaseApiController
    {
        private readonly ILocationsServiceService _locationsServiceService;

        public LocationController(ILocationsServiceService locationsServiceService)
        {
            _locationsServiceService = locationsServiceService;
        }

        [HttpGet]
        public async Task<IActionResult> GetNearbyEntitiesCounts([FromQuery] AddressLtLnVM filter)
        {
            var res = await _locationsServiceService.GetEntitiesCountsNearLocationAsync(filter.Latitude, filter.Longitude);
            return Ok(res);
        }
    }
}
