using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.Companies;
using BusinessLogic.Services;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace JobsMap.Web.Controllers.api
{
    public class CompaniesController : BaseApiController
    {
        private readonly ICompaniesService _companiesService;

        public CompaniesController(ICompaniesService companiesService)
        {
            _companiesService = companiesService;
        }

        [HttpGet]
        public async Task<IActionResult> GetByFilter([FromQuery] CompaniesListFilterReadVM filter)
        {
            var companies = await _companiesService.GetCompaniesByFilterAsync(filter);
            return Ok(companies);
        }

        [HttpGet]
        public async Task<IActionResult> GetDetails([FromQuery] string companyId)
        {
            if (!ValidateStringId(companyId, nameof(companyId))) return BadRequestModel();
            var companyDetails = await _companiesService.GetCompanyDetailsAsync(companyId);
            return Ok(Mapper.Instance.Map<CompanyFullInfoReadVM>(companyDetails));
        }

        [HttpGet]
        public async Task<IActionResult> GetFilials([FromQuery] string companyId)
        {
            if (!ValidateStringId(companyId, nameof(companyId))) return BadRequestModel();
            var companyFilials = await _companiesService.GetCompanyFilialsAsync(companyId);
            return Ok(Mapper.Instance.Map<List<CompanyFilialReadVM>>(companyFilials));
        }


        [HttpGet]
        public async Task<IActionResult> GetVacancies([FromQuery] string companyId)
        {
            if (!ValidateStringId(companyId, nameof(companyId))) return BadRequestModel();
            var companyVacancies = await _companiesService.GetCompanyVacanciesAsync(companyId);
            return Ok(Mapper.Instance.Map<List<CompanyVacancyShortInfoReadVM>>(companyVacancies));
        }


        [HttpGet]
        public async Task<IActionResult> GetPhotos([FromQuery] string companyId)
        {
            if (!ValidateStringId(companyId, nameof(companyId))) return BadRequestModel();
            var companyPhotos = await _companiesService.GetCompanyPhotosAsync(companyId);
            return Ok(Mapper.Instance.Map<List<CompanyPhotoReadVM>>(companyPhotos));
        }


        [HttpGet(AppConstants.FilesUrls.CompanyMainPhotoUrl+ "/" + "{companyId}")]
        public async Task<IActionResult> GetMainPhoto([FromRoute] string companyId)
        {
            if (!ValidateStringId(companyId, nameof(companyId))) return BadRequestModel();
            var fileData = await _companiesService.GetCompanyMainPhotoAsync(companyId);
            return ImageResult(fileData);
        }

        [HttpGet(AppConstants.FilesUrls.CompanyPhotoUrl + "/" + "{companyPhotoId}")]
        public async Task<IActionResult> GetPhoto([FromRoute] string companyPhotoId)
        {
            if (!ValidateStringId(companyPhotoId, nameof(companyPhotoId))) return BadRequestModel();
            var fileData = await _companiesService.GetCompanyPhotoAsync(companyPhotoId);
            // todo, not copy/paste
            return ImageResult(fileData);
        }
    }
}
