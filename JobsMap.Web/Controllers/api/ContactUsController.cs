using System.Threading.Tasks;
using BusinessLogic.Models.Common;
using BusinessLogic.Services;
using BusinessLogic.Utils;
using Microsoft.AspNetCore.Mvc;

namespace JobsMap.Web.Controllers.api
{
    public class ContactUsController : BaseApiController
    {
        private readonly IContactUsService _contactUsService;

        public ContactUsController(IContactUsService contactUsService)
        {
            _contactUsService = contactUsService;
        }


        [HttpPost]
        public async Task<IActionResult> Create([FromBody] ContactUsCreateVM model)
        {
            await _contactUsService.Create(model, HttpContext.GetAbsoluteUrl());
            return Ok();
        }
    }
}
