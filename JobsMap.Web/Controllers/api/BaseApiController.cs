using System;
using BusinessLogic.Utils;
using FluentValidation;
using JobsMap.Web.Custom;
using Microsoft.AspNetCore.Mvc;

namespace JobsMap.Web.Controllers.api
{
    [Route("api/[controller]/[action]")]
    [ApiExceptionHandler]
    [ValidationHandler]
    public class BaseApiController : BaseController
    {
        [NonAction]
        protected IActionResult BadRequestModel()
        {
            return WebAppUtils.GetBadRequestResult(ModelState);
        }

        protected bool ValidateStringId(string itemId, string paramName)
        {
            return ValidateBase(new StringIdValidator(paramName), itemId);
        }

        protected bool ValidateStringGuid(string value, string paramName)
        {
            return ValidateBase(new StringGuidValidator(paramName), value);
        }

        private bool ValidateBase<T>(IValidator<T> validator, T value)
        {

            var vr = validator.Validate(value);
            if (vr.IsValid) return true;
            foreach (var validationFailure in vr.Errors) {
                ModelState.AddModelError(validationFailure.PropertyName, validationFailure.ErrorMessage);
            }
            return false;
        }


        private abstract class BaseStringValidator : AbstractValidator<string>
        {
            protected IRuleBuilderOptions<string, string> RuleForString(string paramName)
            {
                return RuleFor(_ => _).NotNull().NotEmpty().Configure(_ => _.PropertyName = paramName);
            }
        }

        private class StringIdValidator : BaseStringValidator
        {
            public StringIdValidator(string paramName)
            {
                RuleForString(paramName).MinimumLength(2)
                    .MaximumLength(100)
                    .Must(_ => _ != null && !_.Contains(" "))
                    .WithMessage("Must be not empty.");
            }
        }

        private class StringGuidValidator : BaseStringValidator
        {
            public StringGuidValidator(string paramName)
            {
                RuleForString(paramName).LengthBetween(32, 38)
                    .Must(_ => _ != null && Guid.TryParse(_, out var dummy)).WithMessage("Must be Guid.");
            }
        }
    }
}
