using Microsoft.AspNetCore.Authorization;

namespace JobsMap.Web.Controllers.api.ForAuthorized
{
    [Authorize]
    public class BaseAuthorizeApiController : BaseApiController { }
}
