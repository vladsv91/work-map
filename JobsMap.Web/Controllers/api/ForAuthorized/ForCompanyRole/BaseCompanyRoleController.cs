using Infrastructure;
using Microsoft.AspNetCore.Authorization;

namespace JobsMap.Web.Controllers.api.ForAuthorized.ForCompanyRole
{
    [Authorize(Roles = AppConstants.Roles.Company)]
    public class BaseCompanyRoleController : BaseAuthorizeApiController { }
}
