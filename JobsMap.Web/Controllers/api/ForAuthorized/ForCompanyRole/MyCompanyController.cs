using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.MyCompany;
using BusinessLogic.Services;
using BusinessLogic.Services.ForAuthorized.ForCompanyRole;
using BusinessLogic.Utils;
using Microsoft.AspNetCore.Mvc;

namespace JobsMap.Web.Controllers.api.ForAuthorized.ForCompanyRole
{
    public class MyCompanyController : BaseCompanyRoleController
    {
        private readonly ICurrentUser _currentUserService;
        private readonly IMyCompanyService _myCompanyService;
        private readonly IMyCompanyFilialsService _myCompanyFilialsService;
        private readonly IMyCompanyVacanciesService _myCompanyVacanciesService;
        private readonly IMyCompanyPhotosService _myCompanyPhotosService;
        private readonly IMyCompanySendingResumeService _myCompanySendingResumeService;


        public MyCompanyController(IMyCompanyService myCompanyService, ICurrentUser currentUserService,
            IMyCompanySendingResumeService myCompanySendingResumeService,
            IMyCompanyFilialsService myCompanyFilialsService,
        IMyCompanyVacanciesService myCompanyVacanciesService,
        IMyCompanyPhotosService myCompanyPhotosService
            )
        {
            _myCompanyService = myCompanyService;
            _currentUserService = currentUserService;
            _myCompanySendingResumeService = myCompanySendingResumeService;
            _myCompanyFilialsService = myCompanyFilialsService;
            _myCompanyVacanciesService = myCompanyVacanciesService;
            _myCompanyPhotosService = myCompanyPhotosService;
        }

        [HttpGet]
        public async Task<IActionResult> GetMainInfo()
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var company = await _myCompanyService.GetMyCompanyShortInfoAsync(userId);
            return Ok(Mapper.Instance.Map<MyCompanyReadVM>(company));
        }

        //[HttpGet]
        //public async Task<IActionResult> GetAllCompanyEmails()
        //{
        //    var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
        //}


        [HttpPost]
        public async Task<IActionResult> Update([FromBody] MyCompanyEditVM company)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myCompanyService.UpdateCompanyInfoAsync(company, userId);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> UploadMainPhoto(MyCompanyUploadMainPhotoVM imageMyCompanyUploadVM)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var url = await _myCompanyService.UploadMainPhotoAsync(imageMyCompanyUploadVM.Image, userId);
            return Ok(url);
        }


        [HttpPost]
        public async Task<IActionResult> AddPhoto(MyCompanyPhotoAddVM myCompanyPhotoAddPhotoVM)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var addResponseVM = await _myCompanyPhotosService.AddPhotoAsync(myCompanyPhotoAddPhotoVM.Image, userId,
                myCompanyPhotoAddPhotoVM.FilialId, HttpContext.GetAbsoluteUrl());
            return Ok(addResponseVM);
        }

        [HttpPost]
        public async Task<IActionResult> DeletePhoto([FromQuery] string companyPhotoId)
        {
            if (!ValidateStringId(companyPhotoId, nameof(companyPhotoId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myCompanyPhotosService.DeletePhotoAsync(userId, companyPhotoId);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCompanyPhotos()
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var companyPhotos = await _myCompanyPhotosService.GetAllCompanyPhotosAsync(userId);
            return Ok(Mapper.Instance.Map<List<MyCompanyPhotoReadVM>>(companyPhotos));
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCompanyFilials()
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var companyFilials = await _myCompanyFilialsService.GetAllCompanyFilialsAsync(userId);
            return Ok(Mapper.Instance.Map<List<MyCompanyFilialReadVM>>(companyFilials));
        }


        [HttpGet]
        public async Task<IActionResult> GetAllReceivedResumes()
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var sentResumes = await _myCompanySendingResumeService.GetAllReceivedResumesAsync(userId);
            return Ok(sentResumes);
        }


        


        [HttpGet]
        public async Task<IActionResult> GetFilialDetails([FromQuery] string filialId)
        {
            if (!ValidateStringId(filialId, nameof(filialId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var filialDetailsDb = await _myCompanyFilialsService.GetFilialDetailsAsync(filialId, userId);
            return Ok(Mapper.Instance.Map<MyCompanyFilialDetailsReadVM>(filialDetailsDb));
        }

        [HttpPost]
        public async Task<IActionResult> DeactivateFilial([FromQuery] string filialId)
        {
            if (!ValidateStringId(filialId, nameof(filialId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myCompanyFilialsService.DeactivateFilialAsync(filialId, userId);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> ActivateFilial([FromQuery] string filialId)
        {
            if (!ValidateStringId(filialId, nameof(filialId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myCompanyFilialsService.ActivateFilialAsync(filialId, userId);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AddFilial([FromBody] MyCompanyFilialAddVM filialVm)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var filialId = await _myCompanyFilialsService.AddFilialAsync(filialVm, userId);
            return Ok(filialId);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateFilial([FromBody] MyCompanyFilialEditVM filialVm)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myCompanyFilialsService.UpdateFilialAsync(filialVm, userId);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCompanyVacancies()
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var companyVacancies = await _myCompanyVacanciesService.GetAllCompanyVacanciesAsync(userId);
            return Ok(Mapper.Instance.Map<List<MyCompanyVacancyReadVM>>(companyVacancies));
        }

        [HttpGet]
        public async Task<IActionResult> GetVacancyDetails([FromQuery] string vacancyId)
        {
            if (!ValidateStringId(vacancyId, nameof(vacancyId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var vacancyDetailsDb = await _myCompanyVacanciesService.GetVacancyDetailsAsync(vacancyId, userId);
            return Ok(Mapper.Instance.Map<MyCompanyVacancyDetailsReadVM>(vacancyDetailsDb));
        }

        [HttpPost]
        public async Task<IActionResult> AddVacancy([FromBody] MyCompanyVacancyAddVM vacancyVm)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var vacancyId = await _myCompanyVacanciesService.AddVacancyAsync(vacancyVm, userId);
            return Ok(vacancyId);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateVacancy([FromBody] MyCompanyVacancyUpdateVM vacancyVm)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myCompanyVacanciesService.UpdateVacancyAsync(vacancyVm, userId);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> DeactivateVacancy([FromQuery] string vacancyId)
        {
            if (!ValidateStringId(vacancyId, nameof(vacancyId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myCompanyVacanciesService.DeactivateVacancyAsync(vacancyId, userId);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> ActivateVacancy([FromQuery] string vacancyId)
        {
            if (!ValidateStringId(vacancyId, nameof(vacancyId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myCompanyVacanciesService.ActivateVacancyAsync(vacancyId, userId);
            return Ok();
        }
    }
}
