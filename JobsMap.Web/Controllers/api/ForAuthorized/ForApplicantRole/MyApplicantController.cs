using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.MyApplicant;
using BusinessLogic.Services;
using BusinessLogic.Services.ForAuthorized.ForApplicantRole;
using BusinessLogic.Utils;
using Microsoft.AspNetCore.Mvc;

namespace JobsMap.Web.Controllers.api.ForAuthorized.ForApplicantRole
{
    public class MyApplicantController : BaseApplicantRoleController
    {
        private readonly ICurrentUser _currentUserService;
        private readonly IMyApplicantService _myApplicantService;
        private readonly IApplicantSendingResumeService _applicantSendingResumeService;


        public MyApplicantController(IMyApplicantService myApplicantService,
            IApplicantSendingResumeService applicantSendingResumeService,
            ICurrentUser currentUserService)
        {
            _myApplicantService = myApplicantService;
            _currentUserService = currentUserService;
            _applicantSendingResumeService = applicantSendingResumeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetMainInfo()
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var applicant = await _myApplicantService.GetApplicantShortInfoAsync(userId);
            return Ok(Mapper.Instance.Map<MyApplicantReadVM>(applicant));
        }

        [HttpPost]
        public async Task<IActionResult> Update([FromBody] MyApplicantEditVM applicantVm)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myApplicantService.UpdateApplicantInfoAsync(applicantVm, userId);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> UploadPhoto(MyApplicantyUploadPhotoVM applicantyUploadPhotoVM)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var url = await _myApplicantService.UploadPhotoAsync(applicantyUploadPhotoVM.Image, userId,
                HttpContext.GetAbsoluteUrl());
            return Ok(url);
        }

        [HttpPost]
        public async Task<IActionResult> UploadResumeFile(MyResumeUploadFileVM uploadFileVM)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var url = await _myApplicantService.UploadResumeFileAsync(uploadFileVM.File, userId, uploadFileVM.ResumeId,
                HttpContext.GetAbsoluteUrl());
            return Ok(url);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteResumeFile([FromQuery] string resumeId)
        {
            if (!ValidateStringId(resumeId, nameof(resumeId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myApplicantService.DeleteResumeFileAsync(userId, resumeId);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllResumes()
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var applicantResumes = await _myApplicantService.GetAllResumesAsync(userId);
            return Ok(Mapper.Instance.Map<List<MyResumeReadVM>>(applicantResumes));
        }

        [HttpGet]
        public async Task<IActionResult> GetResumeDetails([FromQuery] string resumeId)
        {
            if (!ValidateStringId(resumeId, nameof(resumeId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var resume = await _myApplicantService.GetResumeDetailsAsync(resumeId, userId);
            return Ok(Mapper.Instance.Map<MyResumeDetailsReadVM>(resume));
        }

        [HttpPost]
        public async Task<IActionResult> DeactivateResume([FromQuery] string resumeId)
        {
            if (!ValidateStringId(resumeId, nameof(resumeId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myApplicantService.DeactivateResumeAsync(resumeId, userId);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> ActivateResume([FromQuery] string resumeId)
        {
            if (!ValidateStringId(resumeId, nameof(resumeId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myApplicantService.ActivateResumeAsync(resumeId, userId);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> DeleteResume([FromQuery] string resumeId)
        {
            if (!ValidateStringId(resumeId, nameof(resumeId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myApplicantService.DeleteResumeAsync(resumeId, userId);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> AddResume([FromBody] MyResumeAddVM resumeVm)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var resumeId = await _myApplicantService.AddResumeAsync(resumeVm, userId);
            return Ok(resumeId);
        }

        [HttpPost]
        public async Task<IActionResult> SendResume([FromBody] MyApplicantSendResumeVM sendResumeVM)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _applicantSendingResumeService.SendResume(userId, sendResumeVM, HttpContext.GetAbsoluteUrl());
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllSentResumes()
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var sentResumes = await _applicantSendingResumeService.GetAllSentResumesAsync(userId);
            return Ok(sentResumes);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateResume([FromBody] MyResumeEditVM resumeVm)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myApplicantService.UpdateResumeAsync(resumeVm, userId);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllPreferredAddresses()
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var preferredAddresses = await _myApplicantService.GetAllPreferredAddressesAsync(userId);
            return Ok(Mapper.Instance.Map<List<MyPreferredAddressReadVM>>(preferredAddresses));
        }

        [HttpGet]
        public async Task<IActionResult> GetPreferredAddressDetails([FromQuery] string preferredAddressId)
        {
            if (!ValidateStringId(preferredAddressId, nameof(preferredAddressId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var vacancyDetailsDb =
                await _myApplicantService.GetPreferredAddressDetailsAsync(preferredAddressId, userId);
            return Ok(Mapper.Instance.Map<MyPreferredAddressReadVM>(vacancyDetailsDb));
        }

        [HttpPost]
        public async Task<IActionResult> AddPreferredAddress([FromBody] MyPreferredAddressAddVM preferredAddressVm)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            var preferredAddressAddResultVM =
                await _myApplicantService.AddPreferredAddressAsync(preferredAddressVm, userId);
            return Ok(preferredAddressAddResultVM);
        }

        [HttpPost]
        public async Task<IActionResult> UpdatePreferredAddress([FromBody] MyPreferredAddressEditVM preferredAddressVm)
        {
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myApplicantService.UpdatePreferredAddressAsync(preferredAddressVm, userId);
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> DeletePreferredAddress([FromQuery] string preferredAddressId)
        {
            if (!ValidateStringId(preferredAddressId, nameof(preferredAddressId))) return BadRequestModel();
            var userId = _currentUserService.GetCurrentUserId(HttpContext.User);
            await _myApplicantService.DeletePreferredAddressAsync(preferredAddressId, userId);
            return Ok();
        }
    }
}
