using Infrastructure;
using Microsoft.AspNetCore.Authorization;

namespace JobsMap.Web.Controllers.api.ForAuthorized.ForApplicantRole
{
    [Authorize(Roles = AppConstants.Roles.Applicant)]
    public class BaseApplicantRoleController : BaseAuthorizeApiController { }
}
