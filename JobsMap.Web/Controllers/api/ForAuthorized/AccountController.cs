using System;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.MyAccount;
using BusinessLogic.Services;
using BusinessLogic.Services.ForAuthorized;
using BusinessLogic.Utils;
using Infrastructure;
using JobsMap.Web.Custom;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace JobsMap.Web.Controllers.api.ForAuthorized
{
    public class AccountController : BaseAuthorizeApiController
    {
        private readonly IAccountService _accountService;
        private readonly ICurrentUser _currentUser;


        public AccountController(IAccountService accountService, ICurrentUser currentUser)
        {
            _accountService = accountService;
            _currentUser = currentUser;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterAsCompany([FromBody] CompanyRegisterVM model)
        {
            SetLang(model);
            await _accountService.RegisterAsCompanyAsync(model, HttpContext.GetAbsoluteUrl());
            return Ok();
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterAsApplicant([FromBody] ApplicantRegisterVM model)
        {
            SetLang(model);
            await _accountService.RegisterAsApplicantAsync(model, HttpContext.GetAbsoluteUrl());
            return Ok();
        }

        private void SetLang(RegisterVM model)
        {
            model.Language = AppCulturesWeb.GetCultureFromRequest(HttpContext).ShortName;
        }

        [HttpGet]
        public async Task<IActionResult> GetMyInfo()
        {
            var userId = _currentUser.GetCurrentUserId(HttpContext.User);
            var user = await _accountService.GetMyShortInfoAsync(userId);
            return Ok(Mapper.Instance.Map<UserShortMyInfoReadVM>(user));
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Token([FromBody] LoginVM model)
        {   
            var jwtTokenVm = await _accountService.TokenAsync(model.Email, model.Password);
            return Ok(jwtTokenVm);
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> RefreshToken([FromQuery] string refreshToken)
        {
            if (!ValidateStringGuid(refreshToken, nameof(refreshToken))) return BadRequestModel();
            var jwtTokenVm = await _accountService.RefreshTokenAsync(Guid.Parse(refreshToken));
            return Ok(jwtTokenVm);
        }


        [HttpPost]
        //[AllowAnonymous] // todo, maybe must be loggined
        public async Task<IActionResult> Logout([FromQuery] string refreshToken)
        {
            if (!ValidateStringGuid(refreshToken, nameof(refreshToken))) return BadRequestModel();
            await _accountService.LogoutAsync(Guid.Parse(refreshToken));
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> SetLanguage([FromQuery] string language)
        {
            if (language.IsNullOrEmpty() || !AppCultures.CulturesDictionary.ContainsKey(language)) return BadRequestModel();
            await _accountService.SetLanguageAsync(_currentUser.GetCurrentUserId(HttpContext.User), language);
            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail([FromBody] ConfirmEmailVM model)
        {
            await _accountService.ConfirmEmailAsync(model.UserId, model.Token);
            return Ok();
        }

        //[HttpPost]
        //public async Task<IActionResult> ResendConfirmationEmail()
        //{
        //    
        //    var userId = _currentUser.GetCurrentUserId(HttpContext.User);
        //    await _accountService.ResendConfirmationEmailAsync(userId, HttpContext.GetAbsoluteUrl());
        //    return Ok();
        //}

        [HttpPost]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordVM model)
        {
            var userId = _currentUser.GetCurrentUserId(HttpContext.User);
            await _accountService.ChangePasswordAsync(userId, model.CurrentPassword, model.NewPassword);
            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> SendPasswordResettingEmail([FromBody] SendPasswordResettingEmailVM model)
        {
            await _accountService.SendPasswordResettingEmailAsync(model.Email, HttpContext.GetAbsoluteUrl());
            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordVM model)
        {
            await _accountService.ResetPasswordAsync(model.UserId, model.Token, model.NewPassword);
            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ResendConfirmationEmailByName([FromBody] ResendConfirmationEmailVM model)
        {
            await _accountService.ResendConfirmationEmailAsync(model.Email, model.Password,
                HttpContext.GetAbsoluteUrl());
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> SendEmailChangingEmail([FromBody] SendEmailChangingEmailVM model)
        {
            var userId = _currentUser.GetCurrentUserId(HttpContext.User);
            await _accountService.SendEmailChangingEmailAsync(userId, model.NewEmail, HttpContext.GetAbsoluteUrl());
            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> ChangeEmail([FromBody] ChangeEmailVM model)
        {
            var userId = _currentUser.GetCurrentUserId(HttpContext.User);
            await _accountService.ChangeEmailAsync(userId, model.NewEmail, model.Token);
            return Ok();
        }
    }
}
