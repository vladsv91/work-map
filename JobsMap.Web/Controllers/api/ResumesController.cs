using System.Threading.Tasks;
using BusinessLogic.Models.Resumes;
using BusinessLogic.Services;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace JobsMap.Web.Controllers.api
{
    public class ResumesController : BaseApiController
    {
        private readonly IResumesService _resumesService;

        public ResumesController(IResumesService resumesService)
        {
            _resumesService = resumesService;
        }

        [HttpGet]
        public async Task<IActionResult> GetByFilter([FromQuery] ResumesListFilterReadVM filter)
        {
            var companies = await _resumesService.GetResumesByFilterAsync(filter);
            return Ok(companies);
        }

        [HttpGet]
        public async Task<IActionResult> GetDetails([FromQuery] string resumeId)
        {
            if (!ValidateStringId(resumeId, nameof(resumeId))) return BadRequestModel();
            var companyDetails = await _resumesService.GetResumeDetailsAsync(resumeId);
            return Ok(companyDetails);
        }

        [HttpGet(AppConstants.FilesUrls.ApplicantPhotoUrl + "/" + "{applicantId}")]
        public async Task<IActionResult> GetPhoto([FromRoute] string applicantId)
        {
            if (!ValidateStringId(applicantId, nameof(applicantId))) return BadRequestModel();
            var fileData = await _resumesService.GetApplicantPhotoAsync(applicantId);
            return ImageResult(fileData);
        }

        [HttpGet(AppConstants.FilesUrls.ResumeFileUrl + "/" + "{resumeId}")]
        public async Task<IActionResult> GetResumeFile([FromRoute] string resumeId)
        {
            if (!ValidateStringId(resumeId, nameof(resumeId))) return BadRequestModel();
            var fileData = await _resumesService.GetResumeFileAsync(resumeId);
            return DocumentResult(fileData);
        }
    }
}
