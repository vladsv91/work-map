using System;
using System.Threading.Tasks;
using BusinessLogic.Services;
using Data.Entities.Common;
using Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace JobsMap.Web.Controllers.api
{
    public class FileController : BaseApiController
    {
        private readonly IFileService _fileService;

        public FileController(IFileService fileService)
        {
            _fileService = fileService;
        }


        [HttpGet(AppConstants.FilesUrls.FileBaseUrl + "/" + "{fileId}")]
        public async Task<IActionResult> Get([FromRoute] Guid fileId)
        {
            var fileData = await _fileService.GetFileAsync(fileId);
            return GetFileByType(fileData);
        }

        private IActionResult GetFileByType(FileData fileData)
        {
            switch (fileData.FileType) {
                case FileType.Image: return ImageResult(fileData);
                // ReSharper disable once RedundantCaseLabel
                case FileType.Document:
                default:
                    return DocumentResult(fileData);
            }
        }
    }
}
