using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BusinessLogic.Models.Common;
using Data.Entities.Common;
using Microsoft.AspNetCore.Mvc;
//using JobsMap.Web.Resources.Industries;


//namespace Industries
//{
//    public class Industries { }
//}


namespace JobsMap.Web.Controllers.api
{
    public class LookupController : BaseApiController
    {
        //private readonly IStringLocalizer<Industries> _localizer;

        //public LookupController(IStringLocalizer<Industries> localizer)
        //{
        //    _localizer = localizer;
        //}


        [HttpGet]
        public IActionResult GetAllIndustries()
        {
            return GetGroupedValuesString<IndustryReadVM, IndustryTypeReadVM>(IndustriesInTypes.Instanse.GroupedValues);
        }

        [HttpGet]
        public IActionResult GetAllEmploymentTypes()
        {
            return GetValues<EmploymentType>();
        }

        [HttpGet]
        public IActionResult GetAllCurrencies()
        {
            return GetValues<Currency>();
        }

        [HttpGet]
        public IActionResult GetAllProfessions()
        {
            return GetGroupedValuesString<ProfessionReadVM, ProfessionTypeReadVM>(ProfessionsInTypes.Instanse.GroupedValues);
            //return GetGroupedValues(ProfessionsInTypes.GroupedValues);
        }


        [NonAction]
        private IActionResult GetValues<T>()
        {
            var values = Enum.GetValues(typeof(T));
            var mapped = Mapper.Instance.Map<List<LookupIntReadVM>>(values.Cast<T>());
            return Ok(mapped);
        }

        [NonAction]
        private IActionResult GetGroupedValuesString<TValuesReadVm, TTypeReadVm>(IDictionary<string, HashSet<string>> valuesInTypes)
            where TValuesReadVm : LookupStringReadVM where TTypeReadVm : LookupStringReadVM
        {
            var mapper = Mapper.Instance;
            var res = new List<ValuesInType<TValuesReadVm, TTypeReadVm>>(valuesInTypes.Select(valuesInType =>
                new ValuesInType<TValuesReadVm, TTypeReadVm>(mapper.Map<TTypeReadVm>(valuesInType.Key),
                    mapper.Map<List<TValuesReadVm>>(valuesInType.Value))));

            return Ok(res);
        }

        public class ValuesInType<TValues, TType>
        {
            public ValuesInType(TType type, List<TValues> values)
            {
                Type = type;
                Values = values;
            }

            public TType Type { get; }
            public List<TValues> Values { get; }
        }
    }
}
