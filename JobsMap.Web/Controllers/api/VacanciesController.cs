using System.Threading.Tasks;
using BusinessLogic.Models.Vacancies;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;

namespace JobsMap.Web.Controllers.api
{
    public class VacanciesController : BaseApiController
    {
        private readonly IVacanciesService _vacanciesService;

        public VacanciesController(IVacanciesService vacanciesService)
        {
            _vacanciesService = vacanciesService;
        }

        [HttpGet]
        public async Task<IActionResult> GetByFilter([FromQuery] VacanciesListFilterReadVM filter)
        {
            var companies = await _vacanciesService.GetVacanciesByFilterAsync(filter);
            return Ok(companies);
        }

        [HttpGet]
        public async Task<IActionResult> GetDetails([FromQuery] string vacancyId)
        {
            if (!ValidateStringId(vacancyId, nameof(vacancyId))) return BadRequestModel();
            var companyDetails = await _vacanciesService.GetVacancyDetailsAsync(vacancyId);
            return Ok(companyDetails);
        }
    }
}
