using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Services;
using BusinessLogic.Utils;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using SimpleMvcSitemap;
using SimpleMvcSitemap.Images;

namespace JobsMap.Web.Controllers
{
    public class SitemapController : BaseMvcController
    {
        private readonly IMemoryCache _cache;
        private readonly ICompaniesService _companiesService;
        private readonly IVacanciesService _vacanciesService;
        private readonly IResumesService _resumesService;

        private readonly DateTime _lasModifiedTime = DateTime.UtcNow;

        public SitemapController(ICompaniesService companiesService
            , IVacanciesService vacanciesService
            , IResumesService resumesService
            , IMemoryCache cache
        )
        {
            _companiesService = companiesService;
            _vacanciesService = vacanciesService;
            _resumesService = resumesService;
            _cache = cache;
        }

        // todo: Add lists sitemaps
        [HttpGet("/sitemap*.xml")]
        [Route("/sitemap")]
        public IActionResult Index()
        {
            var nodes = new List<SitemapIndexNode>
            {
                new SitemapIndexNode(Url.Action("Base", "Sitemap")) {LastModificationDate = null},
                new SitemapIndexNode(Url.Action("Companies", "Sitemap")) {LastModificationDate = null},
                new SitemapIndexNode(Url.Action("Vacancies", "Sitemap")) {LastModificationDate = null},
                new SitemapIndexNode(Url.Action("Resumes", "Sitemap")) {LastModificationDate = null},
                new SitemapIndexNode(Url.Action("CompaniesFilials", "Sitemap")) {LastModificationDate = null},
                new SitemapIndexNode(Url.Action("CompaniesVacancies", "Sitemap")) {LastModificationDate = null},
                new SitemapIndexNode(Url.Action("CompaniesPhotos", "Sitemap")) {LastModificationDate = null},
            };
            return new SitemapProvider().CreateSitemapIndex(new SitemapIndexModel(nodes));
        }


        [HttpGet("/sitemap-base.xml")]
        public IActionResult Base()
        {
            var nodes = new List<SitemapNode>
            {
                new SitemapNode(Url.Action("Index", "Home")) {Priority = 0.6m, LastModificationDate = _lasModifiedTime, ChangeFrequency = ChangeFrequency.Weekly},
                new SitemapNode(Url.Action("CompaniesList", "Home")) {LastModificationDate = _lasModifiedTime, ChangeFrequency = ChangeFrequency.Weekly},
                new SitemapNode(Url.Action("VacanciesList", "Home")) {LastModificationDate = _lasModifiedTime, ChangeFrequency = ChangeFrequency.Weekly},
                new SitemapNode(Url.Action("ResumesList", "Home")) {LastModificationDate = _lasModifiedTime, ChangeFrequency = ChangeFrequency.Weekly},
            };
            return new SitemapProvider().CreateSitemap(new SitemapModel(nodes));
        }


        [HttpGet("/sitemap-companies.xml")]
        public Task<IActionResult> Companies()
        {
            return GetEntitySiteMap(nameof(HomeController.CompanyDetails), GetCompaniesListFromCache, (company, node) =>
            {
                if (company.MainPhotoId.HasValue) {
                    node.Images = new List<SitemapImage> {new SitemapImage(BlUtils.CreateCompanyMainPhotoUrl(company.Id))};
                }
            });
        }

        [HttpGet("/sitemap-companies-filials.xml")]
        public Task<IActionResult> CompaniesFilials()
        {
            return GetEntitySiteMap(nameof(HomeController.CompanyDetailsFilials), GetCompaniesListFromCache);
        }

        [HttpGet("/sitemap-companies-vacancies.xml")]
        public Task<IActionResult> CompaniesVacancies()
        {
            return GetEntitySiteMap(nameof(HomeController.CompanyDetailsVacancies), GetCompaniesListFromCache);
        }

        [HttpGet("/sitemap-companies-photos.xml")]
        public Task<IActionResult> CompaniesPhotos()
        {
            return GetEntitySiteMap(nameof(HomeController.CompanyDetailsPhotos), GetCompaniesListFromCache, (company, node) =>
            {
                node.Images = company.CompanyPhotos.Select(cp => new SitemapImage(BlUtils.CreateCompanyPhotoUrl(cp.Id))).ToList();
            });
        }

        [HttpGet("/sitemap-vacancies.xml")]
        public Task<IActionResult> Vacancies()
        {
            return GetEntitySiteMap(nameof(HomeController.VacancyDetails), _vacanciesService.GetListForStitemapAsync);
        }

        [HttpGet("/sitemap-resumes.xml")]
        public Task<IActionResult> Resumes()
        {
            return GetEntitySiteMap(nameof(HomeController.ResumeDetails), _resumesService.GetListForStitemapAsync);
        }

        private Task<IActionResult> GetEntitySiteMap<T>(string itemDetalisMethodName, Func<Task<List<T>>> getList, Action<T, SitemapNode> configureNode = null) where T : IBaseEntity
        {
            return TryGetFromHostScopedMemory<IActionResult>(itemDetalisMethodName + "Sitemap", async () =>
            {
                var list = await getList();
                var nodes = list.Select(_ =>
                {
                    var node = new SitemapNode(Url.Action(itemDetalisMethodName, "Home", new {id = _.Id}))
                    {
                        LastModificationDate = _lasModifiedTime,
                        ChangeFrequency = ChangeFrequency.Weekly,
                        //Images = new List<SitemapImage>()
                        //{
                        //    new SitemapImage()
                        //}
                    };
                    configureNode?.Invoke(_, node);
                    return node;
                }).ToList();
                return new SitemapProvider().CreateSitemap(new SitemapModel(nodes));
            });
        }

        private Task<T> TryGetFromHostScopedMemory<T>(string key, Func<Task<T>> get, int cacheMinutes = 60)
        {
            return TryGetFromMemory(key + "_" + HttpContext.Request.Host, get, cacheMinutes);
        }

        private async Task<T> TryGetFromMemory<T>(string key, Func<Task<T>> get, int cacheMinutes = 60)
        {
            if (!_cache.TryGetValue(key, out var res))
            {
                res = await get();
                _cache.Set(key, res, DateTime.UtcNow.AddMinutes(cacheMinutes));
            }
            return (T)res;
        }

        private Task<List<Company>> GetCompaniesListFromCache()
        {
            return TryGetFromMemory("CompaniesListForStitemap", _companiesService.GetListForStitemapAsync);
        }
    }
}
