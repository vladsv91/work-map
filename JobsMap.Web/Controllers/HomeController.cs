using System;
using System.Threading.Tasks;
using BusinessLogic.Models.Companies;
using BusinessLogic.Services;
using Data.Entities.Common;
using FluentValidation;
using Infrastructure;
using JobsMap.Web.Custom;
using JobsMap.Web.Models;
using Localization.JsLocalization;
using Localization.Texts;
using Microsoft.AspNetCore.Mvc;

namespace JobsMap.Web.Controllers
{
    public class HomeController : BaseMvcController
    {

        private readonly ICompaniesService _companiesService;
        private readonly ICustomLogger _customLogger;
        private readonly IResumesService _resumesService;
        private readonly IVacanciesService _vacanciesService;

        public static string EntityDataFromServer = "EntityDataFromServer";
        public static string CompanyEntityDataFromServer = "CompanyEntityDataFromServer";

        public HomeController(
            ICompaniesService companiesService,
            IVacanciesService vacanciesService,
            IResumesService resumesService,
            ICustomLogger customLogger
        )
        {
            _companiesService = companiesService;
            _vacanciesService = vacanciesService;
            _resumesService = resumesService;
            _customLogger = customLogger;
        }

        [HttpGet]
        [Route("my/{*dummy}")]
        [Route("sign-up/{*dummy}")]
        [Route("confirm-email/{*dummy}")]
        [Route("reset-password/{*dummy}")]
        public IActionResult My(string dummy)
        {
            SetSsrData(new SsrData {DontUseSsr = true});
            return View("Index");
        }

        [HttpGet]
        public IActionResult Index()
        {
            SetSsrData(new SsrData());
            return View();
        }

        // for SEO
        [HttpGet]
        [Route("companies")]
        public IActionResult CompaniesList()
        {
            return GetItemsListView(data => new SsrData { Title = Texts.CompaniesTitlePart });
        }


        [HttpGet]
        [Route("vacancies")]
        public IActionResult VacanciesList()
        {
            return GetItemsListView(data => new SsrData { Title = Texts.VacanciesTitlePart});

        }

        [HttpGet]
        [Route("resumes")]
        public IActionResult ResumesList()
        {
            return GetItemsListView(data => new SsrData {Title = Texts.ResumesTitlePart});
        }

        private async Task SetCompanyDetailsEntity<T>(string entityName, EntityEnum entity, Func<Task<T>> getData, Func<T, SsrData> getSsrData)
        {
            if (AppConstants.UseNodeSsr) {
                var dataFromServer = new DataFromServer
                {
                    EntityDataType = EntityDataType.CompanyEntityList,
                };
                TempData[CompanyEntityDataFromServer] = dataFromServer;

                try {
                    dataFromServer.EntityName = entityName;
                    dataFromServer.Entity = entity;

                    var data = await getData();
                    dataFromServer.Data = data;

                    var ssrData = getSsrData(data);

                    SetSsrData(ssrData);

                }
                catch (Exception e) {
                    await _customLogger.LogAsync(LogType.Error, e.Message, HttpContext);
                }
            }
        }


        [HttpGet]
        [Route("companies/{id}/filials")]
        public async Task<IActionResult> CompanyDetailsFilials(string id)
        {
            var res = await CompanyDetails(id);
            await SetCompanyDetailsEntity("CompanyFilials", EntityEnum.CompanyFilial, async () =>
                (await _companiesService.GetCompanyFilialsAsync(id)).MapToList<CompanyFilialReadVM>(), data =>
            {
                var itemDetails = this.GetItemDetailsDataFromContext<CompanyFullInfoReadVM>();
                var title = string.Format(Texts.CompanyDetailsFilials, itemDetails.Name);
                return new SsrData
                {
                    Title = title,
                    Description = title + ". ",
                };
            });
            return res;
        }

        [HttpGet]
        [Route("companies/{id}/vacancies")]
        public async Task<IActionResult> CompanyDetailsVacancies(string id)
        {
            var res = await CompanyDetails(id);
            await SetCompanyDetailsEntity("CompanyVacancies", EntityEnum.CompanyVacancy, async () =>
                (await _companiesService.GetCompanyVacanciesAsync(id)).MapToList<CompanyVacancyShortInfoReadVM>(), data =>
            {
                var itemDetails = this.GetItemDetailsDataFromContext<CompanyFullInfoReadVM>();
                var title = string.Format(Texts.CompanyDetailsVacancies, itemDetails.Name);
                return new SsrData
                {
                    Title = title,
                    Description = title + ". ",
                };
            });
            return res;
        }

        [HttpGet]
        [Route("companies/{id}/photos")]
        public async Task<IActionResult> CompanyDetailsPhotos(string id)
        {
            var res = await CompanyDetails(id);
            await SetCompanyDetailsEntity("CompanyPhotos", EntityEnum.CompanyPhoto, async () =>
                (await _companiesService.GetCompanyPhotosAsync(id)).MapToList<CompanyPhotoReadVM>(), data =>
            {
                var itemDetails = this.GetItemDetailsDataFromContext<CompanyFullInfoReadVM>();
                var title = string.Format(Texts.CompanyDetailsPhotos, itemDetails.Name);
                return new SsrData
                {
                    Title = title,
                    Description = title + ". ",
                };
            });
            return res;
        }

        [HttpGet]
        [Route("companies/{*id}")]
        public async Task<IActionResult> CompanyDetails(string id)
        {
            return await GetItemDetailsView(id, "Company",
                async itemId => (await _companiesService.GetCompanyDetailsAsync(itemId)).MapTo<CompanyFullInfoReadVM>(),
                EntityEnum.Company, data => new SsrData
                {
                    Description = $"{JsLocalization.GeneralCompany} \"{data.Name.SrinkString(25)}\". {data.Description.SrinkString(150)} ",
                    Title = $"{JsLocalization.GeneralCompany} \"{data.Name}\"",
                });
        }


        [HttpGet]
        [Route("vacancies/{*id}")]
        public Task<IActionResult> VacancyDetails(string id)
        {
            return GetItemDetailsView(id, "Vacancy",
                itemId => _vacanciesService.GetVacancyDetailsAsync(itemId),
                EntityEnum.Vacancy, data => new SsrData
                {
                    Description =
                        $"{JsLocalization.GeneralVacancy} \"{data.Name.SrinkString(50)}\". {data.Description.SrinkString(150)} ",
                    Title = $"{JsLocalization.GeneralVacancy} \"{data.Name}\"",
                });
        }

        [HttpGet]
        [Route("resumes/{*id}")]
        public Task<IActionResult> ResumeDetails(string id)
        {
            return GetItemDetailsView(id, "Resume",
                async itemId => await _resumesService.GetResumeDetailsAsync(itemId),
                EntityEnum.Resume, data => new SsrData
                {
                    Description = $"{JsLocalization.GeneralResume} \"{data.Name.SrinkString(30)}\". {data.Description.SrinkString(110)} ",
                    Title = $"{JsLocalization.GeneralResume} \"{data.Name}\"",
                });
        }

        private void SetSsrData(SsrData ssrData)
        {
            ViewBag.SsrData = ssrData;
        }

        private IActionResult GetItemsListView(Func<object, SsrData> getSsrData)
        {

            var ssrData = getSsrData(null);

            var listHeaderPart = ssrData.Title;
            ssrData.Title =
                string.Format(Texts.ListTitleTemplateWithoutPage, listHeaderPart);
            ssrData.Description = string.Format(Texts.ListTitleTemplateWithoutPage, listHeaderPart) + ". ";

            SetSsrData(ssrData);

            return View("Index");
        }

        private async Task<IActionResult> GetItemDetailsView<T>(string id, string entityName,
            Func<string, Task<T>> getItemDetails, EntityEnum entity, Func<T, SsrData> getSsrData)
        {
            var dataFromServer = new DataFromServer
            {
                EntityName = entityName,
                Entity = entity,
            };
            try {
                var data = await getItemDetails(id);
                dataFromServer.Data = data;
                dataFromServer.EntityDataType = EntityDataType.ItemDetails;
                TempData[EntityDataFromServer] = dataFromServer;

                var ssrData = getSsrData(data);

                //var listHeaderPart = ssrData.Title;
                //ssrData.Title =
                //    page == 1 ?
                //        string.Format(Texts.ListTitleTemplateWithoutPage, listHeaderPart) :
                //        string.Format(Texts.ListTitleTemplate, listHeaderPart, page);
                //ssrData.Description = string.Format(Texts.ListTitleTemplateWithoutPage, listHeaderPart) + ". ";

                SetSsrData(ssrData);
            }
            catch (ValidationException e) {
                await _customLogger.LogAsync(LogType.Error, e.Message, HttpContext);
                return View("Index");
            }
            catch (Exception e) {
                await _customLogger.LogAsync(LogType.Error, e.Message, HttpContext);
                return View("Index");
            }
            return View("Index");
        }
    }
}
