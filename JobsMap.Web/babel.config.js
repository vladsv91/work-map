module.exports = function (api) {
  api.cache(true);

  const presets = [
    '@babel/preset-env',
    '@vue/babel-preset-jsx',
    // '@babel/preset-stage-2',
  ];
  const plugins = [
    // 'transform-runtime',
    // 'transform-vue-jsx',
    // 'babel-plugin-transform-vue-jsx',
    '@babel/plugin-transform-runtime',
    '@vue/babel-plugin-transform-vue-jsx',

    '@babel/plugin-syntax-jsx',
    // Stage 2
    ['@babel/plugin-proposal-decorators', { legacy: true }],
    '@babel/plugin-proposal-function-sent',
    '@babel/plugin-proposal-export-namespace-from',
    '@babel/plugin-proposal-numeric-separator',
    '@babel/plugin-proposal-throw-expressions',

    // Stage 3
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-syntax-import-meta',
    ['@babel/plugin-proposal-class-properties', { loose: false }],
    '@babel/plugin-proposal-json-strings',
  ];

  return {
    presets,
    plugins,
  };
};
