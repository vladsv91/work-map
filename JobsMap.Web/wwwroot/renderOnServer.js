process.env.VUE_ENV = 'server';

const fs = require('fs');
const path = require('path');


const filePath = path.join(__dirname, './dist/main.server.js');

const code = fs.readFileSync(filePath, 'utf8');

const bundleRenderer = require('vue-server-renderer').createBundleRenderer(code);
const createServerRenderer = require('aspnet-prerendering').createServerRenderer;


module.exports = createServerRenderer((params) => {
  return new Promise((resolve, reject) => {
    bundleRenderer.renderToString(params.data).then((resultHtml) => {
      // CRUTCH: fix vue-router hashes
      resultHtml = resultHtml.replace(/href="#\//gi, 'href="/');

      resolve({
        html: resultHtml,
        globals: {
          __INITIAL_STATE__: params.data,
        },
      });
    }, (err) => { reject(err.message); });
  });
});
