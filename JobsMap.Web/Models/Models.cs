namespace JobsMap.Web.Models
{
    public enum EntityDataType
    {
        ItemDetails,
        List,
        CompanyEntityList,
    }

    //public enum EntityData
    //{
    //CompaniesList,
    //VacanciesList,
    //ResumesList,
    //CompanyDetalis,
    //VacancyDetalis,
    //ResumeDetalis,
    //CompanyFilials,
    //CompanyVacancies,
    //CompanyPhotos,
    //}

    public enum EntityEnum
    {
        Company,
        Vacancy,
        Resume,
        CompanyFilial,
        CompanyVacancy,
        CompanyPhoto,
    }

    public class DataFromServer
    {
        public object Data { get; set; }
        public string EntityName { get; set; }
        public EntityDataType EntityDataType { get; set; }
        public EntityEnum Entity { get; set; }
    }

    public class SsrData
    {
        public bool DontUseSsr { get; set; }

        public string Description { get; set; }
        public string Title { get; set; }
    }
}
