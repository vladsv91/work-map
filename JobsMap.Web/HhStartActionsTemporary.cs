//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.IO;
//using System.Linq;
//using System.Threading.Tasks;
//using BusinessLogic.Utils;
//using Data;
//using Data.Entities.Account;
//using Data.Entities.Common;
//using Data.Entities.CompanyEntities;
//using Google.Maps;
//using Google.Maps.Geocoding;
//using Infrastructure;
//using Microsoft.EntityFrameworkCore;
//using Newtonsoft.Json;

//namespace JobsMap.Web
//{
//    public static class HhStartActionsTemporary
//    {
//        // for saving
//        public static JobsMapContext Db;


//        //public static async Task ChangeAddresses()
//        //{
//        //    var db = Db = new DesignTimeDbContextFactory().CreateDbContext(null);
//        //    var companyRep = new Repository<Company>(db);
//        //    var addressRep = new Repository<Address>(db);
//        //    var allCompanies = await companyRep.GetQueriable()
//        //        .Include(_ => _.Filials).ThenInclude(_ => _.Address)
//        //        .Where(_ => _.CreationType == CompanyCreationType.FromDou &&
//        //                    _.Filials.Any(f => f.Address.Latitude == 0 && f.Address.Longitude == 0)).ToListAsync();
//        //    var geocodingService = new GeocodingService();
//        //    var count = -1;

//        //    //var tasks = new Task[allCompanies.Count];
//        //    foreach (var company in allCompanies) {
//        //        count++;
//        //        Debug.WriteLine(count);
//        //        //tasks[count] = SetAddressesToFilials(company, geocodingService, addressRep);
//        //        await SetAddressesToFilials(company, geocodingService, addressRep);
//        //    }
//        //    //Task.WaitAll(tasks);
//        //    await db.SaveChangesAsync();
//        //}

//        //private static async Task SetAddressesToFilials(Company company, GeocodingService geocodingService, Repository<Address> addressRep)
//        //{
//        //    var tasks = new Task[company.Filials.Count];
//        //    var count = -1;
//        //    foreach (var companyFilial in company.Filials) {
//        //        count++;
//        //        tasks[count] = SetAddresDataToFilial(geocodingService, addressRep, companyFilial);
//        //    }
//        //    await Task.WhenAll(tasks);
//        //}

//        //private static async Task SetAddresDataToFilial(GeocodingService geocodingService, Repository<Address> addressRep,
//        //    CompanyFilial companyFilial)
//        //{
//        //    var addressFormattedAddress = companyFilial.Address.FormattedAddress;
//        //    var count = 0;
//        //    do {
//        //        count++;
//        //        GeocodeResponse response = null;

//        //        try {
//        //            response = await
//        //                geocodingService.GetResponseAsync(new GeocodingRequest
//        //                {
//        //                    Address = new Location(addressFormattedAddress),
//        //                    Language = "ru-RU"
//        //                    //Region = "Ru",
//        //                });
//        //        }
//        //        catch (Exception e) {
//        //            break;
//        //        }

//        //        if (response.Status == ServiceResponseStatus.Ok && response.Results.Length > 0) {
//        //            ServiceUtils.SetAddressData(companyFilial.Address, response, false);
//        //            break;
//        //        }
//        //        if (response.Status == ServiceResponseStatus.OverQueryLimit) break;

//        //        var indOfCom = addressFormattedAddress.LastIndexOf(',');
//        //        if (indOfCom == -1) {
//        //            var indOfBrcket = addressFormattedAddress.LastIndexOf('(');
//        //            if (indOfBrcket != -1) addressFormattedAddress = addressFormattedAddress.Substring(0, indOfBrcket);
//        //            else break;
//        //        }
//        //        else {
//        //            addressFormattedAddress = addressFormattedAddress.Substring(0, indOfCom);
//        //        }
//        //    } while (true);
//        //    addressRep.Update(companyFilial.Address);
//        //}

//        public static async void AddCompaniesWithVacancies()
//        {
//            var companiesJson = File.ReadAllText("D:\\vacancies full info (vacacnies & contacts & addr).json");

//            var allCompaniesList = JsonConvert.DeserializeObject<HhCompanyFullInfo[]>(companiesJson);

//            var db = new JobsMapContext();
//            var companyRep = new Repository<Company>(db);

//            var userRep = new Repository<ApplicationUser>(db);
//            var myUser = await userRep.GetQueriable().FirstOrDefaultAsync(_ => _.NormalizedEmail == "VLADSV91@GMAIL.COM");

//            var existingDbCompanies = await companyRep.GetQueriable().Select(_ => _.NormalizedName).ToListAsync();

//            var existingDbCompaniesHash = new HashSet<string>(existingDbCompanies);

//            var compsWithAddr = allCompaniesList.Where(_ => _.Addresses.Length > 0).ToArray();
//            //var countOfDouble = new HashSet<string>(compsWithAddr.Select(_ => _.name.ToUpper()));

//            //var allCompaniesListExisted = allCompaniesList.Where(_ => existingDbCompaniesHash.Contains(_.name.ToUpper()));

//            var companiesDbDic = new Dictionary<string, Company>(compsWithAddr.Length);
//            var count = -1;
//            var existCompanyCount = 0;
//            var longNameCount = 0;
//            foreach (var companyFullInfo in compsWithAddr) {
//                count++;
//                Debug.WriteLine(count);
//                // todod, maybe preserve comps without addresses
//                if (companyFullInfo.Addresses.Length == 0) {
//                    //Debug.Print($@"Without address: {companyFullInfo.name}");
//                    continue;
//                }


//                if (
//                    companyFullInfo.name.Length > 80
//                ) {
//                    Debug.Print($@"Long name: {companyFullInfo.name}, count: {longNameCount++}");
//                    continue;
//                }

//                if (existingDbCompaniesHash.Contains(companyFullInfo.name.ToUpper())) {
//                    existCompanyCount++;
//                    Debug.Print($@"Exists company: {companyFullInfo.name}, count: {existCompanyCount}");
//                    continue;
//                }
//                if (companiesDbDic.ContainsKey(companyFullInfo.name.ToUpper())) {
//                    Debug.Print($@"Double of company: {companyFullInfo.name}");
//                    continue;
//                }


//                var companyDb = new Company
//                {
//                    Id = GeneralUtils.CreateUniqId(),
//                    CreatorId = myUser.Id,
//                    ContactEmail = companyFullInfo.ContactEmail.SubstringSafe(100),
//                    ContactPhone = companyFullInfo.ContactPhone.SubstringSafe(20),
//                    IsDisabled = false,
//                    Name = companyFullInfo.name,

//                    CreationType = CompanyCreationType.FromHh,
//                    Description = "",
//                    IndustryId = "Other",
//                    HhUrl = companyFullInfo.alternate_url.SubstringSafe(200),

//                    EmployeeCount = 0,
//                    NormalizedName = companyFullInfo.name.ToUpper(),
//                    //MainPhotoUri = companyFullInfo.logo_urls?.Values?.FirstOrDefault(),
//                };

//                companyDb.Filials = companyFullInfo.Addresses.Select((address, index) =>
//                {
//                    var addressDb = new Address
//                    {
//                        Id = GeneralUtils.CreateUniqId(),
//                        CreatedOnUtc = DateTime.UtcNow,
//                        UpdatedOnUtc = DateTime.UtcNow,
//                        FormattedAddress = address.raw ?? $" City: {address.city}, {address.street} {address.building}",
//                        Latitude = (decimal) (address.lat ?? 0),
//                        Longitude = (decimal) (address.lng ?? 0),
//                        City = address.city,
//                        StreetNumber = address.building,
//                        Description = address.description,
//                        Street = address.street,
//                        Country = "",
//                    };

//                    if (
//                        addressDb.Street?.Length > 200 ||
//                        addressDb.StreetNumber?.Length > 100 ||
//                        addressDb.City?.Length > 200 ||
//                        addressDb.FormattedAddress.Length > 400 ||
//                        addressDb.Description?.Length > 200
//                    ) { }

//                    var companyFilial = new CompanyFilial
//                    {
//                        Id = GeneralUtils.CreateUniqId(),
//                        CreatedOnUtc = DateTime.UtcNow,
//                        UpdatedOnUtc = DateTime.UtcNow,
//                        Address = addressDb,
//                        IsMain = index == 0,
//                        EmployeeCount = 0,
//                        Name = $@"Filial {index + 1} {address.raw}",
//                    };

//                    if (
//                        companyFilial.Name.Length > 200
//                    ) { }

//                    address.FilialId = companyFilial.Id;
//                    return companyFilial;
//                }).ToList();

//                companyDb.Vacancies = companyFullInfo.Vacancies.Where(_ => _.address != null).Select(vacancy =>
//                {
//                    var created = vacancy.published_at;



//                    //decimal? salaryPerMonth = null;
//                    var salary = vacancy.salary;
//                    //if (salary != null) {
//                    //    //salary.from
//                    //}

//                    var addr = companyFullInfo.Addresses.FirstOrDefault(_ => AddCom.Ins.Equals(_, vacancy.address));
//                    if (addr == null) {
//                        Debug.Print($@"Not found address: {companyFullInfo.name}");
//                        addr = companyFullInfo.Addresses[0];
//                    }

//                    var curr = vacancy.salary?.currency;
//                    Currency currEnum = Currency.Other;
//                    if (curr != null) {
//                        var ttt = Enum.TryParse<Currency>(curr, out currEnum);
//                        if (!ttt) {
//                            Debug.Print($@"Not found currency: {companyFullInfo.name}, curr: {curr}");

//                            currEnum = Currency.Other;
//                        }
//                    }

//                    var vac = new Vacancy
//                    {
//                        Id = GeneralUtils.CreateUniqId(),
//                        CreatedOnUtc = created,
//                        UpdatedOnUtc = created,

//                        CompanyFilialId = addr.FilialId,
//                        Name = vacancy.name.SubstringSafe(200),
//                        CurrencyId = currEnum,
//                        Description = vacancy.description.SubstringSafe(5000),
//                        Duties = "",
//                        EmploymentTypeId = vacancy.EmploymentType,
//                        IsDisabled = false,
//                        ProfessionId = vacancy.specializations.First().id,
//                        Requirements = "",
//                        SalaryPerMonth = null,
//                        MinSalary = salary?.from,
//                        MaxSalary = salary?.to,
//                        WorkingConditions = "",
//                        ContactEmail = vacancy.contacts?.email?.SubstringSafe(100),
//                        ContactPhone = vacancy.contacts?.phones?.FirstOrDefault()?.Formatted?.SubstringSafe(20),
//                        MinExperienceYears = vacancy.experience?.minYears,
//                        MaxExperienceYears = vacancy.experience?.maxYears,
//                        SalaryPeriod = SalaryPeriod.Month,
//                    };
//                    if (vac.ProfessionId?.Length > 50) { }
//                    return vac;
//                }).ToList();

//                if (
//                    companyDb.Name.Length > 100
//                ) {
//                    companyDb.Name = companyDb.Name.SubstringSafe(100);
//                    companyDb.NormalizedName = companyDb.NormalizedName.SubstringSafe(100);
//                }

//                companiesDbDic.Add(companyDb.NormalizedName, companyDb);
//                //companyDb.Vacancies = new List<Vacancy>();
//                //companyDb.Filials = new List<CompanyFilial>();
//                companyRep.Add(companyDb);
//            }


//            await db.SaveChangesAsync();

//        }


//        public static async void SetVacanciesProfessions()
//        {
//            var professions = File.ReadAllText("D:\\hh professions map.json");

//            var professionsListList = JsonConvert.DeserializeObject<Lookup[]>(professions);

//            Db = new JobsMapContext();
//            var vacancyRep = new Repository<Vacancy>(Db);


//            var vacancies = await vacancyRep.GetQueriable().ToListAsync();
//            var vacanciesToUpdate = new List<Vacancy>(vacancies.Count);

//            var professionsDic = professionsListList.ToDictionary(_ => _.id, _ => _.name);

//            var count = 0;
//            foreach (var vacancy in vacancies) {
//                Debug.WriteLine(++count);
//                if (professionsDic.TryGetValue(vacancy.ProfessionId, out var profession)) {
//                    vacancy.ProfessionId = profession;
//                    vacanciesToUpdate.Add(vacancy);
//                    //var entry = Db.Entry(vacancy);
//                    //entry.Property(_ => _.ProfessionId).IsModified = true;
//                }
//            }

//            Db.UpdateRange(vacanciesToUpdate);



//            await Db.SaveChangesAsync();

//        }

//    }



//    class AddCom : IEqualityComparer<HhAddress>
//    {
//        public bool Equals(HhAddress xA, HhAddress yA)
//        {
//            return xA?.lat == yA?.lat && xA?.lng == yA?.lng;
//        }

//        public int GetHashCode(HhAddress addr)
//        {
//            return addr == null ? 0 : (int) ((addr.lat ?? 1) * (addr.lng ?? 1));
//        }

//        public static AddCom Ins = new AddCom();
//    }

//    public class Phone
//    {
//        public object comment { get; set; }
//        public string city { get; set; }
//        public string number { get; set; }
//        public string country { get; set; }
//        public string Formatted { get; set; }
//    }

//    public class Contacts
//    {
//        public List<Phone> phones { get; set; }
//        public string name { get; set; }
//        public string email { get; set; }
//    }

//    public class Employer
//    {
//        public string id { get; set; }
//    }

//    public class Specialization
//    {
//        public string profarea_id { get; set; }
//        public string profarea_name { get; set; }
//        public string id { get; set; }
//        public string name { get; set; }
//    }

//    public class Experience
//    {
//        public string id { get; set; }
//        public string name { get; set; }

//        public int? years { get; set; }


//        public int? minYears
//        {
//            get
//            {
//                switch (id)
//                {
//                    case "noExperience": return 0;
//                    case "between1And3": return 1;
//                    case "between3And6": return 3;
//                    case "moreThan6": return 6;
//                }
//                return null;
//            }
//        }
//        public int? maxYears
//        {
//            get
//            {
//                switch (id)
//                {
//                    case "noExperience": return 0;
//                    case "between1And3": return 3;
//                    case "between3And6": return 6;
//                    case "moreThan6": return 9;
//                }
//                return null;
//            }
//        }
//    }

//    public class HhVacancyFullInfo
//    {
//        public string alternate_url { get; set; }
//        public string description { get; set; }
//        public Lookup schedule { get; set; }
//        public DateTime published_at { get; set; }
//        public Experience experience { get; set; }
//        public HhAddress address { get; set; }
//        public Lookup employment { get; set; }
//        public string id { get; set; }
//        public Salary salary { get; set; }
//        public string name { get; set; }
//        public Contacts contacts { get; set; }
//        public Employer employer { get; set; }
//        public DateTime created_at { get; set; }
//        public List<Specialization> specializations { get; set; }

//        public EmploymentType EmploymentType
//        {
//            get
//            {
//                if (employment == null) return EmploymentType.FullTime;
//                switch (employment.id) {
//                    case "full": return EmploymentType.FullTime;
//                    case "part": return EmploymentType.PartTime;
//                    case "project": return EmploymentType.ProjectWork;
//                    case "probation": return EmploymentType.Probation;
//                    default: return EmploymentType.FullTime;
//                }
//            }
//        }
//    }


//    public class Salary
//    {
//        public int? to { get; set; }
//        public bool? gross { get; set; }
//        public int? from { get; set; }
//        public string currency { get; set; }

//        // dummy
//        public string Currency { get; set; }

//    }

//    public class HhAddress
//    {
//        public string building { get; set; }
//        public string city { get; set; }
//        public string description { get; set; }
//        public string raw { get; set; }
//        public string street { get; set; }
//        public double? lat { get; set; }
//        public double? lng { get; set; }
//        public string id { get; set; }
//        public string FilialId { get; set; }
//    }

//    public class Lookup
//    {
//        public string id { get; set; }
//        public string name { get; set; }
//    }




//    class HhCompanyFullInfo
//    {
//        public Dictionary<string, string> logo_urls { get; set; }
//        public string vacancies_url { get; set; }
//        public string name { get; set; }
//        public int open_vacancies { get; set; }
//        public string url { get; set; }
//        public string alternate_url { get; set; }

//        public string id { get; set; }
//        //public bool trusted { get; set; }

//        public HhVacancyFullInfo[] Vacancies { get; set; }
//        public HhAddress[] Addresses { get; set; }

//        public string ContactPhone { get; set; }
//        public string ContactEmail { get; set; }
//    }


//}
