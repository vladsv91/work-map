using System;
using BusinessLogic.Services;
using Data;
using Data.Entities.Common;
using FluentScheduler;
using Infrastructure;
using Microsoft.Extensions.DependencyInjection;

namespace JobsMap.Web
{
    public static class Schedules
    {
        public static void RegisterJobs(IServiceProvider serviceProvider)
        {
            var registry = new Registry();

            registry.Schedule(()=> new MigrateLogsJob(serviceProvider)).ToRunEvery(Settings.AppSettings.LogsMigrationHours).Hours();

            JobManager.Initialize(registry);
        }
    }

    public class MigrateLogsJob : IJob
    {

        private readonly IServiceProvider _serviceProvider;

        public MigrateLogsJob(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async void Execute()
        {
            var dbLogs = _serviceProvider.GetService<JobsMapLogsContext>();
            var a = new LogMigration(
                new RepositoryGuid<Log>(dbLogs),
                new RepositoryGuid<MigratedLog>(dbLogs),
                new UnitOfWork(dbLogs));
            await a.MigrateLog();
        }
    }

}
