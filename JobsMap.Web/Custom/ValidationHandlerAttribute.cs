using Microsoft.AspNetCore.Mvc.Filters;

namespace JobsMap.Web.Custom
{
    public class ValidationHandlerAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = WebAppUtils.GetBadRequestResult(context.ModelState);
            }
        }
    }
}
