using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Models.Common;
using Infrastructure;
using JobsMap.Web.Controllers;
using JobsMap.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;

namespace JobsMap.Web.Custom
{

    public static class AppCulturesWeb
    {
        public static List<IBaseRequestCultureProvider> RequestCultureProviders { get; }

        static AppCulturesWeb()
        {
            RequestCultureProviders = new List<IBaseRequestCultureProvider>
            {
                new MyCookiesRequestCultureProvider(),
                new MyAcceptLanguageRequestCultureProvider(),
                new MyHostRequestCultureProvider(),
            };
        }

        public static CultureData GetCultureFromRequest(HttpContext context)
        {
            foreach (var baseRequestCultureProvider in RequestCultureProviders) {
                CultureData cult;
                if ((cult = baseRequestCultureProvider.TryGetCultureData(context)) != null) {
                    return cult;
                }
            }
            return AppCultures.DefaultCulture;
        }
    }

    public interface IBaseRequestCultureProvider : IRequestCultureProvider
    {
        CultureData TryGetCultureData(HttpContext httpContext);
    }

    public abstract class BaseRequestCultureProvider : IBaseRequestCultureProvider
    {
        public Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {
            var c = TryGetCultureData(httpContext);
            if (c == null) return Task.FromResult<ProviderCultureResult>(null);
            return Task.FromResult(new ProviderCultureResult(c.FullName));
        }

        public abstract CultureData TryGetCultureData(HttpContext httpContext);
    }

    public class MyAcceptLanguageRequestCultureProvider : BaseRequestCultureProvider
    {
        public override CultureData TryGetCultureData(HttpContext httpContext)
        {
            return AppCultures.GetCultureDataFromHeaders(httpContext.Request);
        }
    }

    public class MyHostRequestCultureProvider : BaseRequestCultureProvider
    {
        public override CultureData TryGetCultureData(HttpContext httpContext)
        {
            return AppCultures.GetCultureDataByHost(httpContext.Request);
        }
    }

    public class MyCookiesRequestCultureProvider : BaseRequestCultureProvider
    {
        public override CultureData TryGetCultureData(HttpContext httpContext)
        {
            return AppCultures.GetCultureDataByCookies(httpContext);
        }
    }
}
