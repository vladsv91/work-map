using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic.Models.Common;
using Infrastructure;
using JobsMap.Web.Controllers;
using JobsMap.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Razor;

namespace JobsMap.Web.Custom
{
    public static class WebAppUtils
    {
        public static string GetCurrentUserId(this HttpContext httpContext)
        {
            return httpContext.User.Claims.First(_ => _.Type == AppConstants.AuthOptions.Claims.Keys.UserId).Value;
        }

        public static BadRequestObjectResult GetBadRequestResult(ModelStateDictionary ms)
        {
            return new BadRequestObjectResult(ms);
        }

        public static IList<T> GetListDataFromContext<T>(this RazorPageBase rp)
        {
            var entityDataFromServer = GetServerDataFromContext(rp);
            var data = (PaginatedDataResultVM<T>) entityDataFromServer.Data;
            return data.Items;
        }

        public static T GetItemDetailsDataFromContext<T>(this RazorPageBase rp)
        {
            return (T) rp.GetServerDataFromContext().Data;
        }

        public static T GetItemDetailsDataFromContext<T>(this Controller rp)
        {
            var dic = rp.TempData;
            var a = dic[HomeController.EntityDataFromServer] as DataFromServer;
            return (T) a.Data;
        }

        public static DataFromServer GetServerDataFromContext(this RazorPageBase rp)
        {
            var dic = rp.TempData;
            return dic[HomeController.EntityDataFromServer] as DataFromServer;
        }

        public static PaginatedDataResultVM GetPaginatedDataFromContext(this RazorPageBase rp)
        {
            var entityDataFromServer = GetServerDataFromContext(rp);
            var data = (PaginatedDataResultVM) entityDataFromServer.Data;
            return data;
        }

        public static IPaginatedDataResultVM<object> GetPaginatedDataListFromContext(this RazorPageBase rp)
        {
            var entityDataFromServer = GetServerDataFromContext(rp);
            var data = (IPaginatedDataResultVM<object>) entityDataFromServer.Data;
            return data;
        }

        public static int GetPageNumberFromHr(this HttpContext httpContext)
        {
            var page = 1;
            if (httpContext.Request.Query.TryGetValue("page", out var pageValues)) {
                var pq = pageValues.FirstOrDefault();
                if (pq != null) {
                    int.TryParse(pq, out page);
                    page = page > 0 ? page : 1;
                }
            }
            return page;
        }

        public static string GetSearchFromHr(this HttpContext httpContext)
        {
            return httpContext.Request.Query.TryGetValue("s", out var stringValues) ? stringValues.FirstOrDefault() : null;
        }

        public static string GetHostWithoutWww(this HttpContext httpContext)
        {
            var host = httpContext.Request.Host.ToString();
            var ind = host.IndexOf("wwww.", StringComparison.Ordinal);
            if (ind != -1) {
                return host.Substring(ind);
            }
            return host;
        }
    }
}
