using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using BusinessLogic.Utils;
using Data.Entities.Common;

namespace JobsMap.Web.Custom
{
    public class ApiExceptionHandlerAttribute : ExceptionFilterAttribute
    {
        public override async Task OnExceptionAsync(ExceptionContext context)
        {
            var exception = context.Exception;

            context.ExceptionHandled = true;
            if (exception is ValidationException validationException) HandleValidationException(validationException, context);
            else await HandleServerErrorException(exception, context);
        }

        private void HandleValidationException(ValidationException validationException, ExceptionContext context)
        {
            Dictionary<string, string[]> dic = null;
            if (validationException.Errors != null && validationException.Errors.Any())
                dic = validationException.Errors.ToDictionary(_ => _.PropertyName,
                    _ => new[] {$"'{_.PropertyName}' {_.ErrorMessage}"});
            context.Result = new JsonResult((object) dic ?? new {ErrorMessage = validationException.Message})
            {
                StatusCode = validationException is CustomValidationException
                    ? (int) ((CustomValidationException) validationException).StatusCode
                    : (int) HttpStatusCode.BadRequest
            };
        }

        private async Task HandleServerErrorException(Exception exception, ExceptionContext context)
        {
            var errMsg = GetExceptionFullMessage(exception);

            await context.HttpContext.GetLogger().LogAsync(LogType.Error, errMsg, context.HttpContext);

#if !DEBUG
            errMsg = "An error occurred.";
#endif
            context.Result = new JsonResult(new { ErrorMessage = errMsg })
            {
                StatusCode = (int)HttpStatusCode.InternalServerError
            };
        }

        public static string GetExceptionFullMessage(Exception exception)
        {
            return GetExceptionAllMessages(exception) + "\nStack Trace: " + exception.StackTrace;
        }

        public static string GetExceptionAllMessages(Exception exception)
        {
            var message = "";
            do {
                message += exception.Message + "\n\n";
                exception = exception.InnerException;
            } while (exception != null);
            return message;
        }
    }

    // public class MvcExceptionLoggerAttribute : ExceptionFilterAttribute
    // {
    //     public override Task OnExceptionAsync(ExceptionContext context)
    //     {
    //         var errMsg = ApiExceptionHandlerAttribute.GetExceptionFullMessage(context.Exception);
    //         return context.HttpContext.GetLogger().LogAsync(LogType.Error, errMsg, context.HttpContext);
    //     }
    // }

}
