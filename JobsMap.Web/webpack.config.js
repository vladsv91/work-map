const path = require('path');
const webpack = require('webpack');
const formatter = require('eslint-friendly-formatter');
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

const bundleOutputDir = './wwwroot/dist';

module.exports = (envParams = { useNodeSsr: false, appName: '' }) => {
  const isDevBuild = process.env.NODE_ENV === 'development';
  // const envName = isDevBuild ? '"development"' : '"production"';

  process.env.appName = envParams.appName;

  const miniCssExtractPlugin = new MiniCssExtractPlugin({
    filename: '[name].css',
    chunkFilename: '[id].css',
  });


  const config1 = {
    mode: isDevBuild ? 'development' : 'production',
    devtool: false,
    stats: { modules: false },
    entry: {
      main: './ClientApp/main.ts',
    },
    resolve: {
      extensions: ['.vue', '.ts', '.tsx', '.js', '.css', '.sass', '.scss'],
      alias: {
        components: path.resolve(__dirname, './ClientApp/components'),
        src: path.resolve(__dirname, './ClientApp'),
        assets: path.resolve(__dirname, './ClientApp/assets'),
      },
      modules: ['node_modules'],
    },
    output: {
      path: path.join(__dirname, bundleOutputDir),
      filename: '[name].js',
      publicPath: '/dist/',
    },
    module: {
      rules: getRules(false),
    },
    plugins: (isDevBuild ? [
      new webpack.SourceMapDevToolPlugin({
        // filename: '[file].map', // Remove this line if you prefer inline source maps
        moduleFilenameTemplate: path.relative(bundleOutputDir, '[resourcePath]'), // Point sourcemap entries to the original file locations on disk
      }),
    ] : [])
      .concat(
        getBasePlugins().concat([
          // new webpack.DllReferencePlugin({
          //   context: __dirname,
          //   manifest: { name: 'vendor_6128f97ffec86a495c66', content: {} },
          // }),
        ]),
      ),

    optimization: {
      minimizer: [
        // new UglifyJsPlugin({
        //   sourceMap: isDevBuild,
        //   uglifyOptions:
        //     { sourceMap: isDevBuild },
        // }),

        new TerserPlugin({
          cache: true,
          parallel: true,
          sourceMap: isDevBuild, // Must be set to true if using source-maps in production
        }),

        new OptimizeCSSAssetsPlugin({}),
      ],
    },

  };

  if (isDevBuild && !envParams.useNodeSsr) return config1;

  const config2 = Object.assign({}, config1, {
    target: 'node',
    entry: {
      'main.server': './ClientApp/server.ts',
    },
    output: {
      libraryTarget: 'commonjs2',
      path: path.join(__dirname, bundleOutputDir),
      filename: '[name].js',
      publicPath: '/dist/',
    },
  });


  // remove minimizing, becouse, Regex pattern exeption
  config2.plugins = getBasePlugins();

  config2.optimization = {
    minimizer: [],
  };

  // TODO: maybe should work with minifying
  // config2.optimization = {
  //   minimizer: [
  //     new TerserPlugin({
  //       cache: true,
  //       parallel: true,
  //       sourceMap: true, // Must be set to true if using source-maps in production
  //       terserOptions: {
  //         // https://github.com/webpack-contrib/terser-webpack-plugin#terseroptions
  //       }
  //     }),
  //   ],
  // };

  config2.module = {
    rules: getRules(true),
  };

  delete config2.devtool;

  return [config1, config2];

  function getBasePlugins() {
    return [
      new VueLoaderPlugin(),
      new webpack.DefinePlugin({ 'envVars': { appName: JSON.stringify(envParams.appName) } }),
      miniCssExtractPlugin,
    ];
  }

  function getRules(isForSsr) {
    const rules = [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [{
          loader: 'ts-loader',
          options: {
            appendTsSuffixTo: [/\.vue$/],
          },
        }],
      },
      {
        test: /\.vue$/,
        include: /ClientApp/,
        use: [{
          loader: 'vue-loader',
          options: {
            extractCSS: true,
          },
        }],
      },
      {
        test: /\.js$/,
        use: [{
          loader: 'babel-loader',
        }],
        include: /ClientApp/,
        // exclude(file) {
        //   return /node_modules/.test(file)
        //     &&
        //     !/\.vue\.js/.test(file);
        // },
      },
      { test: /\.(png|jpg|jpeg|gif|svg)$/, use: 'url-loader?limit=25000' },

      {
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        exclude: /node_modules/,
        options: {
          formatter,
        },
      },
      {
        test: /template\.(html)$/,
        use: [{
          loader: 'html-loader',
        }],
      },
    ];


    // {
    //   test: /\.css$/,
    //   use: [
    //     MiniCssExtractPlugin.loader,
    //     "css-loader"
    //   ]
    // },
    // {
    //   test: /\.scss$/,
    //   use: !buildingForLocal() ?
    //       [
    //         MiniCssExtractPlugin.loader,
    //         "css-loader", 'sass-loader'
    //       ] :
    //       [{
    //           loader: "style-loader" // creates style nodes from JS strings
    //         }, {
    //           loader: "css-loader" // translates CSS into CommonJS
    //         }, {
    //           loader: "sass-loader" // compiles Sass to CSS
    //         }]
    // },

    if (isForSsr) {
      rules.push({
        test: /\.(css|scss)$/,
        use: [
          {
            loader: 'ignore-loader',
          },
        ],
      });
    } else {
      rules.push(
        {
          test: /\.css$/,
          use: [
            {
              loader: isDevBuild ? 'vue-style-loader'
                : MiniCssExtractPlugin.loader,
            },
            {
              loader: 'css-loader',
            },
          ],
        },
        {
          test: /\.scss$/,
          use: [
            {
              loader: isDevBuild ? 'vue-style-loader'
                : MiniCssExtractPlugin.loader,
            },
            {
              loader: 'css-loader',
            },
            {
              loader: 'sass-loader',
            },
          ],
        },
      );
    }

    return rules;
  }
};
