//using System;
//using System.Collections.Generic;
//using System.Diagnostics;
//using System.IO;
//using System.Linq;
//using System.Threading.Tasks;
//using BusinessLogic.Utils;
//using Data;
//using Data.Entities.Account;
//using Data.Entities.Common;
//using Data.Entities.CompanyEntities;
//using Google.Maps;
//using Google.Maps.Geocoding;
//using Infrastructure;
//using Microsoft.EntityFrameworkCore;
//using Newtonsoft.Json;

//namespace JobsMap.Web
//{
//    public static class DouStartActionsTemporary
//    {
//        // for saving
//        public static JobsMapContext Db;

      
//        public static async void SetCompaniesContacts()
//        {
//            var companiesJson = File.ReadAllText("D:\\companies full info 2.txt");

//            var allCompaniesList = JsonConvert.DeserializeObject<List<CompanyFullInfo>>(companiesJson);

//            var db = Db = new JobsMapContext();
//            var companyRep = new Repository<Company>(db);

//            var companiesDictionary = allCompaniesList.ToDictionary(_ => _.CompanyUrl, _ => new {_.ContactEmail, _.ContactPhone});

//            var companiesDb = await companyRep.GetQueriable().Where(_ => _.CreationType == CompanyCreationType.FromDou).ToListAsync();
//            foreach (var companyDb in companiesDb) {
//                var companyDou = companiesDictionary[companyDb.DouUaUrl];

//                companyDb.ContactEmail = companyDou.ContactEmail;
//                companyDb.ContactPhone = companyDou.ContactPhone;

//                if (companyDb.ContactEmail.Length > 100) {
                    
//                }

//                if (companyDb.ContactPhone.Length > 20) {
//                    companyDb.ContactPhone = companyDb.ContactPhone.Substring(0, 20);
//                }

//                companyRep.Update(companyDb);
//            }

//            await db.SaveChangesAsync();
//        }

//        public static async Task ChangeAddresses()
//        {
//            var db = Db = new JobsMapContext();
//            var companyRep = new Repository<Company>(db);
//            var addressRep = new Repository<Address>(db);
//            var allCompanies = await companyRep.GetQueriable()
//                .Include(_ => _.Filials).ThenInclude(_ => _.Address)
//                .Where(_ => _.CreationType == CompanyCreationType.FromDou &&
//                            _.Filials.Any(f => f.Address.Latitude == 0 && f.Address.Longitude == 0)).ToListAsync();
//            var geocodingService = new GeocodingService();
//            var count = -1;

//            //var tasks = new Task[allCompanies.Count];
//            foreach (var company in allCompanies) {
//                count++;
//                Debug.WriteLine(count);
//                //tasks[count] = SetAddressesToFilials(company, geocodingService, addressRep);
//                await SetAddressesToFilials(company, geocodingService, addressRep);
//            }
//            //Task.WaitAll(tasks);
//            await db.SaveChangesAsync();
//        }

//        private static async Task SetAddressesToFilials(Company company, GeocodingService geocodingService, Repository<Address> addressRep)
//        {
//            var tasks = new Task[company.Filials.Count];
//            var count = -1;
//            foreach (var companyFilial in company.Filials) {
//                count++;
//                tasks[count] = SetAddresDataToFilial(geocodingService, addressRep, companyFilial);
//            }
//            await Task.WhenAll(tasks);
//        }

//        private static async Task SetAddresDataToFilial(GeocodingService geocodingService, Repository<Address> addressRep,
//            CompanyFilial companyFilial)
//        {
//            var addressFormattedAddress = companyFilial.Address.FormattedAddress;
//            var count = 0;
//            do {
//                count++;
//                GeocodeResponse response = null;

//                try {
//                    response = await
//                        geocodingService.GetResponseAsync(new GeocodingRequest
//                        {
//                            Address = new Location(addressFormattedAddress),
//                            Language = "ru-RU"
//                            //Region = "Ru",
//                        });
//                }
//                catch (Exception e) {
//                    break;
//                }

//                if (response.Status == ServiceResponseStatus.Ok && response.Results.Length > 0) {
//                    ServiceUtils.SetAddressData(companyFilial.Address, response, false);
//                    break;
//                }
//                if (response.Status == ServiceResponseStatus.OverQueryLimit) break;

//                var indOfCom = addressFormattedAddress.LastIndexOf(',');
//                if (indOfCom == -1) {
//                    var indOfBrcket = addressFormattedAddress.LastIndexOf('(');
//                    if (indOfBrcket != -1) addressFormattedAddress = addressFormattedAddress.Substring(0, indOfBrcket);
//                    else break;
//                }
//                else {
//                    addressFormattedAddress = addressFormattedAddress.Substring(0, indOfCom);
//                }
//            } while (true);
//            addressRep.Update(companyFilial.Address);
//        }

//        public static async void AddCompanies()
//        {
//            //return;
//            var companiesJson = File.ReadAllText("D:\\companies full info.txt");

//            var allCompaniesList = JsonConvert.DeserializeObject<List<CompanyFullInfo>>(companiesJson);

//            var db = new JobsMapContext();
//            var companyRep = new Repository<Company>(db);
//            //var addressRep = new Repository<Address>(db);
//            var userRep = new Repository<ApplicationUser>(db);
//            var myUser = await userRep.GetQueriable().FirstOrDefaultAsync(_ => _.NormalizedEmail == "VLADSV91@GMAIL.COM");
//            //addressRep.Add(new Address
//            //{
//            //    FormattedAddress = "<Unknown>",
//            //});

//            //var geocodingService = new GeocodingService();

//            var companiesDb = new List<Company>(allCompaniesList.Count);
//            var count = -1;
//            foreach (var companyFullInfo in allCompaniesList) {
//                count++;
//                Debug.WriteLine(count);
//                if (companyFullInfo.Addresses.Length == 0) continue;

//                var empCountInt = EployeeCount(companyFullInfo.EmployeeCounts);

//                var companyDb = new Company
//                {
//                    Id = companyFullInfo.Id,
//                    CreatorId = myUser.Id,
//                    ContactEmail = "",
//                    IsDisabled = false,
//                    Name = companyFullInfo.Name,

//                    CreationType = CompanyCreationType.FromDou,
//                    ContactPhone = "",
//                    Description = companyFullInfo.Description.SubstringSafe(3000),
//                    IndustryId = "ITAndNetworkServicesAndSupport",
//                    DouUaUrl = companyFullInfo.CompanyUrl,
//                    EmployeeCount = empCountInt,
//                    NormalizedName = companyFullInfo.Name.ToUpper(),
//                    WebsiteUrl = companyFullInfo.WebsiteUrl
//                };

//                companyDb.Filials = companyFullInfo.Addresses.Select((address, index) =>
//                {
//                    var addressDb = new Address
//                    {
//                        Id = GeneralUtils.CreateUniqId(),
//                        CreatedOnUtc = DateTime.UtcNow,
//                        UpdatedOnUtc = DateTime.UtcNow,
//                        FormattedAddress = address.FormattedAddress
//                    };

//                    //var addressFormattedAddress = address.FormattedAddress;
//                    //do {
//                    //    var response =
//                    //        geocodingService.GetResponse(new GeocodingRequest {Address = new Location(addressFormattedAddress)});

//                    //    if (response.Status == ServiceResponseStatus.Ok && response.Results.Length > 0) {
//                    //        ServiceUtils.SetAddressData(addressDb, response, false);
//                    //        break;
//                    //    }

//                    //    var indOfCom = addressFormattedAddress.LastIndexOf(',');
//                    //    if (indOfCom == -1) {
//                    //        break;
//                    //    }
//                    //    addressFormattedAddress = addressFormattedAddress.Substring(0, indOfCom);
//                    //} while (true);


//                    var companyFilial = new CompanyFilial
//                    {
//                        Id = GeneralUtils.CreateUniqId(),
//                        CreatedOnUtc = DateTime.UtcNow,
//                        UpdatedOnUtc = DateTime.UtcNow,
//                        Address = addressDb,
//                        IsMain = address.Id == 0,
//                        EmployeeCount = 0,
//                        Name = address.City
//                    };
//                    address.FilialId = companyFilial.Id;
//                    return companyFilial;
//                }).ToList();

//                companyDb.Vacancies = companyFullInfo.Vacancies.Where(_ => _.AddressId != null).Select(vacancy =>
//                {
//                    var created = DateTime.Parse(vacancy.Date);


//                    var requirements = vacancy.Requirements;
//                    if (vacancy.WillBePlus.NotNullAndEmpty()) requirements += "\nWill be a plus:\n" + vacancy.WillBePlus;

//                    decimal? salaryPerMonth = null;
//                    var salary = vacancy.Salary;
//                    if (salary.NotNullAndEmpty()) {
//                        var startedNumbersSalary = false;
//                        var salaryStr = "";
//                        for (var i = salary.Length - 1; i >= 0; i--) {
//                            var symbol = salary[i];
//                            if (symbol >= '0' && symbol <= '9') {
//                                startedNumbersSalary = true;
//                                salaryStr = symbol + salaryStr;
//                            }
//                            else {
//                                if (startedNumbersSalary) break;
//                            }
//                        }
//                        salaryPerMonth = decimal.Parse(salaryStr);
//                    }


//                    return new Vacancy
//                    {
//                        Id = GeneralUtils.CreateUniqId(),
//                        CreatedOnUtc = created,
//                        UpdatedOnUtc = created,

//                        CompanyFilialId = companyFullInfo.Addresses.First(_ => _.Id == vacancy.AddressId).FilialId,
//                        Name = vacancy.Name,

//                        CurrencyId = Currency.USD,
//                        Description = vacancy.Description,
//                        Duties = vacancy.Duties,
//                        EmploymentTypeId = EmploymentType.FullTime,
//                        IsDisabled = false,
//                        ProfessionId = "Programmer",
//                        Requirements = requirements,
//                        SalaryPerMonth = salaryPerMonth,
//                        WorkingConditions = vacancy.Suggest
//                    };
//                }).ToList();

//                if (companyDb.Vacancies.Any(_ => _.Description.Length > 5000) ||
//                    companyDb.Vacancies.Any(_ => _.Duties.Length > 5000) ||
//                    companyDb.Vacancies.Any(_ => _.Requirements.Length > 5000) ||
//                    companyDb.Vacancies.Any(_ => _.WorkingConditions.Length > 5000) ||
//                    companyDb.Vacancies.Any(_ => _.Name.Length > 200) ||
//                    companyDb.Name.Length > 200 ||
//                    companyDb.Description.Length > 3000
//                ) { }

//                companiesDb.Add(companyDb);
//                companyRep.Add(companyDb);
//            }

//            await db.SaveChangesAsync();
//        }

//        private static int EployeeCount(string empCount)
//        {
//            if (empCount.IsNullOrEmpty()) return 0;
//            var startedNumbers = false;
//            var empCountStr = "";
//            for (var i = empCount.Length - 1; i >= 0; i--)
//            {
//                var symbol = empCount[i];
//                if (symbol >= '0' && symbol <= '9')
//                {
//                    startedNumbers = true;
//                    empCountStr = symbol + empCountStr;
//                }
//                else
//                {
//                    if (startedNumbers)
//                        break;
//                }
//            }
//            return int.Parse(empCountStr);
//        }

//    }

//    internal class CompanyVacancyDetails
//    {
//        public string Url { get; set; }
//        public string City { get; set; }
//        public string Requirements { get; set; }
//        public string Description { get; set; } // about project
//        public string Name { get; set; }
//        public string Duties { get; set; }
//        public string WillBePlus { get; set; }
//        public string Date { get; set; }
//        public string Suggest { get; set; }
//        public string Salary { get; set; }
//        public int? AddressId { get; set; }
//    }


//    internal class AddressDetails
//    {
//        public int Id { get; set; }
//        public string City { get; set; }
//        public string FormattedAddress { get; set; }

//        // remporary
//        public string FilialId { get; set; }
//    }


//    internal class CompanyFullInfo
//    {
//        public string CompanyUrl { get; set; }
//        public string Id { get; set; }
//        public string Name { get; set; }
//        public string EmployeeCounts { get; set; }
//        public string WebsiteUrl { get; set; }
//        public string LogoUrl { get; set; }
//        public string Description { get; set; }

//        public string ContactEmail { get; set; }
//        public string ContactPhone { get; set; }

//        public AddressDetails[] Addresses { get; set; }
//        public CompanyVacancyDetails[] Vacancies { get; set; }
//    }
//}
