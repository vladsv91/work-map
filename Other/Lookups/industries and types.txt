/////////////////   Not used


Agriculture and Mining
Business Services
Computer and Electronics
Consumer Services
Education
Energy and Utilities
Financial Services
Government
Health, Pharmaceuticals, and Biotech
Manufacturing
Other



Farming and Ranching
Fishing, Hunting and Forestry and Logging
Mining and Quarrying
Other in Agriculture and Mining


Accounting and Tax Preparation
Advertising, Marketing and PR
Data and Records Management
Facilities Management and Maintenance
HR and Recruiting Services
Legal Services
Management Consulting
Payroll Services
Sales Services
Security Services
Other in Business Services


Audio, Video and Photography
Computers, Parts and Repair
Consumer Electronics, Parts and Repair
IT and Network Services and Support
Instruments and Controls
Network Security Products
Networking equipment and Systems
Office Machinery and Equipment
Peripherals Manufacturing
Semiconductor and Microchip Manufacturing
Other in Computer and Electronics


Automotive Repair and Maintenance
Funeral Homes and Services
Laundry and Dry Cleaning
Parking Lots and Garage Management
Personal Care
Photofinishing Services
Other in Consumer Services


Colleges and Universities
Elementary and Secondary Schools
Libraries, Archives and Museums
Sports, Arts, and Recreation Instruction
Technical and Trade Schools
Test Preparation
Other in Education


Alternative Energy Sources
Gas and Electric Utilities
Gasoline and Oil Refineries
Sewage Treatment Facilities
Waste Management and Recycling
Water Treatment and Utilities
Other in Energy and Utilities


Banks
Credit Cards and Related Services
Credit Unions
Insurance and Risk Management
Investment Banking and Venture Capital
Lending and Mortgage
Personal Financial Planning and Private Banking
Securities Agents and Brokers
Trust, Fiduciary, and Custody Activities
Other in Financial Services


International Bodies and Organizations
Local Government
National Government
State/Provincial Government
Other in Government


Biotechnology
Diagnostic Laboratories
Doctors and Health Care Practitioners
Hospitals
Medical Devices
Medical Supplies and Equipment
Outpatient Care Centers
Personal Health Care Products
Pharmaceuticals
Residential and Long-term Care Facilities
Veterinary Clinics and Services
Other in Health, Pharmaceuticals, and Biotech


Aerospace and Defense
Alcoholic Beverages
Automobiles, Boats and Motor Vehicles
Chemicals and Petrochemicals
Concrete, Glass and Building Materials
Farming and Mining Machinery and Equipment
Food and Dairy Product Manufacturing and Packaging
Furniture Manufacturing
Metals Manufacturing
Nonalcoholic Beverages
Paper and Paper Products
Plastics and Rubber Manufacturing
Textiles, Apparel and Accessories
Tools, Hardware and Light Machinery
Other in Manufacturing


Other







// types
AgricultureAndMining
BusinessServices
ComputerAndElectronics
ConsumerServices
Education
EnergyAndUtilities
FinancialServices
Government
HealthPharmaceuticalsAndBiotech
Manufacturing
Other
// types



// only industries
FarmingAndRanching
FishingHuntingAndForestryAndLogging
MiningAndQuarrying
OtherInAgricultureAndMining


AccountingAndTaxPreparation
AdvertisingMarketingAndPR
DataAndRecordsManagement
FacilitiesManagementAndMaintenance
HRAndRecruitingServices
LegalServices
ManagementConsulting
PayrollServices
SalesServices
SecurityServices
OtherInBusinessServices


AudioVideoAndPhotography
ComputersPartsAndRepair
ConsumerElectronicsPartsAndRepair
ITAndNetworkServicesAndSupport
InstrumentsAndControls
NetworkSecurityProducts
NetworkingequipmentAndSystems
OfficeMachineryAndEquipment
PeripheralsManufacturing
SemiconductorAndMicrochipManufacturing
OtherInComputerAndElectronics


AutomotiveRepairAndMaintenance
FuneralHomesAndServices
LaundryAndDryCleaning
ParkingLotsAndGarageManagement
PersonalCare
PhotofinishingServices
OtherInConsumerServices


CollegesAndUniversities
ElementaryAndSecondarySchools
LibrariesArchivesAndMuseums
SportsArtsAndRecreationInstruction
TechnicalAndTradeSchools
TestPreparation
OtherInEducation


AlternativeEnergySources
GasAndElectricUtilities
GasolineAndOilRefineries
SewageTreatmentFacilities
WasteManagementAndRecycling
WaterTreatmentAndUtilities
OtherInEnergyAndUtilities


Banks
CreditCardsAndRelatedServices
CreditUnions
InsuranceAndRiskManagement
InvestmentBankingAndVentureCapital
LendingAndMortgage
PersonalFinancialPlanningAndPrivateBanking
SecuritiesAgentsAndBrokers
TrustFiduciaryAndCustodyActivities
OtherInFinancialServices


InternationalBodiesAndOrganizations
LocalGovernment
NationalGovernment
State/ProvincialGovernment
OtherInGovernment


Biotechnology
DiagnosticLaboratories
DoctorsAndHealthCarePractitioners
Hospitals
MedicalDevices
MedicalSuppliesAndEquipment
OutpatientCareCenters
PersonalHealthCareProducts
Pharmaceuticals
ResidentialAndLong-termCareFacilities
VeterinaryClinicsAndServices
OtherInHealthPharmaceuticalsAndBiotech


AerospaceAndDefense
AlcoholicBeverages
AutomobilesBoatsAndMotorVehicles
ChemicalsAndPetrochemicals
ConcreteGlassAndBuildingMaterials
FarmingAndMiningMachineryAndEquipment
FoodAndDairyProductManufacturingAndPackaging
FurnitureManufacturing
MetalsManufacturing
NonalcoholicBeverages
PaperAndPaperProducts
PlasticsAndRubberManufacturing
TextilesApparelAndAccessories
ToolsHardwareAndLightMachinery
OtherInManufacturing


Other





