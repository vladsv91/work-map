﻿using System.Threading.Tasks;

namespace Data {
    public interface IUnitOfWork
    {
        Task SaveChangesAsync();
        void SaveChanges();
    }
}