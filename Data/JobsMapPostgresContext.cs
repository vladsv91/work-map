﻿using System.IO;
using Data.Builders;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    //    public class JobsMapPostgresContext : DbContext
    //    {
    //        private static bool _hasMigrated;

    //        public JobsMapPostgresContext(DbContextOptions<JobsMapPostgresContext> dbContextOptions)
    //            : base(dbContextOptions)
    //        {
    //            BaseCtor();
    //        }

    //        public JobsMapPostgresContext() : this(JobsAndMapContextFactory.Instance.GetOptions()) { }

    //        void BaseCtor()
    //        {
    //            if (_hasMigrated) return;
    //            // ReSharper disable once VirtualMemberCallInConstructor
    //#if !DEBUG
    //            Database.Migrate();
    //#endif
    //            _hasMigrated = true;
    //        }

    //        protected override void OnModelCreating(ModelBuilder builder)
    //        {
    //            base.OnModelCreating(builder);
    //            builder
    //                .BuildLog()
    //                .BuildMigratedLog();
    //        }
    //    }



    public class JobsMapPostgresContext : DbContext
    {

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=localhost;Database=JobsMap;Username=postgres;Password=879179711");

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder
                .BuildLog()
                .BuildMigratedLog();
        }
    }



}
