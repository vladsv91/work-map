﻿using System;
using Data.Entities.Account;
using Data.Entities.ApplicantEntities;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Data.Builders
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public static class EntityBuilders
    {
        private const string SalaryColumnType = "decimal(20,8)";

        private static EntityTypeBuilder<T> BuildBaseEntityString<T>(this ModelBuilder builder) where T : class, IBaseEntity
        {
            return BuildBaseEntity<T, string>(builder);
        }

        private static EntityTypeBuilder<T> BuildBaseEntityGuid<T>(this ModelBuilder builder, bool ignoreId = false) where T : class, IBaseEntity<Guid>
        {
            return BuildBaseEntity<T, Guid>(builder, "NEWID()", ignoreId);
        }

        private static EntityTypeBuilder<T> BuildBaseEntity<T, TV>(this ModelBuilder builder, string defValueSql = null, bool ignoreId = false)
            where T : class, IBaseEntity<TV>
        {
            //var entity = builder.Entity<T>().ToTable(tableName);
            var entity = builder.Entity<T>().ToTable(typeof(T).Name);

            if (ignoreId) {
                entity.Ignore(_ => _.Id);
            }
            else {
                entity.HasKey(_ => _.Id);

                var idProp = entity.Property(_ => _.Id).IsRequired();
                if (defValueSql != null) {
                    idProp.HasDefaultValueSql(defValueSql);
                }
            }

            entity.Property(_ => _.CreatedOnUtc).IsRequired().HasDefaultValueSql("getutcdate()");
            entity.Property(_ => _.UpdatedOnUtc).IsRequired().HasDefaultValueSql("getutcdate()");

            return entity;
        }


        private static EntityTypeBuilder<T> BuildBaseLookupString<T>(this ModelBuilder builder, bool hasIndex = false)
            where T : class, IBaseLookup
        {
            var entity = builder.BuildBaseEntityString<T>();
            entity.Property(_ => _.Name).HasMaxLength(200).IsRequired();
            if (hasIndex)
                entity.HasIndex(_ => _.Name).IsUnique();

            return entity;
        }

        private static EntityTypeBuilder<T> BuildBaseStatusLookupString<T>(this ModelBuilder builder)
            where T : BaseStatusLookup
        {
            var entity = builder.BuildBaseLookupString<T>();
            entity.Property(_ => _.IsDisabled).IsRequired();

            return entity;
        }

        public static ModelBuilder BuildApplicationUser(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseEntityString<ApplicationUser>();

            entity.Property(_ => _.Email).IsRequired().HasMaxLength(AppConstants.PropertiesRules.ContactEmailMaxLen);
            entity.Property(_ => _.NormalizedEmail).IsRequired().HasMaxLength(AppConstants.PropertiesRules.ContactEmailMaxLen);
            entity.Property(_ => _.UserName).IsRequired().HasMaxLength(AppConstants.PropertiesRules.ContactEmailMaxLen);
            entity.Property(_ => _.NormalizedUserName).IsRequired().HasMaxLength(AppConstants.PropertiesRules.ContactEmailMaxLen);
            entity.Property(_ => _.PhoneNumber).IsRequired(false).HasMaxLength(AppConstants.PropertiesRules.ContactPhoneMaxLen);
            entity.Property(_ => _.Language).IsRequired().HasMaxLength(5).HasDefaultValue("en");

            entity.Property(_ => _.IpAddress).HasMaxLength(50).IsRequired();


            builder.Entity<IdentityUserClaim<string>>().ToTable("UserClaim");
            builder.Entity<IdentityUserRole<string>>().ToTable("UserInRole");
            builder.Entity<IdentityUserLogin<string>>().ToTable("UserLogin");
            builder.Entity<IdentityRoleClaim<string>>().ToTable("RoleClaim");
            builder.Entity<IdentityUserToken<string>>().ToTable("UserToken");
            builder.Entity<IdentityRole<string>>().ToTable("UserRole");


            return builder;
        }

        public static ModelBuilder BuildCompany(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseLookupString<Company>();

            entity.HasOne(_ => _.Creator).WithMany(_ => _.Companies).HasForeignKey(_ => _.CreatorId).IsRequired();
            entity.HasIndex(_ => _.CreatorId);

            entity.Property(_ => _.Name).HasMaxLength(100).IsRequired();
            entity.Property(_ => _.NormalizedName).HasMaxLength(100).IsRequired();
            entity.HasIndex(_ => _.NormalizedName).IsUnique();

            entity.Property(_ => _.Description).HasMaxLength(3000);
            entity.Property(_ => _.EmployeeCount).IsRequired();
            entity.Property(_ => _.IndustryId).IsRequired().HasMaxLength(50);
            entity.Property(_ => _.WebsiteUrl).HasMaxLength(200);
            entity.Property(_ => _.ContactEmail).HasMaxLength(AppConstants.PropertiesRules.ContactEmailMaxLen);
            entity.Property(_ => _.ContactPhone).HasMaxLength(AppConstants.PropertiesRules.ContactPhoneMaxLen);
            entity.Property(_ => _.OtherContact).HasMaxLength(200);
            entity.Property(_ => _.CreationType).IsRequired().HasDefaultValue(CompanyCreationType.Registration);
            entity.Property(_ => _.DouUaUrl).IsRequired(false).HasMaxLength(200);
            entity.Property(_ => _.HhUrl).IsRequired(false).HasMaxLength(200);

            entity.HasOne(_ => _.MainPhoto).WithMany(_ => _.CompaniesMainPhotos).HasForeignKey(_ => _.MainPhotoId)
                .OnDelete(DeleteBehavior.SetNull);
            return builder;
        }

        public static ModelBuilder BuildAddress(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseEntityString<Address>();

            entity.Property(_ => _.Street).IsRequired(false).HasMaxLength(200);
            entity.Property(_ => _.StreetNumber).IsRequired(false).HasMaxLength(100);
            entity.Property(_ => _.City).IsRequired(false).HasMaxLength(200);
            entity.Property(_ => _.Country).IsRequired(false).HasMaxLength(200);
            entity.Property(_ => _.FormattedAddress).IsRequired().HasMaxLength(400);
            entity.Property(_ => _.Description).HasMaxLength(AppConstants.PropertiesRules.ShortDescriptionLength);

            const string precision = "decimal(12,8)";
            entity.Property(_ => _.Latitude).IsRequired().HasColumnType(precision);
            entity.Property(_ => _.Longitude).IsRequired().HasColumnType(precision);

            return builder;
        }

        public static ModelBuilder BuildCompanyFilial(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseStatusLookupString<CompanyFilial>();


            entity.HasOne(_ => _.Company).WithMany(_ => _.Filials).HasForeignKey(_ => _.CompanyId).IsRequired();
            entity.HasOne(_ => _.Address).WithMany(_ => _.CompanyFilials).HasForeignKey(_ => _.AddressId).IsRequired();

            entity.Property(_ => _.EmployeeCount).IsRequired();
            entity.Property(_ => _.IsMain).IsRequired();
            entity.Property(_ => _.Description).HasMaxLength(AppConstants.PropertiesRules.ShortDescriptionLength);

            entity.Property(_ => _.ContactEmail).HasMaxLength(AppConstants.PropertiesRules.ContactEmailMaxLen);
            entity.Property(_ => _.ContactPhone).HasMaxLength(AppConstants.PropertiesRules.ContactPhoneMaxLen);

            return builder;
        }

        public static ModelBuilder BuildVacancy(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseStatusLookupString<Vacancy>();


            entity.HasOne(_ => _.CompanyFilial).WithMany(_ => _.Vacancies).HasForeignKey(_ => _.CompanyFilialId)
                .IsRequired();


            entity.HasOne(_ => _.Company).WithMany(_ => _.Vacancies).HasForeignKey(_ => _.CompanyId).OnDelete(DeleteBehavior.Restrict)
                .IsRequired();


            entity.Property(_ => _.ContactEmail).HasMaxLength(AppConstants.PropertiesRules.ContactEmailMaxLen);
            entity.Property(_ => _.ContactPhone).HasMaxLength(AppConstants.PropertiesRules.ContactPhoneMaxLen);

            entity.Property(_ => _.CurrencyId).IsRequired();
            entity.Property(_ => _.Description).IsRequired().HasMaxLength(5000);
            entity.Property(_ => _.Duties).IsRequired().HasMaxLength(5000);
            entity.Property(_ => _.Requirements).IsRequired().HasMaxLength(5000);
            entity.Property(_ => _.WorkingConditions).IsRequired().HasMaxLength(5000);
            entity.Property(_ => _.EmploymentTypeId).IsRequired();
            entity.Property(_ => _.ProfessionId).IsRequired().HasMaxLength(100);


            entity.Property(_ => _.MinSalary).IsRequired(false).HasColumnType(SalaryColumnType);
            entity.Property(_ => _.MaxSalary).IsRequired(false).HasColumnType(SalaryColumnType);
            entity.Property(_ => _.SalaryPeriod).IsRequired().HasDefaultValue(SalaryPeriod.Month);


            entity.Property(_ => _.SalaryPerMonth).IsRequired(false).HasColumnType(SalaryColumnType);

            entity.Property(_ => _.ViewsCount).IsRequired();

            return builder;
        }


        public static ModelBuilder BuildCompanyPhoto(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseEntityString<CompanyPhoto>();


            entity.HasOne(_ => _.CompanyFilial).WithMany(_ => _.CompanyPhotos).HasForeignKey(_ => _.CompanyFilialId);
            entity.HasOne(_ => _.Company).WithMany(_ => _.CompanyPhotos).HasForeignKey(_ => _.CompanyId).IsRequired();

            entity.Property(_ => _.Description).HasMaxLength(AppConstants.PropertiesRules.ShortDescriptionLength);
            //entity.Property(_ => _.PhotoFileName).IsRequired();
            //entity.Property(_ => _.PhotoUri).IsRequired();
            //entity.Property(_ => _.PhotoId).IsRequired();

            entity.HasOne(_ => _.Photo).WithMany(_ => _.CompaniesPhotos).HasForeignKey(_ => _.PhotoId)
                .OnDelete(DeleteBehavior.Restrict).IsRequired();

            return builder;
        }


        public static ModelBuilder BuildApplicant(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseEntityString<Applicant>();

            entity.HasOne(_ => _.Creator).WithMany(_ => _.Applicants).HasForeignKey(_ => _.CreatorId).IsRequired();
            entity.HasIndex(_ => _.CreatorId);

            entity.HasMany(_ => _.PreferredAddresses).WithOne(_ => _.Applicant);


            entity.Property(_ => _.FullName).HasMaxLength(100).IsRequired();
            entity.Property(_ => _.Description).HasMaxLength(1000);

            //entity.Property(_ => _.DateOfBirth);

            entity.Property(_ => _.WebsiteUrl).HasMaxLength(200);
            entity.Property(_ => _.ContactEmail).HasMaxLength(AppConstants.PropertiesRules.ContactEmailMaxLen);
            entity.Property(_ => _.ContactPhone).HasMaxLength(AppConstants.PropertiesRules.ContactPhoneMaxLen);

            entity.HasOne(_ => _.Photo).WithMany(_ => _.ApplicantsPhotos).HasForeignKey(_ => _.PhotoId)
                .OnDelete(DeleteBehavior.SetNull);

            return builder;
        }

        public static ModelBuilder BuildResume(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseStatusLookupString<Resume>();

            entity.HasOne(_ => _.Applicant).WithMany(_ => _.Resumes).HasForeignKey(_ => _.ApplicantId).IsRequired();
            entity.HasIndex(_ => _.ApplicantId);


            entity.Property(_ => _.ExperienceYears).IsRequired();
            entity.Property(_ => _.Skills).HasMaxLength(2000).IsRequired();
            entity.Property(_ => _.Description).HasMaxLength(2000);

            entity.Property(_ => _.ProfessionId).IsRequired().HasMaxLength(50); //.HasDefaultValue(Profession.Other);

            entity.Property(_ => _.SalaryPerMonth).IsRequired(false).HasColumnType(SalaryColumnType);
            entity.Property(_ => _.CurrencyId).IsRequired(false);


            entity.HasOne(_ => _.ResumeFile).WithMany(_ => _.Resumes).HasForeignKey(_ => _.ResumeFileId)
                .OnDelete(DeleteBehavior.SetNull);

            return builder;
        }

        public static ModelBuilder BuildWorkExperience(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseEntityString<WorkExperience>();

            entity.HasOne(_ => _.Resume).WithMany(_ => _.WorkExperiences).HasForeignKey(_ => _.ResumeId).IsRequired();
            entity.HasIndex(_ => _.ResumeId);


            entity.Property(_ => _.Position).HasMaxLength(100).IsRequired();
            entity.Property(_ => _.CompanyName).HasMaxLength(100).IsRequired();
            entity.Property(_ => _.DateFrom).IsRequired();
            entity.Property(_ => _.DateTo).IsRequired(false);
            return builder;
        }

        public static ModelBuilder BuildApplicantPreferredAddress(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseEntityString<ApplicantPreferredAddress>();

            // todo, maybe delete FK
            entity.HasOne(_ => _.Address).WithMany(_ => _.ApplicantPreferredAddresses).HasForeignKey(_ => _.AddressId)
                .IsRequired();
            entity.HasOne(_ => _.Applicant).WithMany(_ => _.PreferredAddresses).HasForeignKey(_ => _.ApplicantId)
                .IsRequired();

            entity.Property(_ => _.RadiusInKilometers).IsRequired();
            return builder;
        }

        public static ModelBuilder BuildUserSession(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseEntityString<UserSession>();

            entity.Ignore(_ => _.Id);
            entity.HasKey(_ => _.InitialRefreshToken);
            entity.HasIndex(_ => _.UserId); // todo, maybe no

            // todo, without FK for speed
            entity.HasOne(_ => _.User).WithMany(_ => _.UserSessions).HasForeignKey(_ => _.UserId).IsRequired();
            //entity.Property(_ => _.User).IsRequired();



            entity.Property(_ => _.Status).IsRequired().HasDefaultValue(SessionStatus.Logined);

            return builder;
        }

        public static ModelBuilder BuildUserSessionRefreshToken(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseEntityString<UserSessionRefreshToken>();

            entity.Ignore(_ => _.Id);
            entity.HasKey(_ => _.RefreshToken);
            entity.HasIndex(_ => _.InitialRefreshTokenId);


            entity.HasOne(_ => _.InitialRefreshToken).WithMany(_ => _.RefreshTokens).HasForeignKey(_ => _.InitialRefreshTokenId)
                .IsRequired();
            entity.Property(_ => _.RtExpiresOnUtc).IsRequired();
            entity.Property(_ => _.Status).IsRequired().HasDefaultValue(TokenStatus.Unused);

            return builder;
        }

        public static ModelBuilder BuildContactUs(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseEntityString<ContactUs>();

            entity.Property(_ => _.Email).HasMaxLength(AppConstants.PropertiesRules.ContactEmailMaxLen).IsRequired();
            entity.Property(_ => _.FullName).HasMaxLength(AppConstants.PropertiesRules.ContactUs.FullNameMaxLen);
            entity.Property(_ => _.PhoneNumber).HasMaxLength(AppConstants.PropertiesRules.ContactPhoneMaxLen);
            entity.Property(_ => _.Message).HasMaxLength(AppConstants.PropertiesRules.MessageMaxLen);

            return builder;
        }

        public static ModelBuilder BuildSendingResume(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseEntityString<SendingResume>();

            entity.HasOne(_ => _.Company).WithMany(_ => _.SendingResumes).HasForeignKey(_ => _.CompanyId).IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(_ => _.Vacancy).WithMany(_ => _.SendingResumes).HasForeignKey(_ => _.VacancyId)
                .IsRequired(false).OnDelete(DeleteBehavior.SetNull);
            entity.HasOne(_ => _.Resume).WithMany(_ => _.SendingResumes).HasForeignKey(_ => _.ResumeId)
                .IsRequired(false).OnDelete(DeleteBehavior.SetNull);

            entity.HasOne(_ => _.Applicant).WithMany(_ => _.SendingResumes).HasForeignKey(_ => _.ApplicantId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();


            //entity.Property(_ => _.VacancyId).IsRequired(false);
            //entity.Property(_ => _.ResumeId).IsRequired(false);

            entity.Property(_ => _.Message).HasMaxLength(AppConstants.PropertiesRules.MessageMaxLen);
            entity.Property(_ => _.Status).IsRequired().HasDefaultValue(SendingResumeStatus.Sent);

            return builder;
        }

        private static ModelBuilder BuildLogBase<T>(this ModelBuilder builder) where T: BaseLog
        {
            var entity = builder.BuildBaseEntityGuid<T>();

            entity.Property(_ => _.Message).HasMaxLength(10000);
            entity.Property(_ => _.Type).IsRequired();
            entity.Property(_ => _.IpAddress).HasMaxLength(50).IsRequired();
            entity.Property(_ => _.RequestUrl).HasMaxLength(3000).IsRequired();

            entity.Property(_ => _.UserId).IsRequired(false).HasMaxLength(100);
            entity.Property(_ => _.UserAgent).IsRequired(false).HasMaxLength(500);

            entity.Property(_ => _.HttpMethod).IsRequired().HasMaxLength(50);
            entity.Property(_ => _.SerializedHeaders).IsRequired().HasMaxLength(10000);

            entity.Property(_ => _.Host).IsRequired().HasMaxLength(100);

            return builder;
        }

        public static ModelBuilder BuildLog(this ModelBuilder builder)
        {
            return builder.BuildLogBase<Log>();
        }

        public static ModelBuilder BuildMigratedLog(this ModelBuilder builder)
        {
            return builder.BuildLogBase<MigratedLog>();
        }

        public static ModelBuilder BuildFileData(this ModelBuilder builder)
        {
            var entity = builder.BuildBaseEntityGuid<FileData>();

            entity.Property(_ => _.FileExtension).HasMaxLength(100);
            entity.Property(_ => _.FileName).HasMaxLength(300);
            entity.Property(_ => _.FileKind).IsRequired();
            entity.Property(_ => _.FileType).IsRequired();
            entity.Property(_ => _.Data).IsRequired();


            return builder;
        }
    }
}