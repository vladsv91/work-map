﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Data.Entities.Common;

namespace Data
{
    public interface IRepositoryBase<TEntity, in TId> where TEntity : IBaseEntity<TId>
    {
        IQueryable<TEntity> GetQueriable();

        void Add(TEntity entity);
        Task AddRange(IEnumerable<TEntity> entities);
        void RemoveRange(IEnumerable<TEntity> entities);
        Task<TEntity> FindAsync(TId id);
        void Update(TEntity entity);
        void UpdateButIgnoreId(TEntity entity);
        void Delete(TEntity entity);
        void Attach(TEntity entity);
        void Detach(TEntity entity);

        void UpdateSomeFields<T1>(T1 entity, params Expression<Func<T1, object>>[] propertiesToUpdate)
            where T1 : class, IBaseEntity;

        void UpdateButIngoreSomeFields<T1>(T1 entity, params Expression<Func<T1, object>>[] propertiesToIgnore)
            where T1 : class, IBaseEntity;


        void Deactivate<T1>(T1 entity) where T1 : class, IBaseStatusLookup;
        void Activate<T1>(T1 entity) where T1 : class, IBaseStatusLookup;
    }

    public interface IRepository<T> : IRepositoryBase<T, string> where T : IBaseEntity<string> { }

    public interface IRepositoryGuid<T> : IRepositoryBase<T, Guid> where T : IBaseEntity<Guid> { }
}