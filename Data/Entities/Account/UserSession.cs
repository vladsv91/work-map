﻿using System;
using System.Collections.Generic;
using Data.Entities.Common;

namespace Data.Entities.Account
{
    public class UserSession : BaseEntity
    {
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        // key
        public Guid InitialRefreshToken { get; set; }

        public List<UserSessionRefreshToken> RefreshTokens { get; set; }
        public SessionStatus Status { get; set; }
    }


    public class UserSessionRefreshToken : BaseEntity
    {
        public Guid RefreshToken { get; set; }
        public DateTime RtExpiresOnUtc { get; set; }
        public TokenStatus Status { get; set; }

        public Guid InitialRefreshTokenId { get; set; }
        public UserSession InitialRefreshToken { get; set; }
    }


    public enum TokenStatus
    {
        Unused = 1, // new
        Used,
        Revoked,
        RevokedBySystem, // when maybe was hack
        //Expired, // todo, set when user try refresh by expired token
    }

    public enum SessionStatus
    {
        Logined = 1,
        Logouted, // Revoked
        LogoutedBySystem, // when maybe was hack
        Expired,
    }
}