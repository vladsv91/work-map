﻿using System;
using System.Collections.Generic;
using Data.Entities.ApplicantEntities;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using Microsoft.AspNetCore.Identity;

namespace Data.Entities.Account
{
    public class ApplicationUser : IdentityUser<string>, IBaseEntity
    {
        public List<Company> Companies { get; set; }
        public List<Applicant> Applicants { get; set; }
        public List<UserSession> UserSessions{ get; set; }

        public UserType Type { get; set; }

        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        public DateTime CreatedOnUtc { get; set; }

        public DateTime UpdatedOnUtc { get; set; }

        public string Language { get; set; }
        public string IpAddress { get; set; }
    }
}