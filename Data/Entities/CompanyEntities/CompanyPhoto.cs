﻿using System;
using Data.Entities.Common;

namespace Data.Entities.CompanyEntities
{
    public class CompanyPhoto : BaseEntity
    {
        public string CompanyId { get; set; }
        public Company Company { get; set; }

        public string CompanyFilialId { get; set; }
        public CompanyFilial CompanyFilial { get; set; }


        public string Description { get; set; }

        public FileData Photo { get; set; }
        public Guid PhotoId { get; set; }
    }
}