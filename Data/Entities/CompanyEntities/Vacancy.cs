﻿using System.Collections.Generic;
using Data.Entities.Common;

namespace Data.Entities.CompanyEntities
{
    public class Vacancy : BaseStatusLookup
    {

        // ------------------- company ---------------------
        public string CompanyId { get; set; }
        public Company Company { get; set; }

        public string CompanyFilialId { get; set; }
        public CompanyFilial CompanyFilial { get; set; }
        // ------------------- company ---------------------


        // ------------------- salary ---------------------
        public decimal? SalaryPerMonth { get; set; } // todo, rid of this
        // todo, front-end fields & model fields
        public decimal? MinSalary { get; set; }
        public decimal? MaxSalary { get; set; }
        public SalaryPeriod SalaryPeriod { get; set; } = SalaryPeriod.Month;
        public Currency CurrencyId { get; set; }
        public EmploymentType EmploymentTypeId { get; set; }
        // ------------------- salary ---------------------


        //public string ContactPhone { get; set; }
        //public string ContactEmail { get; set; }
        public string Description { get; set; }
        public string Requirements { get; set; }
        public string Duties { get; set; }
        public string WorkingConditions { get; set; }

        public string ProfessionId { get; set; }
        public List<SendingResume> SendingResumes { get; set; }

        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }

        public int? MinExperienceYears { get; set; }
        public int? MaxExperienceYears { get; set; }


        // todo, icrement
        public int ViewsCount { get; set; }
    }
}