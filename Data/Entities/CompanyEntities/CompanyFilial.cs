﻿using System.Collections.Generic;
using Data.Entities.Common;

namespace Data.Entities.CompanyEntities
{
    public class CompanyFilial : BaseStatusLookup
    {
        public string CompanyId { get; set; }
        public Company Company { get; set; }

        public string AddressId { get; set; }
        public Address Address { get; set; }

        public List<Vacancy> Vacancies { get; set; }
        public List<CompanyPhoto> CompanyPhotos { get; set; }

        public bool IsMain { get; set; }

        public int EmployeeCount { get; set; }

        public string Description { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
    }
}