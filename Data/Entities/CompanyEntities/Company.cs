﻿using System;
using System.Collections.Generic;
using Data.Entities.Account;
using Data.Entities.Common;

namespace Data.Entities.CompanyEntities
{
    public class Company : BaseStatusLookup
    {
        //public string DisplayId { get; set; } // todo, create search
        public string CreatorId { get; set; }
        public string NormalizedName { get; set; }
        public ApplicationUser Creator { get; set; }

        public List<CompanyFilial> Filials { get; set; }

        public List<Vacancy> Vacancies { get; set; }
        public List<CompanyPhoto> CompanyPhotos { get; set; }

        public List<SendingResume> SendingResumes { get; set; }

        public string WebsiteUrl { get; set; }
        public string Description { get; set; }
        public int EmployeeCount { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string OtherContact { get; set; }
        public string IndustryId { get; set; }
        public string DouUaUrl { get; set; }
        public string HhUrl { get; set; }

        public FileData MainPhoto { get; set; }
        public Guid? MainPhotoId { get; set; }

        public CompanyCreationType CreationType { get; set; }
    }

   
}