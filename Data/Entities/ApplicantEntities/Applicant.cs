﻿using System;
using System.Collections.Generic;
using Data.Entities.Account;
using Data.Entities.Common;

namespace Data.Entities.ApplicantEntities
{
    public class Applicant : BaseEntity
    {
        public string CreatorId { get; set; }
        public ApplicationUser Creator { get; set; }

        public List<ApplicantPreferredAddress> PreferredAddresses { get; set; }
        public List<Resume> Resumes { get; set; }

        public string FullName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string WebsiteUrl { get; set; }
        public string Description { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }

        public List<SendingResume> SendingResumes { get; set; }

        public FileData Photo { get; set; }
        public Guid? PhotoId { get; set; }

    }
}