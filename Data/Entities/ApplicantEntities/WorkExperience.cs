﻿using System;
using Data.Entities.Common;

namespace Data.Entities.ApplicantEntities
{
    public class WorkExperience : BaseEntity
    {
        public string ResumeId { get; set; }
        public Resume Resume { get; set; }

        public string Position { get; set; }

        public string CompanyName { get; set; }

        public DateTime DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}