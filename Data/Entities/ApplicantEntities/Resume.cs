﻿using System;
using System.Collections.Generic;
using Data.Entities.Common;

namespace Data.Entities.ApplicantEntities
{
    public class Resume : BaseStatusLookup
    {
        public string ApplicantId { get; set; }
        public Applicant Applicant { get; set; }

        public List<SendingResume> SendingResumes { get; set; }

        public decimal ExperienceYears { get; set; }

        public string Skills { get; set; }
        public string Description { get; set; }

        public List<WorkExperience> WorkExperiences { get; set; }

        public decimal? SalaryPerMonth { get; set; }
        public Currency? CurrencyId { get; set; }

        public string ProfessionId { get; set; }


        public FileData ResumeFile { get; set; }
        public Guid? ResumeFileId { get; set; }
    }
}