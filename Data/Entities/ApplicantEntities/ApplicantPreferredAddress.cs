﻿using Data.Entities.Common;

namespace Data.Entities.ApplicantEntities
{
    public class ApplicantPreferredAddress : BaseEntity
    {
        public string AddressId { get; set; }
        public Address Address { get; set; }

        public string ApplicantId { get; set; }
        public Applicant Applicant { get; set; }

        public decimal RadiusInKilometers { get; set; }
    }
}