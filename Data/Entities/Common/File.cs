﻿using System;
using System.Collections.Generic;
using Data.Entities.ApplicantEntities;
using Data.Entities.CompanyEntities;

namespace Data.Entities.Common
{
    public class FileData : BaseEntity<Guid>
    {
        public byte[] Data { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public FileType FileType { get; set; }
        public FileKind FileKind { get; set; }

        public List<Company> CompaniesMainPhotos { get; set; }
        public List<CompanyPhoto> CompaniesPhotos { get; set; }
        public List<Applicant> ApplicantsPhotos { get; set; }
        public List<Resume> Resumes { get; set; }
    }
}