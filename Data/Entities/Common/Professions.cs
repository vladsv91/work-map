﻿using System.Collections.Generic;

namespace Data.Entities.Common
{

    public class ProfessionsInTypes : ItemsInTypes
    {
        protected override List<ItemsInType> GetItems()
        {
            return new List<ItemsInType>
            {
                //Publishing house and printing house, Издательство и типография
                // new ItemsInType("PublishingHouseAndPrintingHouse", new HashSet<string>
                // {
                //     //Commissioning Editor, Выпускающий редактор
                //     "CommissioningEditor",
                //     //Publisher, Издатель
                //     "Publisher",
                //     //Corrector, Корректор
                //     "Corrector",
                //     //Bookbinder, Переплетчик
                //     "Bookbinder",
                //     //Pressman, Печатник
                //     "Pressman",
                //     //Editor, Редактор
                //     "Editor",
                //     //Printer, Типограф
                //     "Printer",
                //     //Folders, Фальцовщик
                //     "Folders",
                // }),

                //Food, Пищевые
                // new ItemsInType("Food", new HashSet<string>
                // {
                //     //Winemaker, Винодел
                //     "Winemaker",
                //     //Confectioner, Кондитер
                //     "Confectioner",
                //     //The Marshal, Месильщик
                //     "TheMarshal",
                //     //Butcher, Мясник
                //     "Butcher",
                //     //Baker, Пекарь
                //     "Baker",
                //     //Cook, Повар
                //     "Cook",
                // }),

                //Agriculture, Сельское хозяйство
                //Agriculture, Сельское хозяйство
                // new ItemsInType("Agriculture", new HashSet<string>
                // {
                //     //Agronomist, Агроном
                //     "Agronomist",
                //     //Vet, Ветеринар
                //     "Vet",
                //     //Milkmaid, Доярка
                //     "Milkmaid",
                //     //Animal husbandry, Животновод
                //     "AnimalHusbandry",
                //     //Combine, Комбайнер
                //     "Combine",
                //     //The operator of machine milking, Оператор машинного доения
                //     "TheOperatorOfMachineMilking",
                //     //Hunter, Охотник
                //     "Hunter",
                //     //Shepherd, Пастух
                //     "Shepherd",
                //     //Beekeeper, Пчеловод
                //     "Beekeeper",
                //     //Skotnik, Скотник
                //     "Skotnik",
                //     //Sheep shearing specialist, Специалист по стрижке овец
                //     "SheepShearingSpecialist",
                //     //Tractor driver, Тракторист
                //     "TractorDriver",
                //     //Farmer, Фермер
                //     "Farmer",
                //     //Fisherman, Рыбак
                //     "Fisherman",
                // }),

                //Service and maintenance, Сервис и обслуживание
                // new ItemsInType("ServiceAndMaintenance", new HashSet<string>
                // {
                //     //Travel agent, Агент по туризму
                //     "TravelAgent",
                //     //Insurance agent, Страховой агент
                //     "InsuranceAgent",
                //     //Hotel administrator, Администратор гостиницы
                //     "HotelAdministrator",
                //     //Restaurant manager, Администратор ресторана
                //     "RestaurantManager",
                //     //Beauty Salon Administrator, Администратор салона красоты
                //     "BeautySalonAdministrator",
                //     //Bartender, Бармен
                //     "Bartender",
                //     //Librarian, Библиотекарь
                //     "Librarian",
                //     //Visagiste, Визажист
                //     "Visagiste",
                //     //Loader, Грузчик
                //     "Loader",
                //     //Janitor, Дворник
                //     "Janitor",
                //     //Storekeeper, Кладовщик
                //     "Storekeeper",
                //     //Tourism Consultant, Консультант по туризму
                //     "TourismConsultant",
                //     //Telephone helpline consultant, Консультант телефона доверия
                //     "TelephoneHelplineConsultant",
                //     //Manicurist, Маникюрша
                //     "Manicurist",
                //     //Refuse Collector, Мусоропроводчик
                //     "RefuseCollector",
                //     //Garbage man, Мусорщик
                //     "GarbageMan",
                //     //The tuner of musical instruments, Настройщик музыкальных инструментов
                //     "TheTunerOfMusicalInstruments",
                //     //Call-center operator, Оператор call-центра
                //     "CallCenterOperator",
                //     //Waiter, Официант
                //     "Waiter",
                //     //The hairdresser, Парикмахер
                //     "TheHairdresser",
                //     //Receptionist, Портье
                //     "Receptionist",
                //     //Postman, Почтальон
                //     "Postman",
                //     //Gardener, Садовник
                //     "Gardener",
                //     //Shoemaker, Сапожник
                //     "Shoemaker",
                //     //Cleaner, Уборщица
                //     "Cleaner",
                //     //Packer, Упаковщик
                //     "Packer",
                //     //Florist, Флорист
                //     "Florist",
                //     //Flower Girl, Цветочница
                //     "FlowerGirl",
                //     //Chef, Шеф-повар
                //     "Chef",
                //     //Guide, Экскурсовод
                //     "Guide",
                // }),
                //Creative, Творческие
                // new ItemsInType("Creative", new HashSet<string>
                // {
                //     //Actor, Актер
                //     "Actor",
                //     //Animator, Аниматор
                //     "Animator",
                //     //Circus artist, Артист цирка
                //     "CircusArtist",
                //     //Architect, Архитектор
                //     "Architect",
                //     //DJ, Диджей
                //     "DJ",
                //     //Designer, Дизайнер
                //     "Designer",
                //     //Ad designer, Дизайнер рекламы
                //     "AdDesigner",
                //     //Designer-designer, Дизайнер-модельер
                //     "DesignerDesigner",
                //     //Journalist, Журналист
                //     "Journalist",
                //     //Cutter, Закройщик
                //     "Cutter",
                //     //Sound Operator, Звукооператор
                //     "SoundOperator",
                //     //Sound producer, Звукорежиссер
                //     "SoundProducer",
                //     //Illustrator, Иллюстратор
                //     "Illustrator",
                //     //Image-maker, Имиджмейкер
                //     "ImageMaker",
                //     //Stuntman, Каскадер
                //     "Stuntman",
                //     //Projectionist, Киномеханик
                //     "Projectionist",
                //     //Cameraman, Кинооператор
                //     "Cameraman",
                //     //Film director, Кинорежиссер
                //     "FilmDirector",
                //     //Composer, Композитор
                //     "Composer",
                //     //Critic, Критик
                //     "Critic",
                //     //Landscaping Designer, Ландшафтный дизайнер
                //     "LandscapingDesigner",
                //     //Mannequin, Манекенщица
                //     "Mannequin",
                //     //Model, Модель
                //     "Model",
                //     //Musician, Музыкант
                //     "Musician",
                //     //Multiplier, Мультипликатор
                //     "Multiplier",
                //     //Film and television operator, Оператор кино и телевидения
                //     "FilmAndTelevisionOperator",
                //     //Singer, Певец
                //     "Singer",
                //     //Writer, Писатель
                //     "Writer",
                //     //Tailor, Портной
                //     "Tailor",
                //     //Producer, Продюсер
                //     "Producer",
                //     //Stage director, Режиссер
                //     "StageDirector",
                //     //Sculptor, Скульптор
                //     "Sculptor",
                //     //Speechwriter, Спичрайтер
                //     "Speechwriter",
                //     //Stylist, Стилист
                //     "Stylist",
                //     //Screenwriter, Сценарист
                //     "Screenwriter",
                //     //Ballet dancer, Танцор балета
                //     "BalletDancer",
                //     //Tattoo artist, Татуировщик
                //     "TattooArtist",
                //     //Technical writer, Технический писатель
                //     "TechnicalWriter",
                //     //Photographer, Фотограф
                //     "Photographer",
                //     //Fashion model, Фотомодель
                //     "FashionModel",
                //     //Choreographer, Хореограф
                //     "Choreographer",
                //     //Artist, Художник
                //     "Artist",
                //     //Costume Designer, Художник по костюму
                //     "CostumeDesigner",
                //     //Jeweler, Ювелир
                //     "Jeweler",
                //     //Stunt Maker, Постановщик трюков
                //     "StuntMaker",
                // }),
                //Technical, Технические
                // new ItemsInType("Technical", new HashSet<string>
                // {
                //     //Race driver, Автогонщик
                //     "RaceDriver",
                //     //Auto-mechanic, Автослесарь
                //     "AutoMechanic",
                //     //Operator-operator, Аппаратчик-оператор
                //     "OperatorOperator",
                //     //Architect-designer, Архитектор-проектировщик
                //     "ArchitectDesigner",
                //     //Cooper, Бондарь
                //     "Cooper",
                //     //Bulldozer, Бульдозерист
                //     "Bulldozer",
                //     //Hot Roller Mill Roller, Вальцовщик стана горячей прокатки
                //     "HotRollerMillRoller",
                //     //Diver, Водолаз
                //     "Diver",
                //     //Surveyor, Геодезист
                //     "Surveyor",
                //     //Geologist, Геолог
                //     "Geologist",
                //     //Geoecologist, Геоэколог
                //     "Geoecologist",
                //     //Chief Engineer, Главный инженер
                //     "ChiefEngineer",
                //     //Chief Designer, Главный конструктор
                //     "ChiefDesigner",
                //     //Chief Technologist, Главный технолог
                //     "ChiefTechnologist",
                //     //Upholsterer, Драпировщик
                //     "Upholsterer",
                //     //Engineer, Инженер
                //     "Engineer",
                //     //Engineer-constructor, Инженер-конструктор
                //     "EngineerConstructor",
                //     //Process Engineer, Инженер-технолог
                //     "ProcessEngineer",
                //     //Chemical Engineer, Инженер-химик
                //     "ChemicalEngineer",
                //     //Mason, Каменщик
                //     "Mason",
                //     //Cartographer, Картограф
                //     "Cartographer",
                //     //Crane operator, Крановщик
                //     "CraneOperator",
                //     //Cabinetmaker, Краснодеревщик
                //     "Cabinetmaker",
                //     //Roofer, Кровельщик
                //     "Roofer",
                //     //Caster, Литейщик
                //     "Caster",
                //     //Painter, Маляр
                //     "Painter",
                //     //Mine surveyor, Маркшейдер
                //     "MineSurveyor",
                //     //Master, Мастер
                //     "Master",
                //     //Metallurgist, Металлург
                //     "Metallurgist",
                //     //Installer, Монтажник
                //     "Installer",
                //     //Motorist, Моторист
                //     "Motorist",
                //     //Accompanist, Наладчик
                //     "Accompanist",
                //     //Facing, Облицовщик
                //     "Facing",
                //     //Decoration, Отделочник
                //     "Decoration",
                //     //A carpenter, Плотник
                //     "ACarpenter",
                //     //Production master, Производственный мастер
                //     "ProductionMaster",
                //     //Foreman, Прораб
                //     "Foreman",
                //     //Spinner, Проходчик
                //     "Spinner",
                //     //Radomechanics, Радиомеханик
                //     "Radomechanics",
                //     //Repairer, Ремонтник
                //     "Repairer",
                //     //Rihtovshchik, Рихтовщик
                //     "Rihtovshchik",
                //     //Plumber, Сантехник
                //     "Plumber",
                //     //Welder, Сварщик
                //     "Welder",
                //     //Locksmith, Слесарь
                //     "Locksmith",
                //     //Stalevar, Сталевар
                //     "Stalevar",
                //     //Wide-spreader of a wide profile, Станочник широкого профиля
                //     "WideSpreaderOfAWideProfile",
                //     //Joiner, Столяр
                //     "Joiner",
                //     //Builder, Строитель
                //     "Builder",
                //     //Operating Technician, Техник по эксплуатации
                //     "OperatingTechnician",
                //     //Technologist, Технолог
                //     "Technologist",
                //     //Turner, Токарь
                //     "Turner",
                //     //Moulder, Формовщик
                //     "Moulder",
                //     //Milling machine, Фрезеровщик
                //     "MillingMachine",
                //     //Chemist-Technologist, Химик-технолог
                //     "ChemistTechnologist",
                //     //Refrigerators, Холодильщик
                //     "Refrigerators",
                //     //Watchmaker, Часовщик
                //     "Watchmaker",
                //     //Miner, Шахтер
                //     "Miner",
                //     //Seamstress, Швея
                //     "Seamstress",
                //     //Grinder, Шлифовщик
                //     "Grinder",
                //     //Plasterer, Штукатур
                //     "Plasterer",
                //     //An electrician, Электрик
                //     "AnElectrician",
                //     //Electrician, Электромонтажник
                //     "Electrician",
                //     //Energetik, Энергетик
                //     "Energetik",
                // }),
                //Transport, Транспортные
                // new ItemsInType("Transport", new HashSet<string>
                // {
                //     //Air traffic controller, Авиадиспетчер
                //     "AirTrafficController",
                //     //Flight Engineer, Бортинженер
                //     "FlightEngineer",
                //     //Bortmehanik, Бортмеханик
                //     "Bortmehanik",
                //     //Flight attendant, Бортпроводник
                //     "FlightAttendant",
                //     //The foreman of the railway track, Бригадир железнодорожного пути
                //     "TheForemanOfTheRailwayTrack",
                //     //Driver, Водитель
                //     "Driver",
                //     //Balloonist, Воздухоплаватель
                //     "Balloonist",
                //     //Road Inspector, Дорожный инспектор
                //     "RoadInspector",
                //     //Dispatcher, Диспетчер
                //     "Dispatcher",
                //     //Shipmaster, Капитан судна
                //     "Shipmaster",
                //     //Conductor, Проводник
                //     "Conductor",
                //     //Cosmonaut, Космонавт
                //     "Cosmonaut",
                //     //Courier, Курьер
                //     "Courier",
                //     //Pilot, Летчик
                //     "Pilot",
                //     //Sailor, Моряк
                //     "Sailor",
                //     //Conductor guide, Проводник
                //     "ConductorGuide",
                //     //Mechanic mechanic, Слесарь-механик
                //     "MechanicMechanic",
                //     //Switchman, Стрелочник
                //     "Switchman",
                //     //Stewardess, Стюардесса
                //     "Stewardess",
                //     //Taxi driver, Таксист
                //     "TaxiDriver",
                //     //Navigator, Штурман
                //     "Navigator",
                //     //Forwarder, Экспедитор
                //     "Forwarder",
                // }),
                //Economic, Экономические
                // new ItemsInType("Economic", new HashSet<string>
                // {
                //     //Product Manager, Продакт-менеджер
                //     "ProductManager",
                //     //Trade Administrator, Администратор предприятия торговли
                //     "TradeAdministrator",
                //     //Sales Assistant Assistant, Ассистент менеджера по продажам
                //     "SalesAssistantAssistant",
                //     //Auditor, Аудитор
                //     "Auditor",
                //     //Banker, Банкир
                //     "Banker",
                //     //Bank cashier-operator, Банковский кассир-операционист
                //     "BankCashierOperator",
                //     //Business analyst, Бизнес-аналитик
                //     "BusinessAnalyst",
                //     //Business consultant, Бизнес-консультант
                //     "BusinessConsultant",
                //     //Business trainer, Бизнес-тренер
                //     "BusinessTrainer",
                //     //Ticket counter, Билетный кассир
                //     "TicketCounter",
                //     //Brand-designer, Бренд-дизайнер
                //     "BrandDesigner",
                //     //Brand Manager, Бренд-менеджер
                //     "BrandManager",
                //     //Broker, Брокер
                //     "Broker",
                //     //Accountant, Бухгалтер
                //     "Accountant",
                //     //Distributor, Дистрибутор
                //     "Distributor",
                //     //Warehouse Manager, Заведующий складом
                //     "WarehouseManager",
                //     //Collector, Сборщик
                //     "Collector",
                //     //Cashier, Кассир
                //     "Cashier",
                //     //Crisis manager, Кризис-менеджер
                //     "CrisisManager",
                //     //Lobbyist, Лоббист
                //     "Lobbyist",
                //     //Logist, Логист
                //     "Logist",
                //     //Marketer, Маркетолог
                //     "Marketer",
                //     //Media Builder, Медиа-байер
                //     "MediaBuilder",
                //     //Manager, Менеджер
                //     "Manager",
                //     //PR Manager, Менеджер по PR
                //     "PRManager",
                //     //Purchasing Manager, Менеджер по закупкам
                //     "PurchasingManager",
                //     //Logistics Manager, Менеджер по логистике
                //     "LogisticsManager",
                //     //Human Resources Manager, Менеджер по персоналу
                //     "HumanResourcesManager",
                //     //Sales Manager, Менеджер по продажам
                //     "SalesManager",
                //     //Account Manager, Менеджер по работе с клиентами
                //     "AccountManager",
                //     //Advertising Manager, Менеджер по рекламе
                //     "AdvertisingManager",
                //     //Tourism manager, Менеджер по туризму
                //     "TourismManager",
                //     //Manager of the trading floor, Менеджер торгового зала
                //     "ManagerOfTheTradingFloor",
                //     //Merchandiser, Мерчендайзер
                //     "Merchandiser",
                //     //Tax inspector, Налоговый инспектор
                //     "TaxInspector",
                //     //Office Manager, Офис-менеджер
                //     "OfficeManager",
                //     //Appraiser, Оценщик
                //     "Appraiser",
                //     //Seller, Продавец
                //     "Seller",
                //     //Shop assistant, Продавец-консультант
                //     "ShopAssistant",
                //     //Promoter, Промоутер
                //     "Promoter",
                //     //Realtor, Риелтор
                //     "Realtor",
                //     //Estimator, Сметчик
                //     "Estimator",
                //     //Supplier, Снабженец
                //     "Supplier",
                //     //Business owner, Собственник бизнеса
                //     "BusinessOwner",
                //     //Specialist in foreign trade activities, Специалист по ВЭД
                //     "SpecialistInForeignTradeActivities",
                //     //Statistician, Статистик
                //     "Statistician",
                //     //Supervisor, Супервайзер
                //     "Supervisor",
                //     //Commodore expert, Товаровед
                //     "CommodoreExpert",
                //     //Sales Representative, Торговый представитель
                //     "SalesRepresentative",
                //     //Trader, Трейдер
                //     "Trader",
                //     //Financier, Финансист
                //     "Financier",
                //     //Financial analyst, Финансовый аналитик
                //     "FinancialAnalyst",
                //     //Economist, Экономист
                //     "Economist",
                // }),

                //Information Technology, Internet, Telecom, Информационные технологии, интернет, телеком
                new ItemsInType("InformationTechnologyInternetTelecom", new HashSet<string>
                {
                    //Банковское ПО
                    //Banking software, Банковское ПО
                    "BankingSoftware", // 1.395
                    //Оптимизация сайта (SEO)
                    //Website Optimization (SEO), Оптимизация сайта (SEO)
                    "WebsiteOptimizationSEO", // 1.400
                    //Администратор баз данных
                    //Database Administrator, Администратор баз данных
                    "DatabaseAdministrator", // 1.420
                    //Стартапы
                    //Startups, Стартапы
                    "Startups", // 1.474
                    //Игровое ПО
                    //Gaming software, Игровое ПО
                    "GamingSoftware", // 1.475
                    //CRM системы
                    //CRM systems, CRM системы
                    "CRMSystems", // 1.536
                    //CTO, CIO, Директор по IT
                    //CTO, CIO, IT Director, CTO, CIO, Директор по IT
                    "CTOCIOITDirector", // 1.3
                    //Web инженер
                    //Web Engineer, Web инженер
                    "WebEngineer", // 1.9
                    //Web мастер
                    //Web master, Web мастер
                    "WebMaster", // 1.10
                    //Аналитик
                    // //Analyst, Аналитик
                    // "Analyst", // 1.25
                    //Арт-директор
                    //Art Director, Арт директор
                    //Системы управления предприятием (ERP)
                    //Enterprise Management Systems (ERP), Системы управления предприятием (ERP)
                    "EnterpriseManagementSystemsERP", // 1.50
                    // //Инженер
                    // "Engineer", // 1.82
                    //Интернет
                    //the Internet, Интернет
                    "theInternet", // 1.89
                    //Компьютерная безопасность
                    //Computer security, Компьютерная безопасность
                    "ComputerSecurity", // 1.110
                    //Консалтинг, Аутсорсинг
                    //Consulting, Outsourcing, Консалтинг, Аутсорсинг
                    "ConsultingOutsourcing", // 1.113
                    //Контент
                    //Content, Контент
                    "Content", // 1.116
                    //Тестирование
                    //Testing, Тестирование
                    "Testing", // 1.117
                    //Маркетинг
                    //Marketing, Маркетинг
                    "Marketing", // 1.137
                    //Мультимедиа
                    //Multimedia, Мультимедиа
                    "Multimedia", // 1.161
                    //Начальный уровень, Мало опыта
                    //Entry level, Little experience, Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 1.172
                    //Передача данных и доступ в интернет
                    //Data transfer and Internet access, Передача данных и доступ в интернет
                    "DataTransferAndInternetAccess", // 1.203
                    //Поддержка, Helpdesk
                    //Support, Helpdesk, Поддержка, Helpdesk
                    "SupportHelpdesk", // 1.211
                    //Программирование, Разработка
                    //Programming, Development, Программирование, Разработка
                    "ProgrammingDevelopment", // 1.221
                    //Продажи
                    // //Sales, Продажи
                    // "Sales", // 1.225
                    //Продюсер
                    "Producer", // 1.232
                    //Развитие бизнеса
                    //Business development, Развитие бизнеса
                    "BusinessDevelopment", // 1.246
                    //Сетевые технологии
                    //Network technologies, Сетевые технологии
                    "NetworkTechnologies", // 1.270
                    //Системная интеграция
                    //System integration, Системная интеграция
                    "SystemIntegration", // 1.272
                    //Системный администратор
                    //System Administrator, Системный администратор
                    "SystemAdministrator", // 1.273
                    //Системы автоматизированного проектирования
                    //Computer aided design systems, Системы автоматизированного проектирования
                    "ComputerAidedDesignSystems", // 1.274
                    //Сотовые, Беспроводные технологии
                    //Cellular, Wireless technologies, Сотовые, Беспроводные технологии
                    "CellularWirelessTechnologies", // 1.277
                    //Телекоммуникации
                    //Telecommunications, Телекоммуникации
                    "Telecommunications", // 1.295
                    //Технический писатель
                    "TechnicalWriter", // 1.296
                    //Управление проектами
                    // //Project management, Управление проектами
                    // "ProjectManagement", // 1.327
                    //Электронная коммерция
                    //E-commerce, Электронная коммерция
                    "ECommerce", // 1.359


                    //HTML web designer, HTML-верстальщик
                    "HTMLWebDesigner",
                    //Web-based integrator, Web-интегратор
                    "WebBasedIntegrator",
                    //Web-designer, Web-дизайнер
                    "WebDesigner",

                    "DatabaseAdministrator",
                    //Site administrator, Администратор сайта
                    "SiteAdministrator",
                    // //Blogger, Блогер
                    // "Blogger",
                    // //Speaker, Диктор
                    // "Speaker",
                    //Encoder, Кодер
                    "Encoder",
                    //Content manager, Контент-менеджер
                    "ContentManager",
                    //Copywriter, Копирайтер
                    "Copywriter",
                    //QA Engineer, Тестировщик
                    "QaEngineer",
                    //PC operator, Оператор ПК
                    "PCOperator",
                    //Radio presenter, Радиоведущий
                    "RadioPresenter",
                    //Radist, Радист
                    "Radist",
                    "SystemAdministrator",
                    //Telegraphist, Телеграфист
                    "Telegraphist",
                    //TV journalist, Тележурналист
                    "TVJournalist",
                    //Obstetrician, Акушер
                    "Obstetrician",
                }),


                //Accounting, management accounting, company finance, Бухгалтерия, управленческий учет, финансы предприятия
                new ItemsInType("AccountingManagementAccountingCompanyFinance", new HashSet<string>
                {
                    // //Экономист
                    // "Economist", // 2.425
                    //Планово-экономическое управление
                    //Planning and economic management, Планово-экономическое управление
                    "PlanningAndEconomicManagement", // 2.434
                    //МСФО, IFRS
                    //IFRS, IFRS, МСФО, IFRS
                    "IFRSIFRS", // 2.454
                    //Бухгалтер-калькулятор
                    //Accountant calculator, Бухгалтер-калькулятор
                    "AccountantCalculator", // 2.463
                    //Основные средства
                    //Fixed assets, Основные средства
                    "FixedAssets", // 2.464
                    //GAAP
                    //GAAP, GAAP
                    "GAAP", // 2.465
                    //ACCA
                    //ACCA, ACCA
                    "ACCA", // 2.467
                    //ТМЦ
                    //Commodities and materials, ТМЦ
                    "CommoditiesAndMaterials", // 2.468
                    //Первичная документация
                    //Primary documents, Первичная документация
                    "PrimaryDocuments", // 2.469
                    //CIPA
                    //CIPA, CIPA
                    "CIPA", // 2.523
                    //Аудит
                    //Audit, Аудит
                    "Audit", // 2.33
                    //Бухгалтер
                    "Accountant", // 2.43
                    //Бюджетирование и планирование
                    //Budgeting and planning, Бюджетирование и планирование
                    "BudgetingAndPlanning", // 2.44
                    // //Валютный контроль
                    // //Exchange control, Валютный контроль
                    // "ExchangeControl", // 2.46
                    //Казначейство
                    //Treasury Department, Казначейство
                    "TreasuryDepartment", // 2.100
                    //Кассир, Инкассатор
                    //Cashier, Collector, Кассир, Инкассатор
                    "CashierCollector", // 2.102
                    //Кредитный контроль
                    //Credit control, Кредитный контроль
                    "CreditControl", // 2.125
                    //Налоги
                    //Taxes, Налоги
                    "Taxes", // 2.164
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 2.179
                    //Оффшоры
                    //Offshore, Оффшоры
                    "Offshore", // 2.200
                    //Расчет себестоимости
                    //Calculation of cost, Расчет себестоимости
                    "CalculationOfCost", // 2.249
                    //Руководство бухгалтерией
                    //Accounting management, Руководство бухгалтерией
                    "AccountingManagement", // 2.261
                    //Учет заработной платы
                    //Accounting of wages, Учет заработной платы
                    "AccountingOfWages", // 2.335
                    //Учет счетов и платежей
                    //Accounting of bills and payments, Учет счетов и платежей
                    "AccountingOfBillsAndPayments", // 2.337
                    //Финансовый анализ
                    //The financial analysis, Финансовый анализ
                    "TheFinancialAnalysis", // 2.342
                    //Финансовый контроль
                    //Financial control, Финансовый контроль
                    "FinancialControl", // 2.343
                    //Финансовый менеджмент
                    //Financial management, Финансовый менеджмент
                    "FinancialManagement", // 2.344
                    //Ценные бумаги
                    //Securities, Ценные бумаги
                    "Securities", // 2.351
                }),


                //Marketing, Advertising, PR, Маркетинг, Реклама, PR
                new ItemsInType("MarketingAdvertisingPR", new HashSet<string>
                    {
                        //Ассистент
                        // //Assistant, Помощник
                        // "Assistant", // 3.423
                        //Тайный покупатель
                        //Mystery shopper, Тайный покупатель
                        "MysteryShopper", // 3.507
                        //Проведение опросов, Интервьюер
                        //Interviewing, Interviewer, Проведение опросов, Интервьюер
                        "InterviewingInterviewer", // 3.508
                        //Промоутер
                        "Promoter", // 3.509
                        //Below The Line (BTL)
                        //Below The Line (BTL), Below The Line (BTL)
                        "BelowTheLineBTL", // 3.1
                        //PR, Маркетинговые коммуникации
                        //PR, Marketing communications, PR, Маркетинговые коммуникации
                        "PRMarketingCommunications", // 3.8
                        //Аналитик
                        "Analyst", // 3.26
                        //Арт директор
                        "ArtDirector", // 3.31
                        //Бренд-менеджмент
                        //Brand Management, Бренд-менеджмент
                        "BrandManagement", // 3.40
                        //Верстальщик
                        //Imposer, Верстальщик
                        "Imposer", // 3.48
                        //Дизайнер
                        "Designer", // 3.64
                        //Интернет-маркетинг
                        //Internet Marketing, Интернет-маркетинг
                        "InternetMarketing", // 3.90
                        //Исследования рынка
                        //Market research, Исследования рынка
                        "MarketResearch", // 3.98
                        //Консультант
                        //Consultant, Консультант
                        "Consultant", // 3.114
                        // //Копирайтер
                        // "Copywriter", // 3.119
                        // //Менеджер по работе с клиентами
                        // "AccountManager", // 3.148
                        //Менеджмент продукта (Product manager)
                        //Product management, Менеджмент продукта (Product manager)
                        "ProductManagement", // 3.150
                        //Мерчендайзинг
                        //Merchandising, Мерчендайзинг
                        "Merchandising", // 3.151
                        //Наружная реклама
                        //Outdoor advertising, Наружная реклама
                        "OutdoorAdvertising", // 3.166
                        //Начальный уровень/Мало опыта
                        // //Beginner / Little experience, Начальный уровень/Мало опыта
                        // "BeginnerLittleExperience", // 3.171
                        //Печатная реклама
                        //Printable advertisement, Печатная реклама
                        "PrintableAdvertisement", // 3.206
                        //Планирование, Размещение рекламы
                        //Planning, Advertising, Планирование, Размещение рекламы
                        "PlanningAdvertising", // 3.209
                        //Политический PR
                        //Political PR, Политический PR
                        "PoliticalPR", // 3.213
                        //Продвижение, Специальные мероприятия
                        //Promotion, Special events, Продвижение, Специальные мероприятия
                        "PromotionSpecialEvents", // 3.230
                        //Производство рекламы
                        //Advertising production, Производство рекламы
                        "AdvertisingProduction", // 3.236
                        //Радио реклама
                        //Radio advertising, Радио реклама
                        "RadioAdvertising", // 3.244
                        //Рекламный агент
                        //Advertising agent, Рекламный агент
                        "AdvertisingAgent", // 3.253
                        //Телевизионная реклама
                        //TV advertising, Телевизионная реклама
                        "TVAdvertising", // 3.294
                        //Торговый маркетинг(Trade marketing)
                        //Trade marketing, Торговый маркетинг(Trade marketing)
                        "TradeMarketing", // 3.305
                        //Управление маркетингом
                        //Marketing Management, Управление маркетингом
                        "MarketingManagement", // 3.318
                        //Управление проектами
                        "ProjectManagement", // 3.328
                    }),


                //Administrative staff, Административный персонал
                //Administrative staff, Административный персонал
                new ItemsInType("AdministrativeStaff", new HashSet<string>
                {
                    //Последовательный перевод
                    //Consecutive translation, Последовательный перевод
                    "ConsecutiveTranslation", // 4.428
                    //Делопроизводство
                    //Office work, Делопроизводство
                    "OfficeWork", // 4.429
                    //Вечерний секретарь
                    //Evening Secretary, Вечерний секретарь
                    "EveningSecretary", // 4.456
                    //Уборщица
                    "Cleaner", // 4.494
                    //Архивист
                    // //Archivist, Архивариус
                    // "Archivist", // 4.32
                    //Ввод данных
                    //Data input, Ввод данных
                    "DataInput", // 4.47
                    // //Водитель
                    // "Driver", // 4.52
                    //Курьер
                    "Courier", // 4.127
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 4.181
                    //Персональный ассистент
                    //Personal assistant, Персональный ассистент
                    "PersonalAssistant", // 4.205
                    //Письменный перевод
                    //Written translation, Письменный перевод
                    "WrittenTranslation", // 4.207
                    //Ресепшен
                    //Reception, Ресепшен
                    "Reception", // 4.255
                    //Секретарь
                    //Secretary, Секретарь
                    "Secretary", // 4.264
                    //Синхронный перевод
                    //Simultaneous translation, Синхронный перевод
                    "SimultaneousTranslation", // 4.271
                    //Сотрудник call-центра
                    //Call center employee, Сотрудник call-центра
                    "CallCenterEmployee", // 4.278
                    //Управляющий офисом(Оffice manager)
                    //Office manager (Office manager), Управляющий офисом(Оffice manager)
                    "OfficeManagerOfficeManager", // 4.332
                    //Учет товарооборота
                    //Accounting of turnover, Учет товарооборота
                    "AccountingOfTurnover", // 4.338
                    //АХО
                    //AXO, АХО
                    "AXO", // 4.374
                }),



                //Banks, investments, leasing, Банки, инвестиции, лизинг
                new ItemsInType("BanksInvestmentsLeasing", new HashSet<string>
                {
                    //Ипотека, Ипотечное кредитование
                    //Mortgage, Mortgage Lending, Ипотека, Ипотечное кредитование
                    "MortgageMortgageLending", // 5.399
                    //Паевые фонды
                    //Mutual funds, Паевые фонды
                    "MutualFunds", // 5.424
                    //Экономист
                    "Economist", // 5.426
                    //Финансовый мониторинг
                    //Financial monitoring, Финансовый мониторинг
                    "FinancialMonitoring", // 5.444
                    //Кредитование малого и среднего бизнеса
                    //SME crediting, Кредитование малого и среднего бизнеса
                    "SMECrediting", // 5.450
                    //Риски: операционные
                    //Risks: operating, Риски: операционные
                    "RisksOperating", // 5.457
                    //Риски: финансовые
                    //Risks: financial, Риски: финансовые
                    "RisksFinancial", // 5.458
                    //Риски: рыночные
                    //Risks: market, Риски: рыночные
                    "RisksMarket", // 5.459
                    //Автокредитование
                    //Car Loans, Автокредитование
                    "CarLoans", // 5.460
                    //Факторинг
                    //Factoring, Факторинг
                    "Factoring", // 5.534
                    //Работа с проблемными заемщиками
                    //Work with problem borrowers, Работа с проблемными заемщиками
                    "WorkWithProblemBorrowers", // 5.579
                    //Private Banking
                    //Private Banking, Private Banking
                    "PrivateBanking", // 5.691
                    //Forex
                    //Forex, Forex
                    "Forex", // 5.4
                    //Акции, Ценные бумаги
                    //Shares, Securities, Акции, Ценные бумаги
                    "SharesSecurities", // 5.23
                    // //Аналитик
                    // "Analyst", // 5.27
                    //Аудит, Внутренний контроль
                    //Audit, Internal control, Аудит, Внутренний контроль
                    "AuditInternalControl", // 5.34
                    //Бумаги с фиксированной доходностью (fixed Income)
                    //Fixed Income Paper, Бумаги с фиксированной доходностью (fixed Income)
                    "FixedIncomePaper", // 5.41
                    // //Бухгалтер
                    // "Accountant", // 5.42
                    //Валютный контроль
                    "ExchangeControl", // 5.45
                    //Внутренние операции (Back Office)
                    //Internal Operations (Back Office), Внутренние операции (Back Office)
                    "InternalOperationsBackOffice", // 5.51
                    //Денежный рынок (money market)
                    //Money market, Денежный рынок (money market)
                    "MoneyMarket", // 5.61
                    //Инвестиционная компания
                    //Investment company, Инвестиционная компания
                    "InvestmentCompany", // 5.79
                    //Трейдинг, Дилинг
                    //Trading, Dealing, Трейдинг, Дилинг
                    "TradingDealing", // 5.101
                    //Кассовое обслуживание, инкассация
                    //Cash services, collection, Кассовое обслуживание, инкассация
                    "CashServicesCollection", // 5.103
                    // //Коммерческий банк
                    // //Commercial Bank, Коммерческий Банк
                    // "CommercialBank", // 5.106
                    //Корпоративное финансирование
                    //Corporate financing, Корпоративное финансирование
                    "CorporateFinancing", // 5.121
                    //Корреспондентские, Международные отношения
                    //Correspondent, International relations, Корреспондентские, Международные отношения
                    "CorrespondentInternationalRelations", // 5.123
                    //Риски: кредитные
                    //Risks: credit, Риски: кредитные
                    "RisksCredit", // 5.124
                    //Кредиты
                    //Credits, Кредиты
                    "Credits", // 5.126
                    //Лизинг
                    //Leasing, Лизинг
                    "Leasing", // 5.132
                    //Риски: лизинговые
                    //Risks: leasing, Риски: лизинговые
                    "RisksLeasing", // 5.133
                    // //Налоги
                    // "Taxes", // 5.163
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 5.177
                    //Обменные пункты, Банкоматы
                    //Exchange offices, ATMs, Обменные пункты, Банкоматы
                    "ExchangeOfficesATMs", // 5.192
                    //ОПЕРУ
                    //OPERA, ОПЕРУ
                    "OPERA", // 5.195
                    //Пластиковые карты
                    //Plastic cards, Пластиковые карты
                    "PlasticCards", // 5.210
                    //Портфельные инвестиции
                    //Portfolio investment, Портфельные инвестиции
                    "PortfolioInvestment", // 5.215
                    //Привлечение клиентов
                    //Attracting customers, Привлечение клиентов
                    "AttractingCustomers", // 5.219
                    //Продажа финансовых продуктов
                    //Sale of financial products, Продажа финансовых продуктов
                    "SaleOfFinancialProducts", // 5.224
                    //Проектное финансирование
                    //Project finance, Проектное финансирование
                    "ProjectFinance", // 5.234
                    //Прямые инвестиции
                    //Direct investments, Прямые инвестиции
                    "DirectInvestments", // 5.241
                    //Расчеты
                    //Calculations, Расчеты
                    "Calculations", // 5.250
                    //Розничный бизнес
                    //Retail business, Розничный бизнес
                    "RetailBusiness", // 5.257
                    // //Руководство бухгалтерией
                    // "AccountingManagement", // 5.262
                    //Торговое финансирование
                    //Trade financing, Торговое финансирование
                    "TradeFinancing", // 5.304
                    //Филиалы
                    //Branches, Филиалы
                    "Branches", // 5.341
                    //Оценка залога, Стоимости имущества
                    //Valuation of collateral, Value of property, Оценка залога, Стоимости имущества
                    "ValuationOfCollateralValueOfProperty", // 5.365
                    //Риски: прочие
                    //Risks: other, Риски: прочие
                    "RisksOther", // 5.366
                    //Методология, Банковские технологии
                    //Methodology, Banking Technologies, Методология, Банковские технологии
                    "MethodologyBankingTechnologies", // 5.367
                    //Разработка новых продуктов, Маркетинг
                    //Development of new products, Marketing, Разработка новых продуктов, Маркетинг
                    "DevelopmentOfNewProductsMarketing", // 5.368
                    //Отчетность
                    //Reporting, Отчетность
                    "Reporting", // 5.369
                    //Казначейство, Управление ликвидностью
                    //Treasury, Liquidity Management, Казначейство, Управление ликвидностью
                    "TreasuryLiquidityManagement", // 5.370
                    //Кредиты: розничные
                    //Loans: retail, Кредиты: розничные
                    "LoansRetail", // 5.371
                    //Бюджетирование
                    //Budgeting, Бюджетирование
                    "Budgeting", // 5.372
                    //Эмиссии
                    //Emissions, Эмиссии
                    "Emissions", // 5.394
                }),



                //Personnel management, trainings, Управление персоналом, тренинги
                new ItemsInType("PersonnelManagementTrainings", new HashSet<string>
                {
                    //Компенсации и льготы
                    //Compensation and benefits, Компенсации и льготы
                    "CompensationAndBenefits", // 6.107
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 6.184
                    //Развитие персонала
                    //Staff development, Развитие персонала
                    "StaffDevelopment", // 6.247
                    //Рекрутмент
                    //Recruitment, Рекрутмент
                    "Recruitment", // 6.254
                    //Тренинги
                    //Trainings, Тренинги
                    "Trainings", // 6.309
                    //Управление персоналом
                    //Personnel Management, Управление персоналом
                    "PersonnelManagement", // 6.319
                    //Учет кадров
                    //Personnel Accounting, Учет кадров
                    "PersonnelAccounting", // 6.336
                }),



                new ItemsInType( //Car business, Автомобильный бизнес
                    "CarBusiness", new HashSet<string>
                    {
                        //Шины, Диски
                        //Tires, Wheels, Шины, Диски
                        "TiresWheels", // 7.455
                        //Тонировщик
                        //Toner, Тонировщик
                        "Toner", // 7.502
                        //Автослесарь
                        "AutoMechanic", // 7.503
                        //Автожестянщик
                        //Auto-driver, Автожестянщик
                        "AutoDriver", // 7.565
                        //Автомойщик
                        //Car Washman, Автомойщик
                        "CarWashman", // 7.566
                        //Автозапчасти
                        //Auto Parts, Автозапчасти
                        "AutoParts", // 7.14
                        //Продажа
                        //Sale, Продажа
                        "Sale", // 7.222
                        //Производство
                        // //Production, Производство
                        // "Production", // 7.235
                        //Прокат, лизинг
                        //Leasing, leasing, Прокат, лизинг
                        "LeasingLeasing", // 7.239
                        //Сервисное обслуживание
                        //Service maintenance, Сервисное обслуживание
                        "ServiceMaintenance", // 7.267
                        // //Начальный уровень /Мало опыта 
                        // "BeginnerLittleExperience", // 7.392
                    }),



                //Security, Безопасность
                new ItemsInType("Security", new HashSet<string>
                {
                    //Системы видеонаблюдения
                    //CCTV systems, Системы видеонаблюдения
                    "CCTVSystems", // 8.461
                    //Взыскание задолженности, Коллекторская деятельность
                    //Debt collection, Collector activity, Взыскание задолженности, Коллекторская деятельность
                    "DebtCollectionCollectorActivity", // 8.462
                    //Инкассатор
                    "Collector", // 8.519
                    //Пожарная безопасность
                    //Fire safety, Пожарная безопасность
                    "FireSafety", // 8.575
                    //Имущественная безопасность
                    //Property security, Имущественная безопасность
                    "PropertySecurity", // 8.77
                    //Личная безопасность
                    //Personal safety, Личная безопасность
                    "PersonalSafety", // 8.135
                    //Охранник
                    //Security guard, Охранник
                    "SecurityGuard", // 8.202
                    //Руководитель СБ
                    //The head of the Security Council, Руководитель СБ
                    "TheHeadOfTheSecurityCouncil", // 8.260
                    //Экономическая и информационная безопасность
                    //Economic and Information Security, Экономическая и информационная безопасность
                    "EconomicAndInformationSecurity", // 8.356
                }),



                //Top management, Высший менеджмент
                new ItemsInType("TopManagement", new HashSet<string>
                {
                    //Юриспруденция
                    //Jurisprudence, Юриспруденция
                    "Jurisprudence", // 9.448
                    //Страхование
                    // //Insurance, Страхование
                    // "Insurance", // 9.452
                    //Спортивные клубы, Фитнес, Салоны красоты
                    //Sport clubs, Fitness, Beauty salons, Спортивные клубы, Фитнес, Салоны красоты
                    "SportClubsFitnessBeautySalons", // 9.521
                    // //Управление закупками
                    // //Procurement management, Управление закупками
                    // "ProcurementManagement", // 9.532
                    //Антикризисное управление
                    //Crisis management, Антикризисное управление
                    "CrisisManagement", // 9.562
                    //Администрирование
                    //Administration, Администрация
                    "Administration", // 9.22
                    // //Добыча cырья
                    // //Extraction of raw materials, Добыча сырья
                    // "ExtractionOfRawMaterials", // 9.67
                    //Инвестиции
                    //Investments, Инвестиции
                    "Investments", // 9.78
                    //Информационные технологии, Интернет, Мультимедиа
                    //Information technology, Internet, Multimedia, Информационные технологии, Интернет, Мультимедиа
                    "InformationTechnologyInternetMultimedia", // 9.94
                    //Искусство, Развлечения, Масс-медиа
                    //Arts, Entertainment, Media, Искусство, Развлечения, Масс-медиа
                    "ArtsEntertainmentMedia", // 9.95
                    // //Коммерческий Банк
                    // "CommercialBank", // 9.105
                    //Консультирование
                    //Counseling, Консультирование
                    "Counseling", // 9.115
                    //Маркетинг, Реклама, PR
                    "MarketingAdvertisingPR", // 9.139
                    // //Медицина, Фармацевтика
                    // //Medicine, Pharmaceutics, Медицина, Фармацевтика
                    // "MedicinePharmaceutics", // 9.145
                    //Наука, Образование
                    // //Science, Education, Наука, Образование
                    // "ScienceEducation", // 9.168
                    //Продажи
                    "Sales", // 9.226
                    //Производство, Технология
                    //Production, Technology, Производство, Технология
                    "ProductionTechnology", // 9.238
                    //Строительство, Недвижимость
                    //Construction, Real Estate, Строительство, Недвижимость
                    "ConstructionRealEstate", // 9.289
                    //Транспорт, Логистика
                    //Transport, Logistics, Транспорт, Логистика
                    "TransportLogistics", // 9.307
                    //Туризм, Гостиницы, Рестораны
                    //Tourism, Hotels, Restaurants, Туризм, Гостиницы, Рестораны
                    "TourismHotelsRestaurants", // 9.312
                    //Управление малым бизнесом
                    //Small Business Management, Управление малым бизнесом
                    "SmallBusinessManagement", // 9.317
                    //Управление персоналом, Тренинги
                    //Human Resource Management, Trainings, Управление персоналом, Тренинги
                    "HumanResourceManagementTrainings", // 9.321
                    //Финансы
                    //Finance, Финансы
                    "Finance", // 9.345
                }),



                //Extraction of raw materials, Добыча сырья
                new ItemsInType("ExtractionOfRawMaterials", new HashSet<string>
                {
                    //Бурение
                    //Drilling, Бурение
                    "Drilling", // 10.470
                    //Маркшейдер
                    "MineSurveyor", // 10.471
                    //Газ
                    //Gas, Газ
                    "Gas", // 10.472
                    //Геологоразведка
                    //Geological prospecting, Геологоразведка
                    "GeologicalProspecting", // 10.54
                    // //Инженер
                    // "Engineer", // 10.80
                    //Нефть
                    //Oil, Нефть
                    "Oil", // 10.191
                    //Руда
                    //Ore, Руда
                    "Ore", // 10.258
                    //Уголь
                    //Coal, Уголь
                    "Coal", // 10.315
                    //Управление предприятием
                    //Business Management, Управление предприятием
                    "BusinessManagement", // 10.323
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 10.393
                }),



                //Arts, Entertainment, Mass Media, Искусство, развлечения, масс-медиа
                new ItemsInType("ArtsEntertainmentMassMedia", new HashSet<string>
                {
                    //Кино
                    //Cinema, Кино
                    "Cinema", // 11.436
                    //Дизайн, графика, живопись
                    //Design, Graphics, Painting, Дизайн, графика, живопись
                    "DesignGraphicsPainting", // 11.62
                    //Журналистика
                    //Journalism, Журналистика
                    "Journalism", // 11.71
                    //Издательская деятельность
                    //Publishing Activities, Издательская деятельность
                    "PublishingActivities", // 11.76
                    //Казино и игорный бизнес
                    //Casino and gaming, Казино и игорный бизнес
                    "CasinoAndGaming", // 11.99
                    //Литературная, Редакторская деятельность
                    //Literary, Editorial Activity, Литературная, Редакторская деятельность
                    "LiteraryEditorialActivity", // 11.134
                    //Мода
                    //Fashion, Мода
                    "Fashion", // 11.157
                    //Музыка
                    //Music, Музыка
                    "Music", // 11.160
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 11.173
                    //Пресса
                    //Press, Пресса
                    "Press", // 11.218
                    //Прочее
                    // //Other, Другое
                    // "Other", // 11.240
                    //Радио
                    //Radio, Радио
                    "Radio", // 11.243
                    //Телевидение
                    //TV, Телевидение
                    "TV", // 11.293
                    //Фотография
                    //The photo, Фотография
                    "ThePhoto", // 11.347
                }),



                //Counseling, Консультирование
                new ItemsInType("Counseling", new HashSet<string>
                {
                    //Internet, E-Commerce
                    //Internet, E-Commerce, Internet, E-Commerce
                    "InternetECommerce", // 12.5
                    //Knowledge management
                    //Knowledge management, Knowledge management
                    "KnowledgeManagement", // 12.6
                    //PR Consulting
                    //PR Consulting, PR Consulting
                    "PRConsulting", // 12.7
                    //Информационные технологии
                    //Information Technology, Информационные технологии
                    "InformationTechnology", // 12.92
                    // //Исследования рынка
                    // "MarketResearch", // 12.97
                    //Корпоративные финансы
                    //Corporate Finance, Корпоративные финансы
                    "CorporateFinance", // 12.122
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 12.180
                    //Организационное консультирование
                    //Organizational Consulting, Организационное консультирование
                    "OrganizationalConsulting", // 12.197
                    //Реинжиниринг бизнес процессов
                    //Reengineering of business processes, Реинжиниринг бизнес процессов
                    "ReengineeringOfBusinessProcesses", // 12.251
                    //Реинжиниринг, Аутсорсинг финансовой функции
                    //Reengineering, Outsourcing of financial functions, Реинжиниринг, Аутсорсинг финансовой функции
                    "ReengineeringOutsourcingOfFinancialFunctions", // 12.252
                    //Стратегия
                    //Strategy, Стратегия
                    "Strategy", // 12.280
                    //Управление практикой
                    //Practice Management, Управление практикой
                    "PracticeManagement", // 12.322
                    // //Управление проектами
                    // "ProjectManagement", // 12.326
                    //Управленческое консультирование
                    //Management Consulting, Управленческое консультирование
                    "ManagementConsulting", // 12.331
                    //Недвижимость
                    //The property, Недвижимость
                    "TheProperty", // 12.376
                }),



                //Medicine, pharmaceutics, Медицина, фармацевтика
                new ItemsInType("MedicinePharmaceutics", new HashSet<string>
                {
                    //Клинические исследования
                    //Clinical researches, Клинические исследования
                    "ClinicalResearches", // 13.398
                    // //Производство
                    // "Production", // 13.432
                    //Регистратура
                    //Registry, Регистратура
                    "Registry", // 13.433
                    //Оптика
                    //Optics, Оптика
                    "Optics", // 13.438
                    //Психология
                    //Psychology, Психология
                    "Psychology", // 13.447
                    //Медицинский представитель
                    //Medical representative, Медицинский представитель
                    "MedicalRepresentative", // 13.489
                    //Медицинский советник
                    //Medical Advisor, Медицинский советник
                    "MedicalAdvisor", // 13.537
                    //Дефектолог, Логопед
                    //Defectologist, Speech therapist, Дефектолог, Логопед
                    "DefectologistSpeechTherapist", // 13.567
                    //Фармацевт
                    //Pharmacist, Провизор
                    "Pharmacist", // 13.578
                    //Врач-эксперт
                    //Expert doctor, Врач-эксперт
                    "ExpertDoctor", // 13.587
                    //Ветеринария
                    //Veterinary Medicine, Ветеринария
                    "VeterinaryMedicine", // 13.49
                    //Лаборант
                    //Laboratory assistant, Лаборант
                    "LaboratoryAssistant", // 13.128
                    //Лечащий врач
                    //Therapist, Лечащий врач
                    "Therapist", // 13.131
                    // //Маркетинг
                    // "Marketing", // 13.138
                    //Младший и средний медперсонал
                    //Junior and middle medical staff, Младший и средний медперсонал
                    "JuniorAndMiddleMedicalStaff", // 13.155
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 13.185
                    //Провизор
                    "Pharmacist", // 13.220
                    // //Продажи
                    // "Sales", // 13.227
                    //Лекарственные препараты
                    //Medications, Лекарственные препараты
                    "Medications", // 13.228
                    //Медицинское оборудование
                    //Medical equipment, Медицинское оборудование
                    "MedicalEquipment", // 13.229
                    // //Сертификация
                    // //Certification, Сертификация
                    // "Certification", // 13.268

                    //Andrologist, Андролог
                    "Andrologist",
                    //Anesthetist, Анестезиолог
                    "Anesthetist",
                    //Venereologist, Венеролог
                    "Venereologist",
                    //Virologist, Вирусолог
                    "Virologist",
                    //The doctor of first aid, Врач скорой помощи
                    "TheDoctorOfFirstAid",
                    //The doctor-dietician, Врач-диетолог
                    "TheDoctorDietician",
                    //Gynecologist, Гинеколог
                    "Gynecologist",
                    //The dermatovenerologist, Дерматовенеролог
                    "TheDermatovenerologist",
                    //Dermatologist, Дерматолог
                    "Dermatologist",
                    //Nutritionist, Диетолог
                    "Nutritionist",
                    //Dental Technician, Зубной техник
                    "DentalTechnician",
                    //Immunologist, Иммунолог
                    "Immunologist",
                    //Infectionist, Инфекционист
                    "Infectionist",
                    //Cardiologist, Кардиолог
                    "Cardiologist",
                    //Cardiac Surgeon, Кардиохирург
                    "CardiacSurgeon",
                    //Cosmetologist, Косметолог
                    "Cosmetologist",
                    "LaboratoryAssistant",
                    //Manualist, Мануалист
                    "Manualist",
                    // //Masseur, Массажист
                    // "Masseur",
                    //Neurologist, Невролог
                    "Neurologist",
                    //Neuropathologist, Невропатолог
                    "Neuropathologist",
                    //Neurosurgeon, Нейрохирург
                    "Neurosurgeon",
                    //Nephrologist, Нефролог
                    "Nephrologist",
                    //The ophthalmologist, Окулист
                    "TheOphthalmologist",
                    //Oncologist, Онколог
                    "Oncologist",
                    //Orthopedist, Ортопед
                    "Orthopedist",
                    //The otorhinolaryngologist (ENT), Оториноларинголог (ЛОР)
                    "TheOtorhinolaryngologistENT",
                    //Ophthalmologist, Офтальмолог
                    "Ophthalmologist",
                    //Pediatrician, Педиатр
                    "Pediatrician",
                    //Dissector, Прозектор
                    "Dissector",
                    //Proctologist, Проктолог
                    "Proctologist",
                    //Psychiatrist, Психиатр
                    "Psychiatrist",
                    //Psychotherapist, Психотерапевт
                    "Psychotherapist",
                    //Rehabilitologist, Реабилитолог
                    "Rehabilitologist",
                    //Resuscitator, Реаниматолог
                    "Resuscitator",
                    //Sexologist, Сексолог
                    "Sexologist",
                    //Sports doctor, Спортивный врач
                    "SportsDoctor",
                    //Dentist, Стоматолог
                    "Dentist",
                    "Therapist",
                    //Toxicologist, Токсиколог
                    "Toxicologist",
                    //Traumatologist, Травматолог
                    "Traumatologist",
                    //Urologist, Уролог
                    "Urologist",
                    "Pharmacist",
                    //Feldsher, Фельдшер
                    "Feldsher",
                    //Surgeon, Хирург
                    "Surgeon",
                    //Endocrinologist (hormone specialist), Эндокринолог (специалист по гормонам)
                    "EndocrinologistHormoneSpecialist",
                }),



                //Science, education, Наука, образование
                new ItemsInType("ScienceEducation", new HashSet<string>
                {
                    //Биотехнологии
                    //Biotechnology, Биотехнологии
                    "Biotechnology", // 14.37
                    //Гуманитарные науки
                    //Humanitarian sciences, Гуманитарные науки
                    "HumanitarianSciences", // 14.60
                    //Инженерные науки
                    //Engineering Sciences, Инженерные науки
                    "EngineeringSciences", // 14.87
                    //Информатика, Информационные системы
                    //Informatics, Information Systems, Информатика, Информационные системы
                    "InformaticsInformationSystems", // 14.91
                    //Математика
                    //Mathematics, Математика
                    "Mathematics", // 14.141
                    //Науки о Земле
                    //Earth sciences, Науки о Земле
                    "EarthSciences", // 14.169
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 14.178
                    //Преподавание
                    //Teaching, Преподавание
                    "Teaching", // 14.217
                    //Физика
                    //Physics, Физика
                    "Physics", // 14.340
                    //Химия
                    //Chemistry, Химия
                    "Chemistry", // 14.349
                    //Экономика, Менеджмент
                    //Economy, Management, Экономика, Менеджмент
                    "EconomyManagement", // 14.355
                    //Языки
                    //Languages, Языки
                    "Languages", // 14.364
                }),



                // //Early career, students, Начало карьеры, студенты
                // new ItemsInType("EarlyCareerStudents", new HashSet<string>
                // {
                //     //Бухгалтерия
                //     //Accounting, Бухгалтерия
                //     "Accounting", // 15.730
                //     //Закупки
                //     //Procurement, Закупки
                //     "Procurement", // 15.731
                //     //Добыча сырья
                //     "ExtractionOfRawMaterials", // 15.68
                //     //Информационные технологии, Интернет, Мультимедиа
                //     "InformationTechnologyInternetMultimedia", // 15.93
                //     //Искусство, Развлечения, Масс-медиа
                //     "ArtsEntertainmentMedia", // 15.96
                //     //Маркетинг, Реклама, PR
                //     "MarketingAdvertisingPR", // 15.140
                //     //Медицина, Фармацевтика
                //     "MedicinePharmaceutics", // 15.146
                //     //Наука, Образование
                //     "ScienceEducation", // 15.167
                //     //Производство, Технологии
                //     //Production, Technologies, Производство, Технологии
                //     "ProductionTechnologies", // 15.237
                //     //Страхование
                //     "Insurance", // 15.281
                //     //Строительство, Архитектура
                //     //Building, Architecture, Строительство, Архитектура
                //     "BuildingArchitecture", // 15.288
                //     //Транспорт, Логистика
                //     "TransportLogistics", // 15.308
                //     //Туризм, Гостиницы, Рестораны
                //     "TourismHotelsRestaurants", // 15.313
                //     //Управление персоналом
                //     "PersonnelManagement", // 15.320
                //     //Финансы, Банки, Инвестиции
                //     //Finance, Banking, Investments, Финансы, Банки, Инвестиции
                //     "FinanceBankingInvestments", // 15.346
                //     //Юристы
                //     //Lawyers, Юристы
                //     "Lawyers", // 15.363
                //     //Административный персонал
                //     "AdministrativeStaff", // 15.388
                //     //Продажи
                //     "Sales", // 15.389
                //     //Автомобильный бизнес
                //     "CarBusiness", // 15.390
                //     //Консультирование
                //     "Counseling", // 15.391
                // }),



                //Government service, non-profit organizations, Государственная служба, некоммерческие организации
                new ItemsInType("GovernmentServiceNonProfitOrganizations", new HashSet<string>
                {
                    //НИИ
                    //Research Institute, НИИ
                    "ResearchInstitute", // 16.435
                    // //Архивариус
                    // "Archivist", // 16.569
                    //Атташе
                    //Attaché, Атташе
                    "Attaché", // 16.570
                    //Библиотекарь
                    "Librarian", // 16.571
                    //Муниципалитет
                    //Municipality, Муниципалитет
                    "Municipality", // 16.658
                    //Благотворительность
                    //Charity, Благотворительность
                    "Charity", // 16.38
                    //Общественные организации
                    //Public organizations, Общественные организации
                    "PublicOrganizations", // 16.194
                    //Правительство
                    //Government, Правительство
                    "Government", // 16.216
                }),



                //Sales, Продажи
                new ItemsInType("Sales", new HashSet<string>
                {
                    //Электротехническое оборудование, Светотехника
                    //Electrotechnical equipment, Light engineering, Электротехническое оборудование, Светотехника
                    "ElectrotechnicalEquipmentLightEngineering", // 17.397
                    //Химическая продукция
                    // //Chemical products, Химическая продукция
                    // "ChemicalProducts", // 17.401
                    // //Сертификация
                    // "Certification", // 17.405
                    //Строительные материалы
                    //Construction Materials, Строительные материалы
                    "ConstructionMaterials", // 17.417
                    //Сельское хозяйство
                    "Agriculture", // 17.418
                    //Текстиль, Одежда, Обувь
                    //Textiles, Clothing, Shoes, Текстиль, Одежда, Обувь
                    "TextilesClothingShoes", // 17.440
                    //Франчайзинг
                    //Franchising, Франчайзинг
                    "Franchising", // 17.441
                    //Мебель
                    //Furniture, Мебель
                    "Furniture", // 17.443
                    //Системы безопасности
                    //Security Systems, Системы безопасности
                    "SecuritySystems", // 17.446
                    //Сантехника
                    //Sanitary engineering, Сантехника
                    "SanitaryEngineering", // 17.486
                    //Бытовая техника
                    //Appliances, Бытовая техника
                    "Appliances", // 17.487
                    //Продавец в магазине
                    //Salesman in the store, Продавец в магазине
                    "SalesmanInTheStore", // 17.520
                    //Торговые сети
                    //Trading networks, Торговые сети
                    "TradingNetworks", // 17.535
                    //Продажи по телефону, Телемаркетинг
                    //Sales by phone, Telemarketing, Продажи по телефону, Телемаркетинг
                    "SalesByPhoneTelemarketing", // 17.538
                    //Тендеры
                    //Tenders, Тендеры
                    "Tenders", // 17.572
                    //Клининговые услуги
                    //Cleaning service, Клининговые услуги
                    "CleaningService", // 17.580
                    //Финансовые услуги
                    //Financial services, Финансовые услуги
                    "FinancialServices", // 17.623
                    //Решения по автоматизации процессов
                    //Automation Solutions, Решения по автоматизации процессов
                    "AutomationSolutions", // 17.625
                    //Автомобили, Запчасти
                    //Cars, Spare parts, Автомобили, Запчасти
                    "CarsSpareParts", // 17.15
                    //Алкоголь
                    //Alcohol, Алкоголь
                    "Alcohol", // 17.24
                    //ГСМ, нефть, бензин
                    //Petroleum, petroleum, gasoline, ГСМ, нефть, бензин
                    "PetroleumPetroleumGasoline", // 17.59
                    //Дилерские сети
                    //Dealer Networks, Дилерские сети
                    "DealerNetworks", // 17.65
                    //Дистрибуция
                    //Distribution, Дистрибуция
                    "Distribution", // 17.66
                    //Компьютерная техника
                    //Computer technology, Компьютерная техника
                    "ComputerTechnology", // 17.111
                    //Компьютерные программы
                    //Computer programs, Компьютерные программы
                    "ComputerPrograms", // 17.112
                    //Медицина, Фармацевтика
                    "MedicinePharmaceutics", // 17.144
                    //Менеджер по работе с клиентами
                    "AccountManager", // 17.149
                    //Металлопрокат
                    //Metal rolling, Металлопрокат
                    "MetalRolling", // 17.152
                    //Многоуровневый маркетинг
                    //Multilevel marketing, Многоуровневый маркетинг
                    "MultilevelMarketing", // 17.156
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 17.183
                    //Оптовая торговля
                    //Wholesale, Оптовая торговля
                    "Wholesale", // 17.196
                    //Продукты питания
                    "Food", // 17.231
                    //Прямые продажи
                    //Direct sales, Прямые продажи
                    "DirectSales", // 17.242
                    //Розничная торговля
                    //Retail, Розничная торговля
                    "Retail", // 17.256
                    //Телекоммуникации, Сетевые решения
                    //Telecommunications, Network Solutions, Телекоммуникации, Сетевые решения
                    "TelecommunicationsNetworkSolutions", // 17.269
                    //Станки, тяжелое оборудование
                    //Machine tools, Heavy equipment, Станки, Тяжелое оборудование
                    "MachineToolsHeavyEquipment", // 17.279
                    //Товары для бизнеса
                    //Goods for business, Товары для бизнеса
                    "GoodsForBusiness", // 17.301
                    //FMCG, Товары народного потребления
                    //FMCG, Consumer Goods, FMCG, Товары народного потребления
                    "FMCGConsumerGoods", // 17.302
                    //Торговля биржевыми товарами
                    //Trading in commodities, Торговля биржевыми товарами
                    "TradingInCommodities", // 17.303
                    //Торговый представитель
                    "SalesRepresentative", // 17.306
                    //Управление продажами
                    //Sales management, Управление продажами
                    "SalesManagement", // 17.324
                    //Услуги для бизнеса
                    //Services for Business, Услуги для бизнеса
                    "ServicesForBusiness", // 17.333
                    //Услуги для населения
                    //Services for the population, Услуги для населения
                    "ServicesForThePopulation", // 17.334
                    //Цветные металлы
                    //Non-ferrous metals, Цветные металлы
                    "NonFerrousMetals", // 17.350
                    //Электроника, фото, видео
                    //Electronics, photo, video, Электроника, фото, видео
                    "ElectronicsPhotoVideo", // 17.358
                }),



                //Production, Производство
                new ItemsInType("Production", new HashSet<string>
                {
                    //Сертификация
                    "Certification", // 18.404
                    //Главный механик
                    //Chief mechanical engineer, Главный механик
                    "ChiefMechanicalEngineer", // 18.431
                    // //Управление проектами
                    // "ProjectManagement", // 18.451
                    //Ювелирная промышленность
                    //Jewelery industry, Ювелирная промышленность
                    "JeweleryIndustry", // 18.453
                    //Метролог
                    //Metrologist, Метролог
                    "Metrologist", // 18.488
                    //Конструктор
                    "Constructor", // 18.568
                    //Атомная энергетика
                    //Nuclear power, Атомная энергетика
                    "NuclearPower", // 18.585
                    //Авиационная промышленность
                    //Aviation industry, Авиационная промышленность
                    "AviationIndustry", // 18.13
                    //Автомобильная промышленность
                    //Automotive industry, Автомобильная промышленность
                    "AutomotiveIndustry", // 18.16
                    //Главный агроном
                    //Chief agronomist, Главный агроном
                    "ChiefAgronomist", // 18.56
                    //Главный инженер
                    "ChiefEngineer", // 18.57
                    //Закупки и снабжение
                    //Procurement and Supply, Закупки и снабжение
                    "ProcurementAndSupply", // 18.73
                    //Зоотехник
                    //Zootechnician, Зоотехник
                    "Zootechnician", // 18.75
                    // //Инженер
                    // "Engineer", // 18.81
                    //Инженер, Мясо- и птицепереработка
                    //Engineer, Meat and poultry processing, Инженер, Мясо- и птицепереработка
                    "EngineerMeatAndPoultryProcessing", // 18.84
                    //Инженер, Производство и переработка зерновых
                    //Engineer, Production and processing of cereals, Инженер, Производство и переработка зерновых
                    "EngineerProductionAndProcessingOfCereals", // 18.85
                    //Инженер, Производство сахара
                    //Engineer, Sugar production, Инженер, Производство сахара
                    "EngineerSugarProduction", // 18.86
                    //Контроль качества
                    //Quality control, Контроль качества
                    "QualityControl", // 18.118
                    //Легкая промышленность
                    //Light industry, Легкая промышленность
                    "LightIndustry", // 18.129
                    //Деревообработка, Лесная промышленность
                    //Woodworking, Timber industry, Деревообработка, Лесная промышленность
                    "WoodworkingTimberIndustry", // 18.130
                    //Машиностроение
                    //Mechanical engineering, Машиностроение
                    "MechanicalEngineering", // 18.142
                    //Мебельное производство
                    //Furniture manufacturing, Мебельное производство
                    "FurnitureManufacturing", // 18.143
                    //Металлургия
                    //Metallurgy, Металлургия
                    "Metallurgy", // 18.153
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 18.174
                    //Нефтепереработка
                    //Oil refining, Нефтепереработка
                    "OilRefining", // 18.190
                    //Охрана труда
                    //Occupational Safety and Health, Охрана труда
                    "OccupationalSafetyAndHealth", // 18.201
                    //Пищевая промышленность
                    //Food industry, Пищевая промышленность
                    "FoodIndustry", // 18.208
                    //Полиграфия
                    //Polygraphy, Полиграфия
                    "Polygraphy", // 18.212
                    //Радиоэлектронная промышленность
                    //Radioelectronic industry, Радиоэлектронная промышленность
                    "RadioelectronicIndustry", // 18.245
                    //Руководство предприятием
                    //Management of the enterprise, Руководство предприятием
                    "ManagementOfTheEnterprise", // 18.263
                    //Сельхозпроизводство
                    //Agricultural production, Сельхозпроизводство
                    "AgriculturalProduction", // 18.265
                    //Стройматериалы
                    //Building materials, Стройматериалы
                    "BuildingMaterials", // 18.290
                    //Табачная промышленность
                    //Tobacco industry, Табачная промышленность
                    "TobaccoIndustry", // 18.291
                    //Технолог
                    "Technologist", // 18.297
                    //Технолог, Мясо- и птицепереработка
                    //Technologist, Meat and poultry processing, Технолог, Мясо- и птицепереработка
                    "TechnologistMeatAndPoultryProcessing", // 18.298
                    //Технолог, Производство и переработка зерновых
                    //Technologist, Production and processing of cereals, Технолог, Производство и переработка зерновых
                    "TechnologistProductionAndProcessingOfCereals", // 18.299
                    //Технолог, Производство сахара
                    //Technologist, Sugar production, Технолог, Производство сахара
                    "TechnologistSugarProduction", // 18.300
                    //Управление цехом
                    //Workshop management, Управление цехом
                    "WorkshopManagement", // 18.330
                    //Фармацевтическая промышленность
                    //Pharmaceutical industry, Фармацевтическая промышленность
                    "PharmaceuticalIndustry", // 18.339
                    //Химическая промышленность
                    //Chemical industry, Химическая промышленность
                    "ChemicalIndustry", // 18.348
                    //Эколог
                    "Ecologist", // 18.354
                    //Электроэнергетика
                    //Power engineering, Электроэнергетика
                    "PowerEngineering", // 18.360
                    //Энергетик производства
                    //Power Generator, Энергетик производства
                    "PowerGenerator", // 18.361
                    //Судостроение
                    //Shipbuilding, Судостроение
                    "Shipbuilding", // 18.373
                }),



                //Insurance, Страхование
                new ItemsInType("Insurance", new HashSet<string>
                {
                    //Актуарий
                    //Actuary, Актуарий
                    "Actuary", // 19.421
                    //Урегулирование убытков
                    //Settlement of losses, Урегулирование убытков
                    "SettlementOfLosses", // 19.430
                    //Перестрахование
                    //Reinsurance, Перестрахование
                    "Reinsurance", // 19.483
                    // //Врач-эксперт
                    // "ExpertDoctor", // 19.586
                    //Автострахование
                    //Auto Insurance, Автострахование
                    "AutoInsurance", // 19.18
                    //Агент
                    // //Agent, Агент
                    // "Agent", // 19.19
                    //Андеррайтер
                    //Underwriter, Андеррайтер
                    "Underwriter", // 19.28
                    //Комплексное страхование физических лиц
                    //Complex insurance of individuals, Комплексное страхование физических лиц
                    "ComplexInsuranceOfIndividuals", // 19.108
                    //Комплексное страхование юридических лиц
                    //Comprehensive insurance of legal entities, Комплексное страхование юридических лиц
                    "ComprehensiveInsuranceOfLegalEntities", // 19.109
                    //Медицинское страхование
                    //Health insurance, Медицинское страхование
                    "HealthInsurance", // 19.147
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 19.170
                    //Руководитель направления
                    //Head of, Руководитель направления
                    "HeadOf", // 19.259
                    //Страхование бизнеса
                    //Business Insurance, Страхование бизнеса
                    "BusinessInsurance", // 19.282
                    //Страхование жизни
                    //Life insurance, Страхование жизни
                    "LifeInsurance", // 19.283
                    //Страхование недвижимости
                    //Insurance of real estate, Страхование недвижимости
                    "InsuranceOfRealEstate", // 19.284
                    //Страхование ответственности
                    //Liability Insurance, Страхование ответственности
                    "LiabilityInsurance", // 19.285
                    //Эксперт-оценщик
                    //Expert appraiser, Эксперт-оценщик
                    "ExpertAppraiser", // 19.357
                }),



                //Construction, real estate, Строительство, недвижимость
                new ItemsInType("ConstructionRealEstate", new HashSet<string>
                {
                    //Эксплуатация
                    //Exploitation, Эксплуатация
                    "Exploitation", // 20.396
                    //ЖКХ
                    //Housing and utilities, ЖКХ
                    "HousingAndUtilities", // 20.445
                    //Геодезия и картография
                    //Geodesy and cartography, Геодезия и картография
                    "GeodesyAndCartography", // 20.484
                    //Рабочие строительных специальностей
                    //Workers of construction specialties, Рабочие строительных специальностей
                    "WorkersOfConstructionSpecialties", // 20.490
                    //Землеустройство
                    //Land management, Землеустройство
                    "LandManagement", // 20.525
                    //Оценка
                    //Evaluation, Оценка
                    "Evaluation", // 20.526
                    //Отопление, вентиляция и кондиционирование
                    //Heating, ventilation and air conditioning, Отопление, вентиляция и кондиционирование
                    "HeatingVentilationAndAirConditioning", // 20.527
                    //Водоснабжение и канализация
                    //Water supply and sewerage, Водоснабжение и канализация
                    "WaterSupplyAndSewerage", // 20.528
                    // //Тендеры
                    // "Tenders", // 20.573
                    // //Агент
                    // "Agent", // 20.20
                    //Гостиницы, Магазины
                    //Hotels, Shops, Гостиницы, Магазины
                    "HotelsShops", // 20.58
                    //Дизайн/Оформление
                    //Design / Decoration, Дизайн/Оформление
                    "DesignDecoration", // 20.63
                    //Жилье
                    //Accomodation, Жилье
                    "Accomodation", // 20.70
                    // //Инженер
                    // "Engineer", // 20.83
                    // //Начальный уровень/Мало опыта
                    // "BeginnerLittleExperience", // 20.186
                    //Нежилые помещения
                    //Non-residential premises, Нежилые помещения
                    "NonResidentialPremises", // 20.189
                    //Проектирование, Архитектура
                    //Designing, Architecture, Проектирование, Архитектура
                    "DesigningArchitecture", // 20.233
                    //Строительство
                    //Building, Строительство
                    "Building", // 20.287
                    // //Управление проектами
                    // "ProjectManagement", // 20.325
                    //Девелопер
                    //The developer, Девелопер
                    "TheDeveloper", // 20.375
                    //Прораб
                    "Foreman", // 20.387
                }),



                //Transport, logistics, Транспорт, логистика
                new ItemsInType("TransportLogistics", new HashSet<string>
                {
                    //Гражданская авиация
                    //civil Aviation, Гражданская авиация
                    "civilAviation", // 21.402
                    //Бизнес-авиация
                    //Business Aviation, Бизнес-авиация
                    "BusinessAviation", // 21.403
                    //Контейнерные перевозки
                    //Container Shipping, Контейнерные перевозки
                    "ContainerShipping", // 21.480
                    //Диспетчер
                    "Dispatcher", // 21.481
                    //Водитель
                    "Driver", // 21.482
                    //Экспедитор
                    "Forwarder", // 21.506
                    //Кладовщик
                    "Storekeeper", // 21.563
                    //Рабочий склада
                    //Work warehouse, Рабочий склада
                    "WorkWarehouse", // 21.564
                    //Авиаперевозки
                    //Air Freight, Авиаперевозки
                    "AirFreight", // 21.12
                    //Автоперевозки
                    //Trucking, Автоперевозки
                    "Trucking", // 21.17
                    //ВЭД
                    //Foreign trade activities, ВЭД
                    "ForeignTradeActivities", // 21.53
                    //Железнодорожные перевозки
                    //Rail transportation, Железнодорожные перевозки
                    "RailTransportation", // 21.69
                    //Закупки, Снабжение
                    //Procurement, Supply, Закупки, Снабжение
                    "ProcurementSupply", // 21.74
                    //Логистика
                    //Logistics, Логистика
                    "Logistics", // 21.136
                    //Морские/Речные перевозки
                    //Sea / River transportations, Морские/Речные перевозки
                    "SeaRiverTransportations", // 21.158
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 21.176
                    //Складское хозяйство
                    //Storage facilities, Складское хозяйство
                    "StorageFacilities", // 21.275
                    //Таможенное оформление
                    //Customs clearance, Таможенное оформление
                    "CustomsClearance", // 21.292
                    //Трубопроводы
                    //Pipelines, Трубопроводы
                    "Pipelines", // 21.310
                }),



                //Tourism, hotels, restaurants, Туризм, гостиницы, рестораны
                new ItemsInType("TourismHotelsRestaurants", new HashSet<string>
                {
                    // //Повар
                    // "Cook", // 22.491
                    //Хостес
                    //Hostess, Хостес
                    "Hostess", // 22.504
                    //Швейцар
                    //Doorman, Швейцар
                    "Doorman", // 22.505
                    //Сомелье
                    //Sommelier, Сомелье
                    "Sommelier", // 22.518
                    //Анимация
                    //Animation, Анимация
                    "Animation", // 22.529
                    //Оформление виз
                    //Visa processing, Оформление виз
                    "VisaProcessing", // 22.530
                    //Управление туристическим бизнесом
                    //Management of tourist business, Управление туристическим бизнесом
                    "ManagementOfTouristBusiness", // 22.531
                    //Авиабилеты
                    //Flights, Авиабилеты
                    "Flights", // 22.11
                    //Банкеты
                    //Banquets, Банкеты
                    "Banquets", // 22.35
                    //Бронирование
                    //Reservation, Бронирование
                    "Reservation", // 22.39
                    //Гид, Экскурсовод
                    //Guide, Tour guide, Гид, Экскурсовод
                    "GuideTourGuide", // 22.55
                    //Кейтеринг
                    //Catering, Кейтеринг
                    "Catering", // 22.104
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 22.175
                    //Официант, Бармен
                    //Waiter, Barman, Официант, Бармен
                    "WaiterBarman", // 22.193
                    //Организация встреч, Конференций
                    //Organization of meetings, Conferences, Организация встреч, Конференций
                    "OrganizationOfMeetingsConferences", // 22.198
                    //Организация туристических продуктов
                    //Organization of tourist products, Организация туристических продуктов
                    "OrganizationOfTouristProducts", // 22.199
                    //Персонал кухни
                    //Kitchen staff, Персонал кухни
                    "KitchenStaff", // 22.204
                    //Продажа туристических услуг
                    //Sale of tourist services, Продажа туристических услуг
                    "SaleOfTouristServices", // 22.223
                    //Размещение, Обслуживание гостей
                    //Accommodation, Guest Services, Размещение, Обслуживание гостей
                    "AccommodationGuestServices", // 22.248
                    //Управление гостиницами
                    //Management of hotels, Управление гостиницами
                    "ManagementOfHotels", // 22.316
                    //Управление ресторанами, Барами
                    //Management of restaurants, Bars, Управление ресторанами, Барами
                    "ManagementOfRestaurantsBars", // 22.329
                    //Шеф-повар
                    "Chef", // 22.353
                }),



                //Lawyers, Юристы
                new ItemsInType("Lawyers", new HashSet<string>
                {
                    //Авторское право
                    //Copyright, Авторское право
                    "Copyright", // 23.422
                    //Международное право
                    //International law, Международное право
                    "InternationalLaw", // 23.442
                    //Земельное право
                    //Land law, Земельное право
                    "LandLaw", // 23.476
                    //Договорное право
                    //Contract law, Договорное право
                    "ContractLaw", // 23.477
                    //Регистрация юридических лиц
                    //Registration of legal entities, Регистрация юридических лиц
                    "RegistrationOfLegalEntities", // 23.478
                    // //Взыскание задолженности, Коллекторская деятельность
                    // "DebtCollectionCollectorActivity", // 23.479
                    //Антимонопольное право
                    //Antimonopoly law, Антимонопольное право
                    "AntimonopolyLaw", // 23.539
                    //Compliance
                    //Compliance, Compliance
                    "Compliance", // 23.2
                    //Адвокат
                    //Lawyer, Адвокат
                    "Lawyer", // 23.21
                    //Арбитраж
                    //Arbitration, Арбитраж
                    "Arbitration", // 23.29
                    //Банковское право
                    //Banking Law, Банковское право
                    "BankingLaw", // 23.36
                    //Законотворчество
                    //Lawmaking, Законотворчество
                    "Lawmaking", // 23.72
                    //Интеллектуальная собственность
                    //Intellectual property, Интеллектуальная собственность
                    "IntellectualProperty", // 23.88
                    //Корпоративное право
                    //Corporate law, Корпоративное право
                    "CorporateLaw", // 23.120
                    //Морское право
                    //Law of the Sea, Морское право
                    "LawOfTheSea", // 23.159
                    //Налоговое право
                    //Tax law, Налоговое право
                    "TaxLaw", // 23.165
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 23.182
                    // //Недвижимость
                    // "TheProperty", // 23.187
                    //Недропользование
                    //Subsoil Use, Недропользование
                    "SubsoilUse", // 23.188
                    // //Помощник
                    // "Assistant", // 23.214
                    //Семейное право
                    //Family law, Семейное право
                    "FamilyLaw", // 23.266
                    //Слияния и поглощения
                    //M & A, Слияния и поглощения
                    "M&A", // 23.276
                    //Страховое право
                    //Insurance law, Страховое право
                    "InsuranceLaw", // 23.286
                    //Трудовое право
                    //Labor law, Трудовое право
                    "LaborLaw", // 23.311
                    //Уголовное право
                    //Criminal law, Уголовное право
                    "CriminalLaw", // 23.314
                    //Ценные бумаги, Рынки капитала
                    //Securities, Capital Markets, Ценные бумаги, Рынки капитала
                    "SecuritiesCapitalMarkets", // 23.352
                    //Юрисконсульт
                    //The Legal Counsel, Юрисконсульт
                    "TheLegalCounsel", // 23.362
                }),



                //Sports clubs, fitness, beauty salons, Спортивные клубы, фитнес, салоны красоты
                new ItemsInType("SportsClubsFitnessBeautySalons", new HashSet<string>
                {
                    //Массажист
                    "Masseur", // 24.492
                    //Парикмахер
                    "TheHairdresser", // 24.493
                    //Ногтевой сервис
                    //Manicure, Ногтевой сервис
                    "Manicure", // 24.624
                    //Косметология
                    //Cosmetology, Косметология
                    "Cosmetology", // 24.377
                    //Тренерский состав
                    //Coaching staff, Тренерский состав
                    "CoachingStaff", // 24.378
                    // //Администрация
                    // "Administration", // 24.379
                    // //Продажи
                    // "Sales", // 24.380

                    //Instructor by sport, Инструктор по видам спорта
                    "InstructorBySport",
                    //Coach, Спортивный тренер
                    "Coach",
                    //Athlete, Спортсмен
                    "Athlete",
                }),



                //Installation and service, Инсталляция и сервис
                new ItemsInType("InstallationAndService", new HashSet<string>
                {
                    //Сервисный инженер
                    //Service Engineer, Сервисный инженер
                    "ServiceEngineer", // 25.381
                    //Руководитель сервисного центра
                    //Head of the Service Center, Руководитель сервисного центра
                    "HeadOfTheServiceCenter", // 25.382
                    //Менеджер по сервису - сетевые и телекоммуникационные технологии
                    //Service Manager - Networking and Telecommunication Technologies, Менеджер по сервису - сетевые и телекоммуникационные технологии
                    "ServiceManagerNetworkingAndTelecommunicationTechnologies", // 25.383
                    //Менеджер по сервису - промышленное оборудование
                    //Service Manager - Industrial Equipment, Менеджер по сервису - промышленное оборудование
                    "ServiceManagerIndustrialEquipment", // 25.384
                    //Менеджер по сервису - транспорт
                    //Service Manager - Transport, Менеджер по сервису - транспорт
                    "ServiceManagerTransport", // 25.385
                    //Инсталляция и настройка оборудования
                    //Installation and configuration of equipment, Инсталляция и настройка оборудования
                    "InstallationAndConfigurationOfEquipment", // 25.386
                }),



                //Procurement, Закупки
                new ItemsInType("Procurement", new HashSet<string>
                {
                    // //Сертификация
                    // "Certification", // 26.406
                    // //Автомобили, Запчасти
                    // "CarsSpareParts", // 26.407
                    // //Алкоголь
                    // "Alcohol", // 26.408
                    // //ГСМ, нефть, бензин
                    // "PetroleumPetroleumGasoline", // 26.409
                    // //Компьютерная техника
                    // "ComputerTechnology", // 26.410
                    //Фармацевтика
                    //Pharmaceuticals, Фармацевтика
                    "Pharmaceuticals", // 26.411
                    // //Продукты питания
                    // "Food", // 26.412
                    // //Товары для бизнеса
                    // "GoodsForBusiness", // 26.413
                    // //Электроника, фото, видео
                    // "ElectronicsPhotoVideo", // 26.414
                    //Химическая продукция
                    "ChemicalProducts", // 26.415
                    // //FMCG, Товары народного потребления
                    // "FMCGConsumerGoods", // 26.416
                    //Начальный уровень, Мало опыта
                    // "EntryLevelLittleExperience", // 26.419
                    // //Металлопрокат
                    // "MetalRolling", // 26.427
                    //Управление закупками
                    "ProcurementManagement", // 26.437
                    // //Строительные материалы
                    // "ConstructionMaterials", // 26.439
                    //Электротехническое оборудование/светотехника
                    //Electrical Equipment / Lighting, Электротехническое оборудование/светотехника
                    "ElectricalEquipmentLighting", // 26.449
                    // //Станки, Тяжелое оборудование
                    // "MachineToolsHeavyEquipment", // 26.473
                    // //Тендеры
                    // "Tenders", // 26.574
                }),



                //Home staff, Домашний персонал
                new ItemsInType("HomeStaff", new HashSet<string>
                {
                    //Воспитатель, Гувернантка, Няня
                    //Teacher, Governess, Nanny, Воспитатель, Гувернантка, Няня
                    "TeacherGovernessNanny", // 27.496
                    //домработница/домработник, Горничная
                    //housekeeper / domestic worker, chambermaid, домработница/домработник, Горничная
                    "housekeeperDomesticWorkerChambermaid", // 27.497
                    //Персональный водитель
                    //Personal driver, Персональный водитель
                    "PersonalDriver", // 27.498
                    //Садовник
                    "Gardener", // 27.499
                    //Повар
                    "Cook", // 27.500
                    //Сиделка
                    "Nurse", // 27.501
                    //Помощник по хозяйству, Управляющий
                    //Housekeeping Assistant, Manager, Помощник по хозяйству, Управляющий
                    "HousekeepingAssistantManager", // 27.533
                    //Репетитор
                    //Tutor, Репетитор
                    "Tutor", // 27.584
                }),



                //Working staff, Рабочий персонал
                new ItemsInType("WorkingStaff", new HashSet<string>
                {
                    //Грузчик
                    "Loader", // 29.495
                    //Слесарь
                    "Locksmith", // 29.510
                    //Токарь, Фрезеровщик
                    //Turner, Milling Machine, Токарь, Фрезеровщик
                    "TurnerMillingMachine", // 29.511
                    //Фасовщик
                    //Fasovshchik, Фасовщик
                    "Fasovshchik", // 29.512
                    //Столяр
                    "Joiner", // 29.513
                    //Сварщик
                    "Welder", // 29.514
                    //Электрик
                    "AnElectrician", // 29.515
                    //Швея
                    "Seamstress", // 29.516
                    // //Сборщик
                    // "Collector", // 29.517
                    //Гардеробщик
                    //Cloakroom attendant, Гардеробщик
                    "CloakroomAttendant", // 29.540
                    //Дворник, Уборщик
                    //Janitor, Cleaner, Дворник, Уборщик
                    "JanitorCleaner", // 29.541
                    //Дорожные рабочие
                    //Road workers, Дорожные рабочие
                    "RoadWorkers", // 29.542
                    //Жестянщик
                    //Tinsmith, Жестянщик
                    "Tinsmith", // 29.543
                    //Заправщик картриджей
                    //Cartridge refiller, Заправщик картриджей
                    "CartridgeRefiller", // 29.544
                    //Комплектовщик, Укладчик-упаковщик
                    //Packer, Packer-packer, Комплектовщик, Укладчик-упаковщик
                    "PackerPackerPacker", // 29.545
                    //Краснодеревщик
                    "Cabinetmaker", // 29.546
                    //Кузнец
                    //Blacksmith, Кузнец
                    "Blacksmith", // 29.547
                    //Лифтер
                    //Liftter, Лифтер
                    "Liftter", // 29.548
                    //Машинист производства
                    //Production Engineer, Машинист производства
                    "ProductionEngineer", // 29.549
                    //Машинист сцены
                    //The stage operator, Машинист сцены
                    "TheStageOperator", // 29.550
                    //Машинист экскаватора
                    //Excavator driver, Машинист экскаватора
                    "ExcavatorDriver", // 29.551
                    //Механик
                    //Mechanic, Механик
                    "Mechanic", // 29.552
                    //Оператор станков
                    //Machine Operator, Оператор станков
                    "MachineOperator", // 29.553
                    //Пастух, Чабан
                    //Shepherd, Shepherd, Пастух, Чабан
                    "ShepherdShepherd", // 29.554
                    //Перемотчик
                    //Rewinders, Перемотчик
                    "Rewinders", // 29.555
                    //Разнорабочий
                    //Handyman, Разнорабочий
                    "Handyman", // 29.556
                    //Сантехник
                    "Plumber", // 29.557
                    //Шлифовщик
                    "Grinder", // 29.558
                    //Штукатур
                    "Plasterer", // 29.559
                    //Электромонтер, Кабельщик
                    //Electrician, Cable man, Электромонтер, Кабельщик
                    "ElectricianCableMan", // 29.560
                    //Ювелир
                    "Jeweler", // 29.561
                    //Монтажник
                    "Installer", // 29.581
                    //Проводник
                    "Conductor", // 29.582
                    //Маляр
                    "Painter", // 29.583
                    // //Другое
                    // "Other", // 29.588
                    //Наладчик
                    "Accompanist", // 29.162
                }),
                //Other, Прочие
                new ItemsInType("Other", new HashSet<string>
                {
                    //Agent, Агент
                    "Agent", // 
                     //Инженер
                    "Engineer", // 
                    "Other",
                }),
            };
        }

        private static ProfessionsInTypes _instanse;
        public static ProfessionsInTypes Instanse => _instanse ?? (_instanse = new ProfessionsInTypes());
    }
}