﻿using System.Collections.Generic;
using Data.Entities.ApplicantEntities;
using Data.Entities.CompanyEntities;

namespace Data.Entities.Common
{
    public class Address : BaseEntity
    {
        public string FormattedAddress { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string StreetNumber { get; set; }

        public decimal Latitude { get; set; } // Latitude: -85 to +85 (actually -85.05115 for some reason)
        public decimal Longitude { get; set; } // Longitude: -180 to +180
        public string Description { get; set; }

        public List<CompanyFilial> CompanyFilials { get; set; }
        public List<ApplicantPreferredAddress> ApplicantPreferredAddresses { get; set; }
    }
}