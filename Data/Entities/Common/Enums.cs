﻿namespace Data.Entities.Common
{
    public enum UserType
    {
        Company = 1,
        Applicant,
        Admin,
    }



    // todo localized name, and other data
    public enum Currency
    {
        USD = 1,
        UAH,
        EUR,
        RUB,
        Bitcoin,

        AZN,
        BYR,
        GEL,
        KGS,
        KZT,
        UZS,

        Other,
    }

    public enum EmploymentType
    {
        FullTime = 1,
        PartTime,
        RemoteWork,
        ProjectWork,
        Probation,
    }

    public enum SalaryPeriod
    {
        Month = 1,
        Day,
        Shift, // smena
        Project,
    }

    public enum CompanyCreationType
    {
        Registration = 1,
        FromDou,
        FromHh,
    }

    public enum FileKind
    {
        CompanyMainPhoto = 1,
        CompanyPhoto,
        ApplicantMainPhoto,
        ApplicantResume,
    }

    public enum FileType
    {
        Image = 1,
        Document,
    }
}