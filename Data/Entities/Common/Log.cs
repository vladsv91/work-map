﻿using System.Collections.Generic;
using Data.Entities.ApplicantEntities;
using Data.Entities.CompanyEntities;

namespace Data.Entities.Common
{
    public class BaseLog : BaseEntityGuid
    {
        public LogType Type { get; set; }
        public string Message { get; set; }
        public string IpAddress { get; set; }
        public string RequestUrl { get; set; }
        public string UserId { get; set; }
        public string UserAgent { get; set; }

        public string HttpMethod { get; set; }
        public string SerializedHeaders { get; set; }

        public string Host { get; set; }
        //public byte[] RequestBody { get; set; } maybe add
    }

    public class Log : BaseLog { }

    public class MigratedLog : BaseLog { }

    public enum LogType
    {
        Info = 1,
        Warning,
        Error
    }
}