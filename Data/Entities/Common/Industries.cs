﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json;

namespace Data.Entities.Common
{

    public class IndustriesInTypes : ItemsInTypes
    {
        protected override List<ItemsInType> GetItems()
        {
            return new List<ItemsInType>
            {
                new ItemsInType("AgricultureAndMining", new HashSet<string>
                {
                    "FarmingAndRanching",
                    "FishingHuntingAndForestryAndLogging",
                    "MiningAndQuarrying",
                    "OtherInAgricultureAndMining",
                }),
                new ItemsInType("BusinessServices", new HashSet<string>
                {
                    "AccountingAndTaxPreparation",
                    "AdvertisingMarketingAndPR",
                    "DataAndRecordsManagement",
                    "FacilitiesManagementAndMaintenance",
                    "HRAndRecruitingServices",
                    "LegalServices",
                    "ManagementConsulting",
                    "PayrollServices",
                    "SalesServices",
                    "SecurityServices",
                    "OtherInBusinessServices",
                }),
                new ItemsInType("ComputerAndElectronics", new HashSet<string>
                {
                    "AudioVideoAndPhotography",
                    "ComputersPartsAndRepair",
                    "ConsumerElectronicsPartsAndRepair",
                    "ITAndNetworkServicesAndSupport",
                    "InstrumentsAndControls",
                    "NetworkSecurityProducts",
                    "NetworkingequipmentAndSystems",
                    "OfficeMachineryAndEquipment",
                    "PeripheralsManufacturing",
                    "SemiconductorAndMicrochipManufacturing",
                    "OtherInComputerAndElectronics",
                }),
                new ItemsInType("ConsumerServices", new HashSet<string>
                {
                    "AutomotiveRepairAndMaintenance",
                    "LaundryAndDryCleaning",
                    "ParkingLotsAndGarageManagement",
                    "PersonalCare",
                    "PhotofinishingServices",
                    "OtherInConsumerServices",
                }),
                new ItemsInType("Education", new HashSet<string>
                {
                    "CollegesAndUniversities",
                    "ElementaryAndSecondarySchools",
                    "LibrariesArchivesAndMuseums",
                    "SportsArtsAndRecreationInstruction",
                    "TechnicalAndTradeSchools",
                    "TestPreparation",
                    "OtherInEducation",
                }),
                new ItemsInType("EnergyAndUtilities", new HashSet<string>
                {
                    "AlternativeEnergySources",
                    "GasAndElectricUtilities",
                    "GasolineAndOilRefineries",
                    "SewageTreatmentFacilities",
                    "WasteManagementAndRecycling",
                    "WaterTreatmentAndUtilities",
                    "OtherInEnergyAndUtilities",
                }),
                new ItemsInType("FinancialServices", new HashSet<string>
                {
                    "Banks",
                    "CreditCardsAndRelatedServices",
                    "CreditUnions",
                    "InsuranceAndRiskManagement",
                    "InvestmentBankingAndVentureCapital",
                    "LendingAndMortgage",
                    "PersonalFinancialPlanningAndPrivateBanking",
                    "SecuritiesAgentsAndBrokers",
                    "TrustFiduciaryAndCustodyActivities",
                    "OtherInFinancialServices",
                }),
                new ItemsInType("Government", new HashSet<string>
                {
                    "InternationalBodiesAndOrganizations",
                    "LocalGovernment",
                    "NationalGovernment",
                    "StateProvincialGovernment",
                    "OtherInGovernment",
                }),
                new ItemsInType("HealthPharmaceuticalsAndBiotech", new HashSet<string>
                {
                    "Biotechnology",
                    "DiagnosticLaboratories",
                    "DoctorsAndHealthCarePractitioners",
                    "Hospitals",
                    "MedicalDevices",
                    "MedicalSuppliesAndEquipment",
                    "OutpatientCareCenters",
                    "PersonalHealthCareProducts",
                    "Pharmaceuticals",
                    "ResidentialAndLongTermCareFacilities",
                    "VeterinaryClinicsAndServices",
                    "OtherInHealthPharmaceuticalsAndBiotech",
                }),
                new ItemsInType("Manufacturing", new HashSet<string>
                {
                    "AerospaceAndDefense",
                    "AlcoholicBeverages",
                    "AutomobilesBoatsAndMotorVehicles",
                    "ChemicalsAndPetrochemicals",
                    "ConcreteGlassAndBuildingMaterials",
                    "FarmingAndMiningMachineryAndEquipment",
                    "FoodAndDairyProductManufacturingAndPackaging",
                    "FurnitureManufacturing",
                    "MetalsManufacturing",
                    "NonalcoholicBeverages",
                    "PaperAndPaperProducts",
                    "PlasticsAndRubberManufacturing",
                    "TextilesApparelAndAccessories",
                    "ToolsHardwareAndLightMachinery",
                    "OtherInManufacturing",
                }),
                new ItemsInType("Other", new HashSet<string>
                {
                    "Other",
                }),
            };
        }

        private static IndustriesInTypes _instanse;
        public static IndustriesInTypes Instanse => _instanse ?? (_instanse = new IndustriesInTypes());
    }


    public abstract class ItemsInTypes 
    {
        public ReadOnlyDictionary<string, HashSet<string>> GroupedValues { get; protected set; }

        public ImmutableHashSet<string> AllItems { get; protected set; }

        protected ItemsInTypes()
        {
            // ReSharper disable once VirtualMemberCallInConstructor
            var values = GetItems();
            GroupedValues =
                new ReadOnlyDictionary<string, HashSet<string>>(
                    values.ToDictionary(_ => _.Type, type => type.Items));
            AllItems = ImmutableHashSet.Create(values.SelectMany(_ => _.Items).ToArray());

            //var a = string.Join('\n', values.SelectMany(_ => _.Items));

#if DEBUG
            if (AllItems.Count != values.Sum(_ => _.Items.Count)) {
                throw new Exception("Has items doubles.");
            }
#endif
        }

        protected abstract List<ItemsInType> GetItems();
    }

    public class ItemsInType
    {
        public string Type { get; set; }
        public HashSet<string> Items { get; set; }

        public ItemsInType(string type, HashSet<string> items)
        {
            Type = type;
            Items = items;
        }
    }
}