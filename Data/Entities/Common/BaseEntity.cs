﻿using System;

namespace Data.Entities.Common
{
    public interface IBaseEntity<T>
    {
        T Id { get; set; }
        DateTime CreatedOnUtc { get; set; }
        DateTime UpdatedOnUtc { get; set; }
    }

    public interface IBaseEntity : IBaseEntity<string> { }
    public interface IBaseEntityGuid : IBaseEntity<Guid> { }

    public interface IBaseLookup : IBaseEntity
    {
        string Name { get; set; }
    }

    public interface IBaseStatusLookup : IBaseLookup
    {
        bool IsDisabled { get; set; }
    }

    public abstract class BaseEntity<T> : IBaseEntity<T>
    {
        public T Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
    }

    public abstract class BaseEntity : BaseEntity<string>, IBaseEntity { }

    public class BaseEntityReadVm : BaseEntity { }

    public abstract class BaseEntityGuid : BaseEntity<Guid>, IBaseEntityGuid { }



    public abstract class BaseLookup : BaseEntity, IBaseLookup
    {
        public string Name { get; set; }
    }

    public abstract class BaseStatusLookup : BaseLookup, IBaseStatusLookup
    {
        public bool IsDisabled { get; set; }
    }
}






//using System;

//namespace Data.Entities.Common
//{

//    public interface IBaseEntity<T>
//    {
//        T Id { get; set; }
//        DateTime CreatedOnUtc { get; set; }
//        DateTime UpdatedOnUtc { get; set; }
//    }

//    public interface IBaseEntity : IBaseEntity<string> { }

//    public interface IBaseLookup : IBaseEntity
//    {
//        string Name { get; set; }
//    }

//    public interface IBaseStatusLookup : IBaseLookup
//    {
//        bool IsDisabled { get; set; }
//    }

//    public abstract class BaseEntity<T> : IBaseEntity<T>
//    {
//        public T Id { get; set; }
//        public DateTime CreatedOnUtc { get; set; }
//        public DateTime UpdatedOnUtc { get; set; }
//    }


//    public abstract class BaseEntity : BaseEntity<string>, IBaseEntity { }

//    public abstract class BaseLookup : BaseEntity, IBaseLookup
//    {
//        public string Name { get; set; }
//    }

//    public abstract class BaseStatusLookup : BaseLookup, IBaseStatusLookup
//    {
//        public bool IsDisabled { get; set; }
//    }
//}