﻿using Data.Entities.ApplicantEntities;
using Data.Entities.CompanyEntities;

namespace Data.Entities.Common
{
    public class SendingResume : BaseEntity
    {
        public string ResumeId { get; set; }
        public Resume Resume { get; set; }

        public string ApplicantId { get; set; }
        public Applicant Applicant { get; set; }

        public string CompanyId { get; set; }
        public Company Company { get; set; }

        public string VacancyId { get; set; }
        public Vacancy Vacancy { get; set; }

        public string Message { get; set; }
        public SendingResumeStatus Status { get; set; }
    }

    public enum SendingResumeStatus
    {
        Sent = 1,
        Viewed,
        RejectedByCompany,
    }
}