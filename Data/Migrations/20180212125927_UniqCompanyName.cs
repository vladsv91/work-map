﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class UniqCompanyName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vacancy_Company_CompanyId",
                table: "Vacancy");

            migrationBuilder.DropIndex(
                name: "IX_Vacancy_CompanyId",
                table: "Vacancy");

            migrationBuilder.DropIndex(
                name: "IX_Company_NormalizedName",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Vacancy");

            migrationBuilder.CreateIndex(
                name: "IX_Company_NormalizedName",
                table: "Company",
                column: "NormalizedName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Company_NormalizedName",
                table: "Company");

            migrationBuilder.AddColumn<string>(
                name: "CompanyId",
                table: "Vacancy",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vacancy_CompanyId",
                table: "Vacancy",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_Company_NormalizedName",
                table: "Company",
                column: "NormalizedName");

            migrationBuilder.AddForeignKey(
                name: "FK_Vacancy_Company_CompanyId",
                table: "Vacancy",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
