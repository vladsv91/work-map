﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class AddResumeFile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ResumeFileFileName",
                table: "Resume",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ResumeFileUri",
                table: "Resume",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ResumeFileFileName",
                table: "Resume");

            migrationBuilder.DropColumn(
                name: "ResumeFileUri",
                table: "Resume");
        }
    }
}
