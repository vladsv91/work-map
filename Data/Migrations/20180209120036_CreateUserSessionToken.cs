﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class CreateUserSessionToken : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UserSession",
                table: "UserSession");

            migrationBuilder.DropIndex(
                name: "IX_UserSession_InitialRefreshToken",
                table: "UserSession");

            migrationBuilder.DropIndex(
                name: "IX_UserSession_RefreshToken",
                table: "UserSession");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "UserSession");

            migrationBuilder.DropColumn(
                name: "RefreshToken",
                table: "UserSession");

            migrationBuilder.DropColumn(
                name: "RtExpiresOnUtc",
                table: "UserSession");

            migrationBuilder.AlterColumn<Guid>(
                name: "InitialRefreshToken",
                table: "UserSession",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserSession",
                table: "UserSession",
                column: "InitialRefreshToken");

            migrationBuilder.CreateTable(
                name: "UserSessionRefreshToken",
                columns: table => new
                {
                    RefreshToken = table.Column<Guid>(nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    InitialRefreshTokenId = table.Column<Guid>(nullable: false),
                    RtExpiresOnUtc = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false, defaultValue: 1),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSessionRefreshToken", x => x.RefreshToken);
                    table.ForeignKey(
                        name: "FK_UserSessionRefreshToken_UserSession_InitialRefreshTokenId",
                        column: x => x.InitialRefreshTokenId,
                        principalTable: "UserSession",
                        principalColumn: "InitialRefreshToken",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserSessionRefreshToken_InitialRefreshTokenId",
                table: "UserSessionRefreshToken",
                column: "InitialRefreshTokenId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserSessionRefreshToken");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserSession",
                table: "UserSession");

            migrationBuilder.AlterColumn<Guid>(
                name: "InitialRefreshToken",
                table: "UserSession",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<string>(
                name: "Id",
                table: "UserSession",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<Guid>(
                name: "RefreshToken",
                table: "UserSession",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<DateTime>(
                name: "RtExpiresOnUtc",
                table: "UserSession",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserSession",
                table: "UserSession",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_UserSession_InitialRefreshToken",
                table: "UserSession",
                column: "InitialRefreshToken");

            migrationBuilder.CreateIndex(
                name: "IX_UserSession_RefreshToken",
                table: "UserSession",
                column: "RefreshToken",
                unique: true);
        }
    }
}
