﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class AddRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"INSERT INTO [UserRole]
           ([Id]
           ,[ConcurrencyStamp]
           ,[Name]
           ,[NormalizedName])
     VALUES ('5FFE4E19', '5FFE4E19', 'Applicant', 'APPLICANT'),
			('6FFE4E19', '6FFE4E19', 'Company', 'COMPANY')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("delete [UserRole] where [Id] in ('5FFE4E19', '6FFE4E19')");
        }
    }
}
