﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class CompanyFileData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "MainPhotoId",
                table: "Company",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Company_MainPhotoId",
                table: "Company",
                column: "MainPhotoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Company_FileData_MainPhotoId",
                table: "Company",
                column: "MainPhotoId",
                principalTable: "FileData",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Company_FileData_MainPhotoId",
                table: "Company");

            migrationBuilder.DropIndex(
                name: "IX_Company_MainPhotoId",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "MainPhotoId",
                table: "Company");
        }
    }
}
