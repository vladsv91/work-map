﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class RemoveLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Log");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Log",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "NEWID()"),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Host = table.Column<string>(maxLength: 100, nullable: false),
                    HttpMethod = table.Column<string>(maxLength: 50, nullable: false),
                    IpAddress = table.Column<string>(maxLength: 50, nullable: false),
                    Message = table.Column<string>(maxLength: 10000, nullable: true),
                    RequestUrl = table.Column<string>(maxLength: 3000, nullable: false),
                    SerializedHeaders = table.Column<string>(maxLength: 10000, nullable: false),
                    Type = table.Column<int>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    UserAgent = table.Column<string>(maxLength: 500, nullable: true),
                    UserId = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Log", x => x.Id);
                });
        }
    }
}
