﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class AddApplicant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Applicant",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ContactEmail = table.Column<string>(maxLength: 100, nullable: true),
                    ContactPhone = table.Column<string>(maxLength: 20, nullable: true),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    CreatorId = table.Column<string>(nullable: false),
                    DateOfBirth = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(maxLength: 1000, nullable: true),
                    FullName = table.Column<string>(maxLength: 100, nullable: false),
                    PhotoFileName = table.Column<string>(nullable: true),
                    PhotoUri = table.Column<string>(nullable: true),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    WebsiteUrl = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applicant", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Applicant_ApplicationUser_CreatorId",
                        column: x => x.CreatorId,
                        principalTable: "ApplicationUser",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApplicantPreferredAddress",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    AddressId = table.Column<string>(nullable: false),
                    ApplicantId = table.Column<string>(nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    RadiusInKilometers = table.Column<decimal>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicantPreferredAddress", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApplicantPreferredAddress_Address_AddressId",
                        column: x => x.AddressId,
                        principalTable: "Address",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ApplicantPreferredAddress_Applicant_ApplicantId",
                        column: x => x.ApplicantId,
                        principalTable: "Applicant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Resume",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    ApplicantId = table.Column<string>(nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Description = table.Column<string>(maxLength: 2000, nullable: true),
                    ExperienceYears = table.Column<decimal>(nullable: false),
                    IsDisabled = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Skills = table.Column<string>(maxLength: 2000, nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Resume", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Resume_Applicant_ApplicantId",
                        column: x => x.ApplicantId,
                        principalTable: "Applicant",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "WorkExperience",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CompanyName = table.Column<string>(maxLength: 100, nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    DateFrom = table.Column<DateTime>(nullable: false),
                    DateTo = table.Column<DateTime>(nullable: true),
                    Position = table.Column<string>(maxLength: 100, nullable: false),
                    ResumeId = table.Column<string>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkExperience", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WorkExperience_Resume_ResumeId",
                        column: x => x.ResumeId,
                        principalTable: "Resume",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Applicant_CreatorId",
                table: "Applicant",
                column: "CreatorId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantPreferredAddress_AddressId",
                table: "ApplicantPreferredAddress",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_ApplicantPreferredAddress_ApplicantId",
                table: "ApplicantPreferredAddress",
                column: "ApplicantId");

            migrationBuilder.CreateIndex(
                name: "IX_Resume_ApplicantId",
                table: "Resume",
                column: "ApplicantId");

            migrationBuilder.CreateIndex(
                name: "IX_WorkExperience_ResumeId",
                table: "WorkExperience",
                column: "ResumeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicantPreferredAddress");

            migrationBuilder.DropTable(
                name: "WorkExperience");

            migrationBuilder.DropTable(
                name: "Resume");

            migrationBuilder.DropTable(
                name: "Applicant");
        }
    }
}
