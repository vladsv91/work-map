﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class IndAndProfessions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IndustryId",
                table: "Vacancy");

            migrationBuilder.AddColumn<int>(
                name: "ProfessionId",
                table: "Vacancy",
                nullable: false,
                defaultValue: 156);

            migrationBuilder.AddColumn<int>(
                name: "ProfessionId",
                table: "Resume",
                nullable: false,
                defaultValue: 156);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProfessionId",
                table: "Vacancy");

            migrationBuilder.DropColumn(
                name: "ProfessionId",
                table: "Resume");

            migrationBuilder.AddColumn<int>(
                name: "IndustryId",
                table: "Vacancy",
                nullable: false,
                defaultValue: 0);
        }
    }
}
