﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class DeleteContactsFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContactEmail",
                table: "Vacancy");

            migrationBuilder.DropColumn(
                name: "ContactPhone",
                table: "Vacancy");

            migrationBuilder.DropColumn(
                name: "ContactEmail",
                table: "CompanyFilial");

            migrationBuilder.DropColumn(
                name: "ContactPhone",
                table: "CompanyFilial");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContactEmail",
                table: "Vacancy",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPhone",
                table: "Vacancy",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactEmail",
                table: "CompanyFilial",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPhone",
                table: "CompanyFilial",
                maxLength: 20,
                nullable: true);
        }
    }
}
