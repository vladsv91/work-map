﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class ApplicantFileData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MainPhotoFileName",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "MainPhotoUri",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "PhotoFileName",
                table: "Applicant");

            migrationBuilder.DropColumn(
                name: "PhotoUri",
                table: "Applicant");

            migrationBuilder.AddColumn<Guid>(
                name: "PhotoId",
                table: "Applicant",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Applicant_PhotoId",
                table: "Applicant",
                column: "PhotoId");

            migrationBuilder.AddForeignKey(
                name: "FK_Applicant_FileData_PhotoId",
                table: "Applicant",
                column: "PhotoId",
                principalTable: "FileData",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Applicant_FileData_PhotoId",
                table: "Applicant");

            migrationBuilder.DropIndex(
                name: "IX_Applicant_PhotoId",
                table: "Applicant");

            migrationBuilder.DropColumn(
                name: "PhotoId",
                table: "Applicant");

            migrationBuilder.AddColumn<string>(
                name: "MainPhotoFileName",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MainPhotoUri",
                table: "Company",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhotoFileName",
                table: "Applicant",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhotoUri",
                table: "Applicant",
                nullable: true);
        }
    }
}
