﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class ContactUs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "ProfessionId",
                table: "Vacancy",
                nullable: false,
                oldClrType: typeof(int),
                oldDefaultValue: 156);

            migrationBuilder.AlterColumn<int>(
                name: "ProfessionId",
                table: "Resume",
                nullable: false,
                oldClrType: typeof(int),
                oldDefaultValue: 156);

            migrationBuilder.CreateTable(
                name: "ContactUs",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    FullName = table.Column<string>(maxLength: 50, nullable: true),
                    Message = table.Column<string>(maxLength: 1000, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 20, nullable: true),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactUs", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContactUs");

            migrationBuilder.AlterColumn<int>(
                name: "ProfessionId",
                table: "Vacancy",
                nullable: false,
                defaultValue: 156,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ProfessionId",
                table: "Resume",
                nullable: false,
                defaultValue: 156,
                oldClrType: typeof(int));
        }
    }
}
