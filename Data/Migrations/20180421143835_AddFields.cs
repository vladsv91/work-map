﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class AddFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "MaxSalary",
                table: "Vacancy",
                type: "decimal(20,8)",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "MinSalary",
                table: "Vacancy",
                type: "decimal(20,8)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SalaryPeriod",
                table: "Vacancy",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "CompanyPhoto",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "CompanyFilial",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "HhUrl",
                table: "Company",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Address",
                maxLength: 200,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxSalary",
                table: "Vacancy");

            migrationBuilder.DropColumn(
                name: "MinSalary",
                table: "Vacancy");

            migrationBuilder.DropColumn(
                name: "SalaryPeriod",
                table: "Vacancy");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "CompanyPhoto");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "CompanyFilial");

            migrationBuilder.DropColumn(
                name: "HhUrl",
                table: "Company");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Address");
        }
    }
}
