﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class GuidIdInLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey("PK_Log", "Log");
            migrationBuilder.Sql("UPDATE [Log] Set Id = NEWID()");
            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "Log",
                nullable: false,
                defaultValueSql: "NEWID()",
                oldClrType: typeof(string));

            migrationBuilder.AddPrimaryKey("PK_Log", "Log", "Id");

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "FileData",
                nullable: false,
                defaultValueSql: "NEWID()",
                oldClrType: typeof(Guid));

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey("PK_Log", "Log");
            migrationBuilder.AlterColumn<string>(
                name: "Id",
                table: "Log",
                nullable: false,
                oldClrType: typeof(Guid),
                oldDefaultValueSql: "NEWID()");
            migrationBuilder.AddPrimaryKey("PK_Log", "Log", "Id");

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "FileData",
                nullable: false,
                oldClrType: typeof(Guid),
                oldDefaultValueSql: "NEWID()");
        }
    }
}
