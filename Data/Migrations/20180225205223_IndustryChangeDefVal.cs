﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class IndustryChangeDefVal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IndustryId",
                table: "Company",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldDefaultValue: "NetworkSecurityProducts");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "IndustryId",
                table: "Company",
                maxLength: 50,
                nullable: false,
                defaultValue: "NetworkSecurityProducts",
                oldClrType: typeof(string),
                oldMaxLength: 50);
        }
    }
}
