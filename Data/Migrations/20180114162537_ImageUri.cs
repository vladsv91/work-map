﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class ImageUri : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MainPhotoUrl",
                table: "Company",
                newName: "MainPhotoUri");

            migrationBuilder.AddColumn<string>(
                name: "MainPhotoFileName",
                table: "Company",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MainPhotoFileName",
                table: "Company");

            migrationBuilder.RenameColumn(
                name: "MainPhotoUri",
                table: "Company",
                newName: "MainPhotoUrl");
        }
    }
}
