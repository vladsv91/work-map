﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class AddSendingResume : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SendingResume",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CompanyId = table.Column<string>(nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Message = table.Column<string>(maxLength: 1000, nullable: true),
                    ResumeId = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false, defaultValue: 1),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    VacancyId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SendingResume", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SendingResume_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SendingResume_Resume_ResumeId",
                        column: x => x.ResumeId,
                        principalTable: "Resume",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                    table.ForeignKey(
                        name: "FK_SendingResume_Vacancy_VacancyId",
                        column: x => x.VacancyId,
                        principalTable: "Vacancy",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SendingResume_CompanyId",
                table: "SendingResume",
                column: "CompanyId");

            migrationBuilder.CreateIndex(
                name: "IX_SendingResume_ResumeId",
                table: "SendingResume",
                column: "ResumeId");

            migrationBuilder.CreateIndex(
                name: "IX_SendingResume_VacancyId",
                table: "SendingResume",
                column: "VacancyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SendingResume");
        }
    }
}
