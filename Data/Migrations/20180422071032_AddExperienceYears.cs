﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class AddExperienceYears : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "MaxExperienceYears",
                table: "Vacancy",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MinExperienceYears",
                table: "Vacancy",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MaxExperienceYears",
                table: "Vacancy");

            migrationBuilder.DropColumn(
                name: "MinExperienceYears",
                table: "Vacancy");
        }
    }
}
