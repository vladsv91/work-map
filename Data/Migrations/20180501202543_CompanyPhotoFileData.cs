﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class CompanyPhotoFileData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhotoFileName",
                table: "CompanyPhoto");

            migrationBuilder.DropColumn(
                name: "PhotoUri",
                table: "CompanyPhoto");

            migrationBuilder.AddColumn<Guid>(
                name: "PhotoId",
                table: "CompanyPhoto",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_CompanyPhoto_PhotoId",
                table: "CompanyPhoto",
                column: "PhotoId");

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyPhoto_FileData_PhotoId",
                table: "CompanyPhoto",
                column: "PhotoId",
                principalTable: "FileData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyPhoto_FileData_PhotoId",
                table: "CompanyPhoto");

            migrationBuilder.DropIndex(
                name: "IX_CompanyPhoto_PhotoId",
                table: "CompanyPhoto");

            migrationBuilder.DropColumn(
                name: "PhotoId",
                table: "CompanyPhoto");

            migrationBuilder.AddColumn<string>(
                name: "PhotoFileName",
                table: "CompanyPhoto",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "PhotoUri",
                table: "CompanyPhoto",
                nullable: false,
                defaultValue: "");
        }
    }
}
