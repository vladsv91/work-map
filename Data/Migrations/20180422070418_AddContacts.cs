﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class AddContacts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContactEmail",
                table: "Vacancy",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPhone",
                table: "Vacancy",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactEmail",
                table: "CompanyFilial",
                maxLength: 100,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPhone",
                table: "CompanyFilial",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "OtherContact",
                table: "Company",
                maxLength: 200,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContactEmail",
                table: "Vacancy");

            migrationBuilder.DropColumn(
                name: "ContactPhone",
                table: "Vacancy");

            migrationBuilder.DropColumn(
                name: "ContactEmail",
                table: "CompanyFilial");

            migrationBuilder.DropColumn(
                name: "ContactPhone",
                table: "CompanyFilial");

            migrationBuilder.DropColumn(
                name: "OtherContact",
                table: "Company");
        }
    }
}
