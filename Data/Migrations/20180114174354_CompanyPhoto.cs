﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class CompanyPhoto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CompanyPhoto",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    CompanyFilialId = table.Column<string>(nullable: true),
                    CompanyId = table.Column<string>(nullable: false),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    PhotoFileName = table.Column<string>(nullable: false),
                    PhotoUri = table.Column<string>(nullable: false),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CompanyPhoto", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CompanyPhoto_CompanyFilial_CompanyFilialId",
                        column: x => x.CompanyFilialId,
                        principalTable: "CompanyFilial",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_CompanyPhoto_Company_CompanyId",
                        column: x => x.CompanyId,
                        principalTable: "Company",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CompanyPhoto_CompanyFilialId",
                table: "CompanyPhoto",
                column: "CompanyFilialId");

            migrationBuilder.CreateIndex(
                name: "IX_CompanyPhoto_CompanyId",
                table: "CompanyPhoto",
                column: "CompanyId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CompanyPhoto");
        }
    }
}
