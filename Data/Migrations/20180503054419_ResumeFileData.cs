﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class ResumeFileData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ResumeFileFileName",
                table: "Resume");

            migrationBuilder.DropColumn(
                name: "ResumeFileUri",
                table: "Resume");

            migrationBuilder.AddColumn<Guid>(
                name: "ResumeFileId",
                table: "Resume",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Resume_ResumeFileId",
                table: "Resume",
                column: "ResumeFileId");

            migrationBuilder.AddForeignKey(
                name: "FK_Resume_FileData_ResumeFileId",
                table: "Resume",
                column: "ResumeFileId",
                principalTable: "FileData",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Resume_FileData_ResumeFileId",
                table: "Resume");

            migrationBuilder.DropIndex(
                name: "IX_Resume_ResumeFileId",
                table: "Resume");

            migrationBuilder.DropColumn(
                name: "ResumeFileId",
                table: "Resume");

            migrationBuilder.AddColumn<string>(
                name: "ResumeFileFileName",
                table: "Resume",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ResumeFileUri",
                table: "Resume",
                nullable: true);
        }
    }
}
