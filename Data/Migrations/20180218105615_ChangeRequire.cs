﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class ChangeRequire : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SendingResume_Vacancy_VacancyId",
                table: "SendingResume");

            migrationBuilder.AddColumn<string>(
                name: "ApplicantId",
                table: "SendingResume",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SendingResume_ApplicantId",
                table: "SendingResume",
                column: "ApplicantId");

            migrationBuilder.AddForeignKey(
                name: "FK_SendingResume_Applicant_ApplicantId",
                table: "SendingResume",
                column: "ApplicantId",
                principalTable: "Applicant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SendingResume_Vacancy_VacancyId",
                table: "SendingResume",
                column: "VacancyId",
                principalTable: "Vacancy",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SendingResume_Applicant_ApplicantId",
                table: "SendingResume");

            migrationBuilder.DropForeignKey(
                name: "FK_SendingResume_Vacancy_VacancyId",
                table: "SendingResume");

            migrationBuilder.DropIndex(
                name: "IX_SendingResume_ApplicantId",
                table: "SendingResume");

            migrationBuilder.DropColumn(
                name: "ApplicantId",
                table: "SendingResume");

            migrationBuilder.AddForeignKey(
                name: "FK_SendingResume_Vacancy_VacancyId",
                table: "SendingResume",
                column: "VacancyId",
                principalTable: "Vacancy",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
