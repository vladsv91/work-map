﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class ChangeVacancy : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CompanyId",
                table: "Vacancy",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Vacancy_CompanyId",
                table: "Vacancy",
                column: "CompanyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Vacancy_Company_CompanyId",
                table: "Vacancy",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Vacancy_Company_CompanyId",
                table: "Vacancy");

            migrationBuilder.DropIndex(
                name: "IX_Vacancy_CompanyId",
                table: "Vacancy");

            migrationBuilder.DropColumn(
                name: "CompanyId",
                table: "Vacancy");
        }
    }
}
