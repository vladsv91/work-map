﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations.JobsMapLogs
{
    public partial class MigratedLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MigratedLog",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false, defaultValueSql: "NEWID()"),
                    CreatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    UpdatedOnUtc = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Type = table.Column<int>(nullable: false),
                    Message = table.Column<string>(maxLength: 10000, nullable: true),
                    IpAddress = table.Column<string>(maxLength: 50, nullable: false),
                    RequestUrl = table.Column<string>(maxLength: 3000, nullable: false),
                    UserId = table.Column<string>(maxLength: 100, nullable: true),
                    UserAgent = table.Column<string>(maxLength: 500, nullable: true),
                    HttpMethod = table.Column<string>(maxLength: 50, nullable: false),
                    SerializedHeaders = table.Column<string>(maxLength: 10000, nullable: false),
                    Host = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MigratedLog", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MigratedLog");
        }
    }
}
