﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Data.Migrations
{
    public partial class ChangeRequireInSending : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SendingResume_Applicant_ApplicantId",
                table: "SendingResume");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicantId",
                table: "SendingResume",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SendingResume_Applicant_ApplicantId",
                table: "SendingResume",
                column: "ApplicantId",
                principalTable: "Applicant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SendingResume_Applicant_ApplicantId",
                table: "SendingResume");

            migrationBuilder.AlterColumn<string>(
                name: "ApplicantId",
                table: "SendingResume",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_SendingResume_Applicant_ApplicantId",
                table: "SendingResume",
                column: "ApplicantId",
                principalTable: "Applicant",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
