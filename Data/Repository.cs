﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Data.Entities.Common;
using Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Data
{

    public abstract class BaseRepositoryBaseContext<TEntity, TId, TContext> : IRepositoryBase<TEntity, TId>
    where TEntity : class, IBaseEntity<TId> where TContext : DbContext
    {
        public readonly TContext Db;

        protected BaseRepositoryBaseContext(TContext db)
        {
            Db = db;
        }


        public IQueryable<TEntity> GetQueriable()
        {
            return Db.Set<TEntity>().AsQueryable();
        }

        public virtual void Add(TEntity entity)
        {
            CheckEntityId(entity);
            entity.CreatedOnUtc = DateTime.UtcNow;
            entity.UpdatedOnUtc = DateTime.UtcNow;
            Db.Set<TEntity>().Add(entity);
        }

        public Task AddRange(IEnumerable<TEntity> entities)
        {
            return Db.AddRangeAsync(entities);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            Db.RemoveRange(entities);
        }

        public Task<TEntity> FindAsync(TId id)
        {
            return Db.FindAsync<TEntity>(id);
        }

        public virtual void CheckEntityId(TEntity entity) { }


        public void Update(TEntity entity)
        {
            var entry = UpdateBase(entity);
            entry.Property(_ => _.Id).IsModified = false;
        }

        public void UpdateButIgnoreId(TEntity entity)
        {
            UpdateBase(entity);
        }

        EntityEntry<TEntity> UpdateBase(TEntity entity)
        {
            entity.UpdatedOnUtc = DateTime.UtcNow;
            var entry = Db.Entry(entity);
            entry.State = EntityState.Modified;
            entry.Property(_ => _.CreatedOnUtc).IsModified = false;
            return entry;
        }

        public void UpdateSomeFields<T1>(T1 entity, params Expression<Func<T1, object>>[] propertiesToUpdate)
            where T1 : class, IBaseEntity
        {
            entity.UpdatedOnUtc = DateTime.UtcNow;
            var entry = Db.Entry(entity);
            entry.Property(_ => _.UpdatedOnUtc).IsModified = true;
            entry.Property(_ => _.CreatedOnUtc).IsModified = false;
            entry.Property(_ => _.Id).IsModified = false;
            foreach (var expression in propertiesToUpdate) entry.Property(expression).IsModified = true;
        }


        public void UpdateButIngoreSomeFields<T1>(T1 entity, params Expression<Func<T1, object>>[] propertiesToIgnore)
            where T1 : class, IBaseEntity
        {
            entity.UpdatedOnUtc = DateTime.UtcNow;
            var entry = Db.Entry(entity);
            entry.State = EntityState.Modified;
            entry.Property(_ => _.UpdatedOnUtc).IsModified = true;
            entry.Property(_ => _.CreatedOnUtc).IsModified = false;
            entry.Property(_ => _.Id).IsModified = false;
            foreach (var expression in propertiesToIgnore) entry.Property(expression).IsModified = false;
        }

        public void Delete(TEntity entity)
        {
            Db.Entry(entity).State = EntityState.Deleted;
            //Db.Set<T>().Remove(entity);
        }

        public void Attach(TEntity entity)
        {
            Db.Set<TEntity>().Attach(entity);
        }

        public void Detach(TEntity entity)
        {
            Db.Entry(entity).State = EntityState.Detached;
        }


        public void Deactivate<T1>(T1 entity) where T1 : class, IBaseStatusLookup
        {
            SetStatus(entity, true);
        }

        public void Activate<T1>(T1 entity) where T1 : class, IBaseStatusLookup
        {
            SetStatus(entity, false);
        }

        private void SetStatus<T1>(T1 entity, bool isDisabled) where T1 : class, IBaseStatusLookup
        {
            if (entity.IsDisabled == isDisabled) return;

            entity.IsDisabled = isDisabled;
            UpdateSomeFields(entity, _ => _.IsDisabled);
        }
    }

    //public class RepositoryGeneric<T, TV, TContext> : BaseRepositoryBaseContext<T, TV, TContext>
    //    where T : class, IBaseEntity<TV> where TContext : DbContext
    //{
    //    public RepositoryGeneric(TContext db) : base(db) { }

    //    public override void CheckEntityId(T entity)
    //    {

    //        // TODO: refactor

    //        if (typeof(TV) == typeof(string) && entity.Id == null)
    //        {
    //            entity.Id = (TV)Convert.ChangeType(GeneralUtils.CreateUniqId(IdGenerator.LongIdLength), typeof(TV));
    //        }
    //        if (typeof(TV) == typeof(Guid) && Equals(entity.Id, Guid.Empty))
    //        {
    //            entity.Id = (TV)Convert.ChangeType(Guid.NewGuid(), typeof(TV));
    //        }
    //    }
    //}

    public  class RepositoryBase<T, TV> : BaseRepositoryBaseContext<T, TV, DbContext> where T : class, IBaseEntity<TV>
    {
        public RepositoryBase(DbContext db) : base(db) { }
    }

    public class Repository<T> : RepositoryBase<T, string>, IRepository<T> where T : class, IBaseEntity<string>
    {
        public override void CheckEntityId(T entity)
        {
            if (entity.Id == null)
            {
                entity.Id = GeneralUtils.CreateUniqId(IdGenerator.LongIdLength);
            }
        }

        public Repository(DbContext db) : base(db) { }
    }

    public class RepositoryGuid<T> : RepositoryBase<T, Guid>, IRepositoryGuid<T> where T : class, IBaseEntity<Guid>
    {
        public RepositoryGuid(DbContext db) : base(db) { }

        public override void CheckEntityId(T entity)
        {
            if (entity.Id == Guid.Empty)
                entity.Id = Guid.NewGuid();
        }
    }
}