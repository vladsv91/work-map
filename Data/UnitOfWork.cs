﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Data {
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly DbContext Db;

        public UnitOfWork(DbContext db)
        {
            Db = db;
        }

        public Task SaveChangesAsync()
        {
            return Db.SaveChangesAsync();
        }

        public void SaveChanges()
        {
            Db.SaveChanges();
        }
    }
}