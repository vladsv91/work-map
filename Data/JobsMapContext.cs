﻿using System.IO;
using Data.Builders;
using Data.Entities.Account;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Data
{
    public abstract class BaseJobsAndMapContextFactory<T> : IDesignTimeDbContextFactory<T> where T : DbContext, new()
    {
        public virtual T CreateDbContext(string[] args)
        {
            return new T();
        }

        protected abstract string ConnectionName { get; }

        private static DbContextOptions<T> _options;

        public DbContextOptions<T> GetOptions()
        {
            if (_options != null) return _options;
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true)
#if DEBUG
                .AddJsonFile("appsettings.Development.json", true)
#endif
                .Build();

            var builder = new DbContextOptionsBuilder<T>();

            var connectionString = configuration.GetConnectionString(ConnectionName);

            return _options = builder.UseSqlServer(connectionString).Options;
        }
    }

    public class JobsAndMapContextFactory : BaseJobsAndMapContextFactory<JobsMapContext>
    {
        private static JobsAndMapContextFactory _instance;
        public static JobsAndMapContextFactory Instance => _instance ?? (_instance = new JobsAndMapContextFactory());
        protected override string ConnectionName => "DefaultConnection";
    }

    public class JobsAndMapLogsContextFactory : BaseJobsAndMapContextFactory<JobsMapLogsContext>
    {
        private static JobsAndMapLogsContextFactory _instance;
        public static JobsAndMapLogsContextFactory Instance => _instance ?? (_instance = new JobsAndMapLogsContextFactory());

        protected override string ConnectionName => "LogsDbConnection";
    }

    // add-migration  -context JobsMapContext
    // update-database -context JobsMapContext
    public class JobsMapContext : IdentityDbContext<ApplicationUser, IdentityRole<string>, string>
    {
        private static bool _hasMigrated;

        public JobsMapContext(DbContextOptions<JobsMapContext> dbContextOptions)
            : base(dbContextOptions)
        {
            BaseCtor();
        }

        public JobsMapContext() : this(JobsAndMapContextFactory.Instance.GetOptions()) { }

        void BaseCtor()
        {
            if (_hasMigrated) return;
            // ReSharper disable once VirtualMemberCallInConstructor
#if !DEBUG
            Database.Migrate();
#endif
            _hasMigrated = true;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder
                .BuildApplicationUser()
                .BuildAddress()
                .BuildCompany()
                .BuildCompanyFilial()
                .BuildVacancy()
                .BuildCompanyPhoto()
                .BuildApplicant()
                .BuildResume()
                .BuildApplicantPreferredAddress()
                .BuildWorkExperience()
                .BuildUserSession()
                .BuildUserSessionRefreshToken()
                .BuildContactUs()
                .BuildSendingResume()
                .BuildFileData()
                ;
        }
    }


    public class JobsMapLogsContext : DbContext
    {
        private static bool _hasMigrated;

        public JobsMapLogsContext(DbContextOptions<JobsMapLogsContext> dbContextOptions)
            : base(dbContextOptions)
        {
            BaseCtor();
        }

        public JobsMapLogsContext() : this(JobsAndMapLogsContextFactory.Instance.GetOptions()) { }

        void BaseCtor()
        {
            if (_hasMigrated) return;
            // ReSharper disable once VirtualMemberCallInConstructor
#if !DEBUG
            Database.Migrate();
#endif
            _hasMigrated = true;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder
                .BuildLog()
                .BuildMigratedLog()
                ;
        }
    }
}