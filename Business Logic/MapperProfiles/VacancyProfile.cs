using AutoMapper;
using BusinessLogic.Models.Vacancies;
using Data.Entities.CompanyEntities;

namespace BusinessLogic.MapperProfiles
{
    public class VacancyProfile : Profile
    {
        public VacancyProfile()
        {
            CreateMap<Vacancy, VacancyShortInfoBaseReadVM>()
                .ForMember(_ => _.Profession, a => a.MapFrom(_ => _.ProfessionId))
                .ForMember(_ => _.EmploymentType, a => a.MapFrom(_ => _.EmploymentTypeId))
                .ForMember(_ => _.Currency, a => a.MapFrom(_ => _.CurrencyId));

            CreateMap<Vacancy, VacancyShortInfoReadVM>().IncludeBase<Vacancy, VacancyShortInfoBaseReadVM>();

            CreateMap<Vacancy, VacancyDetailsReadVM>().IncludeBase<Vacancy, VacancyShortInfoBaseReadVM>();
        }
    }
}