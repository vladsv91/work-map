using System;
using System.Linq.Expressions;
using System.Resources;
using AutoMapper;
using BusinessLogic.Models.Common;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using Localization.EmploymentTypes;
using Localization.Industries;
using Localization.IndustriesTypes;
using Localization.Professions;
using Localization.ProfessionTypes;

namespace BusinessLogic.MapperProfiles
{
    public class LookupProfile : Profile
    {
        public LookupProfile()
        {
            CreateMap<string, IndustryReadVM>().ConvertUsing(GetMapperFuncString<IndustryReadVM>(Industries.ResourceManager));
            CreateMap<string, ProfessionReadVM>().ConvertUsing(GetMapperFuncString<ProfessionReadVM>(Professions.ResourceManager));

            CreateMap<string, IndustryTypeReadVM>().ConvertUsing(GetMapperFuncString<IndustryTypeReadVM>(IndustriesTypes.ResourceManager));
            CreateMap<string, ProfessionTypeReadVM>().ConvertUsing(GetMapperFuncString<ProfessionTypeReadVM>(ProfessionTypes.ResourceManager));

            CreateMap<Currency, LookupIntReadVM>().ConvertUsing(GetMapperFunc<Currency>());
            CreateMap<EmploymentType, LookupIntReadVM>()
                .ConvertUsing(GetMapperFunc<EmploymentType>(EmploymentTypes.ResourceManager));

            //CreateMap<Profession, LookupIntReadVM>()
            //    .ConvertUsing(GetMapperFunc<Profession>(Professions.ResourceManager));

            CreateMap<SendingResumeStatus, LookupIntReadVM>().ConvertUsing(GetMapperFunc<SendingResumeStatus>());


            CreateMap<CompanyFilial, LookupStringReadVM>();
        }

        //private void CreateLookupMap<T, TV>()
        //{
        //    CreateMap<T, TV>().ConvertUsing(GetMapperFunc<T>());

        //}

        private static Expression<Func<T, LookupIntReadVM>>  GetMapperFunc<T>()
        {
            return (s) => new LookupIntReadVM
            {
                Id = Convert.ToInt32(s),
                Name = s.ToString()
            };
        }

        private static Func<T, LookupIntReadVM, LookupIntReadVM> GetMapperFunc<T>(ResourceManager rm)
        {
            return (s, d) =>
            {
                var intValue = Convert.ToInt32(s);
                var localizedName = rm.GetString(intValue.ToString())
#if DEBUG
                                    ?? throw new Exception("Has no localized value.");
#else
                                    ?? rm.GetString(s.ToString()) ?? s.ToString();
                // todo, not throw ?? rm.GetString(s.ToString()) ?? s.ToString(),
#endif
                return new LookupIntReadVM
                {
                    Id = intValue,
                    Name = localizedName
                    //LocalizedName = localizedName,
                    //Code = s.ToString(),
                };
            };
        }

        private static Func<string, T, T> GetMapperFuncString<T>(ResourceManager rm) where T : LookupStringReadVM, new()
        {
            return (s, d) =>
            {
                var localizedName = rm.GetString(s)
//#if DEBUG
//                                    ?? throw new Exception("Has no localized value.");
//#else
                                    ?? rm.GetString(s.ToString()) ?? s.ToString();
                // todo, not throw ?? rm.GetString(s.ToString()) ?? s.ToString(),
//#endif
                return new T
                {
                    Id = s, 
                    Name = localizedName,
                    //LocalizedName = localizedName,
                    //Code = s.ToString(),
                };
            };
        }
    }
}