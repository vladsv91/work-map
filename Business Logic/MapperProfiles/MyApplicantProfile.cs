using AutoMapper;
using BusinessLogic.Models.MyApplicant;
using BusinessLogic.Utils;
using Data.Entities.ApplicantEntities;
using Data.Entities.Common;

namespace BusinessLogic.MapperProfiles
{
    public class MyApplicantProfile : Profile
    {
        public MyApplicantProfile()
        {
            CreateMap<Applicant, MyApplicantReadVM>().ForMember(_ => _.PhotoUri,
                o => o.MapFrom((cdb, c) => cdb.PhotoId == null ? null : BlUtils.CreateGetFileUrl(cdb.PhotoId.Value)));

            CreateMap<MyApplicantEditVM, Applicant>();


            CreateMap<Resume, MyResumeReadVM>()
                .ForMember(_ => _.Currency, o => o.MapFrom(_ => _.CurrencyId))
                .ForMember(_ => _.Profession, o => o.MapFrom(_ => _.ProfessionId))
                .ForMember(_ => _.ResumeFileUri,
                    o => o.MapFrom((cdb, c) => cdb.ResumeFileId == null ? null : BlUtils.CreateGetFileUrl(cdb.ResumeFileId.Value)));

            CreateMap<Resume, MyResumeDetailsReadVM>().IncludeBase<Resume, MyResumeReadVM>();
            CreateMap<MyResumeEditVM, Resume>();
            CreateMap<MyResumeAddVM, Resume>();


            CreateMap<WorkExperience, MyWorkExperienceReadVM>();
            CreateMap<MyWorkExperienceAddVM, WorkExperience>();
            CreateMap<MyWorkExperienceEditVM, WorkExperience>();

            CreateMap<MyApplicantSendResumeVM, SendingResume>();


            CreateMap<ApplicantPreferredAddress, MyPreferredAddressReadVM>();
            CreateMap<MyPreferredAddressAddVM, ApplicantPreferredAddress>().ForMember(_ => _.Address, o => o.Ignore());
            CreateMap<MyPreferredAddressEditVM, ApplicantPreferredAddress>().ForMember(_ => _.Address, o => o.Ignore());

            CreateMap<SendingResume, MyApplicantSentResumeReadVM>();

        }
    }
}