using AutoMapper;
using BusinessLogic.Models.Common;
using Data.Entities.Common;

namespace BusinessLogic.MapperProfiles
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<Address, AddressReadVM>();
            CreateMap<ContactUsCreateVM, ContactUs>();

            CreateMap<Log, MigratedLog>();
        }
    }
}