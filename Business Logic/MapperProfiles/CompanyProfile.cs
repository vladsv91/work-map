using AutoMapper;
using BusinessLogic.Models.Companies;
using BusinessLogic.Utils;
using Data.Entities.CompanyEntities;

namespace BusinessLogic.MapperProfiles
{
    public class CompanyProfile : Profile
    {
        public CompanyProfile()
        {
            CreateMap<Company, CompanyShortInfoReadVM>().ForMember(_ => _.Industry, a => a.MapFrom(_ => _.IndustryId))
                .ForMember(_ => _.MainPhotoUri,
                    o => o.MapFrom((cdb, c) => cdb.MainPhotoId == null ? null : BlUtils.CreateCompanyMainPhotoUrl(cdb.Id)));


            CreateMap<Company, CompanyDetailsReadVM>().IncludeBase<Company, CompanyShortInfoReadVM>();
            CreateMap<Company, CompanyShortInfoWithFilialsReadVM>().IncludeBase<Company, CompanyShortInfoReadVM>();
            CreateMap<CompanyDetailsReadDTO, CompanyFullInfoReadVM>()
                .ForMember(_ => _.Industry, a => a.MapFrom(_ => _.IndustryId))
                .ForMember(_ => _.MainPhotoUri,
                    o => o.MapFrom((cdb, c) => cdb.HasMainPhoto ? BlUtils.CreateCompanyMainPhotoUrl(cdb.Id) : null));

            CreateMap<CompanyFilialReadDTO, CompanyFilialReadVM>();

            CreateMap<Vacancy, CompanyVacancyShortInfoReadVM>()
                .ForMember(_ => _.Profession, o => o.MapFrom(_ => _.ProfessionId))
                .ForMember(_ => _.Currency, o => o.MapFrom(_ => _.CurrencyId))
                .ForMember(_ => _.EmploymentType, o => o.MapFrom(_ => _.EmploymentTypeId));

            CreateMap<Vacancy, CompanyVacancyDetailsReadVM>().IncludeBase<Vacancy, CompanyVacancyShortInfoReadVM>();

            CreateMap<CompanyPhoto, CompanyPhotoReadVM>()
                .ForMember(_ => _.PhotoUri, o => o.MapFrom((cdb, c) => BlUtils.CreateCompanyPhotoUrl(cdb.Id)));


            CreateMap<CompanyFilial, CompanyFilialWithAddressReadVM>();
        }
    }
}