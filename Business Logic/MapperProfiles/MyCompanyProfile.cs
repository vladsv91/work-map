using AutoMapper;
using BusinessLogic.Models.MyCompany;
using BusinessLogic.Utils;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;

namespace BusinessLogic.MapperProfiles
{
    public class MyCompanyProfile : Profile
    {
        public MyCompanyProfile()
        {
            CreateMap<Company, MyCompanyReadVM>().ForMember(_ => _.Industry, a => a.MapFrom(_ => _.IndustryId))
                .ForMember(_ => _.MainPhotoUri, o => o.MapFrom(
                    (cdb, c) => cdb.MainPhotoId == null ? null : BlUtils.CreateGetFileUrl(cdb.MainPhotoId.Value)));

            CreateMap<MyCompanyEditVM, Company>();

            CreateMap<CompanyPhoto, MyCompanyPhotoReadVM>()
                .ForMember(_ => _.PhotoUri, o => o.MapFrom((cdb, c) => BlUtils.CreateGetFileUrl(cdb.PhotoId)));

            CreateMap<CompanyFilial, MyCompanyFilialReadVM>();
            CreateMap<CompanyFilial, MyCompanyFilialDetailsReadVM>();

            CreateMap<MyCompanyFilialAddVM, CompanyFilial>().ForMember(_ => _.Address, o => o.Ignore());
            CreateMap<MyCompanyFilialEditVM, CompanyFilial>().ForMember(_ => _.Address, o => o.Ignore());

            CreateMap<Vacancy, MyCompanyVacancyReadVM>()
                .ForMember(_ => _.Profession, o => o.MapFrom(_ => _.ProfessionId))
                .ForMember(_ => _.Currency, o => o.MapFrom(_ => _.CurrencyId))
                .ForMember(_ => _.EmploymentType, o => o.MapFrom(_ => _.EmploymentTypeId));

            CreateMap<Vacancy, MyCompanyVacancyDetailsReadVM>()
                .IncludeBase<Vacancy, MyCompanyVacancyReadVM>();

            CreateMap<MyCompanyVacancyAddVM, Vacancy>();
            CreateMap<MyCompanyVacancyUpdateVM, Vacancy>();

            CreateMap<SendingResume, MyCompanySentResumeReadVM>();
        }
    }
}