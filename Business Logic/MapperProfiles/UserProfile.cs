using AutoMapper;
using BusinessLogic.Models.MyAccount;
using Data.Entities.Account;

namespace BusinessLogic.MapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<ApplicationUser, UserShortMyInfoReadVM>();
        }
    }
}