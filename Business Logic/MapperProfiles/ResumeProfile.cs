using AutoMapper;
using BusinessLogic.Models.Resumes;
using BusinessLogic.Utils;
using Data.Entities.ApplicantEntities;

namespace BusinessLogic.MapperProfiles
{
    public class ResumeProfile : Profile
    {
        public ResumeProfile()
        {
            CreateMap<Resume, ResumeBaseReadVM>()
                .ForMember(_ => _.Currency, a => a.MapFrom(_ => _.CurrencyId))
                .ForMember(_ => _.Profession, a => a.MapFrom(_ => _.ProfessionId))
                .ForMember(_ => _.PhotoUri,
                    o => o.MapFrom((cdb, c) => cdb.Applicant.PhotoId == null ? null : BlUtils.CreateResumePhotoUrl(cdb.ApplicantId)))
                .ForMember(_ => _.ResumeFileUri,
                    o => o.MapFrom((cdb, c) => cdb.ResumeFileId == null ? null : BlUtils.CreateResumeFileUrl(cdb.Id)));

            CreateMap<Resume, ResumeShortInfoReadVM>().IncludeBase<Resume, ResumeBaseReadVM>();
            CreateMap<Resume, ResumeDetailsReadVM>().IncludeBase<Resume, ResumeBaseReadVM>();

            CreateMap<Applicant, ApplicantBaseReadVM>()
                .ForMember(_ => _.PhotoUri,
                    o => o.MapFrom((cdb, c) => cdb.PhotoId == null ? null : BlUtils.CreateResumePhotoUrl(cdb.Id)));

            CreateMap<Applicant, ApplicantReadVM>().IncludeBase<Applicant, ApplicantBaseReadVM>();

            CreateMap<ApplicantPreferredAddress, ApplicantPreferredAddressReadVM>();
        }
    }
}