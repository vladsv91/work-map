using System;
using System.Collections.Generic;
using BusinessLogic.Models.Common;
using BusinessLogic.Utils;
using Data.Entities.Common;
using FluentValidation;

namespace BusinessLogic.Models.Companies
{
    public class CompaniesListFilterReadVM : BaseSearchableFilterVM
    {
        //public Industry? IndustryId { get; set; }
        public string IndustryId { get; set; }
        public int? MoreThanEmployeeCount { get; set; }
        public int? LessThanEmployeeCount { get; set; }

        public int? MoreThanFilialsCount { get; set; }
        public int? LessThanFilialsCount { get; set; }

        public int? MoreThanVacanciesCount { get; set; }
        public int? LessThanVacanciesCount { get; set; }

        public bool IncludeAddresses { get; set; }

        public AddressWithRangeVM Address { get; set; }
    }

    public class CompaniesListFilterReadVMValidator : BaseSearchableFilterVMValidator<CompaniesListFilterReadVM>
    {
        public CompaniesListFilterReadVMValidator()
        {
            //RuleFor(_ => _.IndustryId).IsInEnum();
            RuleFor(_ => _.IndustryId).IsInIndustriesEmun();

            RuleFor(_ => _.MoreThanEmployeeCount).InclusiveBetween(1, 9999999);
            MoreThanValue(RuleFor(_ => _.LessThanEmployeeCount), _ => _.MoreThanEmployeeCount,
                nameof(CompaniesListFilterReadVM.MoreThanEmployeeCount));

            RuleFor(_ => _.MoreThanVacanciesCount).InclusiveBetween(0, 9999999);
            MoreThanValue(RuleFor(_ => _.LessThanVacanciesCount), _ => _.MoreThanVacanciesCount,
                nameof(CompaniesListFilterReadVM.MoreThanVacanciesCount));

            RuleFor(_ => _.MoreThanFilialsCount).InclusiveBetween(1, 9999999);
            MoreThanValue(RuleFor(_ => _.LessThanFilialsCount), _ => _.MoreThanFilialsCount,
                nameof(CompaniesListFilterReadVM.MoreThanFilialsCount));

            When(_ => _.Address != null,
                () => { RuleFor(_ => _.Address).SetValidator(new AddressWithRangeVMValidator()); });
        }

        private void MoreThanValue(IRuleBuilder<CompaniesListFilterReadVM, int?> rule,
            Func<CompaniesListFilterReadVM, int?> related, string nameOfRelated)
        {
            rule.InclusiveBetween(1, 9999999).Must((model, val) =>
            {
                var relatedVal = related(model);
                if (relatedVal != null && val != null)
                    return val.Value > relatedVal.Value + 1;
                return true;
            }).WithMessage($"Must be greather than {nameOfRelated} + 1");
        }
    }

    public class CompanyShortInfoReadVM
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int EmployeeCount { get; set; }

        public string Description { get; set; }

        public IndustryReadVM Industry { get; set; }
        public string MainPhotoUri { get; set; }
    }

    public class CompanyShortInfoWithFilialsReadVM : CompanyShortInfoReadVM
    {
        public List<CompanyFilialWithAddressReadVM> Filials { get; set; }
    }

    public class CompanyDetailsReadVM : CompanyShortInfoReadVM
    {
        public string WebsiteUrl { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
    }

    public class CompanyFullInfoReadVM : CompanyDetailsReadVM
    {
        public List<CompanyFilialWithAddressReadVM> Filials { get; set; }
        public int FilialsCount { get; set; }
        public int VacanciesCount { get; set; }
        public int PhotosCount { get; set; }
    }

    public class CompanyDetailsReadDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int EmployeeCount { get; set; }

        public string Description { get; set; }

        public int FilialsCount { get; set; }
        public int VacanciesCount { get; set; }
        public int PhotosCount { get; set; }


        //public Industry IndustryId { get; set; }
        public string IndustryId { get; set; }
        public string MainPhotoUri { get; set; }
        public string WebsiteUrl { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public bool HasMainPhoto { get; set; }
    }


    public class CompanyFilialReadBase
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsMain { get; set; }

        public int EmployeeCount { get; set; }
        public int VacanciesCount { get; set; }
    }

    public class CompanyFilialReadDTO : CompanyFilialReadBase
    {
        public Address Address { get; set; }
    }

    public class CompanyFilialReadVM : CompanyFilialReadBase
    {
        public AddressReadVM Address { get; set; }
    }

    public class CompanyFilialWithAddressReadVM
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public AddressReadVM Address { get; set; }
    }


    public class CompanyVacancyShortInfoReadVM
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }

        public decimal? SalaryPerMonth { get; set; }
        public LookupIntReadVM Currency { get; set; }

        public string Description { get; set; }


        public ProfessionReadVM Profession { get; set; }

        public CompanyFilialWithAddressReadVM CompanyFilial { get; set; }
        public LookupIntReadVM EmploymentType { get; set; }
    }


    public class CompanyVacancyDetailsReadVM : CompanyVacancyShortInfoReadVM
    {
        public string Requirements { get; set; }
        public string Duties { get; set; }
        public string WorkingConditions { get; set; }
    }


    public class CompanyPhotoReadVM
    {
        public string Id { get; set; }

        public LookupStringReadVM CompanyFilial { get; set; }

        //public LookupStringReadVM Company { get; set; }
        public string PhotoUri { get; set; }
    }
}