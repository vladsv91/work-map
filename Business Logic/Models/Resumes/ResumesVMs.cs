using System;
using System.Collections.Generic;
using BusinessLogic.Models.Common;
using BusinessLogic.Utils;
using FluentValidation;
using Infrastructure;

namespace BusinessLogic.Models.Resumes
{
    public class ResumesListFilterReadVM : BaseSearchableFilterVM
    {
        public string ProfessionId { get; set; }


        public decimal? MoreThanSalaryPerMonth { get; set; }
        public decimal? LessThanSalaryPerMonth { get; set; }

        public decimal? MoreThanExperienceYears { get; set; }
        public decimal? LessThanExperienceYears { get; set; }


        public bool IncludeAddresses { get; set; }

        public AddressWithRangeVM Address { get; set; }
    }

    public class ResumesListFilterReadVMValidator : BaseSearchableFilterVMValidator<ResumesListFilterReadVM>
    {
        public ResumesListFilterReadVMValidator()
        {
            RuleFor(_ => _.ProfessionId).IsInProfessionsEmun();


            RuleFor(_ => _.MoreThanSalaryPerMonth).ExclusiveBetween(0, AppConstants.PropertiesRules.MaxSalaryPerMonth);
            MoreThanValue(
                RuleFor(_ => _.LessThanSalaryPerMonth)
                    .ExclusiveBetween(0, AppConstants.PropertiesRules.MaxSalaryPerMonth), _ => _.MoreThanSalaryPerMonth,
                nameof(ResumesListFilterReadVM.MoreThanSalaryPerMonth));

            RuleFor(_ => _.MoreThanExperienceYears).ExclusiveBetween(0, 99999);
            MoreThanValue(RuleFor(_ => _.LessThanExperienceYears).ExclusiveBetween(0, 99999),
                _ => _.MoreThanExperienceYears,
                nameof(ResumesListFilterReadVM.MoreThanExperienceYears));


            When(_ => _.Address != null,
                () => { RuleFor(_ => _.Address).SetValidator(new AddressWithRangeVMValidator()); });
        }

        private static void MoreThanValue<T>(IRuleBuilder<T, decimal?> rule,
            Func<T, decimal?> related, string nameOfRelated)
        {
            rule.Must((model, val) =>
            {
                var relatedVal = related(model);
                if (relatedVal != null && val != null)
                    return val.Value > relatedVal.Value;
                return true;
            }).WithMessage($"Must be greather than {nameOfRelated}");
        }
    }


    public abstract class ResumeBaseReadVM
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public LookupIntReadVM Currency { get; set; }
        public ProfessionReadVM Profession { get; set; }
        public string PhotoUri { get; set; }
        public decimal ExperienceYears { get; set; }

        public string Skills { get; set; }

        public string Description { get; set; }
        public string ResumeFileUri { get; set; }

        public decimal? SalaryPerMonth { get; set; }

        public ApplicantReadVM Applicant { get; set; }
    }


    public class ResumeShortInfoReadVM : ResumeBaseReadVM { }

    public class ResumeDetailsReadVM : ResumeBaseReadVM
    {
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
    }


    public class ApplicantPreferredAddressReadVM
    {
        public decimal RadiusInKilometers { get; set; }
        public AddressReadVM Address { get; set; }
    }


    public abstract class ApplicantBaseReadVM
    {
        //public string Id { get; set; }
        public string FullName { get; set; }

        public string ContactPhone { get; set; }

        public string ContactEmail { get; set; }

        public string Description { get; set; }

        public string WebsiteUrl { get; set; }


        public DateTime? DateOfBirth { get; set; }
        public string PhotoUri { get; set; }

        public List<ApplicantPreferredAddressReadVM> PreferredAddresses { get; set; }
    }

    public class ApplicantReadVM : ApplicantBaseReadVM { }
}