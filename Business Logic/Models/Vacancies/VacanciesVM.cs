using System;
using BusinessLogic.Models.Common;
using BusinessLogic.Models.Companies;
using BusinessLogic.Utils;
using Data.Entities.Common;
using FluentValidation;
using Infrastructure;

namespace BusinessLogic.Models.Vacancies
{
    public class VacanciesListFilterReadVM : BaseSearchableFilterVM
    {
        public string ProfessionId { get; set; }
        public Currency? CurrencyId { get; set; }
        public EmploymentType? EmploymentTypeId { get; set; }
        public decimal? MoreThanSalaryPerMonth { get; set; }
        public decimal? LessThanSalaryPerMonth { get; set; }

        public AddressWithRangeVM Address { get; set; }
    }

    public class VacanciesListFilterReadVMValidator : BaseSearchableFilterVMValidator<VacanciesListFilterReadVM>
    {
        public VacanciesListFilterReadVMValidator()
        {
            RuleFor(_ => _.ProfessionId).IsInProfessionsEmun();
            RuleFor(_ => _.CurrencyId).IsInEnum();
            RuleFor(_ => _.EmploymentTypeId).IsInEnum();

            RuleFor(_ => _.MoreThanSalaryPerMonth).ExclusiveBetween(0, AppConstants.PropertiesRules.MaxSalaryPerMonth);
            MoreThanValue(RuleFor(_ => _.LessThanSalaryPerMonth), _ => _.MoreThanSalaryPerMonth,
                nameof(VacanciesListFilterReadVM.MoreThanSalaryPerMonth));

            When(_ => _.Address != null,
                () => { RuleFor(_ => _.Address).SetValidator(new AddressWithRangeVMValidator()); });
        }

        private void MoreThanValue(IRuleBuilder<VacanciesListFilterReadVM, decimal?> rule,
            Func<VacanciesListFilterReadVM, decimal?> related, string nameOfRelated)
        {
            rule.InclusiveBetween(1, AppConstants.PropertiesRules.MaxSalaryPerMonth).Must((model, val) =>
            {
                var relatedVal = related(model);
                if (relatedVal != null && val != null)
                    return val.Value > relatedVal.Value;
                return true;
            }).WithMessage($"Must be greather than {nameOfRelated}");
        }
    }


    public abstract class VacancyShortInfoBaseReadVM
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }

        public decimal? SalaryPerMonth { get; set; }

        public string Description { get; set; }


        public ProfessionReadVM Profession { get; set; }
        public LookupIntReadVM Currency { get; set; }
        public LookupIntReadVM EmploymentType { get; set; }
        public CompanyFilialWithAddressReadVM CompanyFilial { get; set; }
    }

    public class VacancyShortInfoReadVM : VacancyShortInfoBaseReadVM
    {
        public CompanyShortInfoReadVM Company { get; set; }
    }


    public class VacancyDetailsReadVM : VacancyShortInfoBaseReadVM
    {
        public CompanyDetailsReadVM Company { get; set; }
        public string Requirements { get; set; }
        public string Duties { get; set; }
        public string WorkingConditions { get; set; }
    }
}