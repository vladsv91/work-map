using BusinessLogic.Utils;
using Data.Entities.ApplicantEntities;
using Data.Entities.CompanyEntities;
using FluentValidation;
using Infrastructure;

namespace BusinessLogic.Models.Common
{
    public class ContactUsCreateVM
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Message { get; set; }
    }

    public class ContactUsCreateVMValidator : AbstractValidator<ContactUsCreateVM>
    {
        public ContactUsCreateVMValidator()
        {
            RuleFor(_ => _.Email).NotEmpty().EmailAddressValidation();
            RuleFor(_ => _.PhoneNumber).PhoneNumberValidations();
            RuleFor(_ => _.FullName).MaximumLength(AppConstants.PropertiesRules.ContactUs.FullNameMaxLen);
            RuleFor(_ => _.Message).MaximumLength(AppConstants.PropertiesRules.MessageMaxLen);
        }
    }

    public class SendingResumeEmail
    {
        public string Message { get; set; }

        public Resume Resume { get; set; }
        public Vacancy Vacancy { get; set; }
    }
}