namespace BusinessLogic.Models.Common
{
    public class LookupIntReadVM
    {
        public int Id { get; set; }

        public string Name { get; set; }
        //public string LocalizedName { get; set; }
        //public string Code { get; set; }
    }

    //public class CurrencyReadVM
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //    public string Symbol { get; set; }
    //}

    public class LookupStringReadVM
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class IndustryReadVM : LookupStringReadVM { }
    public class ProfessionReadVM : LookupStringReadVM { }
    public class ProfessionTypeReadVM : LookupStringReadVM { }
    public class IndustryTypeReadVM : LookupStringReadVM { }

}