using FluentValidation;

namespace BusinessLogic.Models.Common
{
    public class AddressReadVM : AddressEditVM
    {
        public string FormattedAddress { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string StreetNumber { get; set; }
    }

    public abstract class AddressBaseVM
    {
        public decimal Latitude { get; set; } // Latitude: -85 to +85 (actually -85.05115 for some reason)
        public decimal Longitude { get; set; } // Longitude: -180 to +180}
    }

    public class AddressLtLnVM : AddressBaseVM { }


    public class AddressEditVM : AddressBaseVM { }

    public class AddressWithRangeVM : AddressBaseVM
    {
        public decimal RadiusInKilometers { get; set; }
    }

    public abstract class AddressBaseVMValidator<T> : AbstractValidator<T> where T : AddressBaseVM
    {
        protected AddressBaseVMValidator()
        {
            RuleFor(_ => _.Latitude).ExclusiveBetween(-86, 86);
            RuleFor(_ => _.Longitude).InclusiveBetween(-180, 180);
            When(_ => _.Latitude == 0, () => { RuleFor(_ => _.Longitude).NotEmpty(); });
            When(_ => _.Longitude == 0, () => { RuleFor(_ => _.Latitude).NotEmpty(); });
        }
    }

    public class AddressEditVMValidator : AddressBaseVMValidator<AddressEditVM> { }
    public class AddressLtLnVMValidator : AddressBaseVMValidator<AddressLtLnVM> { }

    public class AddressWithRangeVMValidator : AddressBaseVMValidator<AddressWithRangeVM>
    {
        public AddressWithRangeVMValidator()
        {
            RuleFor(_ => _.RadiusInKilometers).GreaterThan(0).LessThanOrEqualTo(999);
        }
    }
}