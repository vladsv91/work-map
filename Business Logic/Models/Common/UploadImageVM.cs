using FluentValidation;
using Microsoft.AspNetCore.Http;

namespace BusinessLogic.Models.Common
{
    public abstract class UploadImageVM
    {
        public IFormFile Image { get; set; }
    }

    public abstract class UploadImageVMValidator<T> : AbstractValidator<T> where T : UploadImageVM
    {
        protected UploadImageVMValidator()
        {
            RuleFor(_ => _.Image.ContentType).Must(_ => _.StartsWith("image/")).WithMessage("Must be image");
            RuleFor(_ => _.Image.Length).InclusiveBetween(1024, 1024000)
                .WithMessage("File size must be between 1kb and 1mb");
        }
    }
}