using BusinessLogic.Utils;
using FluentValidation;

namespace BusinessLogic.Models.Common
{
    public class PaginationFilterVM
    {
        public int Page { get; set; }
        public int ItemsPerPage { get; set; } = 10;

        public bool RecalculateTotalCount { get; set; } = true;
    }

    public class SortingFilterVM
    {
        public string Field { get; set; }
        public bool IsDescending { get; set; }
    }

    public class BaseFilterVM { }

    public class BasePaginatedFilterVM : BaseFilterVM
    {
        public PaginationFilterVM Pagination { get; set; } // = new PaginationFilterVM();
    }

    public class BaseSearchableFilterVM : BasePaginatedFilterVM
    {
        [Trim]
        public string SearchString { get; set; }
    }

    public class BaseFilterVMValidator<T> : AbstractValidator<T> where T : BaseFilterVM { }

    public class PaginationFilterVMValidator : AbstractValidator<PaginationFilterVM>
    {
        public PaginationFilterVMValidator()
        {
            RuleFor(_ => _.Page).NotEmpty().GreaterThanOrEqualTo(1);
            RuleFor(_ => _.ItemsPerPage).NotEmpty().GreaterThanOrEqualTo(MinPageSize).LessThanOrEqualTo(MaxPageSize);
        }

        public static int MaxPageSize = 50;
        public static int MinPageSize = 1;
        public static int DefaultPageSize = 10;
    }

    public class BasePaginatedFilterVMValidator<T> : BaseFilterVMValidator<T> where T : BasePaginatedFilterVM
    {
        public BasePaginatedFilterVMValidator()
        {
            RuleFor(_ => _.Pagination).NotNull().SetValidator(new PaginationFilterVMValidator());
        }
    }

    public class BaseSearchableFilterVMValidator<T> : BasePaginatedFilterVMValidator<T>
        where T : BaseSearchableFilterVM
    {
        public BaseSearchableFilterVMValidator()
        {
            RuleFor(_ => _.SearchString).MaximumLength(100);
        }
    }
}