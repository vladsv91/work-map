using System.Collections.Generic;

namespace BusinessLogic.Models.Common
{
    public interface IPaginatedDataResultVM<T>
    {
        int? TotalItemsCount { get; set; }
        IList<T> Items { get; set; }
    }

    public class PaginatedDataResultVM
    {
        public int? TotalItemsCount { get; set; }
    }

    public class PaginatedDataResultVM<T> : PaginatedDataResultVM, IPaginatedDataResultVM<T>
    {
        public IList<T> Items { get; set; }
    }
}