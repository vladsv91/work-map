using System;
using System.Collections.Generic;
using System.IO;
using BusinessLogic.Models.Common;
using BusinessLogic.Utils;
using Data.Entities.Common;
using FluentValidation;
using Infrastructure;
using Microsoft.AspNetCore.Http;

namespace BusinessLogic.Models.MyApplicant
{
    public abstract class MyApplicantBaseVM
    {
        [Trim]
        public string WebsiteUrl { get; set; }

        [Trim(Type = TrimType.AllowMultilinesAndTabs)]
        public string Description { get; set; }


        [Trim]
        public string ContactPhone { get; set; }

        [Trim]
        public string ContactEmail { get; set; }


        [Trim]
        public string FullName { get; set; }

        public DateTime? DateOfBirth { get; set; }
    }


    public class MyApplicantEditVM : MyApplicantBaseVM { }

    public class MyApplicantReadVM : MyApplicantBaseVM
    {
        public string Id { get; set; }

        public string PhotoUri { get; set; }
        //public List<ApplicantPreferredAddress> PreferredAddresses { get; set; }
    }

    public class MyApplicantEditVMValidator : AbstractValidator<MyApplicantEditVM>
    {
        public MyApplicantEditVMValidator()
        {
            RuleFor(_ => _.FullName).NotEmpty().LengthBetween(3, 100);

            When(_ => _.WebsiteUrl.NotNullAndEmpty(),
                () => { RuleFor(_ => _.WebsiteUrl).LengthBetween(10, 200).IsUrl("Website Url"); });

            When(_ => _.Description.NotNullAndEmpty(), () => { RuleFor(_ => _.Description).MaximumLength(1000); });

            When(_ => !_.ContactEmail.IsNullOrWhiteSpace(), () =>
            {
                RuleFor(_ => _.ContactEmail).LengthBetween(AppConstants.PropertiesRules.ContactEmailMinLen,
                    AppConstants.PropertiesRules.ContactEmailMaxLen).EmailAddress();
            });

            When(_ => !_.ContactPhone.IsNullOrWhiteSpace(),
                () => { RuleFor(_ => _.ContactPhone).PhoneNumberValidations(); });


            When(_ => _.DateOfBirth != null, () =>
            {
                RuleFor(_ => _.DateOfBirth)
                    .GreaterThan(DateTime.UtcNow.AddYears(AppConstants.PropertiesRules.DateMinYears))
                    .LessThan(DateTime.UtcNow.AddYears(AppConstants.PropertiesRules.DateMaxYears));
                // todo, maybe message about age
            });
        }
    }


    public class MyApplicantyUploadPhotoVM : UploadImageVM { }

    public class MyApplicantyUploadPhotoVMValidator : UploadImageVMValidator<MyApplicantyUploadPhotoVM> { }


    public abstract class MyResumeBaseVM
    {
        [Trim]
        public string Name { get; set; }


        public decimal ExperienceYears { get; set; }

        [Trim(Type = TrimType.AllowMultilinesAndTabs)]
        public string Skills { get; set; }

        [Trim(Type = TrimType.AllowMultilinesAndTabs)]
        public string Description { get; set; }

        public decimal? SalaryPerMonth { get; set; }
    }


    public class MyResumeReadVM : MyResumeBaseVM
    {
        public string Id { get; set; }
        public LookupIntReadVM Currency { get; set; }
        public bool IsDisabled { get; set; }

        public List<MyWorkExperienceReadVM> WorkExperiences { get; set; }

        public ProfessionReadVM Profession { get; set; }

        public string ResumeFileUri { get; set; }
    }

    public class MyResumeDetailsReadVM : MyResumeReadVM { }

    public abstract class MyResumeChangeBaseVM : MyResumeBaseVM
    {
        public Currency? CurrencyId { get; set; }
        public string ProfessionId { get; set; }

        public List<MyWorkExperienceAddVM> WorkExperiencesToAdd { get; set; }
    }

    public class MyResumeEditVM : MyResumeChangeBaseVM
    {
        [Trim]
        public string Id { get; set; }

        public List<MyWorkExperienceEditVM> WorkExperiencesToUpdate { get; set; }
        public List<string> WorkExperiencesIdsToDelete { get; set; }
    }

    public class MyResumeAddVM : MyResumeChangeBaseVM { }


    public abstract class MyResumeChangeBaseVMValidator<T> : AbstractValidator<T> where T : MyResumeChangeBaseVM
    {
        protected MyResumeChangeBaseVMValidator()
        {
            RuleFor(_ => _.Name).NotEmpty().LengthBetween(3, 100);

            RuleFor(_ => _.ExperienceYears).NotNull().InclusiveBetween(0, 100);

            RuleFor(_ => _.Skills).NotEmpty().MaximumLength(2000);
            RuleFor(_ => _.Description).NotEmpty().MaximumLength(2000);
            RuleFor(_ => _.ProfessionId).NotEmpty().IsInProfessionsEmun();

            When(_ => _.SalaryPerMonth != null,
                () =>
                {
                    RuleFor(_ => _.SalaryPerMonth).InclusiveBetween(0, AppConstants.PropertiesRules.MaxSalaryPerMonth);
                    RuleFor(_ => _.CurrencyId).NotEmpty().IsInEnum();
                });


            RuleFor(_ => _.WorkExperiencesToAdd).SetCollectionValidator(arg => new MyWorkExperienceAddVMValidator());
        }
    }

    public class MyResumeEditVMValidator : MyResumeChangeBaseVMValidator<MyResumeEditVM>
    {
        public MyResumeEditVMValidator()
        {
            RuleFor(_ => _.Id).NotEmpty();

            RuleFor(_ => _.WorkExperiencesToUpdate)
                .SetCollectionValidator(arg => new MyWorkExperienceEditVMValidator());

            RuleForEach(_ => _.WorkExperiencesIdsToDelete).NotEmpty();
            //RuleFor(_ => _.WorkExperiencesIdsToDelete)
            //    .SetCollectionValidator(arg => new WorkExperienceIdToDeleteValidator());
        }
    }

    //public class WorkExperienceIdToDeleteValidator : AbstractValidator<string>
    //{
    //    public WorkExperienceIdToDeleteValidator()
    //    {
    //        RuleFor(_ => _).NotEmpty();
    //    }
    //}


    public class MyResumeAddVMValidator : MyResumeChangeBaseVMValidator<MyResumeAddVM> { }


    public abstract class MyPreferredAddressBaseVM
    {
        public decimal RadiusInKilometers { get; set; }
    }

    public class MyPreferredAddressReadVM : MyPreferredAddressBaseVM
    {
        public string Id { get; set; }
        public AddressReadVM Address { get; set; }
    }

    public abstract class MyPreferredAddressChangeBaseVM : MyPreferredAddressBaseVM { }

    public class MyPreferredAddressEditVM : MyPreferredAddressChangeBaseVM
    {
        [Trim]
        public string Id { get; set; }
    }

    public class MyPreferredAddressAddVM : MyPreferredAddressChangeBaseVM
    {
        public AddressEditVM Address { get; set; }
    }

    public class MyPreferredAddressAddResultVM
    {
        public string Id { get; set; }
        public string FormattedAddress { get; set; }
    }


    public abstract class MyPreferredAddressBaseVMValidator<T> : AbstractValidator<T>
        where T : MyPreferredAddressChangeBaseVM
    {
        protected MyPreferredAddressBaseVMValidator()
        {
            // todo, to consts
            RuleFor(_ => _.RadiusInKilometers).NotEmpty().GreaterThan(0).LessThanOrEqualTo(999);
        }
    }

    public class MyPreferredAddressEditVMValidator : MyPreferredAddressBaseVMValidator<MyPreferredAddressEditVM>
    {
        public MyPreferredAddressEditVMValidator()
        {
            RuleFor(_ => _.Id).NotEmpty();
        }
    }

    public class MyPreferredAddressAddVMValidator : MyPreferredAddressBaseVMValidator<MyPreferredAddressAddVM>
    {
        public MyPreferredAddressAddVMValidator()
        {
            RuleFor(_ => _.Address).NotNull().SetValidator(new AddressEditVMValidator());
        }
    }


    public abstract class MyWorkExperienceBaseVM
    {
        [Trim]
        public string Id { get; set; }

        [Trim]
        public string Position { get; set; }

        [Trim]
        public string CompanyName { get; set; }

        public DateTime DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }

    public class MyWorkExperienceReadVM : MyWorkExperienceBaseVM
    {
        public LookupStringReadVM Resume { get; set; }
    }

    // todo, implement work experience
    public class MyWorkExperienceEditVM : MyWorkExperienceBaseVM { }

    public class MyWorkExperienceAddVM : MyWorkExperienceEditVM { }


    public abstract class MyWorkExperienceBaseVMValidator<T> : AbstractValidator<T> where T : MyWorkExperienceEditVM
    {
        protected MyWorkExperienceBaseVMValidator()
        {
            RuleFor(_ => _.Id).NotEmpty();
            RuleFor(_ => _.Position).NotEmpty().LengthBetween(4, 100);
            RuleFor(_ => _.CompanyName).NotEmpty().LengthBetween(2, 100);

            RuleFor(_ => _.DateFrom).NotEmpty()
                .GreaterThan(DateTime.UtcNow.AddYears(AppConstants.PropertiesRules.DateMinYears))
                .LessThan(DateTime.UtcNow.AddYears(AppConstants.PropertiesRules.DateMaxYears));

            When(_ => _.DateTo != null, () =>
            {
                RuleFor(_ => _.DateTo).LessThan(DateTime.UtcNow).Must((model, dateTo) => dateTo >= model.DateFrom)
                    .WithMessage("Date To must be greather than or equals to Date From");
            });
        }
    }

    public class MyWorkExperienceEditVMValidator : MyWorkExperienceBaseVMValidator<MyWorkExperienceEditVM> { }

    public class MyWorkExperienceAddVMValidator : MyWorkExperienceBaseVMValidator<MyWorkExperienceAddVM> { }


    public class MyResumeUploadFileVM
    {
        public string ResumeId { get; set; }
        public IFormFile File { get; set; }
    }

    public class MyResumeUploadFileVMValidator : AbstractValidator<MyResumeUploadFileVM>
    {
        private static readonly HashSet<string> AcceptExtentions = new HashSet<string>(new[] {"pdf", "doc", "docx"});
        public MyResumeUploadFileVMValidator()
        {
            RuleFor(_ => _.File.FileName).Must(fileName =>
            {
                var ext = Path.GetExtension(fileName).Substring(1);

                return AcceptExtentions.Contains(ext);
            }).WithMessage($"Accepted file formats: {string.Join(", ", AcceptExtentions)}");


            RuleFor(_ => _.File.Length).InclusiveBetween(10, 2048000)
                .WithMessage("File size must be between 10byte and 2mb");

            RuleFor(_ => _.ResumeId).NotEmpty();
        }
    }

}