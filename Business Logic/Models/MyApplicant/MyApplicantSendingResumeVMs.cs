using System;
using BusinessLogic.Models.Common;
using BusinessLogic.Models.Vacancies;
using FluentValidation;
using Infrastructure;

namespace BusinessLogic.Models.MyApplicant
{
    public class MyApplicantSentResumeReadVM
    {
        public string Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }

        public MyResumeReadVM Resume { get; set; }

        public VacancyShortInfoReadVM Vacancy { get; set; }

        public string Message { get; set; }
        public LookupIntReadVM Status { get; set; }
    }


    public class MyApplicantSendResumeVM
    {
        public string ResumeId { get; set; }

        public string VacancyId { get; set; }

        public string Message { get; set; }
    }

    public class SendResumeVMValidator : AbstractValidator<MyApplicantSendResumeVM>
    {
        public SendResumeVMValidator()
        {
            RuleFor(_ => _.Message).MaximumLength(AppConstants.PropertiesRules.MessageMaxLen);
            RuleFor(_ => _.ResumeId).NotEmpty();
            RuleFor(_ => _.VacancyId).NotEmpty();
        }
    }
}