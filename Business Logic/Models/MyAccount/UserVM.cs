using System;
using Data.Entities.Common;

namespace BusinessLogic.Models.MyAccount
{
    public class UserShortMyInfoReadVM
    {
        public UserType Type { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime UpdatedOnUtc { get; set; }


        public string UserName { get; set; }

        public string Email { get; set; }

        public bool EmailConfirmed { get; set; }

        public string PhoneNumber { get; set; }
        public string Language { get; set; }

        public bool PhoneNumberConfirmed { get; set; }

        public virtual bool TwoFactorEnabled { get; set; }
    }
}