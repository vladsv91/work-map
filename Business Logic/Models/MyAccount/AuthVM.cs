using System;
using System.Text.RegularExpressions;
using BusinessLogic.Models.Common;
using BusinessLogic.Utils;
using FluentValidation;

namespace BusinessLogic.Models.MyAccount
{
    public class JwtResponseVM
    {
        public JwtResponseVM(string accessToken, Guid refreshToken, DateTime expiresOn)
        {
            AccessToken = accessToken;
            RefreshToken = refreshToken;
            ExpiresOn = expiresOn;
        }

        public string AccessToken { get; set; }
        public Guid RefreshToken { get; set; }
        public DateTime ExpiresOn { get; set; }
    }


    public class CompanyRegisterVM : RegisterVM
    {
        [Trim]
        public string CompanyId { get; set; }

        [Trim]
        public string CompanyName { get; set; }

        public AddressEditVM MainOfficeAddress { get; set; }

        public int EmployeeCount { get; set; }
        public string IndustryId { get; set; }
    }

    public class ApplicantRegisterVM : RegisterVM
    {
        [Trim]
        public string FullName { get; set; }
    }

    public class LoginVM : AuthBaseVM { }

    public abstract class RegisterVM : AuthBaseVM
    {
        public string Language { get; set; }
    }

    public abstract class AuthBaseVM
    {
        [Trim]
        public string Email { get; set; }

        public string Password { get; set; }
    }

    public class ResendConfirmationEmailVM : AuthBaseVM { }


    public class ApplicantRegisterVMValidator : RegisterVMValidator<ApplicantRegisterVM>
    {
        public ApplicantRegisterVMValidator()
        {
            RuleFor(_ => _.FullName).NotEmpty().LengthBetween(3, 100);
        }
    }

    public class CompanyRegisterVMValidator : RegisterVMValidator<CompanyRegisterVM>
    {
        public CompanyRegisterVMValidator()
        {
            When(_ => !string.IsNullOrEmpty(_.CompanyId), () =>
            {
                RuleFor(_ => _.CompanyId).MinimumLength(2).MaximumLength(30)
                    .Matches(new Regex("^[a-z0-9][a-z0-9-]+[a-z0-9]$"));
            });
            const string symbols = "a-zA-Z0-9�-��-��-��-�����������!,. -";
            RuleFor(_ => _.CompanyName).MinimumLength(2).MaximumLength(100)
                .Matches(new Regex($"^[{symbols}]+$"))
                .WithMessage($"Company name must have only these symbols: '{symbols}'");
            RuleFor(_ => _.MainOfficeAddress).NotNull();
            RuleFor(_ => _.EmployeeCount).NotEmpty().InclusiveBetween(1, 999999);
            RuleFor(_ => _.IndustryId).NotEmpty().IsInIndustriesEmun();
        }
    }

    public class RegisterVMValidator<T> : AuthBaseVMValidator<T> where T : RegisterVM { }

    public class LoginVMValidator : AuthBaseVMValidator<LoginVM> { }

    public class AuthBaseVMValidator<T> : AbstractValidator<T> where T : AuthBaseVM
    {
        public AuthBaseVMValidator()
        {
            RuleFor(_ => _.Email).UserNameAndEmailRules();
            RuleFor(_ => _.Password).PasswordRules();
        }
    }

    public class ResendConfirmationEmailVMValidator : AuthBaseVMValidator<ResendConfirmationEmailVM> { }

    public class ConfirmEmailVM
    {
        [Trim]
        public string UserId { get; set; }

        [Trim]
        public string Token { get; set; }
    }

    public class ConfirmEmailVMValidator : AbstractValidator<ConfirmEmailVM>
    {
        public ConfirmEmailVMValidator()
        {
            RuleFor(_ => _.UserId).NotEmpty();
            RuleFor(_ => _.Token).NotEmpty();
        }
    }

    public class ChangePasswordVM
    {
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class ChangePasswordVMValidator : AbstractValidator<ChangePasswordVM>
    {
        public ChangePasswordVMValidator()
        {
            RuleFor(_ => _.CurrentPassword).NotEmpty();
            RuleFor(_ => _.NewPassword).PasswordRules();
        }
    }

    public class SendPasswordResettingEmailVM
    {
        [Trim]
        public string Email { get; set; }
    }

    public class SendPasswordResettingEmailVMValidator : AbstractValidator<SendPasswordResettingEmailVM>
    {
        public SendPasswordResettingEmailVMValidator()
        {
            RuleFor(_ => _.Email).UserNameAndEmailRules();
        }
    }


    public class ResetPasswordVM
    {
        [Trim]
        public string UserId { get; set; }

        [Trim]
        public string Token { get; set; }

        public string NewPassword { get; set; }
    }

    public class ResetPasswordVMValidator : AbstractValidator<ResetPasswordVM>
    {
        public ResetPasswordVMValidator()
        {
            RuleFor(_ => _.UserId).NotEmpty();
            RuleFor(_ => _.Token).NotEmpty();
            RuleFor(_ => _.NewPassword).PasswordRules();
        }
    }


    public class SendEmailChangingEmailVM
    {
        public string NewEmail { get; set; }
    }

    public class SendEmailChangingEmailVMValidator : AbstractValidator<SendEmailChangingEmailVM>
    {
        public SendEmailChangingEmailVMValidator()
        {
            RuleFor(_ => _.NewEmail).UserNameAndEmailRules();
        }
    }


    public class ChangeEmailVM
    {
        [Trim]
        public string NewEmail { get; set; }

        [Trim]
        public string Token { get; set; }
    }

    public class ChangeEmailVMValidator : AbstractValidator<ChangeEmailVM>
    {
        public ChangeEmailVMValidator()
        {
            RuleFor(_ => _.NewEmail).UserNameAndEmailRules();
            RuleFor(_ => _.Token).NotEmpty();
        }
    }
}