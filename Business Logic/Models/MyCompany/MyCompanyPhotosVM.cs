using BusinessLogic.Models.Common;
using BusinessLogic.Utils;

namespace BusinessLogic.Models.MyCompany
{
    public class MyCompanyPhotoReadVM
    {
        public string Id { get; set; }
        public LookupStringReadVM CompanyFilial { get; set; }
        public string PhotoUri { get; set; }
    }


    public class MyCompanyPhotoAddVM : UploadImageVM
    {
        [Trim]
        public string FilialId { get; set; }
    }

    public class MyCompanyUploadMainPhotoVM : UploadImageVM { }


    public class MyCompanyPhotoAddVMValidator : UploadImageVMValidator<MyCompanyPhotoAddVM> { }

    public class UploadCompanyMainPhotoVMValidator : UploadImageVMValidator<MyCompanyUploadMainPhotoVM> { }


    public class MyCompanyPhotoAddResponseVM
    {
        public string Id { get; set; }
        public string Url { get; set; }
    }
}