using System;
using BusinessLogic.Models.Common;
using BusinessLogic.Utils;
using Data.Entities.Common;
using FluentValidation;
using Infrastructure;

namespace BusinessLogic.Models.MyCompany
{
    public abstract class MyCompanyBaseVM
    {
        [Trim]
        public string WebsiteUrl { get; set; }

        [Trim(Type = TrimType.AllowMultilinesAndTabs)]
        public string Description { get; set; }

        public int EmployeeCount { get; set; }

        [Trim]
        public string ContactPhone { get; set; }

        [Trim]
        public string ContactEmail { get; set; }
    }


    public class MyCompanyEditVM : MyCompanyBaseVM
    {
        //public Industry IndustryId { get; set; }
        public string IndustryId { get; set; }
    }

    public class MyCompanyReadVM : MyCompanyBaseVM
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public IndustryReadVM Industry { get; set; }

        public string MainPhotoUri { get; set; }
    }

    public class MyCompanyEditVMValidator : AbstractValidator<MyCompanyEditVM>
    {
        public MyCompanyEditVMValidator()
        {
            When(_ => _.WebsiteUrl.NotNullAndEmpty(),
                () => { RuleFor(_ => _.WebsiteUrl).LengthBetween(10, 200).IsUrl("Website Url"); });
            RuleFor(_ => _.Description).NotEmpty().MaximumLength(1000);

            //When(_ => !_.ContactEmail.IsNullOrWhiteSpace(), () =>
            //{
            RuleFor(_ => _.ContactEmail).NotEmpty().LengthBetween(AppConstants.PropertiesRules.ContactEmailMinLen,
                AppConstants.PropertiesRules.ContactEmailMaxLen).EmailAddress();
            //});

            //When(_ => !_.ContactPhone.IsNullOrWhiteSpace(), () =>
            //{
            RuleFor(_ => _.ContactPhone).NotEmpty().PhoneNumberValidations();

            RuleFor(_ => _.IndustryId).NotEmpty().IsInIndustriesEmun();
            //});
        }
    }


    public class MyCompanyFilialEditBaseVM
    {
        [Trim]
        public string Name { get; set; }

        public int EmployeeCount { get; set; }

        //[Trim]
        //public string ContactPhone { get; set; }
        //[Trim]
        //public string ContactEmail { get; set; }
    }

    public class MyCompanyFilialEditVM : MyCompanyFilialEditBaseVM
    {
        [Trim]
        public string Id { get; set; }
    }

    public class MyCompanyFilialAddVM : MyCompanyFilialEditBaseVM
    {
        public AddressEditVM Address { get; set; }
    }

    public class MyCompanyFilialEditBaseVMValidator<T> : AbstractValidator<T> where T : MyCompanyFilialEditBaseVM
    {
        public MyCompanyFilialEditBaseVMValidator()
        {
            RuleFor(_ => _.Name).NotEmpty().LengthBetween(2, 50);
            //RuleFor(_ => _.Address).NotNull();
            RuleFor(_ => _.EmployeeCount).NotEmpty().InclusiveBetween(1, 999999);

            //When(_ => !_.ContactEmail.IsNullOrWhiteSpace(), () =>
            //{
            //    RuleFor(_ => _.ContactEmail).LengthBetween(AppConstants.PropertiesRules.ContactEmailMinLen,
            //        AppConstants.PropertiesRules.ContactEmailMaxLen).EmailAddress();
            //});

            //When(_ => !_.ContactPhone.IsNullOrWhiteSpace(), () =>
            //{
            //    RuleFor(_ => _.ContactPhone).LengthBetween(AppConstants.PropertiesRules.ContactPhoneMinLen,
            //        AppConstants.PropertiesRules.ContactPhoneMaxLen);
            //});
        }
    }

    public class MyCompanyFilialEditEditVMValidator : MyCompanyFilialEditBaseVMValidator<MyCompanyFilialEditVM>
    {
        public MyCompanyFilialEditEditVMValidator()
        {
            RuleFor(_ => _.Id).NotEmpty();
        }
    }

    public class MyCompanyFilialEditAddVMValidator : MyCompanyFilialEditBaseVMValidator<MyCompanyFilialAddVM>
    {
        public MyCompanyFilialEditAddVMValidator()
        {
            RuleFor(_ => _.Address).NotNull().SetValidator(new AddressEditVMValidator());
        }
    }


    public class MyCompanyFilialReadVM
    {
        public AddressReadVM Address { get; set; }

        public bool IsMain { get; set; }

        public int EmployeeCount { get; set; }
        //public string ContactPhone { get; set; }
        //public string ContactEmail { get; set; }


        public string Name { get; set; }

        public string Id { get; set; }

        public bool IsDisabled { get; set; }
    }

    public class MyCompanyFilialDetailsReadVM : MyCompanyFilialReadVM { }


    public class MyCompanyVacancyBaseVM
    {
        public decimal? SalaryPerMonth { get; set; }

        //[Trim]
        //public string ContactPhone { get; set; }

        //[Trim]
        //public string ContactEmail { get; set; }

        [Trim(Type = TrimType.AllowMultilinesAndTabs)]
        public string Description { get; set; }

        [Trim(Type = TrimType.AllowMultilinesAndTabs)]
        public string Requirements { get; set; }

        [Trim(Type = TrimType.AllowMultilinesAndTabs)]
        public string Duties { get; set; }

        [Trim(Type = TrimType.AllowMultilinesAndTabs)]
        public string WorkingConditions { get; set; }

        [Trim]
        public string Name { get; set; }
    }


    public class MyCompanyVacancyReadVM : MyCompanyVacancyBaseVM
    {
        public LookupStringReadVM CompanyFilial { get; set; }

        public LookupIntReadVM Currency { get; set; }

        public ProfessionReadVM Profession { get; set; }
        public LookupIntReadVM EmploymentType { get; set; }

        public int ViewsCount { get; set; }


        public string Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }
        public bool IsDisabled { get; set; }
    }

    public class MyCompanyVacancyDetailsReadVM : MyCompanyVacancyReadVM { }


    public class MyCompanyVacancyAddVM : MyCompanyVacancyBaseVM
    {
        [Trim]
        public string CompanyFilialId { get; set; }

        public Currency CurrencyId { get; set; }

        public string ProfessionId { get; set; }
        public EmploymentType EmploymentTypeId { get; set; }
    }

    public class MyCompanyVacancyUpdateVM : MyCompanyVacancyAddVM
    {
        [Trim]
        public string Id { get; set; }
    }


    public class MyCompanyVacancyBaseVMValidator<T> : AbstractValidator<T> where T : MyCompanyVacancyAddVM
    {
        public MyCompanyVacancyBaseVMValidator()
        {
            RuleFor(_ => _.Name).NotEmpty().MaximumLength(100).MinimumLength(5);

            RuleFor(_ => _.CompanyFilialId).NotEmpty();
            RuleFor(_ => _.CurrencyId).NotEmpty().IsInEnum();
            RuleFor(_ => _.ProfessionId).NotEmpty().IsInProfessionsEmun();
            RuleFor(_ => _.EmploymentTypeId).NotEmpty().IsInEnum();


            RuleFor(_ => _.SalaryPerMonth).ExclusiveBetween(0, AppConstants.PropertiesRules.MaxSalaryPerMonth);
            RuleFor(_ => _.Description).NotEmpty().LengthBetween(50, 2000);
            RuleFor(_ => _.Duties).NotEmpty().LengthBetween(20, 1000);
            RuleFor(_ => _.Requirements).NotEmpty().LengthBetween(20, 1000);
            RuleFor(_ => _.WorkingConditions).NotEmpty().LengthBetween(20, 1000);

            //RuleFor(_ => _.ContactEmail).LengthBetween(AppConstants.PropertiesRules.ContactEmailMinLen,
            //    AppConstants.PropertiesRules.ContactEmailMaxLen).EmailAddress();

            //RuleFor(_ => _.ContactPhone).LengthBetween(AppConstants.PropertiesRules.ContactPhoneMinLen,
            //    AppConstants.PropertiesRules.ContactPhoneMaxLen);
        }
    }

    public class MyCompanyVacancyAddVMValidator : MyCompanyVacancyBaseVMValidator<MyCompanyVacancyAddVM> { }

    public class MyCompanyVacancyUpdateVMValidator : MyCompanyVacancyBaseVMValidator<MyCompanyVacancyUpdateVM>
    {
        public MyCompanyVacancyUpdateVMValidator()
        {
            RuleFor(_ => _.Id).NotEmpty();
        }
    }
}