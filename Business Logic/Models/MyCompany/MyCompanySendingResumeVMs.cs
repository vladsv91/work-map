using System;
using BusinessLogic.Models.Common;
using BusinessLogic.Models.Resumes;

namespace BusinessLogic.Models.MyCompany
{
    public class MyCompanySentResumeReadVM
    {
        public string Id { get; set; }
        public DateTime CreatedOnUtc { get; set; }
        public DateTime UpdatedOnUtc { get; set; }

        public ResumeShortInfoReadVM Resume { get; set; }

        public MyCompanyVacancyReadVM Vacancy { get; set; }

        public string Message { get; set; }
        public LookupIntReadVM Status { get; set; }
    }
}