﻿using System.Threading.Tasks;
using Data;
using Data.Entities.ApplicantEntities;
using Data.Entities.CompanyEntities;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services
{
    public interface ILocationsServiceService : IBaseSearchService
    {
        Task<EntitiesCountsNearLocation> GetEntitiesCountsNearLocationAsync(decimal lt, decimal ln);
    }


    public class EntitiesCountsNearLocation
    {
        public int CompaniesCount { get; set; }
        public int VacanciesCount { get; set; }
        public int ResumesCount { get; set; }
        public decimal RadiusInKm { get; set; }

        public bool HasData => CompaniesCount > 0 && VacanciesCount > 0;
    }


    public class LocationsServiceService : BaseSearchService, ILocationsServiceService
    {
        private readonly IRepository<Company> _companyRepository;
        private readonly IRepository<Resume> _resumeRepository;
        private readonly IRepository<Vacancy> _vacancyRepository;

        public LocationsServiceService(IUnitOfWork u,
            IRepository<Company> companyRepository,
            IRepository<Vacancy> vacancyRepository,
            IRepository<Resume> resumeRepository
        ) : base(u)
        {
            _companyRepository = companyRepository;
            _vacancyRepository = vacancyRepository;
            _resumeRepository = resumeRepository;
        }


        public async Task<EntitiesCountsNearLocation> GetEntitiesCountsNearLocationAsync(decimal lt, decimal ln)
        {
            var radiuses = new[] {1.5m, 5, 15, 35};

            for (var i = 0; i < radiuses.Length; i++) {
                var res = await GetEntitiesCountsNearLocationAsync(lt, ln, radiuses[i]);
                if (res.HasData) return res;
            }

            return new EntitiesCountsNearLocation();
        }

        private async Task<EntitiesCountsNearLocation> GetEntitiesCountsNearLocationAsync(decimal lt, decimal ln, decimal radiusInKm)
        {
            var companiesQ = GetStatusLookupQueryable(_companyRepository);
            companiesQ = CompaniesSearchItemsProcessor.FilterCompaniesByLocation(companiesQ, lt, ln, radiusInKm);

            var vacQ = GetStatusLookupQueryable(_vacancyRepository);
            vacQ = VacanciesSearchItemsProcessor.FilterVacanciesByLocation(vacQ, lt, ln, radiusInKm);

            var resQ = GetStatusLookupQueryable(_resumeRepository);
            resQ = ResumesSearchItemsProcessor.FilterResumesByLocation(resQ, lt, ln, radiusInKm);

            return new EntitiesCountsNearLocation
            {
                CompaniesCount = await companiesQ.CountAsync(),
                VacanciesCount = await vacQ.CountAsync(),
                ResumesCount = await resQ.CountAsync(),
                RadiusInKm = radiusInKm
            };
        }
    }
}