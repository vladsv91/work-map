using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.MyCompany;
using Data;
using Data.Entities.Common;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services.ForAuthorized.ForCompanyRole
{
    public interface IMyCompanySendingResumeService : IBaseService
    {
        Task<List<MyCompanySentResumeReadVM>> GetAllReceivedResumesAsync(string userId);
    }


    public class MyCompanySendingResumeService : BaseService, IMyCompanySendingResumeService
    {
        private readonly IRepository<SendingResume> _sendingResumeRepository;

        public MyCompanySendingResumeService(IUnitOfWork u,
            IRepository<SendingResume> sendingResumeRepository) : base(u)
        {
            _sendingResumeRepository = sendingResumeRepository;
        }


        public async Task<List<MyCompanySentResumeReadVM>> GetAllReceivedResumesAsync(string userId)
        {
            var sentResumesDb = await _sendingResumeRepository.GetQueriable().Include(_=>_.Resume.Applicant)
                .Include(_=>_.Vacancy) .Where(_ => _.Company.CreatorId == userId).OrderByDescending(_=>_.CreatedOnUtc)
                .ToListAsync();

            var sentResumesVm = Mapper.Instance.Map<List<MyCompanySentResumeReadVM>>(sentResumesDb);

            var hasChanges = false;
            foreach (var sendingResume in sentResumesDb)
                if (sendingResume.Status == SendingResumeStatus.Sent) {
                    hasChanges = true;
                    sendingResume.Status = SendingResumeStatus.Viewed;
                    _sendingResumeRepository.UpdateSomeFields(sendingResume, _ => _.Status);
                }


            if (hasChanges) SaveChangesAsync().Forget();

            return sentResumesVm;
        }
    }
}