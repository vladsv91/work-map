using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.MyCompany;
using Data;
using Data.Entities.CompanyEntities;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services.ForAuthorized.ForCompanyRole
{
    public interface IMyCompanyVacanciesService : IBaseService
    {
        Task<List<Vacancy>> GetAllCompanyVacanciesAsync(string userId);
        Task<string> AddVacancyAsync(MyCompanyVacancyAddVM vacancyVm, string userId);
        Task UpdateVacancyAsync(MyCompanyVacancyUpdateVM vacancyVm, string userId);
        Task DeactivateVacancyAsync(string vacancyId, string userId);
        Task ActivateVacancyAsync(string vacancyId, string userId);
        Task<Vacancy> GetVacancyDetailsAsync(string vacancyId, string userId);
    }

  
    public class MyCompanyVacanciesService : MyCompanyItemBaseService, IMyCompanyVacanciesService
    {
        private readonly IRepository<CompanyFilial> _companyFilialRepository;
        private readonly IRepository<Company> _companyRepository;
        private readonly IRepository<Vacancy> _vacancyRepository;

        public MyCompanyVacanciesService(IUnitOfWork u,
            IRepository<Company> companyRepository,
            IRepository<CompanyFilial> companyFilialRepository,
            IRepository<Vacancy> vacancyRepository,
            IMyCompanyService myCompanyService
            ) : base(u, myCompanyService)
        {
            _companyRepository = companyRepository;
            _companyFilialRepository = companyFilialRepository;
            _vacancyRepository = vacancyRepository;
        }


        public Task<List<Vacancy>> GetAllCompanyVacanciesAsync(string userId)
        {
            return _vacancyRepository.GetQueriable()
                .Where(_ => _.Company.CreatorId == userId).Include(_ => _.CompanyFilial).ToListAsync();
        }

        public async Task<Vacancy> GetVacancyDetailsAsync(string vacancyId, string userId)
        {
            var vacancyDb = await _vacancyRepository.GetQueriable().Include(_ => _.CompanyFilial)
                .FirstOrDefaultAsync(_ => _.Id == vacancyId && _.Company.CreatorId == userId);
            if (vacancyDb == null)
                throw GetValidationException("Vacancy not found");
            return vacancyDb;
        }

        public async Task<string> AddVacancyAsync(MyCompanyVacancyAddVM vacancyVm, string userId)
        {
            var filialDb = await _companyFilialRepository.GetQueriable().Where(_ =>
                _.Id == vacancyVm.CompanyFilialId
                && _.Company.CreatorId == userId).Select(_ => new {_.IsDisabled, _.CompanyId}).FirstOrDefaultAsync();
            if (filialDb == null)
                throw GetValidationException("Filial does not belong to this user.");
            if (filialDb.IsDisabled) throw GetValidationException("Filial is disabled.");

            if (await _vacancyRepository.GetQueriable().AnyAsync(_ =>
                _.CompanyId == filialDb.CompanyId && _.Name == vacancyVm.Name))
                throw GetValidationException("Cannot create vacancy with duplicated name of the company.");

            var vacancyDb = Mapper.Instance.Map<Vacancy>(vacancyVm);
            vacancyDb.Id = GeneralUtils.CreateUniqId();
            vacancyDb.CompanyId = filialDb.CompanyId;
            vacancyDb.ViewsCount = 0;

            _vacancyRepository.Add(vacancyDb);
            await SaveChangesAsync();
            return vacancyDb.Id;
        }

        public async Task UpdateVacancyAsync(MyCompanyVacancyUpdateVM vacancyVm, string userId)
        {
            var vacancyDb = await _vacancyRepository.GetQueriable().Include(_ => _.CompanyFilial).FirstOrDefaultAsync
                (_ => _.Id == vacancyVm.Id && _.Company.CreatorId == userId);

            if (vacancyDb == null)
                throw GetValidationException(nameof(MyCompanyVacancyUpdateVM.Id), "Vacancy does not belong this user.");
            if (vacancyDb.IsDisabled) throw GetValidationException("Cannot update deactivated vacancy.");

            if (vacancyDb.CompanyFilialId != vacancyVm.CompanyFilialId) {
                var filialDb = await _companyFilialRepository.GetQueriable().Where(_ =>
                    _.Id == vacancyVm.CompanyFilialId
                    && _.Company.CreatorId == userId).Select(_ => new {_.IsDisabled}).FirstOrDefaultAsync();
                if (filialDb == null)
                    throw GetValidationException("Filial does not belong to this user.");
                if (filialDb.IsDisabled) throw GetValidationException("Filial is disabled.");
            }

            if (vacancyDb.Name != vacancyVm.Name)
                if (await _vacancyRepository.GetQueriable().AnyAsync(_ =>
                    _.CompanyId == vacancyDb.CompanyId && _.Name == vacancyVm.Name))
                    throw GetValidationException("Cannot set duplicated vacancy name of the company.");

            Mapper.Instance.Map(vacancyVm, vacancyDb);

            _companyRepository.UpdateButIngoreSomeFields(vacancyDb, _ => _.ViewsCount);
            await SaveChangesAsync();
        }

        public async Task DeactivateVacancyAsync(string vacancyId, string userId)
        {
            var vacancyDb = await _vacancyRepository.GetQueriable().FirstOrDefaultAsync(
                _ => _.Id == vacancyId && _.Company.CreatorId == userId);

            if (vacancyDb == null)
                throw GetValidationException("Not found vacancy");

            _vacancyRepository.Deactivate(vacancyDb);
            await SaveChangesAsync();
        }

        public async Task ActivateVacancyAsync(string vacancyId, string userId)
        {
            var vacancyDb = await _vacancyRepository.GetQueriable().Include(_ => _.CompanyFilial).FirstOrDefaultAsync(
                _ => _.Id == vacancyId && _.Company.CreatorId == userId);

            if (vacancyDb == null)
                throw GetValidationException("Not found vacancy");
            if (vacancyDb.CompanyFilial.IsDisabled)
                throw GetValidationException("Cannot activate vacancy with deactivated filial.");

            _vacancyRepository.Activate(vacancyDb);
            await SaveChangesAsync();
        }
    }
}