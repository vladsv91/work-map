using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Models.MyCompany;
using BusinessLogic.Utils;
using Data;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services.ForAuthorized.ForCompanyRole
{
    public interface IMyCompanyPhotosService : IBaseService
    {
        Task<List<CompanyPhoto>> GetAllCompanyPhotosAsync(string userId);

        Task<MyCompanyPhotoAddResponseVM> AddPhotoAsync(IFormFile image, string userId, string filialId,
            string absoluteUrl);

        Task DeletePhotoAsync(string userId, string companyPhotoId);
    }


    public class MyCompanyPhotosService : MyCompanyItemBaseService, IMyCompanyPhotosService
    {
        private readonly IRepository<CompanyFilial> _companyFilialRepository;
        private readonly IRepository<CompanyPhoto> _companyPhotoRepository;
        private readonly IFileService _fileService;

        public MyCompanyPhotosService(IUnitOfWork u,
            IRepository<CompanyFilial> companyFilialRepository,
            IRepository<CompanyPhoto> companyPhotoRepository,
            IFileService fileService,
            IMyCompanyService myCompanyService
            ) : base(u, myCompanyService)
        {
            _companyFilialRepository = companyFilialRepository;
            _companyPhotoRepository = companyPhotoRepository;
            _fileService = fileService;
        }



        public async Task<MyCompanyPhotoAddResponseVM> AddPhotoAsync(IFormFile image, string userId, string filialId,
            string absoluteUrl)
        {
            //throw new NotImplementedException();

            var companyDb = await GetUserCompany(userId);
            var companyId = companyDb.Id;

            var photosCount = await _companyPhotoRepository.GetQueriable().CountAsync(_ => _.CompanyId == companyId);
            var maxPhotosPerCompany = 20;
            if (photosCount == maxPhotosPerCompany)
                throw GetValidationException($"Max count of photos: {maxPhotosPerCompany}");
            if (photosCount > maxPhotosPerCompany) {
                // todo, log this exception
                throw GetValidationException($"Max count of photos: {maxPhotosPerCompany}");
            }

            if (!string.IsNullOrEmpty(filialId)) {
                var dbFilial = await _companyFilialRepository.GetQueriable()
                    .Where(_ => _.Id == filialId && _.CompanyId == companyId).Select(_ => new {_.Id, _.IsDisabled})
                    .FirstOrDefaultAsync();
                if (dbFilial == null) throw GetValidationException("Company filial not belong user company");
                if (dbFilial.IsDisabled) throw GetValidationException("Company filial is disabled");
            }


            var photoId = _fileService.AddOrUpdateFileFile(image, null, FileKind.CompanyPhoto, FileType.Image);

            var companyPhotoDb = new CompanyPhoto
            {
                Id = GeneralUtils.CreateUniqId(),
                CompanyFilialId = filialId,
                PhotoId = photoId,
                CompanyId = companyId
            };

            _companyPhotoRepository.Add(companyPhotoDb);

            await SaveChangesAsync();
            return new MyCompanyPhotoAddResponseVM
            {
                Id = companyPhotoDb.Id,
                Url = BlUtils.CreateGetFileUrl(photoId),
            };
        }

        public async Task DeletePhotoAsync(string userId, string companyPhotoId)
        {
            var companyPhotoDb = await _companyPhotoRepository.GetQueriable()
                .Include(_ => _.Photo)
                .FirstOrDefaultAsync(_ => _.Id == companyPhotoId && _.Company.CreatorId == userId);

            if (companyPhotoDb == null) throw GetValidationException("Company photo not found");

            _companyPhotoRepository.Delete(companyPhotoDb);
            _fileService.DeleteFile(companyPhotoDb.Photo);
            await SaveChangesAsync();
        }

        public Task<List<CompanyPhoto>> GetAllCompanyPhotosAsync(string userId)
        {
            return _companyPhotoRepository.GetQueriable()
                .Where(_ => _.Company.CreatorId == userId).Include(_ => _.CompanyFilial).ToListAsync();
        }

    }
}