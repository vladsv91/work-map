using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.MyCompany;
using Data;
using Data.Entities.CompanyEntities;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services.ForAuthorized.ForCompanyRole
{
    public interface IMyCompanyFilialsService : IBaseService
    {
        Task<List<CompanyFilial>> GetAllCompanyFilialsAsync(string userId);
        Task<string> AddFilialAsync(MyCompanyFilialAddVM filialVm, string userId);
        Task UpdateFilialAsync(MyCompanyFilialEditVM filialVm, string userId);
        Task DeactivateFilialAsync(string filialId, string userId);
        Task ActivateFilialAsync(string filialId, string userId);
        Task<CompanyFilial> GetFilialDetailsAsync(string filialId, string userId);
    }


    public class MyCompanyFilialsService : MyCompanyItemBaseService, IMyCompanyFilialsService
    {
        private readonly IRepository<CompanyFilial> _companyFilialRepository;
        private readonly IRepository<CompanyPhoto> _companyPhotoRepository;
        private readonly IRepository<Company> _companyRepository;
        private readonly IRepository<Vacancy> _vacancyRepository;

        public MyCompanyFilialsService(IUnitOfWork u,
            IRepository<Company> companyRepository,
            IRepository<CompanyFilial> companyFilialRepository,
            IRepository<CompanyPhoto> companyPhotoRepository, IRepository<Vacancy> vacancyRepository,
            IMyCompanyService myCompanyService
            ) : base(u, myCompanyService)
        {
            _companyRepository = companyRepository;
            _companyFilialRepository = companyFilialRepository;
            _companyPhotoRepository = companyPhotoRepository;
            _vacancyRepository = vacancyRepository;
        }


        public async Task<string> AddFilialAsync(MyCompanyFilialAddVM filialVm, string userId)
        {
            var companyDb = await GetUserCompany(userId);
            var companyId = companyDb.Id;

            if (await _companyFilialRepository.GetQueriable()
                .AnyAsync(_ => _.CompanyId == companyId && _.Name == filialVm.Name))
                throw GetValidationException("Cannot create filial with same name.");

            var filialDb = Mapper.Instance.Map<CompanyFilial>(filialVm);
            filialDb.Id = GeneralUtils.CreateUniqId();
            filialDb.CompanyId = companyId;
            filialDb.IsMain = false;
            filialDb.Address = await CreateAddressData(filialVm.Address, false);

            // todo, max 20
            var filialsEmpCountSum = await _companyFilialRepository.GetQueriable().Where(_ => _.CompanyId == companyId)
                .SumAsync(_ => _.EmployeeCount);
            filialsEmpCountSum += filialVm.EmployeeCount;
            if (filialsEmpCountSum > companyDb.EmployeeCount) {
                companyDb.EmployeeCount = filialsEmpCountSum;
                _companyRepository.UpdateSomeFields(companyDb, _ => _.EmployeeCount);
            }

            _companyFilialRepository.Add(filialDb);
            await SaveChangesAsync();
            return filialDb.Id;
        }

        public async Task UpdateFilialAsync(MyCompanyFilialEditVM filialVm, string userId)
        {
            var companyDb = await GetUserCompany(userId);
            var companyId = companyDb.Id;

            var filialId = filialVm.Id;
            //var filialDb = await _companyFilialRepository.GetQueriable().Include(_ => _.Address).FirstOrDefaultAsync(
            //    _ => _.Id == filialId && _.CompanyId == companyId);
            var filialDb = await _companyFilialRepository.GetQueriable().FirstOrDefaultAsync(
                _ => _.Id == filialId && _.CompanyId == companyId);

            if (filialDb == null)
                throw GetValidationException("Not found filial.");

            if (filialDb.IsDisabled)
                throw GetValidationException("Cannot update deactivated filial.");

            if (filialDb.Name != filialVm.Name && await _companyFilialRepository.GetQueriable()
                    .AnyAsync(_ => _.CompanyId == companyId && _.Name == filialVm.Name))
                throw GetValidationException("Cannot set filial name if exists filial with such name of the company.");

            if (filialVm.EmployeeCount > filialDb.EmployeeCount) {
                var filialsEmpCountSum = await _companyFilialRepository.GetQueriable()
                    .Where(_ => _.CompanyId == companyId && _.Id != filialDb.Id)
                    .SumAsync(_ => _.EmployeeCount);
                filialsEmpCountSum += filialVm.EmployeeCount;
                if (filialsEmpCountSum > companyDb.EmployeeCount) {
                    companyDb.EmployeeCount = filialsEmpCountSum;
                    _companyRepository.UpdateSomeFields(companyDb, _ => _.EmployeeCount);
                }
            }


            // todon check address changing
            //var fAddr = filialDb.Address;


            Mapper.Instance.Map(filialVm, filialDb);

            //filialDb.Address = fAddr;
            //if (fAddr.Latitude != filialVm.Address.Latitude ||
            //    fAddr.Longitude != filialVm.Address.Longitude) {
            //    filialDb.Address = fAddr;
            //    await SetAddressData(filialDb.Address, filialVm.Address);
            //}
            _companyFilialRepository.UpdateButIngoreSomeFields(filialDb, _ => _.CompanyId);
            await SaveChangesAsync();
        }

        public async Task DeactivateFilialAsync(string filialId, string userId)
        {
            var filialDb = await _companyFilialRepository.GetQueriable().FirstOrDefaultAsync(
                _ => _.Id == filialId && _.Company.CreatorId == userId);

            var filialDbId = filialDb.Id;

            if (filialDb == null)
                throw GetValidationException("Not found filial");

            if (filialDb.IsDisabled) return;

            if (filialDb.IsMain)
                throw GetValidationException("Cannot deactivate main filial");


            var filialVacancies = await _vacancyRepository.GetQueriable().Where(_ => _.CompanyFilialId == filialDbId)
                .ToListAsync();

            foreach (var filialVacancy in filialVacancies) _vacancyRepository.Deactivate(filialVacancy);

            var filialPhotos = await _companyPhotoRepository.GetQueriable().Where(_ => _.CompanyFilialId == filialDbId)
                .ToListAsync();
            foreach (var filialPhoto in filialPhotos) {
                filialPhoto.CompanyFilialId = null;
                _companyPhotoRepository.UpdateSomeFields(filialPhoto, _ => _.CompanyFilialId);
            }

            _companyRepository.Deactivate(filialDb);
            await SaveChangesAsync();
        }

        public async Task ActivateFilialAsync(string filialId, string userId)
        {
            var filialDb = await _companyFilialRepository.GetQueriable().FirstOrDefaultAsync(
                _ => _.Id == filialId && _.Company.CreatorId == userId);

            if (filialDb == null)
                throw GetValidationException("Not found filial");

            if (!filialDb.IsDisabled) return;

            if (filialDb.IsMain)
                throw GetValidationException("Cannot activate main filial");
            _companyFilialRepository.Activate(filialDb);
            await SaveChangesAsync();
        }


        public Task<List<CompanyFilial>> GetAllCompanyFilialsAsync(string userId)
        {
            return _companyFilialRepository.GetQueriable()
                .Where(_ => _.Company.CreatorId == userId).Include(_ => _.Address).ToListAsync();
        }

        public async Task<CompanyFilial> GetFilialDetailsAsync(string filialId, string userId)
        {
            var filialDb = await _companyFilialRepository.GetQueriable().Include(_ => _.Address)
                .FirstOrDefaultAsync(_ => _.Id == filialId && _.Company.CreatorId == userId);
            if (filialDb == null) throw GetValidationException("Filial not found");
            return filialDb;
        }

    }
}