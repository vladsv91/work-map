using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.MyCompany;
using BusinessLogic.Utils;
using Data;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services.ForAuthorized.ForCompanyRole
{
    public interface IMyCompanyService : IBaseService
    {
        Task<Company> GetMyCompanyShortInfoAsync(string userId);

        Task<string> UploadMainPhotoAsync(IFormFile image, string userId);

        Task UpdateCompanyInfoAsync(MyCompanyEditVM companyVm, string userId);

        Task<Company> GetUserCompany(string userId, params Expression<Func<Company, object>>[] includes);
    }

    //public class MyCompanyCrudService : BaseCrudService<Company, string>
    //{
    //    public MyCompanyCrudService(IUnitOfWork u, IRepository<Company> entityRepository) : base(u, entityRepository)
    //    {

    //    }

    //    public override Task UpdateAsync<TViewModel>(TViewModel viewModel, string entityId)
    //    {
    //        throw new Exception("Use method ''");
    //    }

    //    public async Task UpdateAsync(MyCompanyEditVM viewModel, string userId)
    //    {
    //        //base.UpdateAsync(viewModel, viewModel.);
    //    }
    //}




    public abstract class MyCompanyItemBaseService : BaseService
    {
        protected readonly IMyCompanyService MyCompanyService;

        protected MyCompanyItemBaseService(IUnitOfWork u,
            IMyCompanyService myCompanyService
            ) : base(u)
        {
            MyCompanyService = myCompanyService;
        }

        protected Task<Company> GetUserCompany(string userId, params Expression<Func<Company, object>>[] includes)
        {
            return MyCompanyService.GetUserCompany(userId, includes);
        }
    }


    public class MyCompanyService : BaseService, IMyCompanyService
    {
        private readonly IRepository<CompanyFilial> _companyFilialRepository;
        private readonly IRepository<Company> _companyRepository;
        private readonly IFileService _fileService;

        public MyCompanyService(IUnitOfWork u,
            IRepository<Company> companyRepository,
            IRepository<CompanyFilial> companyFilialRepository,
            IFileService fileService
            ) : base(u)
        {
            _companyRepository = companyRepository;
            _companyFilialRepository = companyFilialRepository;
            _fileService = fileService;
        }

        public async Task UpdateCompanyInfoAsync(MyCompanyEditVM companyVm, string userId)
        {
            var companyDb = await GetUserCompany(userId);

            if (companyVm.EmployeeCount < companyDb.EmployeeCount) {
                var filialsEmpCountSum = await _companyFilialRepository.GetQueriable()
                    .Where(_ => _.CompanyId == companyDb.Id)
                    .SumAsync(_ => _.EmployeeCount);
                if (filialsEmpCountSum > companyVm.EmployeeCount)
                    throw GetValidationException(
                        "Cannot set employee count less than sum of all company filial's employee count.");
            }

            Mapper.Instance.Map(companyVm, companyDb);
            _companyRepository.UpdateButIngoreSomeFields(companyDb, _ => _.Name);
            await SaveChangesAsync();
        }

        public Task<Company> GetMyCompanyShortInfoAsync(string userId)
        {
            return GetUserCompany(userId);
        }


        public async Task<string> UploadMainPhotoAsync(IFormFile image, string userId)
        {
            var companyDb = await GetUserCompany(userId, _ => _.MainPhoto);
            var mainPhotoId = _fileService.AddOrUpdateFileFile(image, companyDb.MainPhoto, FileKind.CompanyMainPhoto, FileType.Image);

            if (companyDb.MainPhotoId != null) {
                _companyRepository.UpdateSomeFields(companyDb);
            }
            else {
                companyDb.MainPhotoId = mainPhotoId;
                _companyRepository.UpdateSomeFields(companyDb, _ => _.MainPhotoId);
            }

            await SaveChangesAsync();
            return BlUtils.CreateGetFileUrl(companyDb.MainPhotoId.Value);
        }


        public async Task<Company> GetUserCompany(string userId, params Expression<Func<Company, object>>[] includes)
        {
            var companyQ = _companyRepository.GetQueriable();
            companyQ = includes.Aggregate(companyQ, (current, t) => current.Include(t));

            var companyDb = await companyQ.FirstOrDefaultAsync(_ => _.CreatorId == userId);

            if (companyDb == null)
                throw GetValidationException("User has not company", HttpStatusCode.Forbidden);
            return companyDb;
        }
    }
}