using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.Common;
using BusinessLogic.Models.MyApplicant;
using Data;
using Data.Entities.ApplicantEntities;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services.ForAuthorized.ForApplicantRole
{
    public interface IApplicantSendingResumeService : IBaseService
    {
        Task SendResume(string userId, MyApplicantSendResumeVM sendResumeVM, string absoluteUrl);
        Task<List<MyApplicantSentResumeReadVM>> GetAllSentResumesAsync(string userId);
    }


    public class ApplicantSendingResumeService : BaseService, IApplicantSendingResumeService
    {
        private readonly IRepository<Resume> _resumeRepository;
        private readonly IRepository<SendingResume> _sendingResumeRepository;
        private readonly IRepository<Vacancy> _vacancyRepository;

        private readonly IMailService _mailService;

        public ApplicantSendingResumeService(IUnitOfWork u,
            IRepository<SendingResume> sendingResumeRepository,
            IRepository<Resume> resumeRepository,
            IRepository<Vacancy> vacancyRepository,
            IMailService mailService) : base(u)
        {
            _vacancyRepository = vacancyRepository;
            _sendingResumeRepository = sendingResumeRepository;
            _resumeRepository = resumeRepository;
            _mailService = mailService;
        }


        public async Task SendResume(string userId, MyApplicantSendResumeVM sendResumeVM, string absoluteUrl)
        {
            // todo, cannot send not active resume, but allow for compnay, who received resume
            var resumeDb = await _resumeRepository.GetQueriable().Include(_ => _.Applicant)
                .FirstOrDefaultAsync(_ => _.Id == sendResumeVM.ResumeId && _.Applicant.CreatorId == userId);

            ValidateApplicantResume(resumeDb);


            var vacancyDb = await _vacancyRepository.GetQueriable().Include(_ => _.Company.Creator)
                .FirstOrDefaultAsync(_ => !_.IsDisabled && _.Id == sendResumeVM.VacancyId);

            var companyDb = vacancyDb.Company;

            // it imposible
            if (companyDb.IsDisabled) throw GetValidationException("Company not found.", HttpStatusCode.NotFound);

            var sendingResumeDb = Mapper.Instance.Map<SendingResume>(sendResumeVM);
            sendingResumeDb.Id = GeneralUtils.CreateUniqId(IdGenerator.LongIdLength);
            sendingResumeDb.CompanyId = vacancyDb.CompanyId;
            sendingResumeDb.ApplicantId = resumeDb.ApplicantId;
            sendingResumeDb.Status = SendingResumeStatus.Sent;
            _sendingResumeRepository.Add(sendingResumeDb);
            await SaveChangesAsync();

            var aendingResumeEmail = new SendingResumeEmail
            {
                Message = sendingResumeDb.Message,
                Resume = resumeDb,
                Vacancy = vacancyDb,
                //ResumeId = sendingResumeDb.ResumeId,
                //ApplicantFullname = resumeDb.Applicant.FullName,
                //ResumeName = resumeDb.Name,
                //VacancyName = vacancyDb.Name,
                //VacancyId = vacancyDb.Id,
            };

            // set company culture
            AppCultures.SetCulture(vacancyDb.Company.Creator.Language);
            _mailService.SendEmailToCompanyAboutReceivingResume(companyDb.ContactEmail, absoluteUrl, aendingResumeEmail).Forget();
        }


        public async Task<List<MyApplicantSentResumeReadVM>> GetAllSentResumesAsync(string userId)
        {
            var sentResumesDb = await _sendingResumeRepository.GetQueriable().Include(_ => _.Resume)
                .Include(_ => _.Vacancy).Where(_ => _.Applicant.CreatorId == userId).OrderByDescending(_ => _.CreatedOnUtc)
                .ToListAsync();

            var sentResumesVm = Mapper.Instance.Map<List<MyApplicantSentResumeReadVM>>(sentResumesDb);

            return sentResumesVm;
        }

    }
}