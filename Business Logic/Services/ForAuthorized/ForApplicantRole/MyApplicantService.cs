﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.MyApplicant;
using BusinessLogic.Utils;
using Data;
using Data.Entities.ApplicantEntities;
using Data.Entities.Common;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services.ForAuthorized.ForApplicantRole
{
    public interface IMyApplicantService : IBaseService
    {
        Task<Applicant> GetApplicantShortInfoAsync(string userId);
        Task UpdateApplicantInfoAsync(MyApplicantEditVM applicantVm, string userId);
        Task<string> UploadPhotoAsync(IFormFile image, string userId, string absoluteUrl);

        Task<string> UploadResumeFileAsync(IFormFile file, string userId, string resumeId, string absoluteUrl);
        Task DeleteResumeFileAsync(string userId, string resumeId);

        Task<List<Resume>> GetAllResumesAsync(string userId);
        Task<Resume> GetResumeDetailsAsync(string resumeId, string userId);
        Task<string> AddResumeAsync(MyResumeAddVM resumeVm, string userId);
        Task UpdateResumeAsync(MyResumeEditVM resumeVm, string userId);
        Task DeactivateResumeAsync(string resumeId, string userId);
        Task DeleteResumeAsync(string resumeId, string userId);
        Task ActivateResumeAsync(string resumeId, string userId);


        Task<List<ApplicantPreferredAddress>> GetAllPreferredAddressesAsync(string userId);
        Task<ApplicantPreferredAddress> GetPreferredAddressDetailsAsync(string preferredAddressId, string userId);

        Task<MyPreferredAddressAddResultVM> AddPreferredAddressAsync(MyPreferredAddressAddVM preferredAddressVm,
            string userId);

        Task UpdatePreferredAddressAsync(MyPreferredAddressEditVM preferredAddressVM, string userId);
        Task DeletePreferredAddressAsync(string preferredAddressId, string userId);
    }


    public class MyApplicantService : BaseService, IMyApplicantService
    {
        private readonly IRepository<Applicant> _applicantRepository;
        private readonly IRepository<ApplicantPreferredAddress> _preferredAddressRepository;
        private readonly IRepository<Resume> _resumeRepository;
        private readonly IRepository<WorkExperience> _workExperienceRepository;
        private readonly IFileService _fileService;



        public MyApplicantService(IUnitOfWork u,
            IRepository<Applicant> applicantRepository,
            IRepository<Resume> resumeRepository,
            IRepository<ApplicantPreferredAddress> preferredAddressRepository,
            IRepository<WorkExperience> workExperienceRepository,
            IFileService fileService
            ) : base(u)
        {
            _applicantRepository = applicantRepository;
            _resumeRepository = resumeRepository;
            _preferredAddressRepository = preferredAddressRepository;
            _workExperienceRepository = workExperienceRepository;
            _fileService = fileService;
        }

        public async Task UpdateApplicantInfoAsync(MyApplicantEditVM applicantVm, string userId)
        {
            var applicantDb = await GetUserApplicant(userId);

            Mapper.Instance.Map(applicantVm, applicantDb);
            _applicantRepository.Update(applicantDb);
            await SaveChangesAsync();
        }

        public Task<Applicant> GetApplicantShortInfoAsync(string userId)
        {
            return GetUserApplicant(userId);
        }

        public async Task<string> UploadPhotoAsync(IFormFile image, string userId, string absoluteUrl)
        {
            var applicantDb = await GetUserApplicant(userId);


            var photoId = _fileService.AddOrUpdateFileFile(image, applicantDb.Photo, FileKind.ApplicantMainPhoto, FileType.Image);

            if (applicantDb.PhotoId != null)
            {
                _applicantRepository.UpdateSomeFields(applicantDb);
            }
            else
            {
                applicantDb.PhotoId = photoId;
                _applicantRepository.UpdateSomeFields(applicantDb, _ => _.PhotoId);
            }

            await SaveChangesAsync();
            return BlUtils.CreateGetFileUrl(applicantDb.PhotoId.Value);

        }



        public async Task<string> UploadResumeFileAsync(IFormFile file, string userId, string resumeId, string absoluteUrl)
        {
            var resumeDb = await _resumeRepository.GetQueriable()
                .FirstOrDefaultAsync(_ => _.Applicant.CreatorId == userId && _.Id == resumeId);
            ValidateApplicantResume(resumeDb);

            var photoId = _fileService.AddOrUpdateFileFile(file, resumeDb.ResumeFile, FileKind.ApplicantResume, FileType.Document);

            if (resumeDb.ResumeFileId != null) {
                _applicantRepository.UpdateSomeFields(resumeDb);
            }
            else {
                resumeDb.ResumeFileId = photoId;
                _applicantRepository.UpdateSomeFields(resumeDb, _ => _.ResumeFileId);
            }




            await SaveChangesAsync();
            return BlUtils.CreateGetFileUrl(resumeDb.ResumeFileId.Value);
        }


        public async Task DeleteResumeFileAsync(string userId, string resumeId)
        {
            var resumeDb = await _resumeRepository.GetQueriable()
                .Include(_ => _.ResumeFile)
                .FirstOrDefaultAsync(_ => _.Applicant.CreatorId == userId && _.Id == resumeId && _.ResumeFileId != null);

            ValidateApplicantResumeExisting(resumeDb);

            _fileService.DeleteFile(resumeDb.ResumeFile);

            resumeDb.ResumeFileId = null;

            _resumeRepository.UpdateSomeFields(resumeDb, _ => _.ResumeFileId);
            await SaveChangesAsync();
        }



        public Task<List<Resume>> GetAllResumesAsync(string userId)
        {
            return _resumeRepository.GetQueriable()
                .Where(_ => _.Applicant.CreatorId == userId).Include(_ => _.WorkExperiences).ToListAsync();
        }

        public async Task<string> AddResumeAsync(MyResumeAddVM resumeVm, string userId)
        {
            var applicantDb = await GetUserApplicant(userId);
            var applicantId = applicantDb.Id;

            var resumesCount = await _resumeRepository.GetQueriable().CountAsync(_ => _.ApplicantId == applicantId);
            var maxResumesCount = 5;
            if (resumesCount >= maxResumesCount) {
                throw GetValidationException($"Cannot create more than {maxResumesCount} resumes.");
            }

            if (await _resumeRepository.GetQueriable()
                .AnyAsync(_ => _.ApplicantId == applicantId && _.Name == resumeVm.Name))
                throw GetValidationException("Cannot create resume with same name.");

            var resumeDb = Mapper.Instance.Map<Resume>(resumeVm);
            var resumeDbId = resumeDb.Id = GeneralUtils.CreateUniqId();
            resumeDb.ApplicantId = applicantId;

            var workExperiences = resumeDb.WorkExperiences;
            // todo, test it or delete it
            if (workExperiences != null)
                foreach (var workExperience in workExperiences) workExperience.ResumeId = resumeDbId;

            _resumeRepository.Add(resumeDb);
            await SaveChangesAsync();
            return resumeDbId;
        }

        public async Task UpdateResumeAsync(MyResumeEditVM resumeVm, string userId)
        {
            var applicantDb = await GetUserApplicant(userId);
            var applicantId = applicantDb.Id;

            var resumeId = resumeVm.Id;
            var resumeDb = await _resumeRepository.GetQueriable().Include(_ => _.WorkExperiences).FirstOrDefaultAsync(
                _ => _.Id == resumeId && _.ApplicantId == applicantId);

            if (resumeDb == null)
                throw GetValidationException("Not found resume.");

            //if (resumeDb.IsDisabled)
            //    throw GetValidationException("Cannot update deactivated resume.");

            if (resumeDb.Name != resumeVm.Name && await _resumeRepository.GetQueriable()
                    .AnyAsync(_ => _.ApplicantId == applicantId && _.Name == resumeVm.Name))
                throw GetValidationException(
                    "Cannot set resume name if exists resume with such name of the applicant.");

            var workExperiencesDb = resumeDb.WorkExperiences;

            // todo, test it or delete it
            if (resumeVm.WorkExperiencesIdsToDelete != null)
                foreach (var workToDeleteId in resumeVm.WorkExperiencesIdsToDelete) {
                    var workToDeleteDb = workExperiencesDb.FirstOrDefault(_ => _.Id == workToDeleteId);
                    if (workToDeleteDb == null)
                        throw GetValidationException("Work experience not found for resume.");
                    _workExperienceRepository.Delete(workToDeleteDb);
                }

            if (resumeVm.WorkExperiencesToUpdate != null)
                foreach (var workExperienceVm in resumeVm.WorkExperiencesToUpdate) {
                    var workToUpdateDb = workExperiencesDb.FirstOrDefault(_ => _.Id == workExperienceVm.Id);
                    if (workToUpdateDb == null)
                        throw GetValidationException("Work experience not found for resume.");
                    Mapper.Instance.Map(workExperienceVm, workToUpdateDb);
                    _workExperienceRepository.Update(workToUpdateDb);
                }

            // todo, max 20
            if (resumeVm.WorkExperiencesToAdd != null)
                foreach (var workExperienceVm in resumeVm.WorkExperiencesToAdd) {
                    var workExperienceToAddDb = Mapper.Instance.Map<WorkExperience>(workExperienceVm);
                    _workExperienceRepository.Add(workExperienceToAddDb);
                }

            resumeDb.WorkExperiences = null;


            Mapper.Instance.Map(resumeVm, resumeDb);

            _resumeRepository.UpdateButIngoreSomeFields(resumeDb, _ => _.ApplicantId);
            await SaveChangesAsync();
        }

        public async Task DeactivateResumeAsync(string resumeId, string userId)
        {
            var resumeDb = await _resumeRepository.GetQueriable().FirstOrDefaultAsync(
                _ => _.Id == resumeId && _.Applicant.CreatorId == userId);


            if (resumeDb == null)
                throw GetValidationException("Not found resume.");

            if (resumeDb.IsDisabled) return;
            _resumeRepository.Deactivate(resumeDb);
            await SaveChangesAsync();
        }

        public async Task DeleteResumeAsync(string resumeId, string userId)
        {
            var resumeDb = await _resumeRepository.GetQueriable().FirstOrDefaultAsync(
                _ => _.Id == resumeId && _.Applicant.CreatorId == userId);

            if (resumeDb == null)
                throw GetValidationException("Not found resume.");

            _resumeRepository.Delete(resumeDb);
            await SaveChangesAsync();
        }

        public async Task ActivateResumeAsync(string resumeId, string userId)
        {
            var resumeDb = await _resumeRepository.GetQueriable().FirstOrDefaultAsync(
                _ => _.Id == resumeId && _.Applicant.CreatorId == userId);

            if (resumeDb == null)
                throw GetValidationException("Not found resume.");

            if (!resumeDb.IsDisabled) return;

            _resumeRepository.Activate(resumeDb);
            await SaveChangesAsync();
        }


        public async Task<Resume> GetResumeDetailsAsync(string resumeId, string userId)
        {
            var resumeDb = await _resumeRepository.GetQueriable().Include(_ => _.WorkExperiences)
                .FirstOrDefaultAsync(_ => _.Id == resumeId && _.Applicant.CreatorId == userId);
            ValidateApplicantResumeExisting(resumeDb);
            return resumeDb;
        }


        public Task<List<ApplicantPreferredAddress>> GetAllPreferredAddressesAsync(string userId)
        {
            return _preferredAddressRepository.GetQueriable()
                .Where(_ => _.Applicant.CreatorId == userId).Include(_ => _.Address).ToListAsync();
        }

        public async Task<ApplicantPreferredAddress> GetPreferredAddressDetailsAsync(string preferredAddressId,
            string userId)
        {
            var preferredAddressDb = await _preferredAddressRepository.GetQueriable().Include(_ => _.Address)
                .FirstOrDefaultAsync(_ => _.Id == preferredAddressId && _.Applicant.CreatorId == userId);
            if (preferredAddressDb == null)
                throw GetValidationException("Preferred address not found.");
            return preferredAddressDb;
        }

        public async Task<MyPreferredAddressAddResultVM> AddPreferredAddressAsync(
            MyPreferredAddressAddVM preferredAddressVm, string userId)
        {
            // todo, max 20
            var applicantDb = await GetUserApplicant(userId);
            var applicantId = applicantDb.Id;

            if (await _preferredAddressRepository.GetQueriable()
                .AnyAsync(_ => _.ApplicantId == applicantId &&
                               _.Address.Longitude == preferredAddressVm.Address.Longitude
                               && _.Address.Latitude == preferredAddressVm.Address.Latitude))
                throw GetValidationException("Cannot create preferred address with same address.");

            var applicantPreferredAddressDb = Mapper.Instance.Map<ApplicantPreferredAddress>(preferredAddressVm);
            applicantPreferredAddressDb.Id = GeneralUtils.CreateUniqId();
            applicantPreferredAddressDb.ApplicantId = applicantId;
            applicantPreferredAddressDb.Address = await CreateAddressData(preferredAddressVm.Address, false);

            _preferredAddressRepository.Add(applicantPreferredAddressDb);
            await SaveChangesAsync();
            return new MyPreferredAddressAddResultVM
            {
                Id = applicantPreferredAddressDb.Id,
                FormattedAddress = applicantPreferredAddressDb.Address.FormattedAddress
            };
        }

        public async Task UpdatePreferredAddressAsync(MyPreferredAddressEditVM preferredAddressVM, string userId)
        {
            var applicantDb = await GetUserApplicant(userId);
            var applicantId = applicantDb.Id;

            var preferredAddressId = preferredAddressVM.Id;
            var preferredAddressDb = await _preferredAddressRepository.GetQueriable().FirstOrDefaultAsync(
                _ => _.Id == preferredAddressId && _.ApplicantId == applicantId);

            if (preferredAddressDb == null)
                throw GetValidationException("Not found preferred address.");

            Mapper.Instance.Map(preferredAddressVM, preferredAddressDb);

            _preferredAddressRepository.UpdateButIngoreSomeFields(preferredAddressDb, _ => _.ApplicantId);
            await SaveChangesAsync();
        }

        public async Task DeletePreferredAddressAsync(string preferredAddressId, string userId)
        {
            var preferredAddressDb = await _preferredAddressRepository.GetQueriable().FirstOrDefaultAsync(
                _ => _.Id == preferredAddressId && _.Applicant.CreatorId == userId);

            if (preferredAddressDb == null)
                throw GetValidationException("Not found preferred address.");
            _preferredAddressRepository.Delete(preferredAddressDb);
            await SaveChangesAsync();
        }


        private async Task<Applicant> GetUserApplicant(string userId)
        {
            var companyDb = await _applicantRepository.GetQueriable().FirstOrDefaultAsync(_ => _.CreatorId == userId);

            if (companyDb == null)
                throw GetValidationException("User has not applicant");
            return companyDb;
        }
    }
}