﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.Models.MyAccount;
using Data;
using Data.Entities.Account;
using Data.Entities.ApplicantEntities;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using FluentValidation;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace BusinessLogic.Services.ForAuthorized
{
    public interface IAccountService : IBaseService
    {
        Task RegisterAsCompanyAsync(CompanyRegisterVM companyVm, string absoluteUrl);
        Task<JwtResponseVM> TokenAsync(string userName, string password);
        Task<JwtResponseVM> RefreshTokenAsync(Guid refreshToken);
        Task LogoutAsync(Guid refreshToken);
        Task SetLanguageAsync(string userId, string language);

        Task RegisterAsApplicantAsync(ApplicantRegisterVM applicantVm, string absoluteUrl);

        Task<ApplicationUser> GetMyShortInfoAsync(string userId);

        Task ConfirmEmailAsync(string userId, string token);

        //Task ResendConfirmationEmailAsync(string userId, string absolutUrl);
        Task ResendConfirmationEmailAsync(string email, string password, string absolutUrl);

        Task ChangePasswordAsync(string userId, string currentPassword, string newPassword);
        Task SendPasswordResettingEmailAsync(string email, string absolutUrl);
        Task ResetPasswordAsync(string userId, string token, string newPassword);
        Task SendEmailChangingEmailAsync(string userId, string newEmail, string absolutUrl);

        Task ChangeEmailAsync(string userId, string newEmail, string token);
        //Task<ApplicationUser> CheckPasswordAsync(string userName, string password);
        //Task<IList<string>> GetUserRolesAsync(string userId);
    }

    public class AccountService : BaseService, IAccountService
    {
        private readonly IRepository<ApplicationUser> _applicationUserRepository;
        private readonly IRepository<Company> _companyRepository;
        private readonly IMailService _mailService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IRepository<UserSessionRefreshToken> _userSessionRefreshTokenRepository;
        private readonly IRepository<UserSession> _userSessionRepository;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICustomLogger _customLogger;


        public AccountService(IUnitOfWork u,
            IRepository<ApplicationUser> applicationUserRepository,
            UserManager<ApplicationUser> userManager,
            IRepository<UserSession> userSessionRepository,
            IRepository<UserSessionRefreshToken> userSessionRefreshTokenRepository,
            IRepository<Company> companyRepository, IMailService mailService,
            IHttpContextAccessor httpContextAccessor,
            ICustomLogger customLogger
            ) : base(u)
        {
            _applicationUserRepository = applicationUserRepository;
            _userManager = userManager;
            _companyRepository = companyRepository;
            _mailService = mailService;
            _userSessionRepository = userSessionRepository;
            _userSessionRefreshTokenRepository = userSessionRefreshTokenRepository;
            _httpContextAccessor = httpContextAccessor;
            _customLogger = customLogger;
        }

        public async Task RegisterAsCompanyAsync(CompanyRegisterVM companyVm, string absolutUrl)
        {
            var normalizedCompanyName = companyVm.CompanyName.ToUpper();

            IQueryable<Company> companiesQ;
            var compamiIdvm = companyVm.CompanyId;
            if (!string.IsNullOrWhiteSpace(compamiIdvm)) {
                compamiIdvm = companyVm.CompanyId;
                companiesQ = _companyRepository.GetQueriable()
                    .Where(_ => _.NormalizedName == normalizedCompanyName || _.Id == compamiIdvm);
            }
            else {
                compamiIdvm = null;
                companiesQ = _companyRepository.GetQueriable().Where(_ => _.NormalizedName == normalizedCompanyName);
            }


            var comsDbList = await companiesQ.Select(_ => new {_.Id, _.NormalizedName}).ToListAsync();
            if (comsDbList.Count > 0) {
                if (compamiIdvm != null && comsDbList.Any(_ => _.Id == compamiIdvm))
                    throw GetValidationException("A company with this id exists.", HttpStatusCode.Conflict);
                if (comsDbList.Any(_ => _.NormalizedName == normalizedCompanyName))
                    throw GetValidationException("A company with this name exists.", HttpStatusCode.Conflict);
            }

            var companyId = compamiIdvm ?? GeneralUtils.CreateUniqId();


            var addrDb = await CreateAddressData(companyVm.MainOfficeAddress, false);

            var userId = GeneralUtils.CreateUniqId();
            var user = new ApplicationUser
            {
                Companies = new List<Company>
                {
                    new Company
                    {
                        Id = companyId,
                        Name = companyVm.CompanyName,
                        NormalizedName = normalizedCompanyName,
                        CreatorId = userId,
                        IndustryId = companyVm.IndustryId,
                        EmployeeCount = companyVm.EmployeeCount,
                        IsDisabled = true,
                        ContactEmail = companyVm.Email,
                        Filials = new List<CompanyFilial>
                        {
                            new CompanyFilial
                            {
                                Id = GeneralUtils.CreateUniqId(),
                                Name = companyVm.CompanyName,
                                Address = addrDb,
                                CompanyId = companyId,
                                EmployeeCount = companyVm.EmployeeCount,
                                IsMain = true
                            }
                        }
                    }
                }
            };


            await RegisterUser(user, companyVm, userId, UserType.Company, AppConstants.Roles.Company, absolutUrl);
            await SaveChangesAsync();
        }

        public async Task<JwtResponseVM> TokenAsync(string userName, string password)
        {
            var user = await CheckPasswordAsync(userName, password);
            var jwtResponseVM = await GenerateTokenAsync(user.Id);
            var userSessionDb = CreateSessionDb(jwtResponseVM.RefreshToken, user.Id);
            var userSessionRtDb = CreateSessionRtDb(jwtResponseVM.RefreshToken, userSessionDb.InitialRefreshToken);
            _userSessionRepository.Add(userSessionDb);
            _userSessionRefreshTokenRepository.Add(userSessionRtDb);
            await SaveChangesAsync();
            return jwtResponseVM;
        }

        public async Task<JwtResponseVM> RefreshTokenAsync(Guid refreshToken)
        {
            var tokenDb = await _userSessionRefreshTokenRepository.GetQueriable().Include(_ => _.InitialRefreshToken)
                .FirstOrDefaultAsync(_ => _.RefreshToken == refreshToken);
            if (tokenDb == null) throw GetValidationException("Token not found.", HttpStatusCode.NotFound);

            var tokenStatus = tokenDb.Status;
            if (tokenStatus != TokenStatus.Unused) {
                if (tokenStatus == TokenStatus.Used) {
                    // close session, becouse maybe hack, tokens was stolen
                    var initialTokenId = tokenDb.InitialRefreshToken.InitialRefreshToken;
                    var unusedTokenDb = await _userSessionRefreshTokenRepository.GetQueriable()
                        .FirstOrDefaultAsync(_ =>
                            _.InitialRefreshToken.InitialRefreshToken == initialTokenId &&
                            _.Status == TokenStatus.Unused);
                    if (unusedTokenDb != null) {
                        unusedTokenDb.Status = TokenStatus.RevokedBySystem;
                        _userSessionRefreshTokenRepository.UpdateButIgnoreId(unusedTokenDb);
                        unusedTokenDb.InitialRefreshToken.Status = SessionStatus.LogoutedBySystem;
                        _userSessionRepository.UpdateButIgnoreId(unusedTokenDb.InitialRefreshToken);
                        await SaveChangesAsync();
                    }
                }
                throw GetValidationException("Invalid token");
            }
            if (tokenDb.RtExpiresOnUtc <= DateTime.UtcNow) throw GetValidationException("Token expired.");

            if (tokenDb.InitialRefreshToken.Status != SessionStatus.Logined) {
                throw GetValidationException("Token expired.");
            }

            var jwtResponseVM = await GenerateTokenAsync(tokenDb.InitialRefreshToken.UserId);


            var newTokenDb =
                CreateSessionRtDb(jwtResponseVM.RefreshToken, tokenDb.InitialRefreshToken.InitialRefreshToken);


            tokenDb.Status = TokenStatus.Used;
            _userSessionRefreshTokenRepository.UpdateButIgnoreId(tokenDb);
            _userSessionRefreshTokenRepository.Add(newTokenDb);
            await SaveChangesAsync();
            return jwtResponseVM;
        }


        public async Task LogoutAsync(Guid refreshToken)
        {
            var tokenDb = await _userSessionRefreshTokenRepository.GetQueriable().Include(_ => _.InitialRefreshToken)
                .FirstOrDefaultAsync(_ => _.RefreshToken == refreshToken);
            if (tokenDb == null) throw GetValidationException("Token not found.", HttpStatusCode.NotFound);
            if (tokenDb.Status == TokenStatus.Unused) {
                tokenDb.Status = TokenStatus.Revoked;
                _userSessionRefreshTokenRepository.UpdateButIgnoreId(tokenDb);

                tokenDb.InitialRefreshToken.Status = SessionStatus.Logouted;
                _userSessionRepository.UpdateButIgnoreId(tokenDb.InitialRefreshToken);
                await SaveChangesAsync();
            }
        }

        public async Task RegisterAsApplicantAsync(ApplicantRegisterVM applicantVm, string absoluteUrl)
        {
            var userId = GeneralUtils.CreateUniqId();

            var user = new ApplicationUser
            {
                Applicants = new List<Applicant>
                {
                    new Applicant
                    {
                        Id = GeneralUtils.CreateUniqId(),
                        FullName = applicantVm.FullName,
                        CreatorId = userId,
                        ContactEmail = applicantVm.Email
                    }
                }
            };

            await RegisterUser(user, applicantVm, userId, UserType.Applicant, AppConstants.Roles.Applicant,
                absoluteUrl);
            await SaveChangesAsync();
        }

        public async Task<ApplicationUser> GetMyShortInfoAsync(string userId)
        {
            var user = await _applicationUserRepository.GetQueriable().FirstOrDefaultAsync(_ => _.Id == userId);
            if (user == null) throw new ValidationException("User not found.");
            return user;
        }


        public async Task ConfirmEmailAsync(string userId, string token)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null) throw GetValidationException("UserId", "User not found.", HttpStatusCode.NotFound);

            if (user.EmailConfirmed) throw GetValidationException("Email is already confirmed.");

            var result = await _userManager.ConfirmEmailAsync(user, token);
            if (!result.Succeeded)
                throw GetValidationException(result);
            if (user.Type == UserType.Company) {
                var company = await _companyRepository.GetQueriable().FirstAsync(_ => _.CreatorId == userId);
                _companyRepository.Activate(company);
            }
            await SaveChangesAsync();
        }

        //public async Task ResendConfirmationEmailAsync(string userId, string absolutUrl)
        //{
        //    var user = await _userManager.FindByIdAsync(userId);
        //    var confirmEmailToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
        //    await _mailService.SendConfirmationEmailAsync(user.Email, user.Id, confirmEmailToken, absolutUrl);
        //}

        public async Task ResendConfirmationEmailAsync(string email, string password, string absolutUrl)
        {
            var user = await _userManager.FindByNameAsync(email);
            if (user == null) throw GetValidationException("User not found.");
            if (user.EmailConfirmed) throw GetValidationException("Email is already confirmed.");
            var passwordIsUser = await _userManager.CheckPasswordAsync(user, password);
            if (!passwordIsUser) throw GetValidationException("User not found.");

            var confirmEmailToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            await _mailService.SendConfirmationEmailAsync(user.Email, user.Id, confirmEmailToken, absolutUrl, false);
        }

        public async Task ChangePasswordAsync(string userId, string currentPassword, string newPassword)
        {
            // todo, close all sessions
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw GetValidationException("User not found", HttpStatusCode.NotFound);
            var res = await _userManager.ChangePasswordAsync(user, currentPassword, newPassword);
            ThrowIfNotSuccess(res);
            await CloseAllSessionsAsync(userId);
            //await SaveChangesAsync();
        }

        public async Task SendPasswordResettingEmailAsync(string email, string absolutUrl)
        {
            var user = await _userManager.FindByNameAsync(email);
            if (user == null)
                throw GetValidationException("User not found.");
            var token = await _userManager.GeneratePasswordResetTokenAsync(user);

            await _mailService.SendPasswordResettingEmailAsync(user.Email, user.Id, token, absolutUrl);
        }

        public async Task ResetPasswordAsync(string userId, string token, string newPassword)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw GetValidationException("User not found");
            var res = await _userManager.ResetPasswordAsync(user, token, newPassword);
            ThrowIfNotSuccess(res);
        }

        public async Task SendEmailChangingEmailAsync(string userId, string newEmail, string absolutUrl)
        {
            var normalizedNewEmail = newEmail.ToUpper();
            var existsEmail = await _applicationUserRepository.GetQueriable()
                .AnyAsync(_ => _.NormalizedEmail == normalizedNewEmail && _.Id != userId);
            if (existsEmail) throw GetValidationException("New Email", "New email exists in other user.");

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
                throw GetValidationException("User not found.");
            if (user.NormalizedEmail == normalizedNewEmail)
                throw GetValidationException("New Email", "Cannot set new email as current email.");

            var token = await _userManager.GenerateChangeEmailTokenAsync(user, newEmail);

            await _mailService.SendEmailChangingEmailAsync(newEmail, token, absolutUrl);
        }

        public async Task ChangeEmailAsync(string userId, string newEmail, string token)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null) throw GetValidationException("User not found.");
            var res = await _userManager.ChangeEmailAsync(user, newEmail, token);
            ThrowIfNotSuccess(res);
            user.UserName = newEmail;
            user.NormalizedUserName = newEmail.ToUpper();
            await _userManager.UpdateNormalizedUserNameAsync(user);
            await SaveChangesAsync();
        }

        public async Task CloseAllSessionsAsync(string userId)
        {
            await InternalCloseAllSessionsAsync(userId);
            await SaveChangesAsync();
        }

        public async Task SetLanguageAsync(string userId, string language)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null) throw GetValidationException("User not found.", HttpStatusCode.NotFound);
            user.Language = language;
            _applicationUserRepository.UpdateSomeFields(user, _ => _.Language);
            await SaveChangesAsync();
        }


        private async Task InternalCloseAllSessionsAsync(string userId)
        {
            var q = from ses in _userSessionRepository.GetQueriable()
                    .Where(_ => _.UserId == userId && _.Status == SessionStatus.Logined)
                join tok in _userSessionRefreshTokenRepository.GetQueriable() on
                    new
                    {
                        ses.InitialRefreshToken,
                        Status = TokenStatus.Unused
                    } equals new
                    {
                        InitialRefreshToken = tok.InitialRefreshTokenId,
                        tok.Status
                    }
                select new {Session = ses, Token = tok};


            //var q = _userSessionRepository.GetQueriable()
            //    .Where(_ => _.UserId == userId && _.Status == SessionStatus.Logined).Join(
            //        _userSessionRefreshTokenRepository.GetQueriable().Where(_ => _.Status == TokenStatus.Unused),
            //        _ => _.InitialRefreshToken, _ => _.InitialRefreshTokenId,
            //        (session, token) => new { Session = session, Token = token });

            var userSessions = await q.ToListAsync();
            foreach (var userSession in userSessions) {
                userSession.Token.Status = TokenStatus.RevokedBySystem;
                _userSessionRefreshTokenRepository.UpdateButIgnoreId(userSession.Token);

                userSession.Session.Status = SessionStatus.LogoutedBySystem;
                _userSessionRepository.UpdateButIgnoreId(userSession.Session);
            }
        }

        private static UserSession CreateSessionDb(Guid refreshToken, string userId)
        {
            return new UserSession
            {
                InitialRefreshToken = refreshToken,
                UserId = userId,
                Status = SessionStatus.Logined
            };
        }

        private static UserSessionRefreshToken CreateSessionRtDb(Guid refreshToken, Guid initialRefreshTokenId)
        {
            return new UserSessionRefreshToken
            {
                RefreshToken = refreshToken,
                InitialRefreshTokenId = initialRefreshTokenId,
                RtExpiresOnUtc = DateTime.UtcNow.AddDays(AppConstants.AuthOptions.RefreshTokenLifetimeDays),
                Status = TokenStatus.Unused
            };
        }

        private async Task<JwtResponseVM> GenerateTokenAsync(string userId)
        {
            var utcNow = DateTime.UtcNow;
            var expires = utcNow.Add(TimeSpan.FromMinutes(AppConstants.AuthOptions.Lifetime));

            var claims = new List<Claim>(4)
            {
                new Claim(AppConstants.AuthOptions.Claims.Keys.UserId, userId)
            };

            var userRoles = await _userManager.GetRolesAsync(new ApplicationUser {Id = userId});
            claims.AddRange(userRoles.Select(_ => new Claim(ClaimTypes.Role, _)));

            var jwt = new JwtSecurityToken(
                AppConstants.AuthOptions.Issuer,
                AppConstants.AuthOptions.Audience,
                notBefore: utcNow,
                claims: claims,
                expires: expires,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.ASCII.GetBytes(AppConstants.AuthOptions.Key)),
                    SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);


            var response = new JwtResponseVM(encodedJwt, Guid.NewGuid(), expires);


            return response;
        }


        public async Task<ApplicationUser> CheckPasswordAsync(string userName, string password)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null) throw GetValidationException("Password or username is not correct.");

            if (!user.EmailConfirmed)
                throw GetValidationException("NeedEmailConfirmation", "Email is not confirmed.");
            var res = await _userManager.CheckPasswordAsync(user, password);
            if (!res)
                throw GetValidationException("Password or username is not correct.");
            return user;
        }

        public Task<IList<string>> GetUserRolesAsync(string userId)
        {
            return _userManager.GetRolesAsync(new ApplicationUser {Id = userId});
        }

        private async Task RegisterUser(ApplicationUser user, RegisterVM registerVM, string userId, UserType userType,
            string roleName, string absolutUrl)
        {
            user.Id = userId;
            user.Email = registerVM.Email;
            user.UserName = registerVM.Email;
            user.Type = userType;
            user.Language = registerVM.Language;
            user.IpAddress = _httpContextAccessor.HttpContext.Connection.RemoteIpAddress.ToString();

            var identity = await _userManager.CreateAsync(user, registerVM.Password);

            if (!identity.Succeeded)
                throw GetValidationException(identity);
            await _userManager.AddToRoleAsync(user, roleName);

            var confirmEmailToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            try {
                await _mailService.SendConfirmationEmailAsync(user.Email, user.Id, confirmEmailToken, absolutUrl, true);
            }
            catch (Exception e) {
                await _customLogger.LogAsync(LogType.Error, e.Message,_httpContextAccessor.HttpContext);
            }
        }
    }
}