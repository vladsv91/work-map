using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Data;
using Data.Entities.Common;
using Infrastructure;

namespace BusinessLogic.Services
{
    public interface IBaseCrudService : IBaseService
    {
        
    }


    public abstract class BaseCrudService<TEntity, TId> : BaseService, IBaseCrudService where TEntity : IBaseEntity<TId>
    {
        private readonly IRepositoryBase<TEntity, TId> _entityRepository;

        protected BaseCrudService(IUnitOfWork u,
            IRepositoryBase<TEntity, TId> entityRepository
            ) : base(u)
        {
            _entityRepository = entityRepository;
        }

        public virtual Task<TEntity> GetByIdAsync(TId id)
        {
            return _entityRepository.FindAsync(id);
        }

        public virtual async Task UpdateAsync<TViewModel>(TViewModel viewModel, TId entityId)
        {
            TEntity entityDb = await GetDbEntityForUpdatingFromViewModel(viewModel, entityId);
            _entityRepository.Update(entityDb);
            await SaveChangesAsync();
        }

        public virtual async Task AddAsync<TViewModel>(TViewModel viewModel)
        {
            TEntity entityDb = viewModel.MapTo<TEntity>();
            _entityRepository.Add(entityDb);
            await SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(TId entityId)
        {
            TEntity entityDb = await GetByIdAsync(entityId);
            if (entityDb == null) {
                throw GetValidationException("Not found", HttpStatusCode.NotFound);
            }
            _entityRepository.Delete(entityDb);
            await SaveChangesAsync();
        }


        // for overriding
        protected virtual Task<TEntity> GetEntityForUpdatingByIdAsync(TId entityId)
        {
            return GetByIdAsync(entityId);
        }

        protected virtual async Task<TEntity> GetDbEntityForUpdatingFromViewModel<TViewModel>(TViewModel viewModel, TId entityId)
        {
            TEntity entityDb = await GetEntityForUpdatingByIdAsync(entityId);
            Mapper.Instance.Map(viewModel, entityDb);
            return entityDb;
        }

        protected virtual void ValidateEntity<TViewModel>(TViewModel viewModel)
        {

        }

        protected virtual async Task<TEntity> GetDbEntityForAddingFromViewModel<TViewModel>(TViewModel viewModel, TId entityId)
        {
            TEntity entityDb = viewModel.MapTo<TEntity>();
            return entityDb;
        }

    }
}