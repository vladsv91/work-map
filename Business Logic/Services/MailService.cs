﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using BusinessLogic.Models.Common;
using BusinessLogic.Utils;
using Infrastructure;
using Localization.Emails;
using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;

namespace BusinessLogic.Services
{
    public interface IMailService : IBaseService
    {
        Task SendEmailAsync(string toEmail, string subject, string message, string absoluteUrl);
        Task SendConfirmationEmailAsync(string toEmail, string userId, string token, string absoluteUrl, bool isRegistering);
        Task SendPasswordResettingEmailAsync(string toEmail, string userId, string token, string absoluteUrl);
        Task SendEmailChangingEmailAsync(string newEmail, string token, string absoluteUrl);

        Task SendEmailToCompanyAboutReceivingResume(string companyEmail, string absoluteUrl, SendingResumeEmail sendingResumeEmail);

        Task SendEmailToAdminAboutContactUsAsync(ContactUsCreateVM contactUsModel, DateTime createdOn, string absoluteUrl);
    }

    public class MailService : BaseService, IMailService
    {
        public MailService() : base(null) { }

        //private static readonly string MainText;

        //static MailService()
        //{

        //}

        public async Task SendEmailAsync(string toEmail, string subject, string message, string absoluteUrl)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress(AppConstants.AppName, Settings.EmailCredentials.Email));
            emailMessage.To.Add(new MailboxAddress("", toEmail));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(TextFormat.Html)
            {
                Text = GetFullEmailText(message, absoluteUrl)
            };

            using (var client = new SmtpClient()) {
                await client.ConnectAsync(Settings.EmailCredentials.Host, 25, false);
                await client.AuthenticateAsync(Settings.EmailCredentials.Email, Settings.EmailCredentials.Password);
                await client.SendAsync(emailMessage);

                // todo, keep authentication
                await client.DisconnectAsync(true);
            }
        }


        public Task SendConfirmationEmailAsync(string toEmail, string userId, string token, string absoluteUrl, bool isRegistering)
        {
            return SendEmailAsync(toEmail, Emails.EmailConfirmation,
                GetMainHeader(Emails.EmailConfirmation) +
                (isRegistering ? GetParagraph(Emails.RegistrationThank) : "") +
                GetLink($"{absoluteUrl}/confirm-email?userId={userId}&token={token}", Emails.ClickHereToConfirmEmail),
                absoluteUrl);
        }

        public Task SendPasswordResettingEmailAsync(string toEmail, string userId, string token, string absoluteUrl)
        {
            return SendEmailAsync(toEmail, Emails.PasswordResetting,
                GetMainHeader(Emails.PasswordResetting) +
                GetLink($"{absoluteUrl}/reset-password?userId={userId}&token={token}", Emails.ClickHereToResetPassword),
                absoluteUrl);
        }

        public Task SendEmailChangingEmailAsync(string newEmail, string token, string absoluteUrl)
        {
            return SendEmailAsync(newEmail, Emails.ChangingEmail,
                GetMainHeader(Emails.ChangingEmail) +
                GetLink($"{absoluteUrl}/my/settings/changing-email?newEmail={newEmail}&token={token}", Emails.ConfirmNewEmailLink),
                absoluteUrl);
        }

        public Task SendEmailToCompanyAboutReceivingResume(string companyEmail, string absoluteUrl, SendingResumeEmail sendingResumeEmail)
        {
            var resume = sendingResumeEmail.Resume;
            var applicant = resume.Applicant;
            var dateOfBirth = applicant.DateOfBirth;
            return SendEmailAsync(companyEmail, Emails.NewResumeSending,
                GetMainHeader(Emails.NewResumeSending) +
                GetParagraph(Emails.NewResumeText) +
                GetLabelValueParagraph(Emails.Vacancy,
                    GetLink($"{absoluteUrl}/vacancy/{sendingResumeEmail.Vacancy.Id}", sendingResumeEmail.Vacancy.Name)) +
                GetLabelValueParagraph(Emails.Resume,
                    GetLink($"{absoluteUrl}/resume/{resume.Id}", resume.Name)) +
                GetLabelValueParagraph(Emails.ApplicantFullName, applicant.FullName) +
                (resume.ResumeFileId!=null
                    ? GetLabelValueParagraph(Emails.ResumeFile, GetLink(BlUtils.CreateResumeFileUrl(resume.Id), applicant.FullName + ": " + resume.Name,
                        $@"download=""{applicant.FullName + ": " + resume.Name}"""))
                    : "") +
                (dateOfBirth != null
                    ? GetLabelValueParagraph(Emails.Age, GeneralUtils.GetDifferenceInYears(dateOfBirth.Value, DateTime.UtcNow).ToString())
                    : "") +
                (applicant.ContactEmail != null ? GetLabelValueParagraph(Emails.ContactEmail, applicant.ContactEmail) : "") +
                (applicant.ContactPhone != null ? GetLabelValueParagraph(Emails.ContactPhoneNumber, applicant.ContactPhone) : "") +
                GetLabelValueParagraph(Emails.ExperienceYears, resume.ExperienceYears.ToString(CultureInfo.InvariantCulture)) +
                GetLabelValueParagraph(Emails.Message, sendingResumeEmail.Message),
                absoluteUrl);
        }

        public Task SendEmailToAdminAboutContactUsAsync(ContactUsCreateVM contactUsModel, DateTime createdOn, string absoluteUrl)
        {
            return SendEmailAsync(Settings.ContactEmails.AdminEmail, "New contact us",
                GetMainHeader($"New contact us at {createdOn:F}") +
                GetLabelValueParagraph("Message", contactUsModel.Message) +
                GetLabelValueParagraph("Full name", contactUsModel.FullName) +
                GetLabelValueParagraph("Email", contactUsModel.Email) +
                GetLabelValueParagraph("Phone number", contactUsModel.PhoneNumber), absoluteUrl);
        }

        private static string GetLabelValueParagraph(string label, string value)
        {
            return $@"<p style=""color: #707070; font-size: 14px; margin: 5px 0; "">{
                    label
                }: <span style=""color: #494949;"">{value}</span></p>";
        }


        private static string GetMainHeader(string innerHtml)
        {
            return $@"<h1 style=""font-size: 18px; margin: 10px 0; color: #323232;"">{innerHtml}</h1>";
        }

        private static string GetLink(string href, string innerHtml, string additionalAttrs = "")
        {
            return $@"<a href=""{href}"" {additionalAttrs}>{innerHtml}</a>";
        }

        private static string GetParagraph(string innerHtml)
        {
            return $@"<p style=""text-indent: 20px; color: #4d4d4d;"">{innerHtml}</p>";
        }

        private static string GetClearFix()
        {
            return @"<div style=""clear: both;""></div>";
        }

        private static string GetFullEmailText(string message, string absoluteUrl)
        {
            return $@"

<div style=""font-family: 'Roboto', sans-serif"">
    <div style=""background-color: #2f3e9b;
        min-height: 78px;
        box-sizing: border-box;
        padding: 22px 40px;
        font-size: 33px;"">
        <a href=""{absoluteUrl}"" style=""color: white; text-decoration: none;"">{AppConstants.AppName}</a>
        <span style=""color: #95a2e7; font-size: 27px; padding: 2px 20px; float: right;"">
            {Emails.HeaderText}
        </span>
        {GetClearFix()}
     </div>


    <div style=""padding: 30px; background-color: #fafafa; color: #4d4d4d; font-size: 16px; "">
        {message}
    </div>
</div>

";
        }
    }
}