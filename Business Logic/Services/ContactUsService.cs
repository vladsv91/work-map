﻿using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.Common;
using Data;
using Data.Entities.Common;
using Infrastructure;

//using System.Linq.Expressions;

namespace BusinessLogic.Services
{
    public interface IContactUsService : IBaseService
    {
        Task Create(ContactUsCreateVM model, string absoluteUrl);
    }

    public class ContactUsService : BaseService, IContactUsService
    {
        private readonly IRepository<ContactUs> _contactUsRepository;
        private readonly IMailService _mailService;

        public ContactUsService(IUnitOfWork u,
            IRepository<ContactUs> contactUsRepository,
            IMailService mailService) : base(u)
        {
            _contactUsRepository = contactUsRepository;
            _mailService = mailService;
        }


        public async Task Create(ContactUsCreateVM model, string absoluteUrl)
        {
            var contactUsDb = Mapper.Instance.Map<ContactUsCreateVM, ContactUs>(model);
            _contactUsRepository.Add(contactUsDb);
            await SaveChangesAsync();

            Task.Factory.StartNew(() =>
            {
                AppCultures.SetAdminCulture();
                _mailService.SendEmailToAdminAboutContactUsAsync(model, contactUsDb.CreatedOnUtc, absoluteUrl).Forget();
            }).Forget();
        }
    }
}