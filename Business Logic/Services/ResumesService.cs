﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.Common;
using BusinessLogic.Models.Resumes;
using BusinessLogic.Utils;
using Data;
using Data.Entities.ApplicantEntities;
using Data.Entities.Common;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services
{
    public interface IResumesService : IBaseSearchService
    {
        Task<PaginatedDataResultVM<ResumeShortInfoReadVM>> GetResumesByFilterAsync(
            ResumesListFilterReadVM filter);

        Task<ResumeDetailsReadVM> GetResumeDetailsAsync(string resumeId);
        Task<FileData> GetApplicantPhotoAsync(string applicantId);
        Task<FileData> GetResumeFileAsync(string resumeId);
        Task<List<BaseEntityReadVm>> GetListForStitemapAsync();
    }


    public class ResumesService : BaseSearchService, IResumesService
    {
        private readonly IRepository<Resume> _resumeRepository;

        public ResumesService(IUnitOfWork u, IRepository<Resume> resumeRepository) : base(u)
        {
            _resumeRepository = resumeRepository;
        }


        public Task<PaginatedDataResultVM<ResumeShortInfoReadVM>> GetResumesByFilterAsync(
            ResumesListFilterReadVM filter)
        {
            return new ResumesSearchItemsProcessor(filter).GetItemsByFilterAsync(
                GetStatusLookupQueryable(_resumeRepository));
        }

        public Task<List<BaseEntityReadVm>> GetListForStitemapAsync()
        {
            return GetStatusLookupQueryable(_resumeRepository).Select(SelectBaseEntityExpression).ToListAsync();
        }


        public async Task<ResumeDetailsReadVM> GetResumeDetailsAsync(string resumeId)
        {
            var c = await GetStatusLookupQueryable(_resumeRepository).Where(_ => _.Id == resumeId)
                .Include(_ => _.Applicant)
                .FirstOrDefaultAsync();
            ValidateApplicantResumeExisting(c);
            return Mapper.Instance.Map<ResumeDetailsReadVM>(c);
        }

        public async Task<FileData> GetApplicantPhotoAsync(string applicantId)
        {
            var c = await GetStatusLookupQueryable(_resumeRepository).Where(_ => _.ApplicantId == applicantId)
                .Include(_ => _.Applicant.Photo).FirstOrDefaultAsync();
            ValidateApplicantResumeExisting(c);
            if (c.Applicant.Photo == null) throw GetValidationException("Resume has no photo.", HttpStatusCode.NotFound);
            return c.Applicant.Photo;
        }

        public async Task<FileData> GetResumeFileAsync(string resumeId)
        {
            var c = await GetStatusLookupQueryable(_resumeRepository).Where(_ => _.Id == resumeId)
                .Include(_ => _.ResumeFile)
                .FirstOrDefaultAsync();
            ValidateApplicantResumeExisting(c);
            return c.ResumeFile;
        }
    }


    public class ResumesSearchItemsProcessor : BaseSearchItemsProcessor<ResumeShortInfoReadVM, Resume,
        ResumesListFilterReadVM>
    {
        public ResumesSearchItemsProcessor(ResumesListFilterReadVM filter) : base(filter) { }

        protected override IQueryable<Resume> FilterData(IQueryable<Resume> q)
        {
            q = q.GetFilterWhere(Filter.LessThanSalaryPerMonth, v => c => c.SalaryPerMonth < v)
                .GetFilterWhere(Filter.MoreThanSalaryPerMonth, v => c => c.SalaryPerMonth > v)
                .GetFilterWhere(Filter.MoreThanExperienceYears, v => c => c.ExperienceYears > v)
                .GetFilterWhere(Filter.LessThanExperienceYears, v => c => c.ExperienceYears < v)
                .GetFilterClassWhere(Filter.ProfessionId, v => c => c.ProfessionId == v);

            var address = Filter.Address;
            if (address == null) return q;

            return FilterResumesByLocation(q, address.Latitude, address.Longitude, address.RadiusInKilometers);
        }

        public static IQueryable<Resume> FilterResumesByLocation(IQueryable<Resume> q, decimal lat, decimal lon, decimal radius)
        {
            GeneralUtils.GetLatLngRadiusesSqrts(radius, out var latitudeRadiusSqr,
                out var longitudeRadiusSqr);
            return q.Where(c => c.Applicant.PreferredAddresses.Any(
                a => (lat - a.Address.Latitude) *
                     (lat - a.Address.Latitude) / latitudeRadiusSqr +
                     (lon - a.Address.Longitude) *
                     (lon - a.Address.Longitude) / longitudeRadiusSqr <= 1));
        }

        protected override IQueryable<Resume> FilterBySearchString(IQueryable<Resume> q,
            string normalizedSearchString, string spls0, string spls1, string spls2)
        {
            return q.Where(c =>
                c.Name.Contains(normalizedSearchString)
                || c.Name.Contains(spls0)
                || c.Name.Contains(spls1)
                || c.Name.Contains(spls2)

                || c.Description.Contains(normalizedSearchString)
                || c.Description.Contains(spls0)
                || c.Description.Contains(spls1)
                || c.Description.Contains(spls2)

                || c.Skills.Contains(normalizedSearchString)
            );
        }

        protected override IQueryable<Resume> Includes(IQueryable<Resume> q)
        {
            if (Filter.IncludeAddresses)
                return q.Include(_ => _.Applicant.PreferredAddresses).ThenInclude(_ => _.Address);

            return q.Include(_ => _.Applicant);
        }

        protected override IOrderedEnumerable<Resume> SortBySearchString(IEnumerable<Resume> list,
            string normalizedSearchString)
        {
            return list.OrderBy(c =>
            {
                var normName = c.Name.ToUpper();
                if (normName == normalizedSearchString) return 1;
                if (normName.StartsWith(normalizedSearchString)) return 2;
                if (normName.Contains(normalizedSearchString)) return 3;

                if (c.Description.ToUpperContainsSafe(normalizedSearchString)) return 4;
                if (c.Skills.ToUpperContainsSafe(normalizedSearchString)) return 5;

                return 100;
            });
        }
    }
}