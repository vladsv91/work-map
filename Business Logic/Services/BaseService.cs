﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Threading.Tasks;
using BusinessLogic.Models.Common;
using BusinessLogic.Utils;
using Data;
using Data.Entities.ApplicantEntities;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using FluentValidation.Results;
using Google.Maps;
using Google.Maps.Geocoding;
using Infrastructure;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services
{
    public interface IBaseService { }

    public class BaseService : IBaseService
    {
        protected readonly IUnitOfWork UnitOfWork;

        public BaseService(IUnitOfWork unitOfWork)
        {
            UnitOfWork = unitOfWork;
        }

        protected Task SaveChangesAsync()
        {
            return UnitOfWork.SaveChangesAsync();
        }

        public static void ThrowIfNotSuccess(IdentityResult identity)
        {
            if (!identity.Succeeded)
                throw GetValidationException(identity, HttpStatusCode.BadRequest);
        }

        public static CustomValidationException GetValidationException(IdentityResult identity, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            return new CustomValidationException(identity.Errors.Select(_ => new ValidationFailure(_.Code, _.Description)), statusCode);
        }

        public static CustomValidationException GetValidationException(string message, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            return new CustomValidationException(message, statusCode);
        }

        public static CustomValidationException GetValidationException(string property, string message, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            return new CustomValidationException(new[] {new ValidationFailure(property, message)}, statusCode);
        }


        protected async Task SetAddressData(Address addressDb, AddressEditVM address, bool throwIfHasNoComponent)
        {
            var request = new GeocodingRequest
            {
                Address = new LatLng(address.Latitude, address.Longitude)
            };
            var response = await new GeocodingService().GetResponseAsync(request);

            if (response.Status == ServiceResponseStatus.Ok && response.Results.Length > 0)
                ServiceUtils.SetAddressData(addressDb, response, throwIfHasNoComponent);
            else
                throw GetValidationException("Address", response.ErrorMessage ?? response.Status.ToString(), HttpStatusCode.BadRequest);
        }

        protected async Task<Address> CreateAddressData(AddressEditVM address, bool throwIfHasNoComponent = true)
        {
            var addr = new Address {Id = GeneralUtils.CreateUniqId()};
            await SetAddressData(addr, address, throwIfHasNoComponent);
            return addr;
        }


        protected void ValidateApplicantResumeExisting(Resume resume)
        {
            if (resume == null)
                throw GetValidationException("Resume not found.", HttpStatusCode.NotFound);
        }

        protected void ValidateApplicantResume(Resume resume)
        {
            ValidateApplicantResumeExisting(resume);

            if (resume.IsDisabled)
                throw GetValidationException("Resume is deactivated.", HttpStatusCode.BadRequest);
        }
    }
}