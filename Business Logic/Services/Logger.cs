﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Utils;
using Data;
using Data.Entities.Common;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace BusinessLogic.Services
{
    public interface ICustomLogger
    {
        Task LogAsync(LogType type, string message, HttpContext httpContext);
    }

    public class DbLogger : ICustomLogger
    {
        private readonly ICurrentUser _currentUser;
        private readonly IRepositoryBase<Log, Guid> _logRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DbLogger(ICurrentUser currentUser, IRepositoryBase<Log, Guid> logRepository, IUnitOfWork unitOfWork)
        {
            _currentUser = currentUser;
            _logRepository = logRepository;
            _unitOfWork = unitOfWork;
        }


        public Task LogAsync(LogType type, string message, HttpContext httpContext)
        {

#if DEBUG
            return Task.CompletedTask;
#endif

            var request = httpContext.Request;
            var headers = request.Headers;
            //byte[] requestBody = null;
            //if (request.Body.Length < 1024000) {
            //    // 1Mb
            //    using (var streamReader = new StreamReader(request.Body)) {
            //        requestBody = streamReader.ReadToEnd()
            //    }
            //}
            var log = new Log
            {
                Message = message,
                Type = type,
                IpAddress = httpContext.Connection.RemoteIpAddress.ToString(),
                RequestUrl = httpContext.Request.Path.Value + httpContext.Request.QueryString.Value,
                UserId = _currentUser.GetCurrentUserIdOrNull(httpContext.User),
                UserAgent = headers["User-Agent"].FirstOrDefault(),
                HttpMethod = request.Method ?? "Unknown",
                SerializedHeaders = JsonConvert.SerializeObject(headers),
                Host = httpContext.GetAbsoluteUrl()
            };

            try {
                _logRepository.Add(log);
                return _unitOfWork.SaveChangesAsync();
            }
            catch (Exception e) {
#if DEBUG
                throw;
#endif
                return Task.CompletedTask;
            }
        }
    }

    public class LogMigration
    {
        private readonly IRepositoryGuid<Log> _logRepository;
        private readonly IRepositoryGuid<MigratedLog> _migratedLogRepository;
        private readonly IUnitOfWork _unitOfWork;

        public LogMigration(IRepositoryGuid<Log> logRepository,
            IRepositoryGuid<MigratedLog> migratedLogRepository, IUnitOfWork unitOfWork)
        {
            _logRepository = logRepository;
            _migratedLogRepository = migratedLogRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task MigrateLog()
        {
            // TODO: Maybe optimize with sql query;
            var logs = await _logRepository.GetQueriable().ToListAsync();

            await _migratedLogRepository.AddRange(logs.Select(_=>_.MapTo<MigratedLog>()));

            _logRepository.RemoveRange(logs);

            await _unitOfWork.SaveChangesAsync();
        }
    }
}