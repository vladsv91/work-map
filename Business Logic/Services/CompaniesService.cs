﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using BusinessLogic.Models.Common;
using BusinessLogic.Models.Companies;
using BusinessLogic.Utils;
using Data;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using FluentValidation;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services
{
    public interface ICompaniesService : IBaseSearchService
    {
        Task<PaginatedDataResultVM<CompanyShortInfoWithFilialsReadVM>> GetCompaniesByFilterAsync(
            CompaniesListFilterReadVM filter);

        Task<CompanyDetailsReadDTO> GetCompanyDetailsAsync(string companyId);
        Task<List<CompanyFilialReadDTO>> GetCompanyFilialsAsync(string companyId);
        Task<List<Vacancy>> GetCompanyVacanciesAsync(string companyId);
        Task<List<CompanyPhoto>> GetCompanyPhotosAsync(string companyId);
        Task<FileData> GetCompanyMainPhotoAsync(string companyId);
        Task<FileData> GetCompanyPhotoAsync(string companyPhotoId);
        Task<List<Company>> GetListForStitemapAsync();
    }



    public class CompaniesService : BaseSearchService, ICompaniesService
    {
        private readonly IRepository<CompanyFilial> _companyFilialRepository;
        private readonly IRepository<CompanyPhoto> _companyPhotoRepository;
        private readonly IRepository<Company> _companyRepository;
        private readonly IRepository<Vacancy> _companyVacancyRepository;

        public CompaniesService(IUnitOfWork u,
            IRepository<CompanyFilial> companyFilialRepository,
            IRepository<CompanyPhoto> companyPhotoRepository,
            IRepository<Vacancy> companyVacancyRepository,
            IRepository<Company> companyRepository) : base(u)
        {
            _companyRepository = companyRepository;
            _companyFilialRepository = companyFilialRepository;
            _companyPhotoRepository = companyPhotoRepository;
            _companyVacancyRepository = companyVacancyRepository;
        }

        public Task<PaginatedDataResultVM<CompanyShortInfoWithFilialsReadVM>> GetCompaniesByFilterAsync(
            CompaniesListFilterReadVM filter)
        {
            return new CompaniesSearchItemsProcessor(filter).GetItemsByFilterAsync(
                GetStatusLookupQueryable(_companyRepository));
        }

        public Task<List<Company>> GetListForStitemapAsync()
        {
            return GetStatusLookupQueryable(_companyRepository).Include(_ => _.CompanyPhotos).ToListAsync();
        }


        public async Task<CompanyDetailsReadDTO> GetCompanyDetailsAsync(string companyId)
        {
            var c = await GetStatusLookupQueryable(_companyRepository).Where(_ => _.Id == companyId)
                .Select(_ => new CompanyDetailsReadDTO
                {
                    Id = _.Id,
                    Name = _.Name,
                    EmployeeCount = _.EmployeeCount,
                    IndustryId = _.IndustryId,
                    ContactEmail = _.ContactEmail,
                    WebsiteUrl = _.WebsiteUrl,
                    Description = _.Description,
                    ContactPhone = _.ContactPhone,
                    FilialsCount = _.Filials.Count(f => !f.IsDisabled),
                    VacanciesCount = _.Filials.Where(f => !f.IsDisabled).SelectMany(f => f.Vacancies)
                        .Count(f => !f.IsDisabled),
                    HasMainPhoto = _.MainPhotoId != null,
                    PhotosCount = _.CompanyPhotos.Count
                })
                .FirstOrDefaultAsync();
            if (c == null) throw ThrowNotExistCompany();
            return c;
        }

        private static ValidationException ThrowNotExistCompany()
        {
            return GetValidationException("Company not found", HttpStatusCode.NotFound);
        }

        public Task<List<CompanyFilialReadDTO>> GetCompanyFilialsAsync(string companyId)
        {
            var queryable = GetStatusLookupQueryable(_companyFilialRepository).Where(_ => _.CompanyId == companyId);
            return queryable.Select(_ =>
                new CompanyFilialReadDTO
                {
                    Id = _.Id,
                    Address = _.Address,
                    Name = _.Name,
                    EmployeeCount = _.EmployeeCount,
                    IsMain = _.IsMain,
                    VacanciesCount = _.Vacancies.Count(v => !v.IsDisabled)
                }
            ).ToListAsync();
        }

        public Task<List<Vacancy>> GetCompanyVacanciesAsync(string companyId)
        {
            var queryable = GetStatusLookupQueryable(_companyVacancyRepository).Include(_ => _.CompanyFilial.Address)
                .Where(_ => _.CompanyId == companyId);
            return queryable.ToListAsync();
        }

        public Task<List<CompanyPhoto>> GetCompanyPhotosAsync(string companyId)
        {
            var queryable = GetQueryable(_companyPhotoRepository).Include(_ => _.CompanyFilial)
                .Where(_ => _.CompanyId == companyId);
            return queryable.ToListAsync();
        }


        public async Task<FileData> GetCompanyMainPhotoAsync(string companyId)
        {
            var c = await GetStatusLookupQueryable(_companyRepository).Where(_ => _.Id == companyId)
                .Include(_=>_.MainPhoto).FirstOrDefaultAsync();
            if (c == null) throw ThrowNotExistCompany();
            if (c.MainPhoto == null) throw GetValidationException("Company has no main photo", HttpStatusCode.NotFound);
            return c.MainPhoto;
        }

        public async Task<FileData> GetCompanyPhotoAsync(string companyPhotoId)
        {
            var c = await GetQueryable(_companyPhotoRepository).Where(_ => _.Id == companyPhotoId)
                .Include(_ => _.Photo).Include(_ => _.Company).FirstOrDefaultAsync();
            if (c == null || c.Company.IsDisabled) throw GetValidationException("Company photo not found", HttpStatusCode.NotFound);
            return c.Photo;
        }
    }
    public class CompaniesSearchItemsProcessor : BaseSearchItemsProcessor<CompanyShortInfoWithFilialsReadVM, Company,
        CompaniesListFilterReadVM>
    {
        public CompaniesSearchItemsProcessor(CompaniesListFilterReadVM filter) : base(filter) { }

        protected override IQueryable<Company> FilterData(IQueryable<Company> q)
        {
            q = q.GetFilterClassWhere(Filter.IndustryId, v => c => c.IndustryId == v)
                .GetFilterWhere(Filter.LessThanEmployeeCount, v => c => c.EmployeeCount < v)
                .GetFilterWhere(Filter.MoreThanEmployeeCount, v => c => c.EmployeeCount > v)
                .GetFilterWhere(Filter.LessThanFilialsCount,
                    v => c => c.Filials.Count(_ => !_.IsDisabled) < v)
                .GetFilterWhere(Filter.MoreThanFilialsCount,
                    v => c => c.Filials.Count(_ => !_.IsDisabled) > v)
                .GetFilterWhere(Filter.LessThanVacanciesCount,
                    v => c => c.Filials.Where(_ => !_.IsDisabled).SelectMany(_ => _.Vacancies)
                                  .Count(_ => !_.IsDisabled) < v)
                .GetFilterWhere(Filter.MoreThanVacanciesCount,
                    v => c => c.Filials.Where(_ => !_.IsDisabled).SelectMany(_ => _.Vacancies)
                                  .Count(_ => !_.IsDisabled) > v);

            var address = Filter.Address;
            if (address == null) return q;

            //var lat = address.Latitude;
            //var lon = address.Longitude;
            //GeneralUtils.GetLatLngRadiusesSqrts(address.RadiusInKilometers, out var latitudeRadiusSqr,
            //    out var longitudeRadiusSqr);
            //return q.Where(c => c.Filials.Any(f =>
            //    !f.IsDisabled &&
            //    (lat - f.Address.Latitude) * (lat - f.Address.Latitude) / latitudeRadiusSqr +
            //    (lon - f.Address.Longitude) * (lon - f.Address.Longitude) / longitudeRadiusSqr <= 1));

            return FilterCompaniesByLocation(q, address.Latitude, address.Longitude, address.RadiusInKilometers);
        }

        public static IQueryable<Company> FilterCompaniesByLocation(IQueryable<Company> q, decimal lat, decimal lon, decimal radiusInKilometers)
        {
            GeneralUtils.GetLatLngRadiusesSqrts(radiusInKilometers, out var latitudeRadiusSqr,
                out var longitudeRadiusSqr);
            return q.Where(c => c.Filials.Any(f =>
                !f.IsDisabled &&
                (lat - f.Address.Latitude) * (lat - f.Address.Latitude) / latitudeRadiusSqr +
                (lon - f.Address.Longitude) * (lon - f.Address.Longitude) / longitudeRadiusSqr <= 1));
        }

        protected override IQueryable<Company> FilterBySearchString(IQueryable<Company> q,
            string normalizedSearchString, string spls0, string spls1, string spls2)
        {
            return q.Where(c =>
                c.Name.Contains(normalizedSearchString)
                || c.Name.Contains(spls0)
                || c.Name.Contains(spls1)
                || c.Name.Contains(spls2)

                //|| c.Description.Contains(normalizedSearchString)
                //|| c.Description.Contains(spls0)
                //|| c.Description.Contains(spls1)
                //|| c.Description.Contains(spls2)

                || c.ContactPhone.Contains(normalizedSearchString)
                || c.ContactEmail.Contains(normalizedSearchString)
                || c.WebsiteUrl.Contains(normalizedSearchString));
        }

        protected override IQueryable<Company> Includes(IQueryable<Company> q)
        {
            return Filter.IncludeAddresses ? q.Include(_ => _.Filials).ThenInclude(_ => _.Address) : q;
        }

        protected override IOrderedEnumerable<Company> SortBySearchString(IEnumerable<Company> list,
            string normalizedSearchString)
        {
            return list.OrderBy(c =>
            {
                var normName = c.Name.ToUpper();
                if (normName == normalizedSearchString)
                    return 1;
                if (normName.StartsWith(normalizedSearchString))
                    return 2;
                if (normName.Contains(normalizedSearchString))
                    return 3;

                if (c.Description.ToUpperContainsSafe(normalizedSearchString))
                    return 4;
                if (c.ContactPhone.ToUpperContainsSafe(normalizedSearchString))
                    return 5;
                if (c.ContactEmail.ToUpperContainsSafe(normalizedSearchString))
                    return 6;
                if (c.WebsiteUrl.ToUpperContainsSafe(normalizedSearchString))
                    return 7;

                return 100;
            });
        }

        protected override void AdjustList(List<Company> list)
        {
            if (Filter.IncludeAddresses)
                foreach (var company in list) company.Filials = company.Filials.Where(_ => !_.IsDisabled).ToList();
        }
    }
}