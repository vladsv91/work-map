﻿using System;
using System.IO;
using System.Threading.Tasks;
using Data;
using Data.Entities.Common;
using Infrastructure;
using Microsoft.AspNetCore.Http;

namespace BusinessLogic.Services
{
    public interface IFileService : IBaseService
    {
        Guid AddOrUpdateFileFile(IFormFile file, FileData oldFileData, FileKind fileKind, FileType fileType);
        void DeleteFile(FileData fileData);
        Task<FileData> GetFileAsync(Guid fileId);
    }

    public class FileService : BaseService, IFileService
    {
        private readonly IRepositoryGuid<FileData> _fileDataRepository;

        public FileService(IUnitOfWork u,
            IRepositoryGuid<FileData> fileDataRepository
            )
            : base(u)
        {
            _fileDataRepository = fileDataRepository;
        }



        public Guid AddOrUpdateFileFile(IFormFile file, FileData existingFileData, FileKind fileKind, FileType fileType)
        {
            var existingFileId = existingFileData?.Id;
            byte[] fileDataData;
            using (var stream = new BinaryReader(file.OpenReadStream()))
            {
                fileDataData = stream.ReadBytes((int)file.Length);
            }

            FileData fileData;
            bool hasImage = false;
            if (existingFileId != null)
            {
                hasImage = true;
                fileData = existingFileData;
            }
            else
            {
                existingFileId = Guid.NewGuid();
                fileData = new FileData { Id = existingFileId.Value };
            }

            fileData.FileExtension = Path.GetExtension(file.FileName);
            fileData.FileName = file.FileName.SubstringSafe(300);
            fileData.FileKind = fileKind;
            fileData.Data = fileDataData;
            fileData.FileType = fileType;


            if (hasImage)
            {
                _fileDataRepository.Update(fileData);
            }
            else
            {
                _fileDataRepository.Add(fileData);
            }
            return existingFileId.Value;
        }


        public void DeleteFile(FileData fileData)
        {
            _fileDataRepository.Delete(fileData);
        }

        // for users, who know fileId, veryyy confident
        public async Task<FileData> GetFileAsync(Guid fileId)
        {
            var fileData = await _fileDataRepository.FindAsync(fileId);
            if (fileData == null)
                throw GetValidationException("File not found");
            return fileData;
        }

    }
}