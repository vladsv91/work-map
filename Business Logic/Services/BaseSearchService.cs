﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.Common;
using Data;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services
{
    public interface IBaseSearchService : IBaseService { }

    public class BaseSearchItemsProcessor<TEntityResult, TEntity, TFilter>
        where TFilter : BaseSearchableFilterVM where TEntity : IBaseEntity
    {

        protected TFilter Filter;

        public BaseSearchItemsProcessor(TFilter filter)
        {
            Filter = filter;
        }


        protected  virtual IQueryable<TEntity> FilterData(IQueryable<TEntity> q)
        {
            return q;
        }

        // todo, hack, hack, hack
        protected virtual IQueryable<TEntity> FilterBySearchString(IQueryable<TEntity> q, string normalizedSearchString, string spls0, string spls1, string spls2)
        {
            return q;
        }
        protected virtual IQueryable<TEntity> Includes(IQueryable<TEntity> q)
        {
            return q;
        }

        //protected virtual IQueryable<TEntity> ApplySorting(IQueryable<TEntity> q)
        //{
        //    return q.OrderByDescending(_ => _.UpdatedOnUtc);
        //}

        protected virtual IOrderedEnumerable<TEntity> SortBySearchString(IEnumerable<TEntity> list, string normalizedSearchString)
        {
            throw new Exception();
            //return q.OrderByDescending(_ => _.UpdatedOnUtc);
        }

        protected virtual void AdjustList(List<TEntity> list)
        {
        }


        public async Task<PaginatedDataResultVM<TEntityResult>> GetItemsByFilterAsync(IQueryable<TEntity> q)
        {
            q = FilterData(q);

            var normalizedSearchString = Filter.SearchString;
            if (normalizedSearchString.NotNullAndWhiteSpace()) {
                normalizedSearchString = normalizedSearchString.ToUpper();
                var splitted = normalizedSearchString.Split(',');
                var splittedLength = splitted.Length;
                const int maxSplCount = 3;
                var splitedRes = new string[maxSplCount];
                for (var i = 0; i < maxSplCount; i++) {
                    if (splittedLength > i) {
                        var str = splitted[i];
                        splitedRes[i] = str.IsNullOrWhiteSpace() ? null : str;
                    }
                    else {
                        splitedRes[i] = null;
                    }
                }
                q = FilterBySearchString(q, normalizedSearchString, splitedRes[0], splitedRes[1], splitedRes[2]);
            }
            else {
                normalizedSearchString = null;
            }


            var totalItemsCount = await GetTotalCount(q, Filter);
            if (normalizedSearchString == null) {
                q = q.OrderByDescending(_ => _.UpdatedOnUtc);
            }
            q = QueriableSkipTake(q, Filter);

            //q = ApplySorting(q);

            q = Includes(q);

            var list = await q.ToListAsync();

            AdjustList(list);

            IEnumerable<TEntity> sortedList = list;
            if (normalizedSearchString != null)
                sortedList = SortBySearchString(list, normalizedSearchString).ThenByDescending(_ => _.UpdatedOnUtc);
            
            return new PaginatedDataResultVM<TEntityResult>
            {
                TotalItemsCount = totalItemsCount,
                Items = Mapper.Instance.Map<List<TEntityResult>>(sortedList)
            };
        }

        protected async Task<int?> GetTotalCount<T>(IQueryable<T> q, BasePaginatedFilterVM filter)
        {
            if (filter.Pagination.RecalculateTotalCount)
                return await q.CountAsync();
            return null;
        }


        protected IQueryable<T> QueriableSkipTake<T>(IQueryable<T> q, BasePaginatedFilterVM filter)
        {
            var itemsCount = filter.Pagination.ItemsPerPage;
            return q.Skip(itemsCount * (filter.Pagination.Page - 1)).Take(itemsCount);
        }

        protected Func<IBaseStatusLookup, bool> NotDisabledPredicate = t => !t.IsDisabled;

        protected Func<T, bool> GetNotDisabledPredicate<T>() where T : IBaseStatusLookup
        {
            return t => !t.IsDisabled;
        }
    }

    public class BaseSearchService : BaseService, IBaseSearchService
    {
        protected Func<IBaseStatusLookup, bool> NotDisabledPredicate = t => !t.IsDisabled;
        public BaseSearchService(IUnitOfWork u) : base(u) { }

        protected Func<T, bool> GetNotDisabledPredicate<T>() where T : IBaseStatusLookup
        {
            return t => !t.IsDisabled;
        }


        protected Expression<Func<BaseEntity, BaseEntityReadVm>>  SelectBaseEntityExpression = _ => new BaseEntityReadVm
        {
            Id = _.Id,
            CreatedOnUtc = _.CreatedOnUtc,
            UpdatedOnUtc = _.UpdatedOnUtc,
        };


    protected async Task<PaginatedDataResultVM<TEntityResult>> GetItemsByFilterAsync<TEntityResult, TEntity, TFilter>(
            TFilter filter, IQueryable<TEntity> q, Func<IQueryable<TEntity>, IQueryable<TEntity>> filterData,
            Func<IQueryable<TEntity>, string, IQueryable<TEntity>> filterBySearchString,
            Func<IQueryable<TEntity>, IQueryable<TEntity>> includes,
            Func<List<TEntity>, string, IOrderedEnumerable<TEntity>> sortBySearchString) where TFilter : BaseSearchableFilterVM  where TEntity : IBaseEntity
        {
            q = filterData(q);

            var searchString = filter.SearchString;
            if (searchString.NotNullAndWhiteSpace())
            {
                searchString = searchString.ToUpper();
                q = filterBySearchString(q, searchString);
            }
            else
            {
                searchString = null;
            }


            var totalItemsCount = await GetTotalCount(q, filter);
            q = QueriableSkipTake(q, filter);

            q = q.OrderByDescending(_ => _.UpdatedOnUtc);

            q = includes(q);

            var list = await q.ToListAsync();



            IEnumerable<TEntity> sortedList = list;
            if (searchString != null)
                sortedList = sortBySearchString(list, searchString).ThenByDescending(_ => _.UpdatedOnUtc);

            return new PaginatedDataResultVM<TEntityResult>
            {
                TotalItemsCount = totalItemsCount,
                Items = Mapper.Instance.Map<List<TEntityResult>>(sortedList)
            };
        }


        protected IQueryable<T> GetStatusLookupQueryable<T>(IRepository<T> rep) where T : IBaseStatusLookup
        {
            return InternalGetQueryable(rep).Where(_ => !_.IsDisabled);
        }

        protected IQueryable<T> GetQueryable<T>(IRepository<T> rep) where T : IBaseEntity
        {
            var q = InternalGetQueryable(rep);
            if (q is IQueryable<IBaseStatusLookup>)
                throw new Exception($"Call method {nameof(GetStatusLookupQueryable)}");
            return q;
        }

        private IQueryable<T> InternalGetQueryable<T>(IRepository<T> rep) where T : IBaseEntity
        {
            return rep.GetQueriable();
        }

        protected async Task<int?> GetTotalCount<T>(IQueryable<T> q, BasePaginatedFilterVM filter)
        {
            if (filter.Pagination.RecalculateTotalCount)
                return await q.CountAsync();
            return null;
        }


        protected IQueryable<T> QueriableSkipTake<T>(IQueryable<T> q, BasePaginatedFilterVM filter)
        {
            var itemsCount = filter.Pagination.ItemsPerPage;
            return q.Skip(itemsCount * (filter.Pagination.Page - 1)).Take(itemsCount);
        }
    }
}