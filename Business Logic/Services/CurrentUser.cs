using System;
using System.Linq;
using System.Security.Claims;
using Infrastructure;

namespace BusinessLogic.Services
{
    public interface ICurrentUser
    {
        //Task<ApplicationUser> GetCurrentUserAsync(ClaimsPrincipal user);
        string GetCurrentUserId(ClaimsPrincipal user);

        string GetCurrentUserIdOrNull(ClaimsPrincipal user);
    }

    public class CurrentUser : ICurrentUser
    {
        public string GetCurrentUserId(ClaimsPrincipal user)
        {
            var id = GetCurrentUserIdOrNull(user);
            if (id == null) {
                throw new Exception("User id not found.");
            }
            return id;
        }

        public string GetCurrentUserIdOrNull(ClaimsPrincipal user)
        {
            return user?.Claims.FirstOrDefault(_ => _.Type == AppConstants.AuthOptions.Claims.Keys.UserId)?.Value;
        }
    }
}
