﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Models.Common;
using BusinessLogic.Models.Vacancies;
using BusinessLogic.Utils;
using Data;
using Data.Entities.Common;
using Data.Entities.CompanyEntities;
using Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace BusinessLogic.Services
{
    public interface IVacanciesService : IBaseSearchService
    {
        Task<PaginatedDataResultVM<VacancyShortInfoReadVM>> GetVacanciesByFilterAsync(
            VacanciesListFilterReadVM filter);

        Task<VacancyDetailsReadVM> GetVacancyDetailsAsync(string companyId);
        Task<List<BaseEntityReadVm>> GetListForStitemapAsync();
    }

    public class VacanciesService : BaseSearchService, IVacanciesService
    {
        private readonly IRepository<Vacancy> _vacancyRepository;

        public VacanciesService(IUnitOfWork u, IRepository<Vacancy> vacancyRepository) : base(u)
        {
            _vacancyRepository = vacancyRepository;
        }


        public Task<PaginatedDataResultVM<VacancyShortInfoReadVM>> GetVacanciesByFilterAsync(
            VacanciesListFilterReadVM filter)
        {
            return new VacanciesSearchItemsProcessor(filter).GetItemsByFilterAsync(
                GetStatusLookupQueryable(_vacancyRepository));
        }

        public Task<List<BaseEntityReadVm>> GetListForStitemapAsync()
        {
            return GetStatusLookupQueryable(_vacancyRepository).Select(SelectBaseEntityExpression).ToListAsync();
        }


        public async Task<VacancyDetailsReadVM> GetVacancyDetailsAsync(string vacancyId)
        {
            var c = await GetStatusLookupQueryable(_vacancyRepository).Where(_ => _.Id == vacancyId)
                .Include(_ => _.CompanyFilial.Address)
                .Include(_ => _.Company)
                .FirstOrDefaultAsync();
            if (c == null) throw GetValidationException("Vacancy not found.", HttpStatusCode.NotFound);
            return Mapper.Instance.Map<VacancyDetailsReadVM>(c);
        }
    }


    public class VacanciesSearchItemsProcessor : BaseSearchItemsProcessor<VacancyShortInfoReadVM, Vacancy,
        VacanciesListFilterReadVM>
    {
        public VacanciesSearchItemsProcessor(VacanciesListFilterReadVM filter) : base(filter) { }

        protected override IQueryable<Vacancy> FilterData(IQueryable<Vacancy> q)
        {
            q = q.GetFilterClassWhere(Filter.ProfessionId, v => c => c.ProfessionId == v)
                .GetFilterWhere(Filter.CurrencyId, v => c => c.CurrencyId == v)
                .GetFilterWhere(Filter.EmploymentTypeId, v => c => c.EmploymentTypeId == v)
                .GetFilterWhere(Filter.LessThanSalaryPerMonth, v => c => c.SalaryPerMonth < v)
                .GetFilterWhere(Filter.MoreThanSalaryPerMonth, v => c => c.SalaryPerMonth > v);

            var address = Filter.Address;
            if (address == null) return q;
            return FilterVacanciesByLocation(q, address.Latitude, address.Longitude, address.RadiusInKilometers);
        }

        public static IQueryable<Vacancy> FilterVacanciesByLocation(IQueryable<Vacancy> q, decimal lat, decimal lon, decimal radius)
        {
            GeneralUtils.GetLatLngRadiusesSqrts(radius, out var latitudeRadiusSqr,
                out var longitudeRadiusSqr);
            return q.Where(c =>
                !c.CompanyFilial.IsDisabled &&
                (lat - c.CompanyFilial.Address.Latitude) *
                (lat - c.CompanyFilial.Address.Latitude) / latitudeRadiusSqr +
                (lon - c.CompanyFilial.Address.Longitude) *
                (lon - c.CompanyFilial.Address.Longitude) / longitudeRadiusSqr <= 1);
        }

        protected override IQueryable<Vacancy> FilterBySearchString(IQueryable<Vacancy> q,
            string normalizedSearchString, string spls0, string spls1, string spls2)
        {

            return q.Where(c =>
                    c.Name.Contains(normalizedSearchString)
                    || c.Name.Contains(spls0)
                    || c.Name.Contains(spls1)
                    || c.Name.Contains(spls2)

                    || c.Description.Contains(normalizedSearchString)
                    || c.Description.Contains(spls0)
                    || c.Description.Contains(spls1)
                    || c.Description.Contains(spls2)

                    || c.Requirements.Contains(normalizedSearchString)
                    || c.Duties.Contains(normalizedSearchString)
                    || c.WorkingConditions.Contains(normalizedSearchString)
            );
        }

        protected override IQueryable<Vacancy> Includes(IQueryable<Vacancy> q)
        {
            return q.Include(_ => _.CompanyFilial.Address)
                // todo, check
                .Include(_ => _.Company);
        }

        protected override IOrderedEnumerable<Vacancy> SortBySearchString(IEnumerable<Vacancy> list,
            string normalizedSearchString)
        {
            return list.OrderBy(c =>
            {
                var normName = c.Name.ToUpper();
                if (normName == normalizedSearchString) return 1;
                if (normName.StartsWith(normalizedSearchString)) return 2;
                if (normName.Contains(normalizedSearchString)) return 3;

                if (c.Description.ToUpperContainsSafe(normalizedSearchString)) return 4;
                if (c.Requirements.ToUpperContainsSafe(normalizedSearchString)) return 5;
                if (c.Duties.ToUpperContainsSafe(normalizedSearchString)) return 6;
                if (c.WorkingConditions.ToUpperContainsSafe(normalizedSearchString)) return 7;

                return 100;
            });
        }
    }
}