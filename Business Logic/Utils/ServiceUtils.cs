using System.Collections.Generic;
using System.Linq;
using BusinessLogic.Services;
using Data.Entities.Common;
using Google.Maps.Geocoding;
using Google.Maps.Shared;

namespace BusinessLogic.Utils
{
    public static class ServiceUtils
    {
        public static void SetAddressData(Address addrDb, GeocodeResponse response, bool throwIfHasNoComponent)
        {
            var result = response.Results.First();

            addrDb.FormattedAddress = result.FormattedAddress;
            addrDb.Latitude = (decimal) result.Geometry.Location.Latitude;
            addrDb.Longitude = (decimal) result.Geometry.Location.Longitude;

            var addressComponents = result.AddressComponents;
            addrDb.City = GetAddressComponent(addressComponents, AddressType.Locality, throwIfHasNoComponent);
            addrDb.Country = GetAddressComponent(addressComponents, AddressType.Country, throwIfHasNoComponent);
            addrDb.Street = GetAddressComponent(addressComponents, AddressType.Route, throwIfHasNoComponent);
            addrDb.StreetNumber =
                GetAddressComponent(addressComponents, AddressType.StreetNumber, throwIfHasNoComponent);
        }

        public static string GetAddressComponent(IEnumerable<AddressComponent> addressComponents,
            AddressType addressType, bool throwIfHasNoComponent)
        {
            foreach (var addressComponent in addressComponents) {
                foreach (var addressComponentType in addressComponent.Types)
                    if (addressComponentType == addressType) return addressComponent.LongName;
            }
            if (throwIfHasNoComponent)
                throw BaseService.GetValidationException($"Address.{addressType.ToString()}",
                    $"Address type \"{addressType.ToString()}\" not found");
            return null;
        }
    }
}