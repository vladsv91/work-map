using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using FluentValidation.Results;
using ValidationException = FluentValidation.ValidationException;

namespace BusinessLogic.Utils
{
    public class CustomValidationException : ValidationException
    {
        public HttpStatusCode StatusCode { get; set; }



        public CustomValidationException(string message, HttpStatusCode statusCode)
            : base(message)
        {
            StatusCode = statusCode;
        }

        public CustomValidationException(IEnumerable<ValidationFailure> errors, HttpStatusCode statusCode)
            : base(errors)
        {
            StatusCode = statusCode;
        }

    }
}
