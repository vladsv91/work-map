using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Infrastructure;

namespace BusinessLogic.Utils
{
    public enum TrimType
    {
        ToInline,
        AllowMultilinesAndTabs,
        AnlyTrim
    }

    public class TrimAttribute : ValidationAttribute
    {
        public TrimType Type { get; set; }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            // todo, is slow, change to getters and setters
            var str = value as string;
            if (str.IsNullOrEmpty())
                return null;

            // todo, maybe set null if empty


            // ReSharper disable once PossibleNullReferenceException
            str = str.Trim();
            switch (Type) {
                case TrimType.ToInline:
                    str = Regex.Replace(str, @"[\s\n]+", " ");
                    break;
                case TrimType.AllowMultilinesAndTabs:
                    str = Regex.Replace(str, @" {2,}", " ");
                    break;
            }

            validationContext.ObjectType.GetProperty(validationContext.MemberName)
                .SetValue(validationContext.ObjectInstance, str, null);


            return null;
        }
    }
}