using System;
using System.Collections.Immutable;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using BusinessLogic.Services;
using Data.Entities.Common;
using FluentValidation;
using Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;

namespace BusinessLogic.Utils
{
    public static class ValidationExtentions
    {
        public static IRuleBuilderOptions<T, string> IsUrl<T>(this IRuleBuilder<T, string> rule, string propertyName)
        {
            return rule.Matches("^http(s)?://([\\w-]+.)+[\\w-]+(/[\\w- ./?%&=])?$")
                .WithMessage($"'{propertyName}' must be a url.");
        }

        public static IRuleBuilderOptions<T, string> IsPhoneNumber<T>(this IRuleBuilder<T, string> rule)
        {
            return rule.Matches("^\\+{0,1}\\d{9,15}$")
                .WithMessage("Phone number must have format +99(999)9999999 or 9999999999.");
        }

        public static IRuleBuilderOptions<T, string> PhoneNumberValidations<T>(this IRuleBuilder<T, string> rule)
        {
            return rule.LengthBetween(AppConstants.PropertiesRules.ContactPhoneMinLen,
                AppConstants.PropertiesRules.ContactPhoneMaxLen).IsPhoneNumber();
        }

        public static IRuleBuilderOptions<T, string> LengthBetween<T>(this IRuleBuilder<T, string> rule, int min,
            int max)
        {
            return rule.MinimumLength(min).MaximumLength(max);
        }

        public static IRuleBuilderOptions<T, string> PasswordRules<T>(this IRuleBuilder<T, string> rule)
        {
            return rule.NotEmpty().MinimumLength(6).MaximumLength(30)
                .Matches(new Regex("^\\S+$", RegexOptions.Singleline))
                .WithMessage("'Password' Should not contains spaces");
        }

        public static IRuleBuilderOptions<T, string> UserNameAndEmailRules<T>(this IRuleBuilder<T, string> rule)
        {
            return rule.NotEmpty().EmailAddress().MinimumLength(6)
                .MaximumLength(AppConstants.PropertiesRules.ContactEmailMaxLen);
        }

        public static IRuleBuilderOptions<T, string> EmailAddressValidation<T>(this IRuleBuilder<T, string> rule)
        {
            return rule.EmailAddress().MaximumLength(AppConstants.PropertiesRules.ContactEmailMaxLen);
        }

        public static IRuleBuilderOptions<T, string> IsInCustomEmun<T>(this IRuleBuilder<T, string> rule,
            ImmutableHashSet<string> items)
        {
            return rule.Must(_ => _ == null || items.Contains(_)).WithMessage("Value must be in collection.");
        }


        public static IRuleBuilderOptions<T, string> IsInIndustriesEmun<T>(this IRuleBuilder<T, string> rule)
        {
            return rule.IsInCustomEmun(IndustriesInTypes.Instanse.AllItems);
        }

        public static IRuleBuilderOptions<T, string> IsInProfessionsEmun<T>(this IRuleBuilder<T, string> rule)
        {
            return rule.IsInCustomEmun(ProfessionsInTypes.Instanse.AllItems);
        }
    }

    public static class QueriableExtentions
    {
        public static IQueryable<T> GetFilterWhere<T, TV>(this IQueryable<T> q, TV? value,
            Func<TV, Expression<Func<T, bool>>> getExpr) where TV : struct
        {
            return value != null ? q.Where(getExpr(value.Value)) : q;
        }

        public static IQueryable<T> GetFilterClassWhere<T, TV>(this IQueryable<T> q, TV value,
            Func<TV, Expression<Func<T, bool>>> getExpr) where TV : class
        {
            return value != null ? q.Where(getExpr(value)) : q;
        }

        //public static IQueryable<T> IncludeNotDisabled<T, TProp>(this IQueryable<T> q,
        //    Expression<Func<T, IEnumerable<TProp>>> propExpr) where T: class  where TProp: IEnumerable<IBaseStatusLookup>
        //{
        //    return q.Include(_=>_.);
        //}
    }

    public static class BlUtils
    {
        // for user owen file access
        public static string CreateGetFileUrl(Guid fileId) => $"{AppConstants.FilesUrls.FileBaseUrl}/{fileId}";

        // for other users file access
        public static string CreateCompanyMainPhotoUrl(string companyId) => $"{AppConstants.FilesUrls.CompanyMainPhotoUrl}/{companyId}";
        public static string CreateCompanyPhotoUrl(string companyPhotoId) => $"{AppConstants.FilesUrls.CompanyPhotoUrl}/{companyPhotoId}";
        public static string CreateResumePhotoUrl(string applicantId) => $"{AppConstants.FilesUrls.ApplicantPhotoUrl}/{applicantId}";
        public static string CreateResumeFileUrl(string resumeId) => $"{AppConstants.FilesUrls.ResumeFileUrl}/{resumeId}";
    }

    public static class BlExtentions
    {
        public static ICustomLogger GetLogger(this HttpContext context)
        {
            return (ICustomLogger) context.RequestServices.GetService(typeof(ICustomLogger));
        }

        public static ICustomLogger GetLogger(this ActionExecutingContext context)
        {
            return context.HttpContext.GetLogger();
        }

        public static string GetAbsoluteUrl(this HttpContext httpContext)
        {
            var req = httpContext.Request;
            return req.Scheme + "://" + req.Host;
        }

    }
}